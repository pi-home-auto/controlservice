#define SW1 2
#define SW2 4

void setup() {
  pinMode(3, OUTPUT);
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);

  pinMode(SW1, INPUT_PULLUP);
  pinMode(SW2, INPUT_PULLUP);

  digitalWrite(3, 1);
  digitalWrite(5, 1);
  analogWrite(6, 128);

  Serial.begin(9600);
}

void loop() {
  delay(1000);
  if (!digitalRead(SW1)) {
    Serial.println("Switch A pressed.");
  }
  if (digitalRead(SW2)) {
    analogWrite(6, 80);
  } else {
    analogWrite(6, 255);
    Serial.println("Switch B pressed.");
  }
}
