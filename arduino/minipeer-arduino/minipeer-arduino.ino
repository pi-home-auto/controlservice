/* logging-ops
 *
 *  Control logging operations on model railroad mountain.
 */

#include <Wire.h>
#include "minipeer.h"

// Note that you cannot use pins 0 and 1 if you are also using the Serial UART
// Comment the following line out if you wish to use pins 0 and 1
#define SKIP_PINS_0_1 1

// This specifies which input will be used to stop the stepping motor
#define MOTOR_KILL_SWITCH (0)

// Number of devices of each type: 0=GPIOs in, 1=GPIOs out, 2=stepping motors
#define DEVIN  (0)
#define DEVOUT (1)
#define DEVMOTOR (2)

#define NUM_PINS MINIPEER_MAX_GPIOS

uint8_t devcount[] = { 4, 4, 1 };

uint8_t pwmPins[] = { 3, 5, 6, 9, 10, 11 };

uint8_t inputPin[MINIPEER_MAX_GPIOS] = { 0 };
uint8_t outputPin[MINIPEER_MAX_GPIOS] = { 0 };
int outValue[MINIPEER_MAX_GPIOS] = { 0 };
bool  usingPwmPin[MINIPEER_MAX_GPIOS] = { false };
uint8_t motorPin[8];

MiniPeerCommands currCommand = mpcNone;
MiniPeerCommands pendingCommand = mpcNone;
uint8_t currDevnum = 0;
unsigned int currParameter = 0;

bool doProcessCmd = false;
int readDataStep = 0;
bool killSwitchpos = false;

struct MotorParams {
  int speed;
  int step;   // current step count
  int stepsperrev;
  int moves_pending;
} motorParams = {
  .speed = 1200,
  .step = 0,
  .stepsperrev = 512,
  .moves_pending = 0
};

enum ReadStates {
  RsNone, RsReadCmd, RsReadDevNum, RsReadHigh, RsReadLow
} readState = RsNone;
bool doReadDevNum = false;
uint8_t doReadParamBytes = 0;

int lookup[] = {B01000, B01100, B00100, B00110, B00010, B00011, B00001, B01001, 0};
unsigned char outbuf[16];

// For debugging ISR's
bool requestCalled = false;
int requestByteSent = 0;

void receiveEvent(int bytes);
void requestData();

//////////////////////////////////////////////////////////////////////////////
void debugSetup(const char* msg, int num, uint8_t* pins) {
#ifdef SKIP_PINS_0_1
  Serial.print(msg);
  Serial.println(num);
  for (int i = 0; i < num; ++i) {
    Serial.print(" - pin ");
    Serial.println(pins[i]);
  }
#endif
}

#define PIN_FREE   0
#define PIN_INPUT  1
#define PIN_OUTPUT 2
#define PIN_MOTOR  3
#define PIN_USED   9

void setup() {
  // Allocate pins
  int icc = 0;
  uint8_t pins[NUM_PINS];
  memset(pins, PIN_FREE, NUM_PINS);
#ifdef SKIP_PINS_0_1
  pins[0] = PIN_USED;
  pins[1] = PIN_USED;
  Serial.begin(9600);
  while (!Serial) {}  // wait for native usb
#endif
  int p4 = devcount[DEVMOTOR] * 4;
  for (uint8_t ii = NUM_PINS - p4; ii < NUM_PINS; ++ii) {
    if (icc >= p4) break;
    if (pins[ii] == PIN_FREE) {
      motorPin[icc++] = ii;
      pins[ii] = PIN_MOTOR;
    }
  }
  debugSetup("Motor pins ", icc, motorPin);

  icc = 0;
  for (uint8_t ii = 0; ii < sizeof(pwmPins); ++ii) {
    if (ii >= devcount[DEVOUT]) break;
    outputPin[icc++] = pwmPins[ii];
    pins[pwmPins[ii]] = PIN_OUTPUT;
  }
  // TODO check if devcount > number of pwm pins
  debugSetup("Output pins ", icc, outputPin);
  icc = 0;
  for (uint8_t ii = 0; ii < sizeof(pins); ++ii) {
    if (icc >= devcount[DEVIN]) break;
    if (pins[ii] == PIN_FREE) {
      inputPin[icc++] = ii;
      pins[ii] = PIN_INPUT;
    }
  }
  debugSetup("Input pins ", icc, inputPin);
  for (int ii = 0; ii < sizeof(pins); ++ii) {
    if (pins[ii] == PIN_INPUT) {
      pinMode(ii, INPUT_PULLUP);    
    } else if (pins[ii] == PIN_OUTPUT || pins[ii] == PIN_MOTOR) {
      pinMode(ii, OUTPUT);
    }
  }
  // status LED
  //pinMode(LED_BUILTIN, OUTPUT);
  //digitalWrite(LED_BUILTIN, LOW);
  // For common anode LEDs use the following to turn them off
  for (int i = 0; i < devcount[DEVIN]; ++i) {
    outValue[i] = 255;
    analogWrite(outputPin[i], 255);
  }

  // i2c setup
  Wire.begin(ARDI_UNO_ADDR);
  Wire.onReceive(receiveEvent);
  Wire.onRequest(requestData);
  readState = RsReadCmd;
}

void loop() {
  if (doProcessCmd) {
    processCmdInput();
    doProcessCmd = false;
  }
  if (requestCalled) {
    requestCalled = false;
    Serial.print("requestData sent byte: ");
    Serial.println(requestByteSent);
  }
#ifdef MOTOR_KILL_SWITCH
  int val = digitalRead(inputPin[MOTOR_KILL_SWITCH]);
  if (val) {
    killSwitchpos = false;
  } else if (!killSwitchpos) {
    currCommand = mpsMoveDown;
    motorParams.moves_pending = 26;
    killSwitchpos = true;
  }
#endif
  if (motorParams.moves_pending == 0) {
    return;
  }
  if (currCommand == mpsMoveDown) {
    clockwise();
  } else if (currCommand == mpsMoveUp) {
    anticlockwise();
  }
  if (--motorParams.moves_pending == 0) {
    setOutput(8);  // Turn off all channels at end of movement
  }
}

// i2c Callbacks:
void receiveEvent(int bytesRead) {
  if (readState == RsReadCmd) {
    MiniPeerCommands cmd = static_cast<MiniPeerCommands>(Wire.read()); 
    int param;
    currParameter = 0;
    doReadDevNum = false;
    doReadParamBytes = 0;
    pendingCommand = cmd;
    readDataStep = 0;    // rewind outbuf
    switch (cmd) {
      case mpcId:
        outbuf[0] = MINIPEER_VERSION >> 8;
        outbuf[1] = MINIPEER_VERSION & 0xff;
        outbuf[2] = devcount[0];
        outbuf[3] = devcount[1];
        outbuf[4] = devcount[2];
        doReadParamBytes = 2;
        break;
      case mpiGet:
      case mpoGet:
        doReadDevNum = true;
        break;
      case mpoPut:
      case mpoPwm:
        doReadDevNum = true;
        doReadParamBytes = 1;
        break;
      case mpsMoveDown:
      case mpsMoveUp:
      case mpsSetPos:
      case mpsSetSpeed:
        doReadDevNum = true;
        doReadParamBytes = 2;
        break;
      case mpsReadPos:
      case mpsStop:
        doReadDevNum = true;
        break;
      case mpcNone:
        motorParams.moves_pending = 0;
        currCommand = mpcNone;
        setOutput(8);
        break;
      default:
        //Serial.println("Bad cmd");
        pendingCommand = mpcNone;
        break;
    }
    if (doReadDevNum) {
      readState = RsReadDevNum;
    } else if (doReadParamBytes == 2) {
      readState = RsReadHigh;
    } else if (doReadParamBytes == 1) {
      readState = RsReadLow;
    } else {
      currCommand = pendingCommand;
      pendingCommand = mpcNone;
    }
  } else if (readState == RsReadDevNum) {
    currDevnum = Wire.read();
    //Serial.print("Devnum = ");
    //Serial.println(currDevnum);
    if (doReadParamBytes == 0) {
      doProcessCmd = true;
      readState = RsReadCmd;
      currCommand = pendingCommand;
      pendingCommand = mpcNone;
    } else {
      readState = doReadParamBytes == 2 ? RsReadHigh : RsReadLow;
    }
  } else if (readState == RsReadHigh) {
    currParameter = Wire.read() << 8;
    readState = RsReadLow;
  } else if (readState == RsReadLow) {
    currParameter |= Wire.read() & 0xff;
    //Serial.print("ParamHL is ");
    //Serial.println(currParameter);
    doProcessCmd = true;
    currCommand = pendingCommand;
    pendingCommand = mpcNone;
    readState = RsReadCmd;
  }
}

void requestData() {
  requestCalled = true;
  if (currCommand == mpcNone) {
    return;
  }
  requestByteSent = outbuf[readDataStep];
  Wire.write(outbuf[readDataStep++]);
  if ((currCommand == mpcId && readDataStep <= 5) ||
      (currCommand == mpsReadPos && readDataStep <= 2)) {
      // More data to come
  } else {
    readDataStep = 0;
    currCommand = mpcNone;
  }
}

/* This function is called when the command and all input parameters have been read */
void processCmdInput() {
  Serial.print("processCmd ");
  Serial.println(currCommand);
  switch (currCommand) {
    case mpcId:
      // TODO Do version check
      break;
    case mpiGet:
      if (currDevnum < devcount[DEVIN]) {
        outbuf[0] = digitalRead(inputPin[currDevnum]);
      }
      break;
    case mpoGet:
      if (currDevnum < devcount[DEVOUT]) {
#if 0
        if (usingPwmPin[outputPin[currDevnum]]) {
          outbuf[0] = analogRead(outputPin[currDevnum]);
        } else {
          outbuf[0] = digitalRead(outputPin[currDevnum]);
        }
#endif
        int& val = outValue[currDevnum];
        if (val == -1 || val == -2) {
          outbuf[0] = val + 2;
        } else {
          outbuf[0] = val;
        }
      }
      break;
    case mpoPut:
      if (currDevnum < devcount[DEVOUT]) {
        digitalWrite(outputPin[currDevnum], currParameter);
        usingPwmPin[outputPin[currDevnum]] = false;
        if (currParameter) {
          outValue[currDevnum] = -1;
        } else {
          outValue[currDevnum] = -2;
        }
      }
      break;
    case mpoPwm:
      if (currDevnum < devcount[DEVOUT]) {
        analogWrite(outputPin[currDevnum], currParameter);
        usingPwmPin[outputPin[currDevnum]] = true;
        outValue[currDevnum] = currParameter;
      }
      break;
    case mpsMoveUp:
    case mpsMoveDown:
      motorParams.moves_pending = currParameter;
      break;
    case mpsReadPos:
      outbuf[0] = motorParams.step >> 8;
      outbuf[1] = motorParams.step & 0xff;
      break;
    case mpsSetPos:
      motorParams.step = currParameter;
      break;
    case mpsSetSpeed:
      if (currParameter >= 0 && currParameter < 10000) {
        motorParams.speed = currParameter;
      }
      break;
    case mpsStop:
      motorParams.moves_pending = 0;
      currCommand = mpcNone;
      setOutput(8);
      break;
  }
}

//////////////////////////////////////////////////////////////////////////////
//set pins to ULN2003 high in sequence from 1 to 4
//delay "motorSpeed" between each pin setting (to determine speed)
void anticlockwise() {
  for (int i = 0; i < 8; i++) {
    setOutput(i);
    delayMicroseconds(motorParams.speed);
  }
  --motorParams.step;
}

void clockwise() {
  for (int i = 7; i >= 0; --i) {
    setOutput(i);
    delayMicroseconds(motorParams.speed);
  }
  ++motorParams.step;
}

void setOutput(int out) {
  digitalWrite(motorPin[0], bitRead(lookup[out], 0));
  digitalWrite(motorPin[1], bitRead(lookup[out], 1));
  digitalWrite(motorPin[2], bitRead(lookup[out], 2));
  digitalWrite(motorPin[3], bitRead(lookup[out], 3));
}
