#pragma once

/*
 * minipeer.h
 *
 * Definitions and i2c protocol for the controlservice's minipeer interface.
 * A minipeer is simply an i2c unit that controls other devices such as LEDs,
 * switches and stepping motors.
 *
 * Note this header is used by both the Arduino code as well as the controlservice.
 * The minipeer.h header is maintained in the Arduino sketch folder and the controlservice
 * has a symbolic link to this file.
 */
#define MINIPEER_VERSION    (2)
// These values are for Arduino UNO. Modify if using a different microcontroller.
#define MINIPEER_MAX_GPIOS  (14)
#define MINIPEER_MAX_MOTORS (3)

// i2c bus to use
#define ARDI_UNO_BUS 1
// i2c address of this minipeer
#define ARDI_UNO_ADDR 0x09

/* MiniPeer commands:  Note the commands are prefixed with mpc for MiniPeer general commands,
 *                       mps for Stepping motor commands, mpi and mpo for Input and Output commands.
 *
 *  Command     Bytes  Parameters                    Returns
 *  -------     -----  ----------                    -------
 *  mpcNone       1
 *  mpcId         3    Version (int)                 Version (int)
 *                                                   Nr. inputs (byte)
 *                                                   Nr. outputs (byte)
 *                                                   Nr. motors (byte)
 *  mpiGet        2    Devnum (byte)                 Value (byte)
 *  mpoGet        2    Devnum (byte)                 Value (byte)
 *  mpoPut        3    Devnum (byte), on/off (byte)
 *  mpoPwm        3    Devnum (byte), Value  (byte)
 *  mpsMoveDown   4    Devnum (byte),
 *                     Number of steps (int)
 *  mpsMoveUp     4    Devnum (byte),
 *                     Number of steps (int)
 *  mpsReadPos    2    Devnum (byte)                 Posiition (int)
 *  mpsSetPos     4    Devnum (byte)
 *                     Position (int)
 *  mpsSetSpeed   4    Devnum (byte),
 *                     Speed (int)
 *  mpsStop       2    Devnum (byte)
 */
 enum MiniPeerCommands {
  mpcNone, mpcId, mpiGet, mpoGet, mpoPut, mpoPwm, mpsMoveDown, mpsMoveUp, mpsReadPos,
  mpsSetPos, mpsSetSpeed, mpsStop
 };

/* This is Version 1 minipeer commands, they are now DEPRECATED
 * The first byte of the i2c packet received from the controller contains the command
 * to execute. The packet is then followed by optional parameters. The following list
 * indicates the number, type and description of each additional parameter, per command:
 *
 * coNone - no parameters
 * coId - 1 int parameter for version number
 * coMoveDown - 1 int parameter for the number of steps to move
 * coMoveUp - 1 int parameter for the number of steps to move
 * coReadPos - no parameters, returns the current step of the motor (int)
 * coReadSwitch - no parameters, returns position of the switch (byte 0 or 1)
 * coSetSpeed - 1 int parameter for the speed
 * coStop - no parameters
 */
//enum CommandOps {
//  coNone, coId, coMoveDown, coMoveUp, coReadPos, coReadSwitch, coSetSpeed, coStop
//};
