//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <string>

#include "handler.h"
#include "handlerloader.h"
#include "handlers/distancehandler.h"
#include "handlers/garagehandler.h"
#include "handlers/gpiohandler.h"
#include "handlers/monitorhandler.h"
#include "handlers/notificationhandler.h"
#include "handlers/pinghandler.h"
#include "handlers/servohandler.h"
#include "handlers/soundhandler.h"
#include "handlers/stepmotorhandler.h"
#include "logger.h"
using std::endl;

namespace ctlsvc {
namespace HandlerLoader {

Handler* load(const std::string& name) {
    if (name == "distance") return new DistanceHandler(name);
    if (name == "garage")  return new GarageHandler(name);
    if (name == "gpio")  return new GpioHandler(name);
    if (name == "monitor") return new MonitorHandler(name);
    if (name == "notification") return new NotificationHandler(name);
    if (name == "ping") return new PingHandler(name);
    if (name == "servo") return new ServoHandler(name);
    if (name == "sound") return new SoundHandler(name);
    if (name == "stepmotor") return new StepMotorHandler(name);

    cslog << loglevel(Error) << "Error: Handler " << name << " not found" << endl;
    return nullptr;
}

}  // namespace HandlerLoader
}  // namespace ctlsvc
