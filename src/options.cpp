//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <json/json.h>
#include "logger.h"
#include "options.h"
#include "string_tools.h"
#include "system.h"

#include <cstdlib>
#include <fstream>
#include <getopt.h>    // for getopt_long; POSIX standard getopt is in unistd.h
#include <iostream>
#include <unistd.h>    // for gethostname
using std::endl;

#ifndef BASE_DIR
#error Must define BASE_DIR variable in Makefile
#endif

namespace {
static const std::string CONFIG_DIR(BASE_DIR "/etc/");
static const std::string COMMON_CONFIG("commonconfig.json");
static const std::string HOST_CONFIG("-config.json");

static const std::string
USAGE("Usage: controlservice [ -b ] [ -p # ] [ -r # ] [ -u # ] [ -S when ]\n"
      "       where:\n"
      " -b,--base            Run as base module\n"
      " -l,--loglevel #      Set the log level (0=Trace, 1=Debug, 2=Info,\n"
      "                                         3=Warning, 4=Error, 5=Fatal)\n"
      " -p,--port #          Listen on TCP port #\n"
      " -r,--rest-port #     Listen on TCP port # for REST calls (future)\n"
      " -u,--udp-port #      List on UDP port # for discovery broadcasts (future)\n"
      " -S,--shutdown <when> Shutdown controlservice at specified time\n");

bool equalsIgnoreCase(const std::string& a, const std::string& b) {
    return std::equal(a.begin(), a.end(),
                      b.begin(), b.end(),
                      [](char a, char b) {
                          return tolower(a) == tolower(b);
                      });
}

ctlsvc::CsLogLevel strtoLogLevel(const std::string& str, ctlsvc::CsLogLevel defaultLevel) {
    if (str == "Trace") {
        return ctlsvc::Trace;
    } else if (equalsIgnoreCase(str, "debug")) {
        return ctlsvc::Debug;
    } else if (str == "Info") {
        return ctlsvc::Info;
    } else if (str == "Warning") {
        return ctlsvc::Warning;
    } else if (str == "Error") {
        return ctlsvc::Error;
    } else if (str == "Fatal") {
        return ctlsvc::Fatal;
    }
    return defaultLevel;
}

}  // namespace

namespace ctlsvc {

class OptionsImpl {
public:
    OptionsImpl();
    ~OptionsImpl() = default;

    void addHandler(const std::string& name);
    bool getConfigValue(const std::string& name, Json::Value& value) const;
    Options::CSTYPE parseCommandLine(int argc, char* argv[]);
    bool readConfig();

private:
    std::string buildDbFile();

public:
    /** Type of controlservice to run */
    enum Options::CSTYPE csType_;

    int controlPort_;		//!< TCP port for internal API
    int restPort_;		//!< TCP port for REST calls
    int udpPort_;		//!< UDP port for service discovery
    CsLogLevel auditLogLevel_;  //!< Log level threshold for auditing
    CsLogLevel logLevel_;       //!< Log level threshold for logging
    std::string whenDown_;	//!< parameter from --shutdown option

    std::string module_name_;
    std::string db_filename_;

    /** Handlers that are started when controlservice boots */
    std::vector<std::string> bootupHandlers_;

private:
    /** settings read from common, shared json config file */
    Json::Value commonConfig_;

    /** settings read from host-specific json config file */
    Json::Value hostConfig_;
};

OptionsImpl::OptionsImpl()
    : csType_(Options::CS_INVALID)
    , controlPort_(CONTROL_PORT)
    , restPort_(REST_PORT)
    , udpPort_(BROADCAST_PORT)
    , auditLogLevel_(Info)
    , logLevel_(Info)
    , commonConfig_()
    , hostConfig_() {
    db_filename_ = buildDbFile();
}

std::string OptionsImpl::buildDbFile() {
    auto sdir = getenv("HA_BASE_DIR");
    auto sfile = getenv("HA_DBFILE");
    if (sdir == nullptr || sfile == nullptr) {
        cslog << loglevel(Error) << "Unable to read HA env variables." << endl;
        return "";
    }
    std::string dbfile = sdir;
    dbfile.append("/data/");
    dbfile.append(sfile);
    return dbfile;
}

void OptionsImpl::addHandler(const std::string& name) {
    bootupHandlers_.push_back(name);
}

bool OptionsImpl::getConfigValue(const std::string& name, Json::Value& value) const {
    std::vector<std::string> names;
    StringTools::splitString(name, names, '.');
    std::vector<std::string>::const_iterator it;

    value = hostConfig_;
    for (it = names.begin(); it != names.end(); ++it) {
        if (value.isMember(*it)) {
            value = value[*it];
        } else {
            break;
        }
    }
    if (it == names.end()) {
        return true;
    }

    value = commonConfig_;
    for (const auto& name : names) {
        if (value.isMember(name)) {
            value = value[name];
        } else {
            return false;
        }
    }
    return true;
}

Options::CSTYPE OptionsImpl::parseCommandLine(int argc, char* argv[]) {
    static struct option longOptions[] = {
        { "base",      0, 0, 'b' },
        { "loglevel",  1, 0, 'l' },
        { "port",      1, 0, 'p' },
        { "rest-port", 1, 0, 'r' },
        { "shutdown",  1, 0, 'S' },
        { "udp-port",  1, 0, 'u' },
        { 0, 0, 0, 0 }
    };

    int optc;
    int optIndex = 0;
    while ((optc = getopt_long(argc, argv, "bl:p:r:S:u:", longOptions, &optIndex)) != -1) {
        switch (optc) {
        case 'b':
            csType_ = Options::CS_BASE;
            cslog << "Option: base" << endl;
            break;
        case 'l':
            if (std::isdigit(*optarg)) {
                logLevel_ = atoi(optarg);
            } else {
                logLevel_ = strtoLogLevel(optarg, logLevel_);
            }
            cslog << "Option: log level=" << logLevel_ << endl;
            break;
        case 'p':
            controlPort_ = atoi(optarg);
            cslog << "Option: control port=" << controlPort_ << endl;
            break;
        case 'r':
            restPort_ = atoi(optarg);
            cslog << "Option: rest port=" << restPort_ << endl;
            break;
        case 'S':
            csType_ = Options::CS_SHUTDOWN;
            whenDown_ = optarg;
            cslog << "Requested shutdown of local controlservice" << endl;
            break;
        case 'u':
            udpPort_ = atoi(optarg);
            cslog << "Option: udp port=" << udpPort_ << endl;
            break;
        default:
            cslog << loglevel(Error) << USAGE << endl;
            //std::cerr << USAGE << endl;
            return Options::CS_INVALID;
        }
    }
    return csType_;
}

bool OptionsImpl::readConfig() {
    bool ret = false;

    // read configuration files (common)
    commonConfig_.clear();
    {
        std::ifstream ifs(CONFIG_DIR + COMMON_CONFIG);
        if (ifs.good()) {
            Json::CharReaderBuilder builder;
            std::string errs;
            builder.strictMode(&builder.settings_);
            if (!parseFromStream(builder, ifs, &commonConfig_, &errs)) {
                cslog << loglevel(Error) << "Unable to parse common configuration file, errors: "
                      << errs << endl;
            } else {
                ret = true;
            }
        }
    }
    // read configuration files (host-specific)
    hostConfig_.clear();
    {
        char hostname[256];
        if (gethostname(hostname, sizeof(hostname)) < 0) {
            cslog << loglevel(Error) << System::getErrorString("Unable to get hostname, ") << endl;
            return ret;
        }
        module_name_ = hostname;
        std::ifstream ifs(CONFIG_DIR + hostname + HOST_CONFIG);
        if (ifs.good()) {
            Json::CharReaderBuilder builder;
            std::string errs;
            builder.strictMode(&builder.settings_);
            if (!parseFromStream(builder, ifs, &hostConfig_, &errs)) {
                cslog << loglevel(Error) << "Unable to parse host configuration file, errors: "
                      << errs << endl;
            } else {
                ret = true;
            }
        }
    }
    return ret;
}


Options::Options()
    : impl_(std::make_unique<OptionsImpl>()) {}

Options::~Options() {}

CsLogLevel Options::getAuditLogLevel() const {
    return impl_->auditLogLevel_;
}

CsLogLevel Options::getLogLevel() const {
    return impl_->logLevel_;
}

enum Options::CSTYPE Options::getType() const {
    return impl_->csType_;
}

int Options::getControlPort() const {
    return impl_->controlPort_;
}

const std::string& Options::getDbFilename() const {
    return impl_->db_filename_;
}

const std::string& Options::getModuleName() const {
    return impl_->module_name_;
}

int Options::getRestPort() const {
    return impl_->restPort_;
}

int Options::getUdpPort() const {
    return impl_->udpPort_;
}

const std::vector<std::string>& Options::getBootupHandlers() const {
    return impl_->bootupHandlers_;
}

std::string Options::getWhenDown() const {
    return impl_->whenDown_;
}

std::string Options::getConfig(const std::string& option) const {
    Json::Value val;
    if (!impl_->getConfigValue(option, val)) {
        return "";
    }
    return val.asString();
}

const std::string& Options::getConfigDir() const {
    return CONFIG_DIR;
}

float Options::getConfigFloat(const std::string& option) const {
    Json::Value val;
    if (!impl_->getConfigValue(option, val)) {
        return -1;
    }
    return val.asFloat();
}

int Options::getConfigInt(const std::string& option) const {
    Json::Value val;
    if (!impl_->getConfigValue(option, val)) {
        return -1;
    }
    return val.asInt();
}

std::vector<std::string> Options::getConfigArray(const std::string& option) const {
    std::vector<std::string> sv;
    Json::Value val;
    if (!impl_->getConfigValue(option, val)) {
        return sv;
    }
    if (val.isArray()) {
        for (unsigned int ix = 0; ix < val.size(); ix++) {
            sv.push_back(val[ix].asString());
        }
    } else {
        cslog << loglevel(Error) << "Value " << option << " is not an array." << endl;
    }
    return sv;
}

std::vector<int> Options::getConfigIntArray(const std::string& option) const {
    std::vector<int> iv;
    Json::Value val;
    if (!impl_->getConfigValue(option, val)) {
        return iv;
    }
    if (val.isArray()) {
        for (unsigned int ix = 0; ix < val.size(); ix++) {
            iv.push_back(val[ix].asInt());
        }
    } else {
        cslog << loglevel(Error) << "Value " << option << " is not an array." << endl;
    }
    return iv;
}

bool Options::getProperties(const std::string& option, Properties& props) const {
    Json::Value val;
    if (!impl_->getConfigValue(option, val)) {
        return false;
    }
    if (!val.isArray()) {
        return false;
    }
    for (unsigned int ix = 0; ix < val.size(); ix++) {
        auto names = val[ix].getMemberNames();
        for (const auto& name : names) {
            auto varval = val[ix][name];
            if (varval.isInt()) {
                props.set(name, varval.asInt());
            } else if (varval.isString()) {
                props.set(name, varval.asString());
            }
        }
    }
    return true;
}

Options::CSTYPE Options::parseCommandLine(int argc, char* argv[]) {
    return impl_->parseCommandLine(argc, argv);
}

bool Options::readConfig() {
    return impl_->readConfig();
}

void Options::setConfig() {
    auto logLevelStr = getConfig("control.loglevel");
    if (!logLevelStr.empty()) {
        impl_->logLevel_ = strtoLogLevel(logLevelStr, Info);
        cslog << loglevel(Info) << "Set log level to " << impl_->logLevel_ << endl;
    }
    logLevelStr = getConfig("control.auditLoglevel");
    if (!logLevelStr.empty()) {
        impl_->auditLogLevel_ = strtoLogLevel(logLevelStr, Info);
        cslog << loglevel(Info) << "Set audit log level to " << impl_->logLevel_ << endl;
    }
    auto handlers = getConfigArray("control.handlers");
    std::vector<std::string>::const_iterator it;
    for (const auto& handler : handlers) {
        cslog << loglevel(Info) << "Adding " << handler << " to bootup handlers." << endl;
        impl_->addHandler(handler);
    }
}

}  // namespace ctlsvc
