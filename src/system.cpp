//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "ipcmessage.h"
#include "logger.h"
#include "protocol.h"
#include "system.h"

#include <cerrno>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string.h>
#include <sockstr/Socket.h>
#include <unistd.h>
using std::endl;

namespace ctlsvc::System {

std::string getErrorString(const std::string& prefix) {
  std::ostringstream oss;
  if (!prefix.empty()) {
    oss << prefix;
  }
  oss << errno << ": " << strerror(errno);

  return oss.str();
}

std::string readFileContent(const std::string& fileName) {
    std::ifstream ifile(fileName.c_str());
    if (!ifile.is_open())
    {
        cslog << loglevel(Error) << "Could not open file " << fileName << endl;
        return std::string();
    }

    //TODO limit size if file too big

    std::string strContent((std::istreambuf_iterator<char>(ifile)),
                           std::istreambuf_iterator<char>());
    ifile.close();
    return strContent;
}

void shutdown(const std::string& when) {
  sockstr::Socket sock;
  if (sock.open("localhost:1800", sockstr::Socket::modeReadWrite)) {
    IpcGreeting greeting("Control Service", "shutdown");
    sock << greeting.toString() << endl;
    std::string strbuf;
    sock.read(strbuf, COMMAND_END);
    if (greeting.fromString(strbuf)) {
      IpcShutdown shutdown(when);
      sock << shutdown.toString() << endl;
      // Don't bother to read the reply
    }
    sock.close();
  } else {
    cslog << loglevel(Error) << "Unable to open socket to local controlservice, errno=" << errno << endl;
  }
}

}  // namespace ctlsvc::System
