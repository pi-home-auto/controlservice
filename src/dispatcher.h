//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "handler.h"

#include <string>

namespace ctlsvc {
// Forward references
class IpcMessage;
class Peer;

/**
 * This class dispatches IPC messages to the appropriate handler. If a message
 * is meant to be handled by a different module, then the Dispatcher forwards
 * the message to the other module, receives the reply and returns the reply to
 * the orignal sender.
 */
class Dispatcher {
public:
  Dispatcher();

  /** Disposition of a dispatched (and handled) message. */
  enum ROUTE {
    DR_INVALID = -1,
    DR_NOREPLY,	//!< message finished normally but no reply necessary
    DR_REPLY,	//!< message finished normally with reply
    DR_ERROR,	//!< message resulted in an error
    DR_ROUTE	//!< results of handler to be rerouted to another handler
  };

  /** Dispatch a message to a handler to process the request and return result */
  ROUTE execute(const IpcMessage& message, IpcReply& reply, Peer* peer);

protected:
  const std::string module_;
  //TODO besides reference to the (global) handler table, also keep a map of
  //     handlers that are local for this dispatcher only.
  /** Reference to the global handler table in #ControlService. */
  const HandlerMap& handlers_;
};

}
