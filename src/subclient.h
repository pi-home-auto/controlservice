//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "ipcmessage.h"

#include <atomic>
#include <memory>
#include <thread>

namespace ctlsvc {
// Forward reference
class PeerManager;

/** Class used by SCS to maintain a connection to the BCS
 */
class SubClient {
public:
  SubClient();
  ~SubClient();

  bool init(const std::string& baseUrl, PeerManager* pm);
  bool isConnected() const;
  bool handle();

  void run(bool background = true);
  void stop();

private:
  bool connectToBase();

private:
  std::unique_ptr<sockstr::Stream> bcsStream_;

  /** Original unparsed URL string of the BCS */
  std::string baseUrl_;
  std::atomic_bool keepRunning_;
  bool is_connected_;
  std::thread thread_;
  IpcGreeting greeting_;
  PeerManager* peerMan_;
};

}
