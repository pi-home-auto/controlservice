//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <signal.h>
#include <sockstr/Stream.h>

#include "ipcmessage.h"
#include "peer.h"
#include "peerthreader.h"
#include "protocol.h"
using namespace ctlsvc;

Peer::Peer(const std::string& name, sockstr::Stream* stream, PeerType type)
    : name_(name)
    , type_(type)
    , stream_(stream)
    , peerThreader_(std::make_unique<PeerThreader>(this))
    , isConnected_(stream && stream->is_open())
    , pingPending_(true)
    , pingResponse_(true) {}

Peer::~Peer() {
    if (stream_) {
        delete stream_;
    }
}

bool Peer::isRunning() const {
    return peerThreader_->isRunning();
}

void Peer::start() {
    if (peerThreader_->isRunning()) return;

    peerThreader_->start();
}

void Peer::stop() {
    if (!peerThreader_->isRunning()) return;

    peerThreader_->stop();

    if (stream_ && stream_->is_open() && stream_->good()) {
        IpcGoodbye goodbye;
        *stream_ << goodbye.toString() << std::endl;

        //TODO currently the peerthreader does not send an ACK for goodbye messages.
        //     Either remove the stream read here, or have peerthreader send an ACK.
        std::string str;
        stream_->read(str, COMMAND_END);
    }
    if (stream_) {
        stream_->close();
    }
}

// It takes 2x false to turn off ping response
void Peer::setPingResponse(bool response) {
    if (response) {
        pingPending_ = response;
        pingResponse_ = response;
    } else {
        if (pingPending_) {
            pingPending_ = false;
        } else {
            pingResponse_ = false;
        }
    }
}
