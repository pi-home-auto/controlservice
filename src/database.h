//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <string>
#include <unordered_map>
#include <variant>
#include <vector>

// Forward reference
class sqlite3;

namespace ctlsvc {

/** Data holder class that represents a device in the automation dataabase. */
class DbDataHolder {
public:
  using FieldType = std::variant<std::monostate, int, float, std::string>;
  using FieldMap = std::unordered_map<std::string, FieldType>;

  DbDataHolder(const std::string& name);
  virtual ~DbDataHolder() = default;

  bool get(const std::string& name, FieldType& value) const;
  void set(const std::string& name, const FieldType& value);

  /** A null value read from the database is represented in a ::string as
   *  the value of this constant. */
  static const std::string NullValueString;

private:
  std::string name_;
  FieldMap fields_;
};


/** Data holder class that represents a device in the automation dataabase. */
class DbDevice : public DbDataHolder {
public:
  DbDevice();
  virtual ~DbDevice() = default;

  std::string name() const;
  std::string uuid() const;
  std::string description() const;
  std::string module_name() const;
  std::string purpose() const;
  std::string type() const;
  std::string state() const;
  std::string addresses() const;
  std::string parameters() const;
  std::string location() const;
  std::string grouping() const;
  std::string gps() const;
  /** Optional sequence number, i.e., for input LEDs.
   *  @returns Sequence number or -1 if no sequence was set for this device.  */
  int seq_order() const;
  // TODO join the device_type fields here, especially ctlsvc_handler
  std::string ctlsvc_handler() const;
};

/** Data holder class that represents a module in the automation dataabase. */
class DbModule : public DbDataHolder {
public:
  DbModule();
  virtual ~DbModule() = default;

  std::string name() const;
  std::string role() const;
  std::string url() const;
  std::string state() const;
  std::string controlUri() const;
  int controlPort() const;
};

/**
 * This class handles all operations involving the sqlite automation database. It provides
 * access methods (such as list of devices, module details, etc.). Commonly accessed data
 * within the database are also cached in this class.
 * This class does not provide methods to modify the database thus it is READ only from here.
 */
class Database {
public:
  using DbDeviceList = std::vector<DbDevice>;
  using DbModuleList = std::vector<DbModule>;

  Database(const std::string& dbfile = std::string());
  ~Database();

  // Public API

  /** Open the sqlite3 database in readonly mode.
   *  @return  true if the database was opened successfully, otherwise false
   */
  bool init();

  /** Set the filename of the database file to open. This overrides the filename
   *  specified in the constructor.
   *  Note ths must be called before calling init().
   *  @param dbfile  Filename of the sqlite3 database file (.db)
   */
  void setDbFile(const std::string& dbfile);

  /** Close the sqlite3 database, if it was open */
  void uninit();

  /** Check if database is open */
  bool isOpen() const;

  /** Read a device from the database with the specified uuid as unique key.
   *  @param key  The unique UUID of the device to read.
   *  @param device  If the device is found in the database, it is copied to this parameter.
   *  @return  True if the device was found, otherwise false.
   */
  bool getDevice(const std::string& key, DbDevice& device) const;

  /** Read a module from the database with the specified name.
   *  @param name  The name of module to read.
   *  @param module  If the module is found in the database, it is copied to this parameter.
   *  @return  True if the module was found, otherwise false.
   */
  bool getModule(const std::string& name, DbModule& module) const;

  /** Return a list of the devices that are the specified type.
   *  If type is not specified (empty string) then all devices in the
   *  database are returned.
   *  Note that the devices are copied into a new list, since the returned
   *  list is likely to be a subset of all devices.
   *  @param type  The device type to read into the list. If this parameter
   *               is empty, then all devices in the database are returned.
   *  @param devices  The device list requested will be stored in the specified list.
   *  @return  Number of devices returned in the list.
   */
  size_t devices(const std::string& type,  DbDeviceList& devices) const;

  /** Return a list of the devices for the specified module that are of the
   *  specified type.
   *  If type is not specified (empty string) then all devices for the module
   *  are returned.
   *  Note that the devices are copied into a new list, since the returned
   *  list is likely to be a subset of all devices.
   *  @param module  The module name of the devices to retrieve.
   *  @param type    The device type to read into the list. If this parameter
   *                 is empty, then all devices in the database are returned.
   *  @param devices The device list requested will be stored in the specified list.
   *  @return  Number of devices returned in the list.
   */
  size_t moduleDevices(const std::string& module, const std::string& type, DbDeviceList& devices) const;

  /** Returns a list of all the system modules in the database. */
  const DbModuleList& modules() const;

private:
  std::string dbfile_;
  sqlite3* db_;
  // Caches for frequently requested db records
  mutable DbDeviceList devices_;
  mutable DbModuleList modules_;
};

}  // namespace ctlsvc
