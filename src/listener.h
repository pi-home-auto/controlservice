//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <memory>

namespace ctlsvc {
// Forward references
class PeerManager;
class ListenerImpl;

class Listener {
public:
  Listener();
  virtual ~Listener();

  /** Initialize variables and open listener stream */
  virtual bool init(int port, PeerManager* pm);

  /** Runs the #ListenerImpl::handle method either blocking or in a thread.
   *  @param background If true runs #handle in a thread, else runs directly.
   */
  virtual void run(bool background = true);

  PeerManager* peerManager();
  const PeerManager* peerManager() const;
  void setPeerManager(PeerManager* pm);

  /** Tell the thread to stop processing. */
  void stop();

 private:
  std::unique_ptr<ListenerImpl> impl_;
};

}  // namespace ctlsvc
