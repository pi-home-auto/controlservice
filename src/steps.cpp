//
//   Copyright 2021-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "devices/gpiooutputdevice.h"
#include "steps.h"
#include "logger.h"

#include <sstream>
#include <thread>
using std::endl;

namespace ctlsvc {

void GpioOutStep::init() {
    // query device to find the pin number
    auto prop = device_->query(device::GpioOutputDevice::USE_SERVO);
    if (std::holds_alternative<bool>(prop)) {
        use_servo_ = std::get<bool>(prop);
    }
    prop = device_->query(device::GpioOutputDevice::USE_PWM);
    if (std::holds_alternative<bool>(prop)) {
        use_pwm_ = std::get<bool>(prop);
    }
    if (value_ == -1) {
        value_ = 0;
        randomize_ = true;
        rand_ = std::make_unique<std::random_device>();
    }
}

bool GpioOutStep::run() {
    if (use_pwm_) {
        if (value_ < 0 || value_ > 255) {
            cslog << loglevel(Error) << "Pwm value to write GPIO must be 0-255, not " << value_ << endl;
            return false;
        }
        if (randomize_) {
            auto rv = (*rand_)() % 256;
            value_ = 255 - (65025 - 510 * rv + rv * rv) / 256;
            auto td = ((*rand_)() % 999000) + 1000;
            delay_ = std::chrono::microseconds(td);
        }
        device_->write(1, &value_);
        std::this_thread::sleep_for(delay_);
    } else if (use_servo_) {
        device_->write(sizeof(int), &value_);
        std::this_thread::sleep_for(delay_);
    } else {
        if (value_ < 0 || value_ > 4) {
            cslog << loglevel(Error) << "Value to write GPIO must be 0-3, not " << value_ << endl;
            return false;
        }
        int val = value_ == 2 ? 1 : value_ == 3 ? 0 : value_;
        device_->write(1, &val);
        std::this_thread::sleep_for(delay_);
        if (value_ > 1) {
            val = !val;
            device_->write(1, &val);
        }
    }
    std::ostringstream oss;
    oss << "Set gpio " << device_->getName() << " to " << value_ << " delay: " << delay_.count();
    results_ = oss.str();
    return true;
}

bool GpioOutStep::set(const std::string& params) {
    return true;
}

std::string GpioOutStep::results() const {
    return results_;
}

}  // namespace ctlsvc
