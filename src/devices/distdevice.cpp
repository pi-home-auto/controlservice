//
//   Copyright 2022-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "distdevice.h"
#include "eval/eventfactory.h"
#include "eval/events.h"
#include "logger.h"

#include <cstring>

using ctlsvc::driver::DevMode;
using ctlsvc::driver::DriverBuffer;
using ctlsvc::driver::DriverStatus;
using ctlsvc::driver::DistDriver;
using std::endl;

namespace ctlsvc::device {

const std::string DistDevice::POLL_DELAY_MS("poll_delay_ms");
const std::string DistDevice::SENSOR_NUMBER("sensor");
const std::string DistDevice::UNITS("units");


DistDevice::DistDevice(int sensor)
    : Device("dist." + std::to_string(sensor))
    , sensor_(sensor)
    , handle_(nullptr) {}

DistDevice::~DistDevice() {
    deinit();
}

void DistDevice::deinit() {
    close();
}

bool DistDevice::init() {
    return true;
}

bool DistDevice::open(Mode mode) {
    DevMode devmode;
    switch (mode) {
        case Mode::ReadOpen:
            devmode = DevMode::Read;
            break;
        case Mode::WriteOpen:
            devmode = DevMode::Write;
            break;
        case Mode::ReadWriteOpen:
            devmode = DevMode::ReadWrite;
            break;
        default:
            devmode = DevMode::None;
    }
    if (devmode == DevMode::None) {
        return false;
    }
    handle_ = driver_.open(getName(), devmode);
    return handle_ != nullptr;
}

bool DistDevice::close() {
    driver_.close(handle_);
    handle_ = nullptr;
    return true;
}

bool DistDevice::configure(const std::string& propname, const PropertyVar& prop) {
    if (handle_ == nullptr) {
        return false;
    }
    if (std::holds_alternative<int>(prop) && propname == POLL_DELAY_MS) {
        poll_delay_ms_ = std::get<int>(prop);
        return true;
    } else if (std::holds_alternative<int>(prop) && propname == SENSOR_NUMBER) {
        sensor_ = std::get<int>(prop);
        return true;
    } else if (std::holds_alternative<std::string>(prop) && propname == UNITS) {
        auto units = std::get<std::string>(prop);
        driver_.setUnits(units);
        return true;
    }
    return false;
}

PropertyVar DistDevice::query(const std::string& propname) {
    PropertyVar var;
    if (propname == SENSOR_NUMBER) {
        var = sensor_;
    } else if (propname == UNITS) {
        var = static_cast<std::string>(driver_.getUnits());
    }
    return var;
}

int DistDevice::read(size_t num, void* buf) {
    if (num == 0 || buf == nullptr || handle_ == nullptr) {
        return -1;
    }
    DriverBuffer drvbuf(sizeof(int));
    auto count = driver_.read(handle_, drvbuf);
    if (count > 0) {
        memcpy(buf, drvbuf.data(), count);
    }
    return count;
}

int DistDevice::write(size_t num, const void* buf) {
    if (num == 0 || buf == nullptr || handle_ == nullptr) {
        return -1;
    }
    DriverBuffer drvbuf(sizeof(int));
    drvbuf.writeInt(*(int *)buf);
    auto count = driver_.write(handle_, drvbuf);
    return count;
}

Device::Mode DistDevice::status() {
    Device::Mode mode = Device::Mode::Unknown;
    if (handle_ == nullptr) {
        return mode;
    }
    DevMode drvmode = driver_.mode(handle_);
    switch (drvmode) {
        case DevMode::None:
            mode = Device::Mode::Unopened;
            break;
        case DevMode::Read:
            mode = Device::Mode::ReadOpen;
            break;
        case DevMode::Write:
            mode = Device::Mode::WriteOpen;
            break;
        case DevMode::ReadWrite:
            mode = Device::Mode::ReadWriteOpen;
            break;
    }
    return mode;
}

bool DistDevice::handles(DeviceEvent event) {
    return event == DeviceEvent::DataAvailable ||
           event == DeviceEvent::StateChange;
}

bool DistDevice::is_listening(DeviceEvent event) {
    bool ret = false;
    switch (event) {
        case DeviceEvent::DataAvailable:
            ret = newDataCb_ != nullptr;
            break;
        case DeviceEvent::StateChange:
            ret = stateChangeCb_ != nullptr;
            break;
        default:
            break;
    }
    return ret;
}

Device::Handle DistDevice::listen(DeviceEvent event, Callback cb) {
    Device::Handle handle = nullptr;
    if (event == DeviceEvent::DataAvailable) {
        newDataCb_ = cb;
        handle = &newDataCb_;
    } else if (event == DeviceEvent::StateChange) {
        stateChangeCb_ = cb;
        handle = &stateChangeCb_;
    }
    if (handle != nullptr && !running_) {
        running_ = true;
        thread_ = std::thread(&DistDevice::poll_loop, this);
    }
    return handle;
}

bool DistDevice::unlisten(Handle handle) {
    bool ret = true;
    if (handle == nullptr) {
        ret = false;
    } else if (handle == &newDataCb_) {
        newDataCb_ = nullptr;
    } else if (handle == &stateChangeCb_) {
        stateChangeCb_ = nullptr;
    } else {
        ret = false;
    }
    if (ret) {
        if (newDataCb_ == nullptr && stateChangeCb_ == nullptr) {
            running_ = false;
            if (thread_.joinable()) {
                thread_.join();
            }
        }
    }
    return ret;
}

int DistDevice::lastValue() const {
    return last_value_;
}

void DistDevice::poll_loop() {
    cslog << loglevel(Info) << "Distance poll loop starting for " << getName() << endl;
    while (running_) {
        int value;
        char inbuf[40];
        auto num_read = read(sizeof(inbuf), inbuf);
        if (num_read > 0) {
            inbuf[num_read] = 0;
            value = atoi(inbuf);
            if (last_value_ != value) {
                last_value_ = value;
                cslog << loglevel(Info) << "Dist got value " << value << endl;
                int ret = 0;
                if (stateChangeCb_ != nullptr) {
                    ret = stateChangeCb_(DeviceEvent::StateChange, *this);
                }
                if (ret >= 0) {
                    Properties props;
                    props.set("sensor", sensor_);
                    props.set("value", value);
                    auto event = eval::EventFactory::instance()->makeEvent("distance", props);
                    ControlService::instance()->pushEvent(event);
                }
            }
            if (newDataCb_ != nullptr) {
                auto ret = newDataCb_(DeviceEvent::DataAvailable, *this);
                if (ret >= 0) {
                    Properties props;
                    props.set("sensor", sensor_);
                    props.set("value", value);
                    auto event = eval::EventFactory::instance()->makeEvent("distance", props);
                    ControlService::instance()->pushEvent(event);
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(poll_delay_ms_));
    }
    cslog << loglevel(Info) << "Distance Poll loop exit" << endl;
}

}  // namespace ctlsvc::device
