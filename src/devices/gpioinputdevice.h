//
//   Copyright 2020-2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner
#pragma once

#include "device.h"
#include "drivers/gpiodriver.h"

#include <atomic>
#include <thread>
#include <utility>

namespace ctlsvc {
namespace device {

class GpioInputDevice : public Device {
public:
    using Device::read;

    /** Create a gpio input device
     *  @param pin The gpio pin number as specified by Broadcom.
     *  @param sensor Sequential number of the input gpios used.
     */
    GpioInputDevice(int pin, int sensor);
    GpioInputDevice(GpioInputDevice&& other);
    virtual ~GpioInputDevice();

    // Custom getters for this device
    int pin() const;
    int value() const;

    virtual void deinit() override;
    virtual bool init() override;

    virtual bool close() override;
    virtual bool configure(const std::string& propname, const PropertyVar& prop) override;
    virtual bool open(Mode mode) override;
    virtual PropertyVar query(const std::string& propname) override;
    virtual int read(size_t num, void* buf) override;
    virtual Mode status() override;
    virtual int write(size_t num, const void* buf) override;

    virtual bool handles(DeviceEvent event) override;
    virtual bool is_listening(DeviceEvent event) override;
    virtual Handle listen(DeviceEvent event, Callback cb) override;
    virtual bool unlisten(Handle handle) override;

    // Properties for this device
    static const std::string PIN_NUMBER;
    static const std::string SENSOR_NUMBER;
    static const std::string POLL_DELAY_MS;;

    // TODO allow configuring read to be blocking

private:
    void poll_loop();

    ctlsvc::driver::GpioDriver driver_;
    uint32_t pin_;
    uint32_t sensor_;
    int last_value_;
    int poll_delay_ms_;
    ctlsvc::driver::DriverHandle handle_;
    Callback newDataCb_;
    Callback stateChangeCb_;
    std::atomic_bool running_;
    std::thread thread_;
};

}  // namespace device
}  // namespace ctlsvc
