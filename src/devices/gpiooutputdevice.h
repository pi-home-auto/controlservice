//
//   Copyright 2020-2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "device.h"
#include "drivers/gpiodriver.h"

namespace ctlsvc {
namespace device {

class GpioOutputDevice : public Device {
public:
    using Device::read;
    using Device::write;

    /** Construct a GpioOutputDevice.
     *  @param pin  Pin number of the gpio (hardware dependent). For RaspberryPi, this
     *              is the Broadcom pin number.
     *  @param sensor  The number of this (output) gpio, numbered sequentially
     *                 from 0 to (total output gpios - 1). This is 0 for the status LED.
     *  @param use_pwm Specify true if this pin uses pwm
     */
    GpioOutputDevice(int pin, int sensor, bool use_pwm = false);
    virtual ~GpioOutputDevice();

    GpioOutputDevice(const GpioOutputDevice&) = delete;
    GpioOutputDevice(GpioOutputDevice&&);
    GpioOutputDevice& operator=(const GpioOutputDevice&) = delete;

    virtual void deinit() override;
    virtual bool init() override;

    virtual bool close() override;
    virtual bool configure(const std::string& propname, const PropertyVar& prop) override;
    virtual bool open(Mode mode) override;
    virtual PropertyVar query(const std::string& propname) override;
    virtual int read(size_t num, void* buf) override;
    virtual Mode status() override;
    virtual int write(size_t num, const void* buf) override;

    // Properties for this device
    static const std::string PIN_NUMBER;
    static const std::string SENSOR_NUMBER;
    static const std::string USE_PWM;
    static const std::string USE_SERVO;

private:
    ctlsvc::driver::GpioDriver driver_;
    uint32_t pin_;
    int sensor_;
    bool usePwm_;
    ctlsvc::driver::DriverHandle handle_;
};

}  // namespace device
}  // namespace ctlsvc
