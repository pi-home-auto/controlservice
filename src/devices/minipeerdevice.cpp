//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner


#include "logger.h"
#include "minipeer.h"
#include "minipeerdevice.h"

#include <cstddef>
#include <unistd.h>

using ctlsvc::driver::DevMode;
using ctlsvc::driver::DriverBuffer;
using ctlsvc::driver::DriverStatus;
using ctlsvc::driver::I2cDriver;
using std::endl;


namespace ctlsvc::device {

namespace minipeer {

Input::Input(MiniPeerDevice& dev, int devnum)
    : dev_(dev)
    , devnum_(devnum) {}

int Input::get() {
  unsigned char buf[2] = { mpiGet, devnum_ };
  auto szBuf = sizeof(buf);
  decltype(szBuf) sz = dev_.write(szBuf, buf);
  if (sz == szBuf) {
    sz = dev_.read(1, buf);
    if (sz == 1) {
      uint16_t pos = buf[0] & 0xff;
      return pos;
    }
  }
  return -1;
}

Output::Output(MiniPeerDevice& dev, int devnum)
    : dev_(dev)
    , devnum_(devnum) {}

int Output::get() {
  unsigned char buf[2] = { mpoGet, devnum_ };
  auto szBuf = sizeof(buf);
  decltype(szBuf) sz = dev_.write(szBuf, buf);
  if (sz == szBuf) {
    sz = dev_.read(1, buf);
    if (sz == 1) {
      uint16_t pos = buf[0] & 0xff;
      return pos;
    }
  }
  return -1;
}

void Output::pwm(int value) {
  using UC = unsigned char;
  UC buf[] = { mpoPwm, devnum_, (UC)value };
  auto szBuf = sizeof(buf);
  dev_.write(szBuf, buf);
}

void Output::set(int value) {
  using UC = unsigned char;
  UC buf[] = { mpoPut, devnum_, (UC)value };
  auto szBuf = sizeof(buf);
  dev_.write(szBuf, buf);
}

StepMotor::StepMotor(MiniPeerDevice& dev, int devnum)
    : dev_(dev)
    , devnum_(devnum) {}

int StepMotor::home() {
  return -1;
}

int StepMotor::moveDown(int steps) {
  unsigned char buf[] = { mpsMoveDown, devnum_, (unsigned char)(steps >> 8), (unsigned char)(steps & 0xff) };
  auto szBuf = sizeof(buf);
  decltype(szBuf) sz = dev_.write(szBuf, buf);
  return sz == szBuf ? steps : 0;
}

int StepMotor::moveUp(int steps) {
  unsigned char buf[] = { mpsMoveUp, devnum_, (unsigned char)(steps >> 8), (unsigned char)(steps & 0xff) };
  auto szBuf = sizeof(buf);
  decltype(szBuf) sz = dev_.write(szBuf, buf);
  return sz == szBuf ? steps : 0;
}

void StepMotor::position(int pos) {
  unsigned char stepH = pos >> 8;
  unsigned char stepL = pos & 8;
  unsigned char buf[] = { mpsSetPos, devnum_, stepH, stepL };
  dev_.write(sizeof(buf), buf);
}

const int StepMotor::position() const {
  unsigned char buf[2] = { mpsReadPos, devnum_ };
  auto szBuf = sizeof(buf);
  decltype(szBuf) sz = dev_.write(szBuf, buf);
  if (sz == szBuf) {
    //usleep(1400);
    sz = dev_.read(szBuf, buf);
    if (sz == szBuf) {
      uint16_t pos = (buf[0] << 8) | (buf[1] & 0xff);
      return pos;
    } else {
      cslog << loglevel(Error) << "Failed reading i2c, bytes=" << sz << endl;
    }
  } else {
      cslog << loglevel(Error) << "Failed writing i2c, bytes=" << szBuf << endl;
  }
  return -1;
}

void StepMotor::stop() {
  unsigned char buf[] = { mpsStop, devnum_ };
  dev_.write(sizeof(buf), buf);
}

} // namespace minipeer


const std::string MiniPeerDevice::INPUTS("inputs");
const std::string MiniPeerDevice::OUTPUTS("outputs");
const std::string MiniPeerDevice::STEPPING_MOTORS("steppers");

MiniPeerDevice::MiniPeerDevice(const std::string& name, int bus, int addr)
    : Device("minipeer." + name)
    , bus_(bus)
    , addr_(addr)
    , mode_(Device::Mode::Unknown)
    , handle_(nullptr)
    , i2c_() {}

MiniPeerDevice::~MiniPeerDevice() {
  deinit();
}

void MiniPeerDevice::deinit() {
  close();
  remove_devices();
}

bool MiniPeerDevice::init() {
  return true;
}

bool MiniPeerDevice::close() {
  if (handle_ != nullptr) {
    i2c_.close(handle_);
    handle_ = nullptr;
    mode_ = Device::Mode::Unknown;
    return true;
  }
  return false;
}

bool MiniPeerDevice::configure([[maybe_unused]] const std::string& propname,
                               [[maybe_unused]] const PropertyVar& prop) {
  return false;
}

bool MiniPeerDevice::open(Mode mode) {
  DevMode devmode;
  switch (mode) {
    case Mode::ReadOpen:
      devmode = DevMode::Read;
      break;
    case Mode::WriteOpen:
      devmode = DevMode::Write;
      break;
    case Mode::ReadWriteOpen:
      devmode = DevMode::ReadWrite;
      break;
    default:
      devmode = DevMode::None;
  }
  if (devmode == DevMode::None) {
    mode_ = Device::Mode::Unknown;
    return false;
  }
  mode_ = mode;
  std::string name("i2c." + std::to_string(bus_) + "." + std::to_string(addr_));
  handle_ = i2c_.open(name, devmode);
  if (handle_ != nullptr) {
    setup_devices();
    return true;
  }
  return false;
}

PropertyVar MiniPeerDevice::query(const std::string& propname) {
  PropertyVar var;
  if (propname == INPUTS) {
    var = static_cast<int>(inputs_.size());
  } else if (propname == OUTPUTS) {
    var = static_cast<int>(outputs_.size());
  } else if (propname == STEPPING_MOTORS) {
    var = static_cast<int>(stepmotors_.size());
  }
  return var;
}

bool MiniPeerDevice::peripheral(uint32_t num, minipeer::Input** peri) {
  if (inputs_.size() > num) {
    *peri = inputs_[num];
    return true;
  }
  return false;
}

bool MiniPeerDevice::peripheral(uint32_t num, minipeer::Output** peri) {
  if (outputs_.size() > num) {
    *peri = outputs_[num];
    return true;
  }
  return false;
}

bool MiniPeerDevice::peripheral(uint32_t num, minipeer::StepMotor** peri) {
  if (stepmotors_.size() > num) {
    *peri = stepmotors_[num];
    return true;
  }
  return false;
}

int MiniPeerDevice::read(size_t num, void* buf) {
  if (num == 0 || buf == nullptr || handle_ == nullptr) {
    cslog << loglevel(Error) << "Bad parameter to MiniPeerDevice::read" << endl;
    return -1;
  }
  DriverBuffer drvbuf(num);
  auto count = i2c_.read(handle_, drvbuf);
  if (count > 0) {
    std::copy(drvbuf.data(), drvbuf.data() + count, (std::byte*) buf);
  }
  return count;
}

void MiniPeerDevice::remove_devices() {
  for (auto d : inputs_) {
    delete d;
  }
  inputs_.clear();
  for (auto d : outputs_) {
    delete d;
  }
  outputs_.clear();
  for (auto d : stepmotors_) {
    delete d;
  }
  stepmotors_.clear();
}

void MiniPeerDevice::setup_devices() {
  remove_devices();
  if (status() == Device::Mode::Unopened || status() == Device::Mode::Unknown) {
    return;
  }
  unsigned char buf[8] = { mpcId, MINIPEER_VERSION >> 8, MINIPEER_VERSION & 8 };
  auto sz = write(3, buf);
  if (sz != 3) {
    cslog << loglevel(Error) << "Got error writing to i2c device, bytes=" << sz << endl;
  } else {
    buf[0] = 0;
    sz = read(5, buf);
    cslog << loglevel(Info) << "Sent mpcId i2c command, returned=" << sz << endl;
    if (sz != 5) {
      return;
    }
    uint16_t vers = (buf[0] << 8) | buf[1];
    uint16_t num_gpioins = buf[2];
    uint16_t num_gpioouts = buf[3];
    uint16_t num_steppers = buf[4];
    cslog << loglevel(Info) << "minipeer Version " << vers << " number of gpioIn=" << num_gpioins
          << ", gpioOut=" << num_gpioouts << " steppers=" << num_steppers << endl;
    if (MINIPEER_VERSION != vers) {
      cslog << loglevel(Error) << "Minipeer version mismatch, expecting " << MINIPEER_VERSION
            << " but got version " << vers << endl;
    } else {
      if (num_gpioins > 0 && num_gpioins <= MINIPEER_MAX_GPIOS) {
        for (int ii = 0; ii < num_gpioins; ++ii) {
          inputs_.push_back(new minipeer::Input(*this, ii));
        }
      }
      if (num_gpioouts > 0 && num_gpioouts <= MINIPEER_MAX_GPIOS) {
        for (int ii = 0; ii < num_gpioouts; ++ii) {
          outputs_.push_back(new minipeer::Output(*this, ii));
        }
      }
      if (num_steppers > 0 && num_steppers <= MINIPEER_MAX_MOTORS) {
        for (int ii = 0; ii < num_steppers; ++ii) {
          stepmotors_.push_back(new minipeer::StepMotor(*this, ii));
        }
      }
    }
  }
}

Device::Mode MiniPeerDevice::status() {
  return mode_;
}

int MiniPeerDevice::write(size_t num, const void* buf) {
  if (num == 0 || buf == nullptr || handle_ == nullptr) {
    cslog << loglevel(Error) << "Bad parameter to MiniPeerDevice::write" << endl;
    return -1;
  }
  DriverBuffer drvbuf((const std::byte*)buf, num);
  auto count = i2c_.write(handle_, drvbuf);
  return count;
}

}  // namespace ctlsvc::device
