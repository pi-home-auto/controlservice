//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner


#include "gpiooutputdevice.h"
#include "logger.h"

#include <cstddef>

using ctlsvc::driver::DevMode;
using ctlsvc::driver::DriverBuffer;
using ctlsvc::driver::DriverStatus;
using ctlsvc::driver::GpioDriver;

namespace {

constexpr uint32_t kInvalidPin = 9999;

}

namespace ctlsvc::device {

const std::string GpioOutputDevice::PIN_NUMBER("pin");
const std::string GpioOutputDevice::SENSOR_NUMBER("sensor");
const std::string GpioOutputDevice::USE_PWM("use_pwm");
const std::string GpioOutputDevice::USE_SERVO("use_servo");


GpioOutputDevice::GpioOutputDevice(int pin, int sensor, bool use_pwm)
    : Device("gpio." + std::to_string(pin))
    , pin_(pin)
    , sensor_(sensor)
    , usePwm_(use_pwm)
    , handle_(nullptr) {}

GpioOutputDevice::GpioOutputDevice(GpioOutputDevice&& other)
    : Device(other.getName())
    , pin_(other.pin_)
    , sensor_(other.sensor_)
    , usePwm_(other.usePwm_)
    , handle_(other.handle_) {}

GpioOutputDevice::~GpioOutputDevice() {
    deinit();
}

void GpioOutputDevice::deinit() {
    close();
    pin_ = 0;
}

bool GpioOutputDevice::init() {
    return true;
}

bool GpioOutputDevice::open(Mode mode) {
    if (pin_ == kInvalidPin) {
        return false;
    }
    DevMode devmode;
    switch (mode) {
        case Mode::ReadOpen:
            devmode = DevMode::Read;
            break;
        case Mode::WriteOpen:
            devmode = DevMode::Write;
            break;
        case Mode::ReadWriteOpen:
            devmode = DevMode::ReadWrite;
            break;
        default:
            devmode = DevMode::None;
    }
    if (devmode == DevMode::None) {
        return false;
    }
    handle_ = driver_.open(getName(), devmode);
    return handle_ != nullptr;
}

bool GpioOutputDevice::close() {
    driver_.close(handle_);
    handle_ = nullptr;
    return true;
}

bool GpioOutputDevice::configure(const std::string& propname, const PropertyVar& prop) {
    if (handle_ == nullptr || !std::holds_alternative<bool>(prop)) {
        return false;
    }
    if (propname == USE_PWM) {
        usePwm_ = std::get<bool>(prop);
        driver_.usePwm(handle_, usePwm_);
        if (usePwm_) {
            driver_.useServo(handle_, false);
        }
        return true;
    } else if (propname == USE_SERVO) {
        bool us = std::get<bool>(prop);
        if (us) {
            driver_.usePwm(handle_, false);
        }
        driver_.useServo(handle_, us);
        return true;
    }
    return false;
}

PropertyVar GpioOutputDevice::query(const std::string& propname) {
    PropertyVar var;
    if (propname == SENSOR_NUMBER) {
        var = sensor_;
    } else if (propname == PIN_NUMBER) {
        var = static_cast<int>(pin_);
    } else if (propname == USE_PWM) {
        var = usePwm_;
    } else if (propname == USE_SERVO && handle_ != nullptr) {
        var = driver_.useServo(handle_);
    }
    return var;
}

int GpioOutputDevice::read(size_t num, void* buf) {
    if (num == 0 || buf == nullptr || handle_ == nullptr) {
        return -1;
    }
    DriverBuffer drvbuf(sizeof(int));
    auto count = driver_.read(handle_, drvbuf);
    if (count > 0) {
        *(int *)buf = std::to_integer<int>(*drvbuf.data());
    }
    return count;
}

int GpioOutputDevice::write(size_t num, const void* buf) {
    if (num == 0 || buf == nullptr || handle_ == nullptr) {
        return -1;
    }
    DriverBuffer drvbuf(sizeof(int));
    drvbuf.writeInt(*(int *)buf);
    auto count = driver_.write(handle_, drvbuf);
    return count;
}

Device::Mode GpioOutputDevice::status() {
    Device::Mode mode = Device::Mode::Unknown;
    if (handle_ == nullptr) {
        return mode;
    }
    DevMode drvmode = driver_.mode(handle_);
    switch (drvmode) {
        case DevMode::None:
            mode = Device::Mode::Unopened;
            break;
        case DevMode::Read:
            mode = Device::Mode::ReadOpen;
            break;
        case DevMode::Write:
            mode = Device::Mode::WriteOpen;
            break;
        case DevMode::ReadWrite:
            mode = Device::Mode::ReadWriteOpen;
            break;
    }
    return mode;
}

}  // namespace ctlsvc::device
