//
//   Copyright 2021-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "eval/eventfactory.h"
#include "eval/events.h"
#include "gpioinputdevice.h"
#include "logger.h"
#include "statistics.h"

#include <cstddef>

using ctlsvc::driver::DevMode;
using ctlsvc::driver::DriverBuffer;
using ctlsvc::driver::DriverStatus;
using ctlsvc::driver::GpioDriver;
using std::endl;


namespace ctlsvc::device {

const std::string GpioInputDevice::PIN_NUMBER("pin");
const std::string GpioInputDevice::SENSOR_NUMBER("sensor");
const std::string GpioInputDevice::POLL_DELAY_MS("polldelay_ms");


GpioInputDevice::GpioInputDevice(int pin, int sensor)
    : Device("gpio." + std::to_string(pin))
    , pin_(pin)
    , sensor_(sensor)
    , last_value_(-1)
    , poll_delay_ms_(100)
    , handle_(nullptr)
    , newDataCb_(nullptr)
    , stateChangeCb_(nullptr)
    , running_(false) {}

GpioInputDevice::GpioInputDevice(GpioInputDevice&& other)
    : Device(other.getName())
    , pin_(other.pin_)
    , sensor_(other.sensor_)
    , last_value_(other.last_value_)
    , poll_delay_ms_(other.poll_delay_ms_)
    , handle_(other.handle_)
    , newDataCb_(other.newDataCb_)
    , stateChangeCb_(other.stateChangeCb_)
    , running_(other.running_.load())
    , thread_(std::move(other.thread_)) {}

GpioInputDevice::~GpioInputDevice() {
    deinit();
}

void GpioInputDevice::deinit() {
    close();
    // TODO unlisten all
    pin_ = 0;
}

bool GpioInputDevice::init() {
    if (sensor_ == 0) {
        cslog << loglevel(Error) << getName() << " must specify device number (1 to nr. of sensors)." << std::endl;
        return false;
    }
    cslog << loglevel(Info) << "GpioInput::init for pin " << pin_ << endl;
    return true;
}

bool GpioInputDevice::open(Mode mode) {
    if (pin_ == 0) {
        return false;
    }
    if (mode == Mode::ReadOpen) {
        handle_ = driver_.open(getName(), DevMode::Read);
        if (handle_ == nullptr) {
            return false;
        }
        return true;
    }
    return false;
}

bool GpioInputDevice::close() {
    if (running_) {
        running_ = false;
        if (thread_.joinable()) {
            thread_.join();
        }
    }
    driver_.close(handle_);
    handle_ = nullptr;
    return true;
}

bool GpioInputDevice::configure(const std::string& propname, const PropertyVar& prop) {
    if (propname == POLL_DELAY_MS && std::holds_alternative<int>(prop)) {
        int snum = std::get<int>(prop);
        if (snum >= 0) {
            poll_delay_ms_ = snum;
            return true;
        }
    }
    return false;
}

PropertyVar GpioInputDevice::query(const std::string& propname) {
    PropertyVar var;
    if (propname == SENSOR_NUMBER) {
        var = static_cast<int>(sensor_);
    } else if (propname == PIN_NUMBER) {
        var = static_cast<int>(pin_);
    } else if (propname == POLL_DELAY_MS) {
        var = poll_delay_ms_;
    }
    return var;
}

int GpioInputDevice::read(size_t num, void* buf) {
    if (num == 0 || buf == nullptr || handle_ == nullptr) {
        return -1;
    }
    DriverBuffer drvbuf(sizeof(int));
    auto count = driver_.read(handle_, drvbuf);
    if (count > 0) {
        int value = std::to_integer<int>(*drvbuf.data());
        if (value != last_value_) {
            last_value_ = value;
            if (stateChangeCb_ != nullptr) {
                stateChangeCb_(DeviceEvent::StateChange, *this);
            }
            Properties props;
            props.set("pin", static_cast<int>(pin_));
            props.set("value", value);
            auto event = eval::EventFactory::instance()->makeEvent("pinChanged", props);
            ControlService::instance()->pushEvent(event);
        }
        *(int *)buf = value;
    }
    return count;
}

int GpioInputDevice::write([[maybe_unused]] size_t num,
                            [[maybe_unused]] const void* buf) {
    return -1;
}

Device::Mode GpioInputDevice::status() {
    Device::Mode mode = Device::Mode::Unknown;
    if (handle_ == nullptr) {
        return mode;
    }
    DevMode drvmode = driver_.mode(handle_);
    switch (drvmode) {
        case DevMode::None:
            mode = Device::Mode::Unopened;
            break;
        case DevMode::Read:
            mode = Device::Mode::ReadOpen;
            break;
        case DevMode::Write:
            mode = Device::Mode::WriteOpen;
            break;
        case DevMode::ReadWrite:
            mode = Device::Mode::ReadWriteOpen;
            break;
    }
    return mode;
}

bool GpioInputDevice::handles(DeviceEvent event) {
    return event == DeviceEvent::DataAvailable ||
           event == DeviceEvent::StateChange;
}

bool GpioInputDevice::is_listening(DeviceEvent event) {
    bool ret = false;
    switch (event) {
        case DeviceEvent::DataAvailable:
            ret = newDataCb_ != nullptr;
            break;
        case DeviceEvent::StateChange:
            ret = stateChangeCb_ != nullptr;
            break;
        default:
            break;
    }
    return ret;
}

Device::Handle GpioInputDevice::listen(DeviceEvent event, Callback cb) {
    Device::Handle handle = nullptr;
    if (event == DeviceEvent::DataAvailable) {
        newDataCb_ = cb;
        handle = &newDataCb_;
    } else if (event == DeviceEvent::StateChange) {
        stateChangeCb_ = cb;
        handle = &stateChangeCb_;
    }
    if (handle != nullptr && !running_) {
        running_ = true;
        thread_ = std::thread(&GpioInputDevice::poll_loop, this);
    }
    return handle;
}

bool GpioInputDevice::unlisten(Handle handle) {
    bool ret = true;
    if (handle == nullptr) {
        ret = false;
    } else if (handle == &newDataCb_) {
        newDataCb_ = nullptr;
    } else if (handle == &stateChangeCb_) {
        stateChangeCb_ = nullptr;
    } else {
        ret = false;
    }
    if (ret) {
        if (newDataCb_ == nullptr && stateChangeCb_ == nullptr) {
            running_ = false;
            if (thread_.joinable()) {
                thread_.join();
            }
        }
    }
    return ret;
}

// TODO Use pigpio alerts instead
// TODO support DataAvailable callback by getting the fd from the handler and doing a
//      poll (POLLPRI)
void GpioInputDevice::poll_loop() {
    cslog << loglevel(Info) << "Gpio input poll loop starting for " << getName()
          << " pin " << pin_ << endl;
    while (running_) {
        int value;
        if (read(sizeof(int), &value) > 0) {
            if (last_value_ != value) {
                last_value_ = value;
                cslog << loglevel(Info) << "Gpio got input on " << pin_ << " value " << value << endl;
                int ret = 0;
                if (stateChangeCb_ != nullptr) {
                    ret = stateChangeCb_(DeviceEvent::StateChange, *this);
                }
                if (ret >= 0) {
                    Properties props;
                    props.set("pin", static_cast<int>(pin_));
                    props.set("value", value);
                    auto event = eval::EventFactory::instance()->makeEvent("pinChanged", props);
                    ControlService::instance()->pushEvent(event);
                }
            }
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(poll_delay_ms_));
    }
    cslog << loglevel(Info) << "Poll loop exit for pin " << pin_ << endl;
}

int GpioInputDevice::pin() const {
    return pin_;
}

int GpioInputDevice::value() const {
    return last_value_;
}

}  // namespace ctlsvc::device
