//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "device.h"
#include "driver.h"
#include "drivers/i2cdriver.h"
#include <vector>

namespace ctlsvc {
namespace device {

/**
 *  Device to communicate with a peer that is connected via i2c (instead of TCP/IP)
 *  A mini-peer is typically used to control peripherals that are not directly
 *  connected to the Raspberry Pi running an automation module.
 *  This is useful to reduce the number of wires used to develop a solution and
 *  helps distribute power usage to prevent the Pi's GPIOs from using too much
 *  current (the total current consumed by all GPIOs should total less than 150mA).
 *
 *  (see the Architecture.md document for more details about minipeers)
 */
class MiniPeerDevice;

namespace minipeer {

class Input {
public:
  Input(MiniPeerDevice& dev, int devnum);
  int get();
private:
  MiniPeerDevice& dev_;
  unsigned char devnum_;
};

class Output {
public:
  Output(MiniPeerDevice& dev, int devnum);
  int get();
  /** Write a pwm value between 0 and 255 */
  void pwm(int value);
  /** Write a digital value (0 or 1) */
  void set(int value);
private:
  MiniPeerDevice& dev_;
  unsigned char devnum_;
};

class StepMotor {
public:
  StepMotor(MiniPeerDevice& dev, int devnum);
  /** Move to the zero (or home) position.
   *  @return The number of steps that were moved
   */
  int home();
  int moveDown(int steps);
  int moveUp(int steps);
  void position(int pos);
  const int position() const;
  void stop();
private:
  MiniPeerDevice& dev_;
  unsigned char devnum_;
};

} // namespace minipeer

class MiniPeerDevice : public Device {
public:
  MiniPeerDevice(const std::string& name, int bus, int addr);
  virtual ~MiniPeerDevice();

  MiniPeerDevice(const MiniPeerDevice&) = delete;
  MiniPeerDevice& operator=(const MiniPeerDevice&) = delete;

  [[nodiscard]] bool peripheral(uint32_t num, minipeer::Input** peri);
  [[nodiscard]] bool peripheral(uint32_t num, minipeer::Output** peri);
  [[nodiscard]] bool peripheral(uint32_t num, minipeer::StepMotor** peri);

  // Implement the Device interface
  virtual void deinit() override;
  virtual bool init() override;

  virtual bool close() override;
  virtual bool configure(const std::string& propname, const PropertyVar& prop) override;
  virtual bool open(Mode mode) override;
  virtual PropertyVar query(const std::string& propname) override;
  virtual int read(size_t num, void* buf) override;
  virtual Mode status() override;
  virtual int write(size_t num, const void* buf) override;

  // Properties for this device
  static const std::string INPUTS;
  static const std::string OUTPUTS;
  static const std::string STEPPING_MOTORS;

private:
  void remove_devices();
  void setup_devices();

  uint32_t bus_;
  int addr_;
  Device::Mode mode_;
  ctlsvc::driver::DriverHandle handle_;
  ctlsvc::driver::I2cDriver i2c_;
  std::vector<minipeer::Input*> inputs_;
  std::vector<minipeer::Output*> outputs_;
  std::vector<minipeer::StepMotor*> stepmotors_;
};

}  // namespace device
}  // namespace ctlsvc
