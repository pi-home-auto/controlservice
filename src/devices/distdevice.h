//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "device.h"
#include "drivers/distdriver.h"

#include <atomic>
#include <thread>

namespace ctlsvc {
namespace device {

class DistDevice : public Device {
public:
    using Device::read;
    using Device::write;

    /** Construct a DistDevice.
     *  @param sensor  The number of this distance sensor, numbered sequentially
     *                 from 0 to (total dist sensors - 1).
     */
    DistDevice(int sensor = 0);
    virtual ~DistDevice();

    DistDevice(const DistDevice&) = delete;
    DistDevice(DistDevice&&) = delete;
    DistDevice& operator=(const DistDevice&) = delete;

    virtual void deinit() override;
    virtual bool init() override;

    virtual bool close() override;
    virtual bool configure(const std::string& propname, const PropertyVar& prop) override;
    virtual bool open(Mode mode) override;
    virtual PropertyVar query(const std::string& propname) override;
    virtual int read(size_t num, void* buf) override;
    virtual Mode status() override;
    virtual int write(size_t num, const void* buf) override;

    virtual bool handles(DeviceEvent event) override;
    virtual bool is_listening(DeviceEvent event) override;
    virtual Handle listen(DeviceEvent event, Callback cb) override;
    virtual bool unlisten(Handle handle) override;

    int lastValue() const;

    // Properties for this device
    static const std::string POLL_DELAY_MS;;
    static const std::string SENSOR_NUMBER;
    static const std::string UNITS;

private:
    void poll_loop();

    ctlsvc::driver::DistDriver driver_;
    int sensor_;
    int last_value_;
    int poll_delay_ms_;
    ctlsvc::driver::DriverHandle handle_;
    Callback newDataCb_;
    Callback stateChangeCb_;
    std::atomic_bool running_;
    std::thread thread_;
};

}  // namespace device
}  // namespace ctlsvc
