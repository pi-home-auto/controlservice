//
//   Copyright 2021-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <sys/types.h>
#include <signal.h>
#include <string>
#include <unistd.h>
#include <vector>

namespace ctlsvc {

enum class Command {
    Unknown,
    Exit,
    ReadError,
    ReadOut,
    Spawn,
    SpawnIO,
    Terminate,
    Write
};

enum class Response {
    Unknown,
    Bad,    // This indicates a problem with the command/response
    Error,  // This indicates an error with the spawned process
    Ok,
    Running,
    Spawned
};

struct ForkEntry {
    int handle;
    pid_t pid;
    int pipes[3];
    const char* cmdline;
    std::string stdout;
    std::string stderr;

    static constexpr int PIPEIN  = 0;
    static constexpr int PIPEOUT = 1;
    static constexpr int PIPEERR = 2;
    static constexpr int INVALID_HANDLE = -1;

    ForkEntry()
        : handle(INVALID_HANDLE)
        , pid(0)
        , pipes{ -1, -1, -1 }
        , cmdline(nullptr) { }
    ~ForkEntry() {
        if (cmdline) {
            delete[] cmdline;
        }
        for (int ii = 0; ii < 3; ++ii) {
            if (pipes[ii] >= 0) {
                ::close(pipes[ii]);
            }
        }
    }
};

/**
 * This class will open a PIPE, fork the process and execute the launcher() method, which
 * reads JSON from the PIPE and controls external processes.  Programs can be fork/exec'd,
 * queried for status, request stdout/stderr, issue command on stdin, and finally 
 * terminated.
 */
class Forker {
public:
    using ForkHandle = int;
    static constexpr int kInvalidForkHandle = -1;

    Forker();
    ~Forker();

    // Public API
    bool init();
    void uninit();
    ForkHandle spawn(const std::string& cmd, bool do_io = false);
    ForkHandle spawn(const std::string& cmd, const std::vector<std::string>& args, bool do_io = false);
    int  getStatus(ForkHandle handle);
    bool isRunning(ForkHandle handle);
    bool readFrom(ForkHandle handle, std::string& stdout, std::string& stderr);
    bool writeTo(ForkHandle handle, const std::string& input, bool append_return = true);
    bool terminate(ForkHandle handle, int signal = SIGKILL);

protected:
    void command(Command cmd, const std::string& parameters = std::string());
    Response response(std::string& resp);
    ForkEntry* find(ForkHandle handle);
    void handleSigChld(int sig);
    void launcher();
    bool removePid(pid_t pid);
    void resync(bool is_launcher);
    bool _terminate(int pid, int signal);

private:
    void setupPath();
    /** This is used to communicate with the launcher.
     *  Commands are written to command pipe and responses from the launcher
     *  are read from the reponse pipe
     */
    int cmd_pipe_[2];
    int resp_pipe_[2];
    std::vector<ForkEntry*> entries_;
};

}  // namespace ctlsvc
