//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "database.h"
#include "logger.h"
#include <sqlite3.h>
using std::endl;

namespace {

// sqlite3_exec callback that fills a single record (DbDataHolder subclass)
int dbcallback(void *ptr, int argc, char **argv, char **azColName) {
  using namespace ctlsvc;

  if (ptr != nullptr) {
    auto holder = static_cast<DbDataHolder *>(ptr);
    int idx;
    for (idx = 0; idx < argc; ++idx) {
      cslog << loglevel(Trace) << azColName[idx] << " = " << (argv[idx] == nullptr ? "(null)" : argv[idx]) << endl;
      holder->set(azColName[idx], argv[idx] == nullptr ? "(null)" : argv[idx]);
    }
    cslog << loglevel(Trace) << endl;
    return 0;
  } else {
    cslog << loglevel(Debug) << "In dbcallback, but no holder was set" << endl;
  }
  return 0;  // Must return 0 or sqlite_exec will abort
}


// sqlite3_exec callback that fills a list of records (vector<DbDataHolder subclass>)
template <typename T>
int dbmulticallback(T* list, int argc, char **argv, char **azColName) {
  using namespace ctlsvc;

  if (list != nullptr) {
    typename T::value_type holder;
    int idx;
    for (idx = 0; idx < argc; ++idx) {
      holder.set(azColName[idx], argv[idx] == nullptr ? "(null)" : argv[idx]);
    }
    list->push_back(holder);
    return 0;
  } else {
    for (int i = 0; i < argc; i++) {
      cslog << loglevel(Debug) << azColName[i] << " = " << argv[i] << endl;
    }
    cslog << loglevel(Debug) << endl;
  }
  return 0;  // Must return 0 or sqlite_exec will abort
}

int dbmultimodules(void *ptr, int argc, char **argv, char **azColName) {
  auto modules = static_cast<ctlsvc::Database::DbModuleList*>(ptr);
  return dbmulticallback(modules, argc, argv, azColName);
}

int dbmultidevices(void *ptr, int argc, char **argv, char **azColName) {
  auto devices = static_cast<ctlsvc::Database::DbDeviceList*>(ptr);
  return dbmulticallback(devices, argc, argv, azColName);
}

}  // namespace


namespace ctlsvc {

// Static member variable instanciated here
const std::string DbDataHolder::NullValueString("(null)");

DbDataHolder::DbDataHolder(const std::string& name)
    : name_(name) {}

bool DbDataHolder::get(const std::string& name, FieldType& value) const {
  auto it = fields_.find(name);
  if (it == fields_.end()) {
    return false;
  }
  value = it->second;
  return true;
}

void DbDataHolder::set(const std::string& name, const FieldType& value) {
  if (name.empty()) {
    return;
  }
  fields_[name] = value;
}


DbDevice::DbDevice()
    : DbDataHolder("device") {}

std::string DbDevice::name() const {
  FieldType val;
  if (get("name", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::uuid() const {
  FieldType val;
  if (get("uuid", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::description() const {
  FieldType val;
  if (get("description", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::module_name() const {
  FieldType val;
  if (get("module_name", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::purpose() const {
  FieldType val;
  if (get("purpose", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::type() const {
  FieldType val;
  if (get("type", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::state() const {
  FieldType val;
  if (get("state", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::addresses() const {
  FieldType val;
  if (get("addresses", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::parameters() const {
  FieldType val;
  if (get("parameters", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::location() const {
  FieldType val;
  if (get("location", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::grouping() const {
  FieldType val;
  if (get("grouping", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbDevice::gps() const {
  FieldType val;
  if (get("gps", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

int DbDevice::seq_order() const {
  FieldType val;
  if (get("seq_order", val)) {
    if (std::holds_alternative<std::string>(val)) {
      // convert to int first
      auto str = std::get<std::string>(val);
      // store it as int so we don't have to convert again
      if (str == NullValueString) {
        val = -1;
      } else {
        val = std::stoi(str);
      }
    }
  }
  if (std::holds_alternative<int>(val)) {
    return std::get<int>(val);
  }
  return -1;
}

std::string DbDevice::ctlsvc_handler() const {
  FieldType val;
  if (get("ctlsvc_handler", val)) {
    return std::get<std::string>(val);
  }
  return "";
}


DbModule::DbModule()
    : DbDataHolder("system_module") {}

std::string DbModule::name() const {
  FieldType val;
  if (get("name", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbModule::role() const {
  FieldType val;
  if (get("role", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbModule::url() const {
  FieldType val;
  if (get("url", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbModule::state() const {
  FieldType val;
  if (get("state", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

std::string DbModule::controlUri() const {
  FieldType val;
  if (get("control_uri", val)) {
    return std::get<std::string>(val);
  }
  return "";
}

int DbModule::controlPort() const {
  FieldType val;
  if (get("control_port", val)) {
    if (std::holds_alternative<std::string>(val)) {
      // convert to int first
      auto str = std::get<std::string>(val);
      // store it as int so we don't have to convert again
      if (str == NullValueString) {
        val = 0;
      } else {
        val = std::stoi(str);
      }
    }
  }
  if (std::holds_alternative<int>(val)) {
    return std::get<int>(val);
  }
  return -1;
}


Database::Database(const std::string& dbfile)
    : dbfile_(dbfile)
    , db_(nullptr) {}

Database::~Database() {}

bool Database::init() {
    if (dbfile_.empty()) {
        cslog << loglevel(Error) << "No automation database specified" << endl;
        return false;
    }
    if (sqlite3_open_v2(dbfile_.c_str(), &db_, SQLITE_OPEN_READONLY, nullptr)) {
        cslog << loglevel(Error) << "Cannot open automation database " << dbfile_
              << " Error: %s" << sqlite3_errmsg(db_) << endl;
        sqlite3_close(db_);
        db_ = nullptr;
        return false;
    }
    cslog << loglevel(Info) << "Automation database opened" << endl;
    return true;
}

void Database::setDbFile(const std::string& dbfile) {
    dbfile_ = dbfile;
}

void Database::uninit() {
    if (db_ != nullptr) {
        sqlite3_close(db_);
        cslog << loglevel(Info) << "Automation database closed" << endl;
    }
}

bool Database::isOpen() const {
  return db_ != nullptr;
}

bool Database::getDevice(const std::string& key, DbDevice& device) const {
  if (devices_.empty()) {
    char* err = nullptr;
    //std::string query("select * from device where uuid = '");
    std::string query("select device.*,device_type.ctlsvc_handler from device inner join device_type on device.type = device_type.name where device.uuid = '");
    query.append(key).append("'");
    cslog << loglevel(Trace) << "Going to run sql query: " << query << endl;
    int ret = sqlite3_exec(db_, query.c_str(), dbcallback, &device, &err);
    cslog << loglevel(Trace) << "Read device from database, returned = " << ret << endl;
    if (err != nullptr) {
      cslog << loglevel(Error) << "Sqlite3 error on select device: " << err << endl;
      sqlite3_free(err);
      return false;
    }
  } else {
    //TODO find device in devices_ list
    return false;
  }

  return true;
}

bool Database::getModule(const std::string& name, DbModule& module) const {
  if (modules_.empty()) {
    char* err = nullptr;
    std::string query("select * from system_module where name = '");
    query.append(name).append("'");
    cslog << loglevel(Trace) << "Going to run sql query: " << query << endl;
    int ret = sqlite3_exec(db_, query.c_str(), dbcallback, &module, &err);
    cslog << loglevel(Trace) << "Read system module from database, returned = " << ret << endl;
    if (err != nullptr) {
      cslog << loglevel(Error) << "Sqlite3 error on select system_module: " << err << endl;
      sqlite3_free(err);
      return false;
    }
  } else {
    //TODO find module in modules_ list
    return false;
  }

  return true;
}

size_t Database::devices(const std::string& type,  Database::DbDeviceList& devices) const {
  // TODO: should devices by cleared here?

  char* err = nullptr;
  std::string query("select * from device");
  if (!type.empty()) {
    query.append(" where type = '").append(type).append("'");
  }
  int ret = sqlite3_exec(db_, query.c_str(), dbmultidevices, &devices, &err);
  cslog << loglevel(Trace) << "Running sql query: " << query << "\n    returned = " << ret << endl;
  if (err != nullptr) {
    cslog << loglevel(Error) << "Sqlite3 error on select system_module: " << err << endl;
    sqlite3_free(err);
  }
  return devices.size();
}

size_t Database::moduleDevices(const std::string& module, const std::string& type, DbDeviceList& devices) const {
  // TODO: should devices by cleared here?

  char* err = nullptr;
  std::string query("select * from device where module_name = '");
  query.append(module).append("'");
  if (!type.empty()) {
    query.append(" AND type = '").append(type).append("'");
  }
  int ret = sqlite3_exec(db_, query.c_str(), dbmultidevices, &devices, &err);
  cslog << loglevel(Trace) << "Running sql query: " << query << "\n    returned = " << ret << endl;
  if (err != nullptr) {
    cslog << loglevel(Error) << "Sqlite3 error on select system_module: " << err << endl;
    sqlite3_free(err);
  }
  return devices.size();
}

const Database::DbModuleList& Database::modules() const {
  if (modules_.empty()) {
    char* err = nullptr;
    sqlite3_exec(db_, "select * from system_module", dbmultimodules, &modules_, &err);
    if (err != nullptr) {
      cslog << loglevel(Error) << "Sqlite3 error on select system_module: " << err << endl;
      sqlite3_free(err);
    }
  }
  return modules_;
}

}  // namespace ctlsvc
