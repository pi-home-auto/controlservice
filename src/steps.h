//
//   Copyright 2021-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "device.h"
#include "program.h"

#include <chrono>
#include <memory>
#include <random>

namespace ctlsvc {

class GpioOutStep : public Step {
public:
    /** Construct a step to output to a GPIO port
     *  @param dev Device instance for GPIO, assumed to already be initialized and open.
     *  @param value 0 = turn off, 1 = turn on, 2 = blink on (on/delay/off),
     *               3 = blink off (off/delay/on).
     *               Note that if the pin is set for PWM or SERVO mode then the value is
     *               written as is. For PWM devices only, when the value is -1 then a random
     *               value will be written to the device and the delay will vary randomly
     *               between 1,000 and 1,000,000 microseconds.
     *  @param delay_ Amount of time to delay.
     */
    template<typename T, typename P>
    GpioOutStep(Device* dev, int value, const std::chrono::duration<T,P>& delay)
        : device_(dev)
        , value_(value)
        , delay_(std::chrono::duration_cast<decltype(delay_)>(delay))
        , randomize_(false) {
        init();
    }
    virtual ~GpioOutStep() = default;
    GpioOutStep(const GpioOutStep& other) = delete;
    GpioOutStep& operator=(const GpioOutStep& other) = delete;

    virtual bool run() override;
    virtual bool set(const std::string& params) override;
    virtual std::string results() const override;
private:    
    void init();

    Device* device_;
    int value_;
    std::chrono::microseconds delay_;
    bool use_pwm_;
    bool use_servo_;
    bool randomize_;
    std::unique_ptr<std::random_device> rand_;
    std::string results_;
};
    
}  // namespace ctlsvc
