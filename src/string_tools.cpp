//
//   Copyright 2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner
//
//  string_tools.cpp
//

#include "string_tools.h"
#include <iomanip>
#include <sstream>
using std::string;

namespace ctlsvc {

std::size_t StringTools::mergeStrings(const StringArray& words, std::string& str,
                                      const std::string& separator) {
    //str.clear();
    for (const auto& word : words) {
        str.append(word).append(separator);
    }
    if (str.size() >= separator.size() && !words.empty()) {
        // Get rid of trailing separator
        str.erase(str.end() - separator.size(), str.end());
    }
    return str.size();
}

std::string StringTools::msecsToString(uint64_t msecs) {
    std::ostringstream oss;
    uint64_t val = msecs / (24 * 60 * 60 * 1000);
    if (val > 0) {
        oss << val << "D";
        msecs %= val * (24 * 60 * 60 * 1000);
    }
    val = msecs / (60 * 60 * 1000);  // Hours
    oss << std::setw(2) << std::setfill('0') << val << ':';
    msecs %= (60 * 60 * 1000);
    val = msecs / (60 * 1000);       // Minutes
    oss << std::setw(2) << std::setfill('0') << val << ':';
    msecs %= (60 * 1000);
    val = msecs / 1000;              // Seconds
    oss << std::setw(2) << std::setfill('0') << val;
    val = msecs % 1000;
    oss << '.' << std::setw(3) << std::setfill('0') << val;
    return oss.str();
}
// Example input:  "1D03:46:39.927"
uint64_t StringTools::stringToMsecs(const std::string& ts) {
    uint64_t ret = 0;
    size_t len = ts.size();
    if (len >= 12 && ts[len - 10] == ':' && ts[len - 7] == ':' && ts[len - 4] == '.') {
        ret = std::stoull(ts.substr(len - 12, 2)) * 60 * 60 * 1000
        + std::stoull(ts.substr(len - 9, 2)) * 60 * 1000
        + std::stoull(ts.substr(len - 6, 2)) * 1000
        + std::stoull(ts.substr(len - 3));
        // add in days
        if (len > 13 && ts[len - 13] == 'D') {
            ret += std::stoull(ts.substr(0, len - 13)) * 24 * 60 * 60 * 1000;
        }
    }
    return ret;
}

#if 0
void tokenize(string s, string del = " ")
{
    int start = 0;
    int end = s.find(del);
    while (end != -1) {
        cout << s.substr(start, end - start) << endl;
        start = end + del.size();
        end = s.find(del, start);
    }
    cout << s.substr(start, end - start);
}
#endif

std::size_t StringTools::splitString(const std::string& str, StringArray& words,
                                     std::vector<size_t>& offsets, const char delimiter) {
    words.clear();
    offsets.clear();
    if (str.empty()) {
        return 0;
    }
    size_t pos = 0;
    size_t prev = 0;
    do {
        pos = str.find(delimiter, prev);
        if (string::npos == pos) {
            words.push_back(str.substr(prev));
            offsets.push_back(prev);
        } else {
            words.push_back(str.substr(prev, pos - prev));
            offsets.push_back(prev);
            prev = pos + 1;
        }
    } while (string::npos != pos);
    return words.size();
}

std::size_t StringTools::splitString(const std::string& str, StringArray& words, const char delimiter) {
    words.clear();
    if (str.empty()) {
        return 0;
    }
    size_t prev = 0;
    size_t pos = 0;
    do {
        pos = str.find(delimiter, prev);
        if (string::npos == pos) {
            words.push_back(str.substr(prev));
        } else {
            words.push_back(str.substr(prev, pos - prev));
            prev = pos + 1;
        }
    } while (string::npos != pos);
    return words.size();
}

}  // namespace ctlsvc
