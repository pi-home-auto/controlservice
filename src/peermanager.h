//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

// Interface that control service implements and listeners call to manage peer connections

#include <string>
#include "peer.h"


namespace ctlsvc {

class PeerManager {
public:
  virtual ~PeerManager() = default;

  virtual void addPeer(Peer* peer, bool isRest = false) = 0;
  virtual bool deletePeer(Peer* peer) = 0;
  /** Find the closest node.js peer */
  virtual Peer* findNodePeer() = 0;
  /** Find a peer based on name */
  virtual Peer* findPeer(const std::string& name) = 0;
  /** Find a peer based on cookie */
  virtual Peer* findPeer(CookieType cookie) = 0;
};

}
