//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "devices/distdevice.h"
#include "devices/minipeerdevice.h"
#include "logger.h"
#include "object_depot.h"
#include "options.h"
#include "string_tools.h"
using std::endl;


namespace ctlsvc {

class ObjectDepotImpl {
public:
  ObjectDepotImpl(const std::string& my_module)
      : module_(my_module) {}
  ~ObjectDepotImpl() = default;

  using MiniPeerMap = std::unordered_map<std::string, std::pair<int, int>>;

  void addMinipeerEntry(const std::string& name, int bus, int addr);
  ObjectDepot::ObjectType find(const std::string& objType, const std::string& objName) const;
  std::vector<std::string> splitUName(const std::string& uname) const;
  ObjectDepot::ObjectStore& store() { return store_; }
  std::string uuidToUName(const std::string& uuid) const;

private:
  std::string module_;
  mutable ObjectDepot::ObjectStore store_;
  mutable MiniPeerMap minipeers_;
};


void ObjectDepotImpl::addMinipeerEntry(const std::string& name, int bus, int addr) {
  minipeers_[name] = { bus, addr };
}

ObjectDepot::ObjectType ObjectDepotImpl::find(const std::string& objType, const std::string& objName) const {
  // TODO lookup optional name resolver based on object type

  std::string uname(objType);
  uname.append(":").append(objName);
  auto it = store_.find(uname);
  if (it != store_.end()) {
    cslog << loglevel(Info) << "Found \"" << uname << "\" in ObjectDepot" << endl;
    return it->second;
  }
  if (objType == "device") {
    StringArray parts;
    auto num = StringTools::splitString(objName, parts, '.');
    if (parts[0] == "dist") {
      if (objName.size() != 6 || objName.back() != '0') {
        cslog << loglevel(Error) << "Invalid dist device name of " << objName << endl;
        return ObjectDepot::ObjectType();
      }
      cslog << loglevel(Info) << "Creating dist.0 device in ObjectDepot" << endl;
      store_[uname] = DeviceObject(new ctlsvc::device::DistDevice(0));
      return store_[uname];
    } else if (parts[0] == "minipeer") {
      if (num != 2) {
        cslog << loglevel(Error) << "Invalid minipeer device name of " << objName << endl;
        return ObjectDepot::ObjectType();
      }
      auto& mp_name = parts[1];
      auto it = minipeers_.find(mp_name);
      if (it == minipeers_.end()) {
        cslog << loglevel(Error) << "Minipeer name " << mp_name << " not found in mp table." << endl;
      } else {
        int bus = it->second.first;
        int addr = it->second.second;
        cslog << loglevel(Info) << "Creating minipeer at i2c " << bus << ":" << addr << endl;
        auto mpdev = new ctlsvc::device::MiniPeerDevice(mp_name, bus, addr);
        mpdev->init();
        store_[uname] = DeviceObject(mpdev);
        return store_[uname];
      }
    }
  }
  return ObjectDepot::ObjectType();
}

std::vector<std::string> ObjectDepotImpl::splitUName(const std::string& uname) const {
  // Split name into module, type and device name. Use defaults for missing parts.
  StringArray parts;
  bool is_valid = false;
  if (!uname.empty()) {
    auto num = StringTools::splitString(uname, parts, ':');
    parts.resize(3);
    switch (num) {
      case 1:
        parts[2] = parts[0];
        parts[1] = "device";
        parts[0].clear();
        is_valid = true;
        break;
      case 2:
        parts[2] = parts[1];
        parts[1] = parts[0];
        parts[0].clear();
        is_valid = true;
        break;
      case 3:
        if (!module_.empty() && module_ == parts[0]) {
          parts[0].clear();
        }
        if (parts[1].empty()) {
          parts[1] = "device";
        }
        is_valid = true;
        break;
      default:
        cslog << loglevel(Error) << "Invalid Universal Name: " << uname
              << ". Found " << num << " components in name." << endl;
    }
    if (is_valid) {
      auto ix = parts[2].find("minipeer.");
      if (ix != std::string::npos) {
        parts[2] = parts[2].substr(ix);
      }
    } else {
      parts.clear();
    }
    cslog << loglevel(Info) << "UN parts: module=" << parts[0] << "  type=" << parts[1]
          << "  name=" << parts[2] << endl;
  }
  return parts;
}

std::string ObjectDepotImpl::uuidToUName(const std::string& uuid) const {
  std::string uname;
  if (!uuid.empty()) {
    auto& db = ControlService::instance()->getDatabase();
    if (db.isOpen()) {
      DbDevice dev;
      if (db.getDevice(uuid, dev)) {
        uname = dev.module_name() + ":device:" + dev.ctlsvc_handler() + "." + dev.addresses();
      }
    }
  }
  return uname;
}


ObjectDepot::ObjectDepot(const std::string& my_module)
    : impl_(std::make_unique<ObjectDepotImpl>(my_module)) {}

ObjectDepot::~ObjectDepot() {}

void ObjectDepot::init(const Options* options) {
  // control."peers.:
  //   "miniPeers": [ "logops" ],
  //   "logops": {
  //     "i2cbus": 1,
  //     "i2caddress": 9
  auto mp_names = options->getConfigArray("control.peers.miniPeers");
  for (const auto& name : mp_names) {
    std::string pbase("control.peers.");
    pbase.append(name).append(".i2c");
    int bus = options->getConfigInt(pbase + "bus");
    int addr = options->getConfigInt(pbase + "address");
    cslog << loglevel(Info) << "Minipeer table: " << name
          << ", bus=" << bus << ", addr=" << addr << endl;
    impl_->addMinipeerEntry(name, bus, addr);
  }
}

ObjectDepot::ObjectType ObjectDepot::find(const std::string& uname) const {
  auto parts = splitUName(uname);
  if (parts.size() == 3 && !parts[0].empty()) {
    return ObjectDepot::ObjectType();
  }
  return impl_->find(parts[1], parts[2]);
}

ObjectDepot::ObjectType ObjectDepot::find(const std::string& objType, const std::string& objName) const {
  return impl_->find(objType, objName);
}

void ObjectDepot::listContents(DebugCallback cb) const {
  for (const auto& [name, obj] : impl_->store()) {
    cb(name, obj);
  }
}

std::vector<std::string> ObjectDepot::splitUName(const std::string& uname) const {
  return impl_->splitUName(uname);
}

std::string ObjectDepot::uuidToUName(const std::string& uuid) const {
  return impl_->uuidToUName(uuid);
}

std::ostream& operator<<(std::ostream& os, const ObjectDepot::ObjectType& obj) {
  struct PrintObj {
    PrintObj(std::ostream& os) : os_(os) {}
    void operator()(const std::monostate&) {
      os_ << "(no object)";
    }
    void operator()(const DeviceObject& dev) {
      os_ << "\"device:" << dev->getName() << "\" mode:" << dev->getMode()
          << " status:" << dev->status();
    }
    void operator()(const HandlerObject& handler) {
      os_ << "(handler)";
    }
    std::ostream& os_;
  };
  PrintObj po(os);
  std::visit(po, obj);

  return os;
}

}  // namespace ctlsvc
