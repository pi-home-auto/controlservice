//
//   Copyright 2020-2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <cassert>
#include <iostream>
#include <sstream>

#include <json/json.h>
#include "ipcmessage.h"
#include "logger.h"
#include "protocol.h"
using std::endl;

namespace ctlsvc {

class IpcMessageImpl {
public:
    IpcMessageImpl(const std::string& id)
        : id_(id)
        , structure_(nullptr)
        , frozen_(false) {
        if (!id.empty()) {
            setId(id);
        }
    }

    void setId(const std::string& id) {
        if (frozen_) {
            return;
        }
        id_ = id;
        json_.clear();
        json_[id_] = Json::Value(Json::objectValue);
    }

public:
    std::string id_;
    Json::Value json_;
    const char* const* structure_;

    bool frozen_;		//!< true if message structure can be changed
};


IpcMessage::IpcMessage(const std::string& id)
    : impl_(std::make_unique<IpcMessageImpl>(id)) {}

IpcMessage::IpcMessage(const char* const* structure)
    : impl_(std::make_unique<IpcMessageImpl>("")) {
    impl_->structure_ = structure;
}

// copy constructor and assignment operator
IpcMessage::IpcMessage(const IpcMessage& other)
    : impl_(std::make_unique<IpcMessageImpl>(other.impl_->id_)) {
    impl_->json_ = other.impl_->json_;
    impl_->structure_ = other.impl_->structure_;
    impl_->frozen_ = other.impl_->frozen_;
}

IpcMessage& IpcMessage::operator=(const IpcMessage& other) {
    impl_->id_ = other.impl_->id_;
    impl_->json_ = other.impl_->json_;
    impl_->structure_ = other.impl_->structure_;
    impl_->frozen_ = other.impl_->frozen_;

    return *this;
}

IpcMessage::~IpcMessage() {}

bool IpcMessage::addMember(const std::string& name, const std::string& value) {
    assert(!getId().empty());

    if (impl_->frozen_) {
        return setMember(name, value);
    }
    impl_->json_[getId()][name] = value;
    return true;
}

bool IpcMessage::addMember(const std::string& name, int value) {
    assert(!getId().empty());

    if (impl_->frozen_) {
        return setMember(name, value);
    }
    impl_->json_[getId()][name] = value;
    return true;
}

bool IpcMessage::addMember(const std::string& name, float value) {
    assert(!getId().empty());

    if (impl_->frozen_) {
        return setMember(name, value);
    }
    impl_->json_[getId()][name] = value;
    return true;
}

bool IpcMessage::addMember(const IpcMessage& subMessage) {
    assert(!getId().empty());
    std::string name = subMessage.getId();
    if (impl_->frozen_) {
        return setMember(name, subMessage);
    }
    impl_->json_[getId()][name] = subMessage.impl_->json_[name];
    return true;
}

void IpcMessage::clear() {
    if (!impl_->frozen_) {
        impl_->json_.clear();
    }
}

void IpcMessage::freeze() {
    impl_->frozen_ = true;
}

const std::string& IpcMessage::getId() const {
    return impl_->id_;
}

std::string IpcMessage::getMember(const std::string& name) const {
    std::string val = impl_->json_[getId()][name].asString();
    return val;
}

float IpcMessage::getFloatMember(const std::string& name) const {
    float val = impl_->json_[getId()][name].asFloat();
    return val;
}

int IpcMessage::getIntMember(const std::string& name) const {
    int val = impl_->json_[getId()][name].asInt();
    return val;
}

std::string IpcMessage::getStatus() const {
    // TODO fix it
    assert(!getId().empty());
    if (!impl_->json_[getId()].isMember("status")) {
        return "(unknown)";
    }
    return getMember("status");
}

bool IpcMessage::isMember(const std::string& name) const {
    return impl_->json_[getId()].isMember(name);
}

void IpcMessage::setId(const std::string& id) {
    impl_->setId(id);
}

bool IpcMessage::setMember(const std::string& name, const std::string& value) {
    assert(!getId().empty());
    if (!impl_->json_[getId()].isMember(name)) {
        return false;
    }
    impl_->json_[getId()][name] = value;
    return true;
}

bool IpcMessage::setMember(const std::string& name, int value) {
    assert(!getId().empty());
    if (!impl_->json_[getId()].isMember(name)) {
        return false;
    }
    impl_->json_[getId()][name] = value;
    return true;
}

bool IpcMessage::setMember(const std::string& name, float value) {
    assert(!getId().empty());
    if (!impl_->json_[getId()].isMember(name)) {
        return false;
    }
    impl_->json_[getId()][name] = value;
    return true;
}

bool IpcMessage::setMember(const std::string& name, const IpcMessage& subMessage) {
    assert(!getId().empty());
    if (!impl_->json_[getId()].isMember(name)) {
        return false;
    }
    impl_->json_[getId()][name] = subMessage.impl_->json_[name];
    return true;
}

IpcMessage* IpcMessage::factory(const std::string& jsonStr) {
    if (jsonStr.empty()) {
        cslog << loglevel(Error) << "Cannot create IPC message from empty string" << endl;
        return nullptr;
    }
    std::istringstream iss(jsonStr);
    Json::Value jval;
    Json::CharReaderBuilder builder;
    std::string errs;
    builder.strictMode(&builder.settings_);
    if (!parseFromStream(builder, iss, &jval, &errs)) {
        cslog << loglevel(Error) << "Unable to parse IPC message, errors: "
              << errs << endl;
        return nullptr;
    }
    // read ident and setId
    if (impl_->json_.size() != 1) {
        cslog << loglevel(Error) << "Unable to parse: " << jsonStr << endl;
        return nullptr;
    }
    Json::Value::Members members = impl_->json_.getMemberNames();
    assert(members.size() == 1);

    //TODO make a private constructor that takes id and json value
    IpcMessage* msg = new IpcMessage;
    msg->impl_->id_ = members[0];
    msg->impl_->json_ = jval;
    return msg;
}

bool IpcMessage::fromString(const std::string& jsonStr) {
    if (jsonStr.empty()) {
        cslog << loglevel(Error) << "Cannot create message from empty string" << endl;
        return false;
    }
    impl_->json_.clear();
    std::istringstream iss(jsonStr);
    Json::CharReaderBuilder builder;
    std::string errs;
    builder.strictMode(&builder.settings_);
    if (!parseFromStream(builder, iss, &impl_->json_, &errs)) {
        cslog << loglevel(Error) << "Unable to parse IPC message, errors: "
              << errs << endl;
        return false;
    }
    // read ident and setId
    assert(impl_->json_.size() == 1);
    Json::Value::Members members = impl_->json_.getMemberNames();
    assert(members.size() == 1);
    //cout << "fromString got id=" << members[0] << endl;
    impl_->id_ = members[0];

    return true;
}

std::string IpcMessage::toString() const {
    std::string ret = impl_->json_.toStyledString() + COMMAND_ENDL;
    return ret;
}

std::string IpcMessage::toShortString() const {
    Json::StreamWriterBuilder builder;
    builder["indentation"] = "    ";
    std::string ret = Json::writeString(builder, impl_->json_);
    return ret;
}

///////////////////////////////////////////////////////////////////////////////

const std::string IpcControl::ID("control");
const std::string IpcControl::HANDLER("handler");
const std::string IpcControl::METHOD("method");
const std::string IpcControl::MODULE("module");

static const char* const controlStructure[] = {
  NULL,
  "control:",
  "handler",
  "method",
  "module",
  "param1,O",
  "param2,O",
  NULL
};

IpcControl::IpcControl()
    : IpcMessage(controlStructure) {
    setId(ID);
}

IpcControl::IpcControl(const char* const* structure)
    : IpcMessage(structure) {
    setId(ID);
}

IpcControl::IpcControl(const std::string& handler, const std::string& method) {
    setId(ID);
    addMember(HANDLER, handler);
    addMember(METHOD, method);
}

IpcControlAlert IpcControl::respond(IpcControl::Level level,
                                    IpcControl::Status status,
                                    const std::string& statusText) {
    IpcControlAlert alert(getMember(HANDLER), getMember(METHOD), level,
                          status, statusText);
    return alert;
}

static const char* const controlAlertStructure[] =
{ controlStructure[0],
  "controlalert:",
  //TODO "level",
  "status",
  //TODO "statustext,O",
  NULL
};

const std::string IpcControlAlert::ID("controlalert");
const std::string IpcControlAlert::LEVEL("level");
const std::string IpcControlAlert::STATUS("status");
const std::string IpcControlAlert::STATUSTEXT("statustext");

IpcControlAlert::IpcControlAlert(const std::string& handler, const std::string& method,
                                 IpcControl::Level level,
                                 IpcControl::Status status,
                                 const std::string& statusText)
    : IpcControl(controlAlertStructure) {
    setId(ID);
    addMember(HANDLER, handler);
    addMember(METHOD, method);
    addMember(LEVEL, level);
    addMember(STATUS, status);
    if (!statusText.empty()) addMember(STATUSTEXT, statusText);
}

///////////////////////////////////////////////////////////////////////////////

const std::string IpcEvent::ID("event");
const std::string IpcEvent::NAME("name");

///////////////////////////////////////////////////////////////////////////////

const std::string IpcGoodbye::ID("goodbye");

const std::string IpcGreeting::ID("greeting");
const std::string IpcGreeting::VERSION("version");
const std::string IpcGreeting::TYPE("type");
const std::string IpcGreeting::NAME("name");
const std::string IpcGreeting::ROLE("role");
const std::string IpcGreeting::MAIN_GROUP("mainGroup");

IpcGreeting::IpcGreeting()
    : name_("") {
    setId(ID);
    addMember(VERSION, CS_VERSION);
    addMember(TYPE, "control");
}

IpcGreeting::IpcGreeting(const std::string& name, const std::string& role,
                         const std::string& mainGroup)
    : name_(name) {
    setId(ID);
    addMember(VERSION, CS_VERSION);
    addMember(TYPE, "control");
    addMember(NAME, name_);
    addMember(ROLE, role);
    addMember(MAIN_GROUP, mainGroup);
}

const std::string& IpcGreeting::getName() const {
    if (name_.empty()) {
        name_ = getMember(NAME);
    }
    return name_;
}

const std::string IpcReply::ID("reply");
const std::string IpcReply::STATUS("status");
const std::string IpcReply::RESULT("result");

IpcReply::IpcReply() {
    setId(ID);
}

IpcReply::IpcReply(IpcControl::Status status, const std::string& result) {
    setId(ID);
    addMember(STATUS, status);
    addMember(RESULT, result);
}

void IpcReply::setReply(IpcControl::Status status, const std::string& result) {
    addMember(STATUS, status);
    addMember(RESULT, result);
}

const std::string IpcShutdown::ID("shutdown");
const std::string IpcShutdown::WHEN("when");

IpcShutdown::IpcShutdown(const std::string& when) {
    setId(ID);
    addMember(WHEN, when);
}

const std::string IpcStatistics::ID("statistics");
const std::string IpcStatistics::DETAIL("detail");

IpcStatistics::IpcStatistics(const std::string& detail) {
    setId(ID);
    if (!detail.empty()) {
        addMember(DETAIL, detail);
    }
}

//TODO custom extractor for IpcMessages
/*
std::istream& operator>>(std::istream& is, IpcMessage& message)
{
    std::string strbuf;
    client->read(strbuf, COMMAND_END);
    std::istream::sentry s(is);
    if (s) while (is.good()) {
      char c = is.get();
      if (std::isspace(c,is.getloc())) break;
      if (std::isdigit(c,is.getloc())) tel.digits+=c;
    }
    return is;
}
*/

}  // namespace ctlsvc
