//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <sockstr/Socket.h>

#include "controlservice.h"
#include "ipcmessage.h"
#include "listener.h"
#include "logger.h"
#include "options.h"
#include "peer.h"
#include "peermanager.h"
#include "protocol.h"
#include "transientpeer.h"

// #include <gperftools/profiler.h>

#include <algorithm>
#include <atomic>
#include <cerrno>
#include <cstring>
#include <thread>

using namespace sockstr;
using std::endl;

namespace {

ctlsvc::PeerType strToPeerType(const std::string& type) {
  using namespace ctlsvc;
  PeerType pt = PeerType::None;
  if (type == "control") {
    pt = PeerType::Control;
  } else if (type == "rest") {
    pt = PeerType::Rest;
  } else if (type == "userApp") {
    pt = PeerType::UserApp;
  } else if (type == "tester") {
    pt = PeerType::Tester;
  } else if (type == "transient") {
    pt = PeerType::Transient;
  }
  return pt;
}

}  // namespace


namespace ctlsvc {

class ListenerImpl {
public:
    ListenerImpl()
        : stream_()
        , greeting_{"Control Service", CS_ROLE_BCS}
        , keepRunning_(true) {}

    ~ListenerImpl() {
        if (stream_.is_open()) stream_.close();
    }

    /** Block listening to the stream for connections.
     *  Spawns PeerThreader threads based on greeting exchanged.
     */
    bool handle();

    sockstr::Socket stream_;
    /** IPC message used to exchange greetings on a new connection.
     *  It is cached here because it is used frequently.   */
    IpcGreeting greeting_;
    std::atomic_bool keepRunning_;
    int port_;
    PeerManager* peerMan_;
    std::thread thread_;
};

bool ListenerImpl::handle() {
  while (keepRunning_ && stream_.good() && stream_.is_open()) {
    cslog << "Listen for peers" << endl;
    Stream* client = stream_.listen();
    if (client && client->good() && client->is_open()) {
      // exchange greetings with peer
      std::string strbuf;
      IpcGreeting peerGreeting;
      client->read(strbuf, COMMAND_END);
      if (peerGreeting.fromString(strbuf)) {
        std::string peerIpAddr = std::string((const char *) *client);
        auto peerName = peerGreeting.getName();
        std::string peerTypeStr = peerGreeting.getMember(IpcGreeting::TYPE);
        std::string peerRole = peerGreeting.getMember(IpcGreeting::ROLE);
        std::string peerVersion = peerGreeting.getMember(IpcGreeting::VERSION);
        cslog << loglevel(Info) << "Peer greeting from " << peerName
              << ", role=" << peerRole
              << ", version=" << peerVersion
              << ", type=" << peerTypeStr
              << ", ipaddress=" << peerIpAddr << endl;
        if (peerVersion != std::string(CS_VERSION)) {
          cslog << loglevel(Warning) << "Version mismatch: running " << CS_VERSION
                << " and client has " << peerVersion << endl;
        }
        auto peerType = strToPeerType(peerTypeStr);
        *client << greeting_.toString() << endl;

        if (peerType == PeerType::Control && peerRole == "shutdown") {
          // ProfilerStop();
          cslog << "Shutdown received -- listener exiting" << endl;
          keepRunning_ = false;
          ControlService* cs = ControlService::instance();
          cs->stop();
          break;
        }
#if 0
        // Turn off keepalive
        int koFlag = 0;
        ((Socket *)client)->setSockOpt(SO_KEEPALIVE, &koFlag, sizeof(koFlag));
#endif
        Peer* peer;
        if (peerType == PeerType::Transient) {
          peer = new TransientPeer(peerName, client);
        } else {
          peer = new Peer(peerName, client);
        }
        peer->start();
        //TODO there can only be 1 peer of type == "rest"
        peerMan_->addPeer(peer, (peerType == PeerType::Rest));
      } else {
        cslog << loglevel(Error) << "Error: cannot parse greeting message from peer: "
              << peerGreeting.getStatus() << endl;
      }
    }
  }
  cslog << loglevel(Info) << "Listener handle loop exited." << endl;
  return true;
}

Listener::Listener()
    : impl_(std::make_unique<ListenerImpl>()) {}

Listener::~Listener() {
  stop();
}

bool Listener::init(int port, PeerManager* pm) {
  impl_->port_ = port;
  SocketAddr saddr(port);
  if (!impl_->stream_.open(saddr, Socket::modeReadWrite)) {
    cslog << loglevel(Error) << "Error opening server socket: "
          << errno << ": " << strerror(errno) << endl;
    return false;
  }

  cslog << "Server listening on port " << impl_->port_ << endl;
  impl_->peerMan_ = pm;
  return true;
}

void Listener::run(bool background /*= true*/) {
  if (background) {
    impl_->thread_ = std::thread(&ListenerImpl::handle, impl_.get());
  } else {
    bool lstat = impl_->handle();
    cslog << "Listener exited with " << std::boolalpha << lstat << endl;
    stop();
  }
}

PeerManager* Listener::peerManager() {
  return impl_->peerMan_;
}

const PeerManager* Listener::peerManager() const {
  return impl_->peerMan_;
}

void Listener::setPeerManager(PeerManager* pm) {
  impl_->peerMan_ = pm;
}

void Listener::stop() {
  cslog << "Listener::stop called" << endl;
  impl_->keepRunning_ = false;
  if (impl_->stream_.is_open()) impl_->stream_.close();
  if (impl_->thread_.joinable()) {
    impl_->thread_.join();
  }
}

}  // namespace ctlsvc
