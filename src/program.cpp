//
//   Copyright 2021-2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "logger.h"
#include "program.h"
#include <algorithm>
#include <iterator>
using std::endl;

namespace ctlsvc {

ProgramStepper::ProgramStepper(const ProgramStepper& other)
    : val_(other.val_) {}

ProgramStepper ProgramStepper::operator=(const ProgramStepper& other) {
    val_ = other.val_;
    return *this;
}

bool ProgramStepper::operator==(const ProgramStepper& other) const {
    return val_ == other.val_;
}

bool ProgramStepper::operator!=(const ProgramStepper& other) const {
    return val_ != other.val_;
}

ProgramStepper& ProgramStepper::operator++() {
    ++val_;
    return *this;
}

ProgramStepper ProgramStepper::operator++(int) {
    ProgramStepper prev(*this);
    val_++;
    return prev;
}

Step* ProgramStepper::operator*() {
    return *val_;
}


Program::Program(const std::string& name)
    : name_(name) {
    cslog << "Program construct, name=" << name_ << endl;
}

Program::~Program() {
    cslog << "Program destructor, name=" << name_ << endl;
    clear();
}

const std::string& Program::name() const {
    return name_;
}

size_t Program::size() const {
    return steps_.size();
}

bool Program::addStep(Step* step) {
    steps_.push_back(step);
    return true;
}

bool Program::addSteps(const std::vector<Step *>& steps) {
    if (steps.empty()) {
        return false;
    }
    std::back_insert_iterator<std::vector<Step *>> it(steps_);
    std::copy(steps.begin(), steps.end(), it);
    return true;
}

void Program::clear() {
    for (auto step : steps_) {
        delete step;
    }
    steps_.clear();
}

Program::iterator Program::execute(iterator it, bool single_step) {
    // cslog << loglevel(Debug) << "- Run program " << name_ << std::endl;
    while (it.val_ != steps_.end()) {
        auto step = *it;
        auto ok = step->run();
        if (!ok) {
            cslog << loglevel(Error) << "  - Program " << name_ << " Step bad results: "
                  << step->results() << std::endl;
        }
        ++it;
        if (single_step) {
            break;
        }
    }
    return it;
}

Program::iterator Program::begin() {
    iterator it;
    it.val_ = steps_.begin();
    return it;
}

Program::iterator Program::end() {
    iterator it;
    it.val_ = steps_.end();
    return it;
}

}  // namespace ctlsvc
