//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "driver.h"

#include <algorithm>
#include <cstring>
#include <unordered_map>

using std::byte;

namespace ctlsvc {
namespace driver {


DriverBuffer::DriverBuffer()
    : buf_()
    , size_(0) {}

DriverBuffer::DriverBuffer(int size)
    : buf_(std::make_unique<byte[]>(size))
    , size_(size) {}

DriverBuffer::DriverBuffer(const byte* data, BufSize size)
    : buf_(std::make_unique<byte[]>(size))
    , size_(size) {
  std::copy(data, data + size, buf_.get());
}

DriverBuffer::DriverBuffer(const std::vector<byte>& data)
    : buf_(std::make_unique<byte[]>(data.size()))
    , size_(data.size()) {
  std::copy(data.begin(), data.end(), buf_.get());
}

DriverBuffer::DriverBuffer(const std::string& str)
    : buf_(std::make_unique<byte[]>(str.size()))
    , size_(str.size()) {
  memcpy(buf_.get(), str.c_str(), str.size());
}


DriverBuffer::~DriverBuffer() {}

byte* DriverBuffer::data() {
  return buf_.get();
}

const byte* DriverBuffer::data() const {
  return buf_.get();
}

BufSize DriverBuffer::size() const {
  return size_;
}

void DriverBuffer::writeInt(int value) {
  if (size_ != sizeof(int)) {
    buf_ = std::make_unique<byte[]>(sizeof(int));
    size_ = sizeof(int);
  }
  std::copy((std::byte *)&value, (std::byte *)&value + sizeof(int), buf_.get());
}

void DriverBuffer::writeString(const std::string& str) {
    if (str.empty()) {
        size_ = 0;
        buf_.release();
        return;
    }
    if (size_ < str.size()) {
        buf_ = std::make_unique<byte[]>(str.size());
    }
    memcpy(buf_.get(), str.c_str(), str.size());
    size_ = str.size();
}

}  // namespace driver
}  // namespace ctlsvc
