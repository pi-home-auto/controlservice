//
//   Copyright 2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner
//
//  string_tools.h
//

#pragma once

#include <string>
#include <vector>

namespace ctlsvc {

using StringArray = std::vector<std::string>;

/** A collection of string utilities for conversions, manipulations, etc. */
//TODO just make this a namespace instead of a class
class StringTools {
public:
  static std::size_t mergeStrings(const StringArray& words, std::string& str,
                                  const std::string& separator = std::string()); 
  static std::string msecsToString(uint64_t msecs);
  static uint64_t stringToMsecs(const std::string& ts);
  static std::size_t splitString(const std::string& str, StringArray& words, const char delimiter = ' ');
  static std::size_t splitString(const std::string& str, StringArray& words, std::vector<size_t>& offsets,
                                 const char delimiter = ' ');
};

}  // namespace ctlsvc
