//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <memory>
#include <string>
#include <unordered_map>

namespace ctlsvc {
// Forward references
class IpcMessage;
class IpcReply;
class Options;
class Peer;
class Statistics;

/**
 * This class handles requests from IPC.
 * Many handlers will also spawn threads from their #init method to do polling
 * and asynchronous events.
 */
class Handler {
protected:
  /** Construct a handler with a given name. */
  Handler(const std::string& name);
public:
  /** Destruct the handler. */
  virtual ~Handler();

  virtual void deinit() { }
  virtual bool init(const Options*) { return true; }
  /** This method is used to set up or configure items, which leaves init() to just
   *  initialize this handler and any devices, drivers (or other resources) used.
   */
  virtual bool setup() { return true; }
  /** This method is called when an IPC message is routed
   *  to a handler.
   *  @param message Message that is routed from a peer to this handler
   *  @param reply Message returned from request
   *  @param peer Pointer to the peer that made the request
   *  @return True if successful, otherwise false
   */
  virtual bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) = 0;
  /** Forward the IPC message to the node.js server
   *  @param msg IPC message to send
   *  @return True if successful, false if no node server is running or an error
   *          occurred writing to the node peer's stream.
   */
  bool sendToNode(const IpcMessage* msg);

  // getters and setters
  /** Get the name of the handler */
  std::string getName()  const { return name_; }
  /** Get the optional handler statistics.
   *  It is up to the sub-class handler implementation to initalize and use
   *  statistics #stats_.
   *  @return pointer to the statistics packet, or nullptr if not using statistics.
   */
  Statistics* getStats() const { return stats_.get(); }

private:
  /** Handler name */
  const std::string name_;

protected:
  /** Statistics that sub-class can instanciate and use, or leave nullptr. */
  std::unique_ptr<Statistics> stats_;
};

/** Type used for handler dispatching table */
using HandlerMap = std::unordered_map<std::string, Handler*>;

}  // namespace ctlsvc
