//
//   Copyright 2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "program.h"

//#include <cstdint>
#include <memory>

namespace ctlsvc {

class SequencerImpl;

using LevelNum = uint32_t;

struct SequenceEntry {
    /** Type that defines the behavior for running a program. */
    enum EntryType {
        Off,  //!< Program is skipped while running sequence
        On,   //!< Program is run as part of sequence
        RunOnce,   //!< Run program once and then remove it from sequence
        RunOnceKeep, //!< Run program once and then set its type to Off (but keep in sequence)
        Loop         //!< Run program in a loop
    };

    SequenceEntry()
        : level(0)
        , repeat(0)
        , type(Off) {}
    ~SequenceEntry() = default;
    SequenceEntry(std::unique_ptr<Program>&& prog, LevelNum lvl, int rpt, EntryType typ)
        : program(std::move(prog))
        , level(lvl)
        , repeat(rpt)
        , type(typ) {}
    SequenceEntry(SequenceEntry&& other)
        : program(std::move(other.program))
        , level(other.level)
        , repeat(other.repeat)
        , type(other.type) {}

    std::unique_ptr<Program> program;
    LevelNum level;
    int repeat;
    EntryType type;
};

/** The Sequencer runs programs that are organized in sequences. There are multiple
 *  sequences, each with a different level number. The programs within a
 *  sequence are run, one after the other. The program sequence on the highest
 *  numbered level are run first, then the next highest level, down to level 1.
 *
 *  > Note that if any program is set to loop indefinitely, then no other program
 *  > or sequence will be able to run.
 *
 *  The sequencer allows adding a default program that will run when no other
 *  program is in the sequences. There is also an option to have a background
 *  program running on a separate thread.
 */
class Sequencer {
public:
    /** Maximum number of levels of program sequences. */
    static constexpr size_t kMaxLevelNum = 12U;

    // TODO Add a name since we have multiple sequencers running
    Sequencer();
    ~Sequencer();

    /** Add a program that plays in the background on a separate thread.
     *  For now, only one background program is allowed, thus adding a
     *  background program will replace the current background program.
     *
     *  TODO? The background thread will only run when there is an active
     *  background program.
     *  @param program Program to add as background program.
     *  @return true if the program was successfully added, otherwise false.
     */
    bool addBackgroundProgram(std::unique_ptr<Program>&& program);
    /** Add a default program to play when nothing else is playing.
     *  @param program Program to add as default program.
     *  @return true if the program was successfully added, otherwise false.
     */
    bool addDefaultProgram(std::unique_ptr<Program>&& program);
    /** Add a program to the sequencer
     *  TODO When running, need to be careful not to invalidate the iterators by adding
     *       or removing entries in the level currently being played. Also, need to take
     *       care when adding or removing a whole level.
     *  @param entry   The sequence entry to add
     *  @return true if the program was successfully added, otherwise false.
     */
    bool addProgram(SequenceEntry& entry);
    /** Add a program to the sequencer
     *  @param program Program to add
     *  @param level   Which level to add program. The highest level is played until done, then
     *                 the next level is played, and so forth until level 0 is reached.
     *  @param repeat  Number of times to play program: -1 = loop, 0 = off, 1 = once, 2 = twice, ...
     *  @param type    The type of this sequence entry
     *  @return true if the program was successfully added, otherwise false.
     */
    bool addProgram(std::unique_ptr<Program>&& program, LevelNum level, int repeat = 1,
                    SequenceEntry::EntryType type = SequenceEntry::On);
    /** Get a program by name
     *  @param name Name of program
     *  @return A reference to the program, or to an empty program if the program requested was not found
     */
    const SequenceEntry& getProgram(const std::string& name) const;

    bool isBackgroundRunning() const;
    /** Check if the sequencer thread is waiting for a runnable program to run.
     *  @return true if the sequencer (thread) is polling, otherwise false.
     */
    bool isPolling() const;
    /** Check if the sequencer is actively running programs.
     *  @return true if the sequencer (thread) is running programs, otherwise false.
     */
    bool isRunning() const;

    bool modifyProgram(const std::string& name, LevelNum level, int repeat = 1,
                       SequenceEntry::EntryType type = SequenceEntry::On);

    bool removeBackgroundProgram();
    /** Remove a program from the sequencer. A program with the given name is searched for in each level,
     *  starting with level 0. The first program with that name is removed.
     *  @param name  Name of program to delete.
     *  @return true if the program was removed successfully.
     */
    bool removeProgram(const std::string& name);
    /** Remove a program from the sequencer. A program with the given name is searched for in the specified
     *  level. The first program with that name is removed.
     *  @param name  Name of program to delete.
     *  @param level Level to search for the program to delete. 
     *  @return true if the program was removed successfully.
     */
    bool removeProgram(const std::string& name, LevelNum level);
    /** Start running the sequencer
     *  @param spawn_thread If true then the sequencer is run in its own thread. If false then the
     *                      sequencer runs in the caller's thread (thus a blocking call).
     *  @param one_shot If true then the sequence of programs in run only once, otherwise it is run in a loop.
     *  @return True if the sequencer is able to run.
     */
    bool run(bool spawn_thread = false, bool one_shot = false);
    /** Stop the sequencer.
     *  @param immediate If false then the sequencer will be set to stop after the current run through the sequence.
     *                   If true, then the sequencer is stopped as soon as possible.
     */
    void stop(bool immediate = false);

private:
    std::unique_ptr<SequencerImpl> impl_;
};

}  // namespace ctlsvc
