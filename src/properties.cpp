//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "properties.h"

#include <algorithm>
#include <cstring>
#include <unordered_map>

using std::byte;

namespace ctlsvc {


class PropertiesImpl {
public:
    using PropMap = std::unordered_map<std::string, PropertyVar>;

    PropertyVar& emptyVar() { return emptyVar_; }
    PropMap& props() { return props_; }
    const PropMap& props() const { return props_; }

private:
    PropMap props_;
    PropertyVar emptyVar_;
};


Properties::Properties()
    : impl_(std::make_unique<PropertiesImpl>()) {}

Properties::~Properties() {}

PropertyVar Properties::get(const std::string& name) const {
    auto it = impl_->props().find(name);
    if (it == impl_->props().end()) {
        return {};
    }
    return it->second;
}

bool Properties::getAsInt(const std::string& name, int& value) const {
    auto prop = get(name);
    if (std::holds_alternative<int>(prop)) {
        value = std::get<int>(prop);
        return true;
    }
    return false;
}

std::string Properties::getString(const std::string& name) const {
    auto prop = get(name);
    if (std::holds_alternative<std::string>(prop)) {
        return std::get<std::string>(prop);
    }
    return "";
}

bool Properties::remove(const std::string& name) {
    auto it = impl_->props().find(name);
    if (it == impl_->props().end()) {
        return false;
    }
    impl_->props().erase(it);
    return true;
}

bool Properties::set(const std::string& name, const PropertyVar& var) {
    impl_->props()[name] = var;
    return true;
}

size_t Properties::size() const {
    return impl_->props().size();
}

PropertyVar& Properties::operator[](const std::string& name) {
    auto it = impl_->props().find(name);
    if (it == impl_->props().end()) {
        return impl_->emptyVar();
    }
    return it->second;
}

const PropertyVar& Properties::operator[](const std::string& name) const {
    return const_cast<Properties *>(this)->operator[](name);
}

std::vector<std::string> Properties::propertyNames() const {
    std::vector<std::string> names;
    std::for_each(impl_->props().begin(), impl_->props().end(),
                  [&names](const std::pair<std::string, PropertyVar>& entry) {
                      names.push_back(entry.first);
                  });
    return names;
}

size_t Properties::visit(std::function<bool(const std::string&, PropertyVar&)> visitor) const {
    size_t count = 0;
    for (auto& [name, prop] : impl_->props()) {
        ++count;
        if (!visitor(name, prop)) {
            break;
        }
    }
    return count;
}

}  // namespace ctlsvc
