//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "eval/evaluator.h"
#include "database.h"
#include "forker.h"
#include "handler.h"
#include "options.h"
#include "peermanager.h"

#include <chrono>
#include <map>
#include <memory>
#include <vector>

namespace ctlsvc {
// Forward references
class IpcStatistics;
class Listener;
class ObjectDepot;
class Peer;

/**
 *  Main controller class for the control-service program.
 *  It has one or more listener threads that manage peer connections.
 */
class ControlService : public PeerManager {
public:
    static ControlService* instance();
    virtual ~ControlService();

    virtual bool init(std::unique_ptr<Options> options);
    bool isActive() const;
    virtual void run();
    virtual void stop(bool gracefully = true);
    virtual void wait();

    void addHandler(Handler* handler);
    void setForker(Forker* forker);

    // Implement PeerManager interface
    virtual void addPeer(Peer* peer, bool isRest = false) override;
    virtual bool deletePeer(Peer* peer) override;
    virtual Peer* findNodePeer() override;
    virtual Peer* findPeer(const std::string& name) override;
    virtual Peer* findPeer(CookieType cookie) override;

    //TODO maybe add these to PeerManager interface?
    const std::vector<Peer*>& getPeers() const { return peers_; }
    std::vector<Peer*>& getPeers() { return peers_; }

    const Database& getDatabase() const { return database_; }
    static eval::Evaluator& getEvaluator();
    static Handler* getHandler(const std::string& name);
    const HandlerMap& getHandlers() const { return handlers_; }
    const ObjectDepot* getObjectDepot() const { return depot_.get(); }
    const Options* getOptions() const { return options_.get(); }

    void getStats(IpcStatistics& stats);

    void pushEvent(std::shared_ptr<eval::Event> event);
    Forker* forker() { return forker_; }
    const Forker* forker() const { return forker_; }

protected:
    // this is a singleton-only class
    ControlService();
    static ControlService* instance_;

    // Unique ID for a peer connection
    static CookieType peerCookie_;

private:
    // Disable copy constructor and assignment operator
    ControlService(const ControlService&);
    ControlService& operator=(const ControlService&);

protected:
    Forker* forker_;  // TODO unique_ptr
    std::unique_ptr<Listener> listener_;
    std::unique_ptr<Options> options_;
    std::unique_ptr<ObjectDepot> depot_;
    Database database_;

    /** List of all peers.  Used for status and shutdown only */
    std::vector<Peer*> peers_;
    Peer* restPeer_;

    /** All handlers are registered here */
    HandlerMap handlers_;
    eval::Evaluator evaluator_;
};


class BaseControlService : public ControlService {
private:
  BaseControlService();
public:
  virtual ~BaseControlService() = default;
  static ControlService* instance();

  virtual void run() override;
};

#if 0
class SubControlService : public ControlService {
private:
  SubControlService();
public:
  virtual ~SubControlService();
  static ControlService* instance();

  virtual bool init(std::unique_ptr<Options> options) override;
  virtual void run() override;
  virtual void stop(bool gracefully = true) override;

  virtual void addBcsPeer(Peer* peer) override { bcs_ = peer; }

private:
  std::unique_ptr<SubClient> subClient_;
  Peer* bcs_;
};
#endif

}  // namespace ctlsvc
