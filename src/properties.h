//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <functional>
#include <memory>
#include <string>
#include <variant>
#include <vector>

namespace ctlsvc {

// Forward reference
class PropertiesImpl;
namespace eval {
class Event;
}

/** A property variable that holds the value of a property. */
using PropertyVar = std::variant<std::monostate, bool, int, float, std::string, std::vector<bool>,
                                 std::vector<int>, std::vector<float>, std::vector<std::string>,
                                 std::shared_ptr<eval::Event>>;

/** Container that holds a set of named properties. */
class Properties {
public:
    Properties();
    ~Properties();

    PropertyVar get(const std::string& name) const;
    bool getAsInt(const std::string& name, int& value) const;
    std::string getString(const std::string& name) const;
    bool remove(const std::string& name);
    bool set(const std::string& name, const PropertyVar& var);
    size_t size() const;
    PropertyVar& operator[](const std::string& name);
    const PropertyVar& operator[](const std::string& name) const;
    std::vector<std::string> propertyNames() const;
    /** Call the visitor function with each property in sequence.
     *  If the visitor returns true then the remaining properties are also visited, but if the
     *  visitor returns false, no further properties are visited.
     *  @param visitor Function that will be called with each property.
     *  @return The number of properties visited.
     */
    size_t visit(std::function<bool(const std::string&, PropertyVar&)> visitor) const;

private:
    std::unique_ptr<PropertiesImpl> impl_;
};

}  // namespace ctlsvc
