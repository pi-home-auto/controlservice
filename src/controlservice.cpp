//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

// Main control class

#include "controlservice.h"
#include "eval/actions.h"
#include "eval/context.h"
#include "eval/eventfactory.h"
#include "eval/events.h"
#include "eval/rulebase.h"
#include "handler.h"
#include "handlerloader.h"
#include "ipcmessage.h"
#include "listener.h"
#include "logger.h"
#include "object_depot.h"
#include "peer.h"
#include "protocol.h"
#include "statistics.h"
#include "subclient.h"

// #include <gperftools/profiler.h>
#include <algorithm>
#include <cassert>
#include <sys/sysinfo.h>
using std::endl;

namespace ctlsvc {

ControlService* ControlService::instance_ = nullptr;
CookieType ControlService::peerCookie_ = 111;

ControlService::ControlService()
    : forker_(nullptr)
    , restPeer_(nullptr) {}

ControlService::~ControlService() {
    cslog << loglevel(Info) << "ControlService destructor called" << endl;
    for (auto [_, handler] : handlers_) {
        // TODO handler->deinit() ?
        delete handler;
    }
    if (forker_ != nullptr) {
        forker_->uninit();
    }
    instance_ = nullptr;
}

ControlService*
ControlService::instance() {
    return instance_;
}

bool ControlService::init(std::unique_ptr<Options> options) {
    options_ = std::move(options);
    depot_ = std::make_unique<ObjectDepot>(options_->getModuleName());
    depot_->init(options_.get());

    database_.setDbFile(options_->getDbFilename());
    database_.init();
    
    auto modules = database_.modules();
    cslog << loglevel(Info) << " DB: there are " << modules.size() << " modules in database." << endl;
#if 0
    for (auto& module : modules) {
      cslog << " - Module " << module.name() << " role=" << module.role() << endl;
    }
#endif
    const auto& handlerList = options_->getBootupHandlers();
    for (const auto& handlerName : handlerList) {
        cslog << loglevel(Info) << "ControlService load handler " << handlerName << endl;
        Handler* handler = HandlerLoader::load(handlerName);
        if (handler != nullptr) {
            addHandler(handler);
        } else {
            cslog << loglevel(Error) << "Unable to load handler: " << handlerName << endl;
        }
    }
    bool ret = listener_->init(options_->getControlPort(), this);
    if (!ret) {
        return ret;
    }

    auto rulebases = options_->getConfigArray("rules.ruleBases");
    for (const auto& rbname : rulebases) {
        auto file = options_->getConfig("rules." + rbname + ".fileName");
        cslog << loglevel(Info) << "Load rules from rulebase \"" << rbname << "\" file: \"" << file << "\"" << endl;
        eval::Rulebase rb;
        eval::Evaluator eval;
        eval::Context context(&eval);
        Properties vars;
        if (options_->getProperties("rules." + rbname + ".vars", vars)) {
            auto numvars = vars.visit([&context](const std::string& name, PropertyVar& var) {
                                          cslog << loglevel(Info) << " - var " << name << "=";
                                          if (std::holds_alternative<int>(var)) {
                                              int value = std::get<int>(var);
                                              context.setVar(name, value);
                                              cslog << value << endl;
                                          } else {
                                              auto value = std::get<std::string>(var);
                                              context.setVar(name, value);
                                              cslog << value << endl;
                                          }
                                          return true;
                                      });
            cslog << loglevel(Info) << "Set " << numvars << " variables." << endl;
        }
        auto num_rules = rb.parse(options_->getConfigDir() + file, context);
        cslog << loglevel(Info) << "parsed " << num_rules << " rules to rulebase " << rb.name() << "."  << endl;
        num_rules = evaluator_.addRules(rb.rules());
        cslog << loglevel(Info) << "Added " << num_rules << " to evaluator" << endl;
    }
    auto rules = evaluator_.getRules();
    auto num_rules = rules.size();
    cslog << loglevel(Info) << "Rules in evaluator=" << num_rules << endl;
    for (const auto& rule : rules) {
        cslog << loglevel(Info) << rule.description() << endl;
    }

    // Since handlers such as GpioHandler start pushing events, we need to start the evaluator beforehand
    // and also send the Init event so it is first.
    evaluator_.start();
    evaluator_.push(eval::EventFactory::instance()->makeEvent("init"));

    for (auto& [name, handler] : handlers_) {
        handler->setup();
    }
    return ret;
}

bool ControlService::isActive() const {
    return false;	//TODO
}

Handler* ControlService::getHandler(const std::string& name) {
    const auto& handlers = instance()->handlers_;
    auto it = handlers.find(name);
    if (it == handlers.end()) {
        cslog << loglevel(Error) << "Error: Handler \"" << name << "\" not found" << endl;
    } else {
        return it->second;
    }
    return nullptr;
}

void ControlService::run() {
    //listener_->run();
    // ProfilerStart("csprof0.prof");
}

void ControlService::stop(bool gracefully /*= true*/) {
    cslog << "ControlService stop called" << endl;
    evaluator_.stop();
    // listener_->stop();

    // Tell all the peers to stop processing
    for (auto peer : peers_) {
        peer->stop();
        delete peer;
    }
    peers_.clear();

    // Clean up handlers
    for (auto [_, handler] : handlers_) {
        cslog << loglevel(Info) << "Cleanup handler " << handler->getName() << endl;
        handler->deinit();
    }
    database_.uninit();
}

void ControlService::wait() {
    listener_->run(false);	// run in foreground
    cslog << loglevel(Info) << "listener exited from controlservice" << endl;
}

void ControlService::addHandler(Handler* handler) {
    if (handler == nullptr) {
        return;
    }
    const auto& handlerName = handler->getName();
    assert(!handlerName.empty());
    if (handler->init(getOptions())) {
        handlers_[handlerName] = handler;
    } else {
        cslog << loglevel(Error) << "Error: Could not init handler " << handlerName << endl;
    }
}

void ControlService::setForker(Forker* forker) {
    forker_ = forker;
}

// Implement the PeerManager interface:
void ControlService::addPeer(Peer* peer, bool isRest) {
    //TODO check if already in list
    if (!peer) return;

    peer->setCookie(++peerCookie_);
    peers_.push_back(peer);
    if (isRest) {
        restPeer_ = peer;
    }
    cslog << "Added peer " << peer->getName() << (isRest ? " as REST peer" : " as cs peer") << endl;
}

bool ControlService::deletePeer(Peer* peer) {
    auto it = find(peers_.begin(), peers_.end(), peer);
    if (it == peers_.end()) {
        cslog << loglevel(Warning) << "Peer not found in list" << endl;
        return false;
    }
    peers_.erase(it);
    cslog << loglevel(Info) << "Remove peer from list" << endl;
    if (peer == restPeer_) {
        restPeer_ = nullptr;
    }
    return true;
}

Peer* ControlService::findNodePeer() {
    return restPeer_;
}

Peer* ControlService::findPeer(const std::string& name) {
    auto it = std::find_if(peers_.begin(), peers_.end(),
                           [&name](const Peer* peer) {
                               return name == peer->getName();
                           });
    if (it == peers_.end()) {
        return nullptr;
    }
    return *it;
}

Peer* ControlService::findPeer(CookieType cookie) {
  for (auto peer :peers_) {
    if (cookie == peer->getCookie()) {
      return peer;
    }
  }
  return nullptr;
}

void ControlService::getStats(IpcStatistics& stats) {
  stats.addMember("handler_count", static_cast<int>(handlers_.size()));
  stats.addMember("peer_count", static_cast<int>(peers_.size()));

    if (stats.getMember(IpcStatistics::DETAIL) == "detail") {
        cslog << "Get stats details" << endl;
        IpcMessage msg;

        // handlers
        msg.setId("handlers");
        HandlerMap::const_iterator it;
        for (auto [hname, handler] : handlers_) {
            cslog << " +handler: " << hname << endl;
            if (handler->getStats()) {
                IpcMessage mstats(hname);
                if (handler->getStats()->toMessage(mstats)) {
                    msg.addMember(mstats);
                } else {
                    cslog << loglevel(Warning) << "Cannot get stats for handler " << hname << endl;
                    msg.addMember(hname, "No stats");
                }
            } else {
                msg.addMember(hname, "OK");
            }
        }
        stats.addMember(msg);

        // peers
        msg.clear();
        msg.setId("peers");
        for (auto peer : peers_) {
            std::string peerStats = peer->isConnected() ? "connected" : "disconnected";
            msg.addMember(peer->getName(), peerStats);
        }
        stats.addMember(msg);

    }

    struct sysinfo si;
    if (!sysinfo(&si)) {
      stats.addMember("uptime", static_cast<int>(si.uptime));
      stats.addMember("freeMemory", std::to_string(si.freeram));
      stats.addMember("memunit", static_cast<int>(si.mem_unit));
    }
}

void ControlService::pushEvent(std::shared_ptr<eval::Event> event) {
  evaluator_.push(event);
}

////////  sub-class implementations  ////////

BaseControlService::BaseControlService() {
  listener_ = std::make_unique<Listener>();
}

ControlService*
BaseControlService::instance() {
    if (instance_ == nullptr) {
        instance_ = new BaseControlService;
    }
    return instance_;
}

void BaseControlService::run() {
    cslog << "Starting BCS" << endl;
    ControlService::run();
}

#if 0
SubControlService::SubControlService()
    : subClient_(std::make_unique<SubClient>())
    , bcs_(nullptr) {
  listener_ = std::make_unique<Listener>(CS_ROLE_SCS);
}

SubControlService::~SubControlService() {
  if (bcs_ != nullptr) {
    delete bcs_;
  }
}

ControlService*
SubControlService::instance() {
    if (instance_ == nullptr) {
        instance_ = new SubControlService;
    }
    return instance_;
}

bool SubControlService::init(std::unique_ptr<Options> options) {
    if (!ControlService::init(std::move(options))) {
        return false;
    }
    auto baseUrl = options_->getBaseUrl();
    baseUrl.append(1, ':').append(std::to_string(options_->getControlPort()));
    return subClient_->init(baseUrl, this);
}

void SubControlService::run() {
    cslog << "Starting SCS" << endl;
    ControlService::run();
    subClient_->run();
}

void SubControlService::stop(bool gracefully /*= true*/) {
    ControlService::stop(gracefully);
    subClient_->stop();
    bcs_->stop();
    //TODO deinit, delete handlers
}
#endif

}  // namespace ctlsvc
