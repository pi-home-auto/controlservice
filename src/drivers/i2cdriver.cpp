//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "gpiodriver.h"
#include "i2cdriver.h"
#include "logger.h"
#include "string_tools.h"

#include <pigpio.h>
#include <cstddef>
#include <optional>
#include <unistd.h>
#include <unordered_map>

using std::endl;

namespace ctlsvc::driver {

class I2cEntry {
public:
  I2cEntry()
      : bus_(0)
      , addr_(0)
      , mode_(DevMode::None)
      , handle_(-1)
      , listener_(nullptr) {}
  I2cEntry(int bus, int addr, DevMode mode = DevMode::None)
      : bus_(bus)
      , addr_(addr)
      , mode_(mode)
      , handle_(-1)
      , listener_(nullptr) {}
  ~I2cEntry() = default;

  int& bus() { return bus_; }
  const int& bus() const { return bus_; }
  int& addr() { return addr_; }
  const int& addr() const { return addr_; }
  int& handle() { return handle_; }
  const int& handle() const { return handle_; }
  bool isOpen() const { return handle_ >= 0; }
  DevMode& mode() { return mode_; }
  const DevMode& mode() const { return mode_; }
  DriverListener& listener() { return listener_; }
  const DriverListener& listener() const { return listener_; }

private:
  int bus_;
  int addr_;
  DevMode mode_;
  int handle_;
  DriverListener listener_;
};


class I2cDriverImpl {
public:
  I2cDriverImpl()
      : status_(DriverStatus::Unknown) {}
  ~I2cDriverImpl() {}

  I2cEntry* entry() { return &entry_; }
  DriverStatus& status() { return status_; }
  GpioDriver& gpio() { return gpio_; }

private:
  DriverStatus status_;
  I2cEntry entry_;
  GpioDriver gpio_;
};


///////   MAIN  DRIVER  CLASS   //////

I2cDriver::I2cDriver()
    : impl_(std::make_unique<I2cDriverImpl>()) {}

I2cDriver::~I2cDriver() {
  close(impl_->entry());
}

void I2cDriver::close(DriverHandle handle) {
  auto entry = impl_->entry();
  if (entry == nullptr || entry->mode() == DevMode::None) {
    // i2c handle not open
    return;
  }
  if (entry->handle() >= 0) {
    i2cClose(entry->handle());
    entry->handle() = -1;
    //impl_->gpio().close();
  } else {
    cslog << "Invalid i2c handle: " << entry->handle() << endl;
  }
  entry->mode() = DevMode::None;
}

bool I2cDriver::listen(DriverHandle handle, DriverListener listener) {
  return false;
}

DriverName I2cDriver::name() const {
  return "I2c";
}

DriverHandle I2cDriver::open(const DriverName& instance_name, DevMode mode) {
  // Split name into driver name "." bus number "." and device address
  StringArray parts;
  auto num = StringTools::splitString(instance_name, parts, '.');
  int bus;
  int addr;
  bool isValid = false;
  if (num == 3 && parts[0] == "i2c") {
    bus = std::stoi(parts[1]);
    addr = std::stoi(parts[2]);
    if (bus > 0 && bus < 100 && addr > 0 && addr <= 0x7f) {
      isValid = true;
      cslog << loglevel(Info) << "Opening i2c bus " << bus << ", address=" << addr << endl;
    }
  }
  if (!isValid) {
    cslog << loglevel(Error) << "Invalid i2c name " << instance_name << endl;
    impl_->status() = DriverStatus::InvalidParameter;
    return nullptr;
  }
  auto gpio_handle = impl_->gpio().open(instance_name, mode);
  if (gpio_handle == nullptr) {
    impl_->status() = DriverStatus::InvalidParameter;
    cslog << loglevel(Error) << "Error opening gpio driver for i2c driver" << endl;
    return nullptr;
  }

  auto entry = impl_->entry();
  if (entry == nullptr) {
    cslog << loglevel(Error) << "Error: i2c entry not found" << endl;
    impl_->status() = DriverStatus::NotFound;
  } else if (entry->mode() != DevMode::None) {
    cslog << loglevel(Error) << "Error: i2c entry already open" << endl;
    impl_->status() = DriverStatus::AlreadyOpen;
  } else {
    entry->handle() = i2cOpen(bus, addr, 0);
    entry->bus() = bus;
    entry->addr() = addr;
    entry->mode() = mode;
    impl_->status() = entry->isOpen() ? DriverStatus::Success : DriverStatus::Fail;
  }
  return entry;
}

BufSize I2cDriver::read(DriverHandle handle, DriverBuffer& buffer) {
  if (handle != nullptr && buffer.size() > 0) {
    auto entry = static_cast<I2cEntry *>(handle);
    if (entry->isOpen() && (entry->mode() == DevMode::Read || entry->mode() == DevMode::ReadWrite)) {
      usleep(50000);
      BufSize ii;
      for (ii = 0; ii < buffer.size(); ++ii) {
        int value = i2cReadByte(entry->handle());
        buffer.data()[ii] = std::byte(value);
      }
      cslog << loglevel(Info) << "I2cDriver::read called bytes=" << buffer.size()
            << " buf(" << (int)buffer.data()[0] << ", " << (int)buffer.data()[1] << ")" << endl;
      return ii;
    }
  }
  cslog << loglevel(Warning) << "I2cDriver::read nothing read" << endl;
  return 0;
}

void I2cDriver::unlisten(DriverHandle handle) {
}

BufSize I2cDriver::write(DriverHandle handle, const DriverBuffer& buffer) {
  if (handle != nullptr && buffer.size() > 0) {
    cslog << loglevel(Info) << "I2cDriver::write called bytes=" << buffer.size()
          << " buf(" << (int)buffer.data()[0] << ", " << (int)buffer.data()[1] << ")" << endl;
    auto entry = static_cast<I2cEntry *>(handle);
    if (entry->isOpen() && (entry->mode() == DevMode::Write || entry->mode() == DevMode::ReadWrite)) {
      BufSize ii;
      for (ii = 0; ii < buffer.size(); ++ii) {
        int val = (int) buffer.data()[ii];
        int ret = i2cWriteByte(entry->handle(), val);
        if (ret != 0) {
          cslog << loglevel(Error) << "Error writing to i2c, err=" << ret << endl;
        }
      }
      return ii;
    }
  }
  return 0;
}

}  // namespace ctlsvc::driver
