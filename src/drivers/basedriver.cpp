//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "basedriver.h"

namespace ctlsvc {
namespace driver {

class BaseDriverImpl {
public:
    BaseDriverImpl(const std::string& name)
        : name_(name) {}

private:
    std::string name_;

    friend BaseDriver;
};


BaseDriver::BaseDriver(const std::string& name)
    : impl_(std::make_unique<BaseDriverImpl>(name)) {}

BaseDriver::~BaseDriver() {}

void BaseDriver::close(DriverHandle handle) {
}

bool BaseDriver::listen(DriverHandle handle, DriverListener listener) {
    return false;
}

DriverName BaseDriver::name() const {
    return impl_->name_;
}

DriverHandle BaseDriver::open(const DriverName& instance_name, DevMode mode) {
    return nullptr;
}

BufSize BaseDriver::read(DriverHandle handle, DriverBuffer& buffer) {
    return 0;
}

void BaseDriver::unlisten(DriverHandle handle) {
}

BufSize BaseDriver::write(DriverHandle handle, const DriverBuffer& buffer) {
    return buffer.size();
}


}  // namespace driver
}  // namespace ctlsvc
