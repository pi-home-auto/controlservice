//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "driver.h"

namespace ctlsvc {
namespace driver {

// Forward reference
class GpioDriverImpl;

class GpioDriver : public Driver {
public:
    GpioDriver();
    virtual ~GpioDriver();

    // Driver-specific routines
    int dutyCycle(DriverHandle handle) const;
    DevMode mode(DriverHandle handle) const;
    bool usePwm(DriverHandle handle) const;
    void usePwm(DriverHandle handle, bool u);
    bool useServo(DriverHandle handle) const;
    void useServo(DriverHandle handle, bool u);

    // Implement the Driver interface
    void close(DriverHandle handle) override;
    bool listen(DriverHandle handle, DriverListener listener) override;
    DriverName name() const override;
    DriverHandle open(const DriverName& instance_name, DevMode mode) override;
    BufSize read(DriverHandle handle, DriverBuffer& buffer) override;
    void unlisten(DriverHandle handle) override;
    BufSize write(DriverHandle handle, const DriverBuffer& buffer) override;

private:
    std::unique_ptr<GpioDriverImpl> impl_;
};

}  // namespace driver
}  // namespace ctlsvc
