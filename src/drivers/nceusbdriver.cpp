//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "nceusbdriver.h"
#include "logger.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>

using std::endl;

namespace ctlsvc {
namespace driver {

class NceUsbDriverImpl {
public:
    NceUsbDriverImpl()
        : status_(DriverStatus::Unknown)
        , fd_(-1) {
        init();
    }
    ~NceUsbDriverImpl() {
        deinit();
    }
    void close();
    void deinit();
    void init();
    DriverHandle open(const std::string& instance);
    BufSize read(DriverHandle handle, DriverBuffer& buffer);
    BufSize write(DriverHandle handle, const DriverBuffer& buffer);

    DriverStatus& status() { return status_; }

private:
    DriverStatus status_;
    int fd_;
    std::string devFile_;
};

void NceUsbDriverImpl::close() {
    if (fd_ >= 0) {
        ::close(fd_);
        fd_ = -1;
    }
    status_ = DriverStatus::Closed;
}

void NceUsbDriverImpl::deinit() {
    close();
    cslog << loglevel(Info) << "NceUsb Driver deinitialized" << endl;
}

void NceUsbDriverImpl::init() {
    cslog << loglevel(Info) << "Initializing NceUsb Driver" << endl;
    status_ = DriverStatus::Success;
}

DriverHandle NceUsbDriverImpl::open(const std::string& instance) {
    if (fd_ >= 0) {
        status_ = DriverStatus::AlreadyOpen;
        return nullptr;
    }
    if (instance.empty()) {
        cslog << loglevel(Error) << "No USB device name specified" << endl;
        status_ = DriverStatus::InvalidParameter;
        return nullptr;
    }
    devFile_ = instance;
    fd_ = ::open(instance.c_str(), O_RDWR | O_NONBLOCK | O_NOCTTY);
    if (fd_ < 0) {
        cslog << loglevel(Error) << "Unable to open serial port, error=" << errno << endl;
        status_ = DriverStatus::Fail;
        return nullptr;
    }
    struct termios attrs;
    tcgetattr(fd_, &attrs);
    cfmakeraw(&attrs);
    cfsetspeed(&attrs, B9600);
    if (tcsetattr(fd_, TCSANOW, &attrs)) {
        cslog << loglevel(Error) << "Error setting baud rate, error=" << errno << endl;
    }

    // Verify response from NOP
    DriverBuffer buf({ 0x80 });
    auto sz = write(this, buf);
    if (sz == 1) {
        sz = read(this, buf);
        if (sz == 1 && (int)buf.data()[0] == 33) {
            cslog << loglevel(Info) << "NceUsb driver verified" << endl;
        }
    }            
    status_ = DriverStatus::Success;
    return this;
}

BufSize NceUsbDriverImpl::read(DriverHandle handle, DriverBuffer& buffer) {
    int sz = 0;
    if (handle != nullptr && buffer.size() > 0 && fd_ > 0) {
        for (int i = 0; i < 3; ++i) {
            sz = ::read(fd_, buffer.data(), buffer.size());
            if (sz < 0) {
                if (errno == EAGAIN) {
                    cslog << loglevel(Debug) << "Nothing to read on try #" << i+1 << endl;
                    usleep(200000);
                } else {
                    cslog << loglevel(Error) << "Error reading error=" << errno << endl;
                }
            } else {
                cslog << loglevel(Debug) << "Read " << sz << " bytes, dat[0] = "
                      << std::hex << (int)buffer.data()[0] << endl;
                break;
            }
        }
    }
    return sz;
}

BufSize NceUsbDriverImpl::write(DriverHandle handle, const DriverBuffer& buffer) {
    BufSize sz = 0;
    if (handle != nullptr && buffer.size() > 0 && fd_ >= 0) {
        sz = ::write(fd_, buffer.data(), buffer.size());
    }
    return sz;
}

///////   MAIN  DRIVER  CLASS   //////

NceUsbDriver::NceUsbDriver()
    : impl_(std::make_unique<NceUsbDriverImpl>()) {}

NceUsbDriver::~NceUsbDriver() {}

bool NceUsbDriver::getAccessory(unsigned int accy, bool& state) const {
    return false;
}

bool NceUsbDriver::setAccessory(unsigned int accy, bool state) {
    if (accy == 0) {
        impl_->status() = DriverStatus::InvalidParameter;
        return false;
    }
    void* handle = impl_.get();
    std::byte accylo{(uint8_t)(accy >> 8)};
    std::byte accyhi{(uint8_t)(accy && 0xff)};
    //    std::vector<std::byte> contents( {{  0xad, accylo, accyhi, state, 0 }} );
    std::vector<std::byte> contents = { std::byte(0xad), accylo, accyhi, std::byte((int)state), std::byte(0) };
    DriverBuffer buf(contents);
    auto sz = write(handle, buf);
    if (sz == 1) {
        buf.writeString(" ");
        sz = read(this, buf);
        if (sz == 1 && (int)buf.data()[0] == 33) {
            cslog << loglevel(Info) << "NceUsb accy " << accy << " set to " << state << endl;
        } else {
            cslog << loglevel(Error) << "NceUsb setAccessory failed" << endl;
            impl_->status() = DriverStatus::Fail;
            return false;
        }
    }            
    impl_->status() = DriverStatus::Success;
    return true;
}

bool NceUsbDriver::getTrainFunction(unsigned int train, int func, int& state) const {
    return false;
}

bool NceUsbDriver::setTrainFunction(unsigned int train, int func, int state) {
    return false;
}

bool NceUsbDriver::moveTrain(unsigned int train, int speed) {
    if (train == 0) {
        impl_->status() = DriverStatus::InvalidParameter;
        return false;
    }
    void* handle = impl_.get();
    DriverBuffer buf({ 0xa2, (uint8_t)(train >> 8), (uint8_t)(train & 0xff), 0x04, (uint8_t)speed });
    if (speed == 0) {
        buf.data()[3] = std::byte(0x06);
        buf.data()[4] = std::byte(0);
    } else if (speed < 0) {
        buf.data()[3] = std::byte(0x03);
        buf.data()[4] = std::byte(-speed);
    }
    auto sz = write(handle, buf);
    if (sz == 1) {
        buf.writeString(" ");
        sz = read(this, buf);
        if (sz == 1 && (int)buf.data()[0] == 33) {
            cslog << loglevel(Info) << "NceUsb train " << train << " set to speed " << speed << endl;
        } else {
            cslog << loglevel(Error) << "NceUsb moveTrain failed" << endl;
            impl_->status() = DriverStatus::Fail;
            return false;
        }
    }            
    impl_->status() = DriverStatus::Success;
    return true;
}

void NceUsbDriver::close(DriverHandle handle) {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return;
    }
    impl_->close();
}

bool NceUsbDriver::listen(DriverHandle handle, DriverListener listener) {
    if (handle == nullptr || listener == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return false;
    }
    impl_->status() = DriverStatus::Unsupported;
    return false;
}

DriverName NceUsbDriver::name() const {
    return "NceUsb";
}

DriverHandle NceUsbDriver::open(const DriverName& instance_name, DevMode mode) {
    if (mode == DevMode::None) {
        impl_->status() = DriverStatus::InvalidParameter;
        return nullptr;
    }
    auto handle = impl_->open(instance_name);
    return handle;
}

BufSize NceUsbDriver::read(DriverHandle handle, DriverBuffer& buffer) {
    return impl_->read(handle, buffer);
}

void NceUsbDriver::unlisten(DriverHandle handle) {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
    } else {
        impl_->status() = DriverStatus::Unsupported;
    }
}

BufSize NceUsbDriver::write(DriverHandle handle, const DriverBuffer& buffer) {
    return impl_->write(handle, buffer);
}

}  // namespace driver
}  // namespace ctlsvc
