//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "driver.h"

namespace ctlsvc {
namespace driver {

// Forward reference
class I2cDriverImpl;

class I2cDriver : public Driver {
public:
  I2cDriver();
  virtual ~I2cDriver();

  // Driver-specific routines

  // Implement the Driver interface
  void close(DriverHandle handle) override;
  bool listen(DriverHandle handle, DriverListener listener) override;
  DriverName name() const override;
  DriverHandle open(const DriverName& instance_name, DevMode mode) override;
  BufSize read(DriverHandle handle, DriverBuffer& buffer) override;
  void unlisten(DriverHandle handle) override;
  BufSize write(DriverHandle handle, const DriverBuffer& buffer) override;

 private:
  std::unique_ptr<I2cDriverImpl> impl_;
};

}  // namespace driver
}  // namespace ctlsvc
