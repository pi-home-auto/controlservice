//
//   Copyright 2022-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "gpiodriver.h"
#include "logger.h"

#include <pigpio.h>
#include <cstddef>
#include <optional>
#include <unordered_map>

using std::endl;


namespace ctlsvc {
namespace driver {

class GpioEntry {
public:
    GpioEntry(int pin, DevMode mode = DevMode::None)
        : pin_(pin)
        , mode_(mode)
        , use_pwm_(false)
        , use_servo_(false)
        , duty_cycle_(0)
        , listener_(nullptr) {}
    ~GpioEntry() = default;

    int& pin() { return pin_; }
    const int& pin() const { return pin_; }
    DevMode& mode() { return mode_; }
    const DevMode& mode() const { return mode_; }
    int& dutyCycle() { return duty_cycle_; }
    const int& dutyCycle() const { return duty_cycle_; }
    DriverListener& listener() { return listener_; }
    const DriverListener& listener() const { return listener_; }
    bool pwm() const { return use_pwm_; }
    bool pwm(int duty_cycle) {
        if (duty_cycle >= 0) {
            duty_cycle_ = duty_cycle;
            use_pwm_ = true;
        } else {
            use_pwm_ = false;
        }
        return true;
    }
    void servo(int dc) {
        duty_cycle_ = dc;
    }
    bool& use_servo() { return use_servo_; }

private:
    int pin_;
    DevMode mode_;
    bool use_pwm_;
    bool use_servo_;
    int duty_cycle_;
    DriverListener listener_;
};

}  // namespace
}  // namespace

namespace {
using namespace ctlsvc::driver;

void _alertGpioFunc(int gpio, int level, uint32_t tick, void* handle) {
    if (handle != nullptr) {
        auto entry = static_cast<GpioEntry *>(handle);
        if (entry->listener() != nullptr) {
            auto alert = level ? DriverAlert::PinHigh : DriverAlert::PinLow;
            entry->listener()(entry, alert);
        }
    }
}

}  // namespace


namespace ctlsvc {
namespace driver {

class GpioRegistry {
public:
  // An entry is kept for each unique gpio, referenced by name
  using GpioEntries = std::unordered_map<std::string, GpioEntry>;

  GpioRegistry() {
    gpios_.clear();
    for (int pin = 2; pin < 28; ++pin) {
      if (pin == 3) { // reserved for i2c
        continue;
      }
      std::string name("gpio.");
      if (pin == 2) {
        name = "i2c";
      } else {
        name.append(std::to_string(pin));
      }
      gpios_.emplace(name, GpioEntry(pin));
    }
  }
  ~GpioRegistry() = default;

  std::optional<GpioEntry *> entry(const std::string& name) {
    if (name.empty()) {
      return {};
    }
    auto it = gpios_.find(name);
    if (it == gpios_.end()) {
      return {};
    }
    return &it->second;
  }
  const std::optional<GpioEntry *> entry(const std::string& name) const {
    return const_cast<GpioRegistry *>(this)->entry(name);
  }

private:
    GpioEntries gpios_;
};


class GpioDriverImpl {
public:
    GpioDriverImpl()
        : status_(DriverStatus::Unknown) {
        init();
    }
    ~GpioDriverImpl() {
        deinit();
    }
    void init();
    void deinit();

    GpioEntry* getEntry(const std::string& name);
    bool setupGpio(GpioEntry& entry, DevMode mode);
    DriverStatus& status() { return status_; }

private:
    DriverStatus status_;

    static uint32_t count_;
    static GpioRegistry dict_;
};

// Statics
uint32_t GpioDriverImpl::count_ = 0;
GpioRegistry GpioDriverImpl::dict_;

void GpioDriverImpl::init() {
    if (count_ == 0) {
        cslog << loglevel(Info) << "Initializing Gpio Driver" << endl;
        int cfg = gpioCfgGetInternals();
        cfg |= PI_CFG_NOSIGHANDLER;
        cfg |= 15 << PI_CFG_ALERT_FREQ;
        gpioCfgSetInternals(cfg);
        cfg = gpioCfgInterfaces(PI_DISABLE_FIFO_IF | PI_DISABLE_SOCK_IF | PI_DISABLE_ALERT);
        int ver = gpioInitialise();
        if (ver == PI_INIT_FAILED) {
            cslog << loglevel(Error) << "Error intializing pigpio" << endl;
            return;
        } else {
            cslog << loglevel(Info) << "Pigpio initialized" << endl;
        }
    }
    ++count_;

    status_ = DriverStatus::Success;
}

void GpioDriverImpl::deinit() {
    --count_;
    if (count_ == 0) {
        gpioTerminate();
        cslog << loglevel(Info) << "Pigpio deinitialized" << endl;
    }
}

GpioEntry* GpioDriverImpl::getEntry(const std::string& name) {
    auto entry = dict_.entry(name);
    if (!entry) {
        return nullptr;
    }
    return *entry;
}

bool GpioDriverImpl::setupGpio(GpioEntry& entry, DevMode mode) {
    int pin = entry.pin();
    int in_out, pull_up;
    if (mode == DevMode::Read) {
        in_out = PI_INPUT;
        pull_up = PI_PUD_DOWN;
    } else if (mode == DevMode::Write || mode == DevMode::ReadWrite) {
        in_out = PI_OUTPUT;
        pull_up = PI_PUD_OFF;
    } else {
      cslog << loglevel(Error) << "Gpio not open in read and/or write mode" << endl;
      return false;
    }
    int ret = gpioSetMode(pin, in_out);
    if (ret != 0) {
        cslog << loglevel(Error) << "Cannot setup gpio pin " << pin
              << " for input, error=" << ret << endl;
        return false;
    }
    /* TODO Is this the alert, notify in pigpio?
       oss << "/sys/class/gpio/gpio" << pin << "/edge";
       writeToFile(oss.str(), "both\n"); // rising|falling|both
    */
    gpioSetPullUpDown(pin, pull_up);
    entry.mode() = mode;
    return true;
}

///////   MAIN  DRIVER  CLASS   //////

GpioDriver::GpioDriver()
    : impl_(std::make_unique<GpioDriverImpl>()) {}

GpioDriver::~GpioDriver() {}

int GpioDriver::dutyCycle(DriverHandle handle) const {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return -1;
    }
    auto entry = static_cast<GpioEntry *>(handle);
    return entry->dutyCycle();
}

DevMode GpioDriver::mode(DriverHandle handle) const {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return DevMode::None;
    }
    auto entry = static_cast<GpioEntry *>(handle);
    return entry->mode();
}

bool GpioDriver::usePwm(DriverHandle handle) const {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return false;
    }
    auto entry = static_cast<GpioEntry *>(handle);
    return entry->pwm();
}

void GpioDriver::usePwm(DriverHandle handle, bool u) {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return;
    }
    auto entry = static_cast<GpioEntry *>(handle);
    if (u) {
        entry->use_servo() = false;
        entry->pwm(255);
        gpioPWM(entry->pin(), 255);
    } else {
        entry->pwm(-1);
    }
}

bool GpioDriver::useServo(DriverHandle handle) const {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return false;
    }
    auto entry = static_cast<GpioEntry *>(handle);
    return entry->use_servo();
}

void GpioDriver::useServo(DriverHandle handle, bool u) {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return;
    }
    auto entry = static_cast<GpioEntry *>(handle);
    if (u) {
        entry->pwm(-1);
        entry->servo(0);
        entry->use_servo() = true;;
    } else {
        entry->pwm(0);
    }
}


void GpioDriver::close(DriverHandle handle) {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return;
    }
    auto entry = static_cast<GpioEntry *>(handle);
    entry->mode() = DevMode::None;
}

bool GpioDriver::listen(DriverHandle handle, DriverListener listener) {
    if (handle == nullptr || listener == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return false;
    }
    auto entry = static_cast<GpioEntry *>(handle);
    int pin = entry->pin();
    if (pin > 0) {
        entry->listener() = listener;
        if (gpioSetAlertFuncEx(pin, _alertGpioFunc, entry)) {
            cslog << loglevel(Error) << "Error setting alert callback for pin " << pin << endl;
        }
    }
    return true;
}

DriverName GpioDriver::name() const {
    return "Gpio";
}

DriverHandle GpioDriver::open(const DriverName& instance_name, DevMode mode) {
  if (mode == DevMode::None) {
    impl_->status() = DriverStatus::InvalidParameter;
    return nullptr;
  }
  bool isI2c = instance_name.substr(0, 3) == "i2c";
  auto entry = impl_->getEntry(isI2c ? "i2c" : instance_name);
  if (entry == nullptr) {
    impl_->status() = DriverStatus::NotFound;
  } else if (!isI2c) {  // i2c is shared device and must not be set as gpio input or output
    if (entry->mode() != DevMode::None) {
      impl_->status() = DriverStatus::AlreadyOpen;
      return nullptr;
    } else {
      impl_->setupGpio(*entry, mode);
    }
  }
  return entry;
}

BufSize GpioDriver::read(DriverHandle handle, DriverBuffer& buffer) {
    if (handle != nullptr && buffer.size() > 0) {
        auto entry = static_cast<GpioEntry *>(handle);
        if (entry->mode() == DevMode::Read || entry->mode() == DevMode::ReadWrite) {
            int value;
            if (entry->pwm()) {
                value = gpioGetPWMdutycycle(entry->pin());
            } else {
                value = gpioRead(entry->pin());
            }
            buffer.data()[0] = std::byte(value);
            return 1;
        }
    }
    return 0;
}

void GpioDriver::unlisten(DriverHandle handle) {
    if (handle != nullptr) {
        auto entry = static_cast<GpioEntry *>(handle);
        entry->listener() = nullptr;
        int pin = entry->pin();
        if (pin > 0) {
            gpioSetAlertFuncEx(pin, nullptr, nullptr);
        }
    }
}

BufSize GpioDriver::write(DriverHandle handle, const DriverBuffer& buffer) {
    if (handle != nullptr && buffer.size() > 0) {
        auto entry = static_cast<GpioEntry *>(handle);
        if (entry->mode() == DevMode::Write || entry->mode() == DevMode::ReadWrite) {
            int val = *(int *)buffer.data();
            if (entry->pwm()) {
                gpioPWM(entry->pin(), val);
            } else if (entry->use_servo()) {
                gpioServo(entry->pin(), val);
            } else {
                gpioWrite(entry->pin(), val);
            }
            return sizeof(int);
        }
    }
    return 0;
}

}  // namespace driver
}  // namespace ctlsvc
