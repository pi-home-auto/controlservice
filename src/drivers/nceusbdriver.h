//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "driver.h"

namespace ctlsvc {
namespace driver {

// Forward reference
class NceUsbDriverImpl;

class NceUsbDriver : public Driver {
public:
    NceUsbDriver();
    virtual ~NceUsbDriver();

    // Driver-specific routines
    bool getAccessory(unsigned int accy, bool& state) const;
    bool setAccessory(unsigned int accy, bool state);
    bool getTrainFunction(unsigned int train, int func, int& state) const;
    bool setTrainFunction(unsigned int train, int func, int state);
    /** Set speed of specified train
     *  @param train DCC Train number
     *  @param speed If positive then move train forward, if negative then
     *               move backwards else if 0 then stop train.
     */
    bool moveTrain(unsigned int train, int speed);

    // Implement the Driver interface
    void close(DriverHandle handle) override;
    bool listen(DriverHandle handle, DriverListener listener) override;
    DriverName name() const override;
    DriverHandle open(const DriverName& instance_name, DevMode mode) override;
    BufSize read(DriverHandle handle, DriverBuffer& buffer) override;
    void unlisten(DriverHandle handle) override;
    BufSize write(DriverHandle handle, const DriverBuffer& buffer) override;

private:
    std::unique_ptr<NceUsbDriverImpl> impl_;
};

}  // namespace driver
}  // namespace ctlsvc
