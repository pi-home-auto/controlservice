//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "distdriver.h"
#include "logger.h"
#include <fstream>

using std::endl;

namespace ctlsvc {
namespace driver {

class DistDriverImpl {
public:
    DistDriverImpl()
        : mode_(DevMode::None)
        , status_(DriverStatus::Unknown)
        , units_("us") {
        init();
    }
    ~DistDriverImpl() {
        deinit();
    }
    void close();
    void deinit();
    void init();
    DriverHandle open(const std::string& instance);

    DevMode mode() const { return mode_; }
    DriverStatus& status() { return status_; }
    std::string& units() { return units_; }
    std::fstream& devStream() { return devStream_; }

private:
    DevMode mode_;
    DriverStatus status_;
    std::string units_;
    std::string devFile_;
    std::fstream devStream_;
};

void DistDriverImpl::close() {
    if (devStream_.is_open()) {
        devStream_.close();
    }
    mode_ = DevMode::None;
    status_ = DriverStatus::Closed;
}

void DistDriverImpl::deinit() {
    close();
    cslog << loglevel(Info) << "Dist Driver deinitialized" << endl;
}

void DistDriverImpl::init() {
    cslog << loglevel(Info) << "Initializing Dist Driver" << endl;
    status_ = DriverStatus::Success;
}

DriverHandle DistDriverImpl::open(const std::string& instance) {
    if (instance != "/dev/dist") {
        cslog << loglevel(Error) << "Only a single distance sensor is supported: /dev/dist" << endl;
        status_ = DriverStatus::InvalidParameter;
        return nullptr;
    }
    if (devStream_.is_open()) {
        status_ = DriverStatus::AlreadyOpen;
        return nullptr;
    }
    devFile_ = instance;
    devStream_.open(devFile_);
    if (!devStream_.is_open()) {
        status_ = DriverStatus::Fail;
        return nullptr;
    }
    mode_ = DevMode::ReadWrite;
    status_ = DriverStatus::Success;
    return this;
}

///////   MAIN  DRIVER  CLASS   //////

DistDriver::DistDriver()
    : impl_(std::make_unique<DistDriverImpl>()) {}

DistDriver::~DistDriver() {}

void DistDriver::setUnits(const std::string& units) {
    if (units != getUnits()) {
        if (units == "cm" || units == "mm" || units == "m" || units == "us" || units == "in"|| units ==  "ft") {
            impl_->units() = units;
            std::string strbuf(units + "\n");;
            impl_->devStream().write(strbuf.c_str(), strbuf.size());
        }
    }
}

const std::string& DistDriver::getUnits() const {
    return impl_->units();
}

DevMode DistDriver::mode(DriverHandle handle) const {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return DevMode::None;
    }
    return impl_->mode();
}

void DistDriver::close(DriverHandle handle) {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return;
    }
    impl_->close();
}

bool DistDriver::listen(DriverHandle handle, DriverListener listener) {
    if (handle == nullptr || listener == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
        return false;
    }
    impl_->status() = DriverStatus::Unsupported;
    return false;
}

DriverName DistDriver::name() const {
    return "Dist";
}

DriverHandle DistDriver::open(const DriverName& instance_name, DevMode mode) {
    if (mode == DevMode::None) {
        impl_->status() = DriverStatus::InvalidParameter;
        return nullptr;
    }
    auto handle = impl_->open(instance_name);
    return handle;
}

BufSize DistDriver::read(DriverHandle handle, DriverBuffer& buffer) {
    if (handle != nullptr && buffer.size() > 0) {
        char buf[100];
        impl_->devStream().getline(buf, 100);
        buffer.writeString(buf);
        return buffer.size();
    }
    return 0;
}

void DistDriver::unlisten(DriverHandle handle) {
    if (handle == nullptr) {
        impl_->status() = DriverStatus::InvalidParameter;
    } else {
        impl_->status() = DriverStatus::Unsupported;
    }
}

BufSize DistDriver::write(DriverHandle handle, const DriverBuffer& buffer) {
    BufSize sz = 0;
    if (handle != nullptr && buffer.size() > 0 &&
        (impl_->mode() == DevMode::Write || impl_->mode() == DevMode::ReadWrite)) {
        auto strbuf = (char *)buffer.data();
        sz = buffer.size();
        impl_->devStream().write(strbuf, sz);
    }
    return sz;
}

}  // namespace driver
}  // namespace ctlsvc
