//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <memory>
#include <string>

namespace ctlsvc {
// Forward reference
class IpcControlAlert;
class IpcMessageImpl;

/**
 * Objects in this class hierarchy are the C++ representation of the
 * control service internal IPC, whose serialized form is JSON.
 *
 * Note: This class has an opaque dependency to JsonCpp.  This is not exposed
 *       to the caller.
 */
class IpcMessage {
public:
    IpcMessage(const std::string& id = std::string());
    virtual ~IpcMessage();

    /** Add a new name/value pair, or update the value for name if already
     *  existing.  This will fail to add a new name/value if #endMembers
     *  has been called.
     *  @return true if name has been set to value, otherwise false.
     */
    bool addMember(const std::string& name, const std::string& value);
    bool addMember(const std::string& name, int value);
    bool addMember(const std::string& name, float value);
    bool addMember(const IpcMessage& subMessage);

    /** Clear the message contents, if not frozen. */
    void clear();

    /** Do not allow any more name/value pairs to be added to the message. */
    void freeze();

    const std::string& getId() const;
    std::string getMember(const std::string& name) const;
    float getFloatMember(const std::string& name) const;
    int getIntMember(const std::string& name) const;
    std::string getStatus() const;

    bool isMember(const std::string& name) const;

    void setId(const std::string& id);
    /** similar to addMember, but only sets existing members */
    bool setMember(const std::string& name, const std::string& value);
    bool setMember(const std::string& name, int value);
    bool setMember(const std::string& name, float value);
    bool setMember(const std::string& name, const IpcMessage& subMessage);

    /**
     * Create an IPC message from a json string.  Similar to #fromString method.
     */
    IpcMessage* factory(const std::string& jsonStr);

    /**
     * Deserialize a json string into an IpcMessage
     */
    bool fromString(const std::string& jsonStr);

    /**
     * Serialize the message to internal IPC format (currently JSON).
     * This default (base class) implementation constructs a flat JSON structure with
     * #id_ as root node consisting of an object of all elements as name/value pairs.
     */
    virtual std::string toString() const;

    virtual std::string toShortString() const;

protected:
    IpcMessage(const char* const* structure);

    // copy constructor and assignment operator
    IpcMessage(const IpcMessage&);
    IpcMessage& operator=(const IpcMessage&);

protected:
    std::unique_ptr<IpcMessageImpl> impl_;
};

/** Main communication to handlers */
class IpcControl : public IpcMessage {
public:
    enum Level {
        ALERTDEBUG,
        ALERTLOW,
        ALERTMEDIUM,
        ALERTHIGH
    };
    enum Status {
        UNKNOWN = -1,
        OK,
        WARNING,
        ERROR
    };
    /** Define message structure constants */
    static const std::string ID;       //!< identifies the message
    static const std::string HANDLER;  //!< handler name
    static const std::string METHOD;   //!< method name within handler
    static const std::string MODULE;   //!< module name that should process message

    IpcControl();
    IpcControl(const std::string& handler, const std::string& method);

    /** Return a control alert reply based on this control message.
     *  @param alert An alert level
     *  @param statusText Short description of status.
     *  @return a control alert. */
    IpcControlAlert respond(IpcControl::Level level,
                            IpcControl::Status status,
                            const std::string& statusText = std::string());

protected:
    IpcControl(const char* const* structure);
};

/** Main communication from handler threads.
 *  When a handler needs to send an alert asynchronously, it can 
 *  call Handler::sendToNode with this message. */
class IpcControlAlert : public IpcControl {
public:
    static const std::string ID;
    static const std::string LEVEL;
    static const std::string STATUS;
    static const std::string STATUSTEXT;

    IpcControlAlert(const std::string& handler, const std::string& method,
                    IpcControl::Level level = ALERTLOW, IpcControl::Status status = OK,
                    const std::string& statusText = std::string());
};

/** Receive an event remotely and process it */
class IpcEvent: public IpcMessage {
public:
    static const std::string ID;
    static const std::string NAME;
};

/** Request node server to execute a function */
class IpcDoNode : public IpcMessage {
public:
    static const std::string ID;
};


/** Tell your peer to hang up */
class IpcGoodbye : public IpcMessage
{
public:
    static const std::string ID;
};

/** First message sent and received for a new connection. */
class IpcGreeting : public IpcMessage
{
public:
    static const std::string ID;
    static const std::string VERSION;
    static const std::string TYPE;
    static const std::string NAME;
    static const std::string ROLE;
    static const std::string MAIN_GROUP;

    IpcGreeting();
    IpcGreeting(const std::string& name, const std::string& role,
                const std::string& mainGroup = std::string());

    const std::string& getName() const;

private:
    mutable std::string name_;
};

class IpcReply : public IpcMessage
{
public:
    static const std::string ID;
    static const std::string STATUS;
    static const std::string RESULT; //!< The result string

    IpcReply();
    IpcReply(IpcControl::Status status, const std::string& result);

    void setReply(IpcControl::Status status, const std::string& result);
};

class IpcShutdown : public IpcMessage
{
public:
    static const std::string ID;
    static const std::string WHEN;

    IpcShutdown(const std::string& when);
};

class IpcStatistics : public IpcMessage
{
public:
    static const std::string ID;
    static const std::string DETAIL;

    IpcStatistics(const std::string& detail = std::string());
};

}  // namespace ctlsvc
