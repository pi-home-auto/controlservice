//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <fstream>
#include <memory>

#define DEFAULT_HOMEDIR BASE_DIR

namespace ctlsvc {

/**
 * Log levels
 */
typedef int CsLogLevel;

extern const int Trace;
extern const int Debug;
extern const int Info;
extern const int Warning;
extern const int Error;
extern const int Fatal;

enum CsLogType {
    INVALID = -1,
    ALERT,
    AUDIT,
    LOG
};

// Forward reference
class LogStreamBufImpl;

class LogStreamBuf : public std::filebuf {
public:
    LogStreamBuf(CsLogType logType, const std::string& file);
    virtual ~LogStreamBuf() = default;

    virtual int sync() override;
  /* Need to override all of these (for thread-safety):
     int overflow(int ch)
     int underflow()
     int sync()
     streamsize xsputn(const char* s, streamsize n)
     streamsize xsgetn(char* s, streamsize n)
  */
    CsLogLevel getLogLevel() const;
    void setAuditLogLevel(CsLogLevel level);
    void setLogLevel(CsLogLevel level);
    void setDefaultLogLevel(CsLogLevel level);

private:
    std::unique_ptr<LogStreamBufImpl> impl_;
};

class Logger : public std::ofstream {
public:
  Logger(CsLogType logType, const std::string& file = std::string(),
         bool append_pid = false);
  virtual ~Logger();

  CsLogLevel getLowLogLevel() const;
  std::ostream& getNullStream();
  const std::ostream& getNullStream() const;

  void setAuditLogLevel(CsLogLevel level = Info);
  void setDefaultLogLevel(CsLogLevel level = Debug);
  void setLogLevel(CsLogLevel level = Debug);
  void writeAlert(const std::string& msg);
  void writeAudit(const std::string& msg);
  void writeLog(const std::string& msg);

 private:
  CsLogLevel lowLogLevel_;
  LogStreamBuf streamBuf_;
  std::ostream nullStream_;
};


struct _Level { CsLogLevel level; };

_Level loglevel(CsLogLevel level = Debug);
std::ostream& operator<<(Logger& logger, _Level level);

extern Logger& csalert;
extern Logger& csaudit;
extern Logger& cslog;

}  // namespace ctlsvc
