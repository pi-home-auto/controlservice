//
//   Copyright 2020-2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "device.h"

#include <cassert>

namespace ctlsvc {

Device::Device(const std::string& name)
    : name_(name)
    , mode_(Unknown) {}

Device::~Device() {}

bool Device::configureSet(const Properties& props) {
    auto count = props.visit([this](const std::string& name, const PropertyVar& prop) {
                                 return configure(name, prop);
                             });
    return count == props.size();
}

bool Device::querySet(Properties& props) {
    auto count = props.visit([this](const std::string& name, PropertyVar& prop) {
                                 prop = query(name);
                                 return true;
                             });
    return count == props.size();
}

bool Device::read(int& value) {
    return read(sizeof(int), &value) > 0;
}

bool Device::write(int value) {
    return write(1, &value);
}

}  // namespace ctlsvc
