//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <functional>
#include <memory>
#include <unordered_map>
#include <variant>
#include <vector>

namespace ctlsvc {
// Forward references
class Device;
class Handler;
class ObjectDepotImpl;
class Options;

// TODO these should be weak_ptr
using DeviceObject = std::shared_ptr<Device>;
using HandlerObject = std::shared_ptr<Handler>;

/** This class is used for overall, generic (shared) Object storage. Objects can be a Device,
 *  a Handler, a Module, etc. Objects are located and retrieved based on their Universal Name.
 *  This class is the single point for universal name resolution.
 *
 *  Note that not all objects are actually stored here. For example, gpio devices are owned by the GpioHandler.
 *  For each object type, a custom name resolver can be implemented.
 */
class ObjectDepot {
public:
  using ObjectType = std::variant<std::monostate, DeviceObject, HandlerObject>;
  /** Type of the object database.
   *  The first parameter is specified as the "objectType:objectName", for example
   *  "device:dist.0"
   */
  using ObjectStore = std::unordered_map<std::string, ObjectType>;

  ObjectDepot(const std::string& my_module = std::string());
  ~ObjectDepot();

  void init(const Options* options);

  //TODO addNameResolver(objType)
  //TODO aliases
  //TODO method to store an Object. Is remove method needed too?

  ObjectType find(const std::string& uname) const;
  ObjectType find(const std::string& objType, const std::string& objName) const;

  // for debugging
  using DebugCallback = std::function<void(const std::string&, const ObjectType&)>;
  void listContents(DebugCallback cb) const;

  /** Split a universal name into 3 components: module, type and name.
   *  If module was not specified as part of the name or the module is
   *  is the current module, then the first part will be empty.
   *  If the type was not specified in the name, then the second part will
   *  default to "device".  The name (third part) cannot be empty.
   *  @param uname  Universe name to parse
   *  @return An array of 3 strings, one for each part of the Uname.
   *          If the Uname was invalid, then an empty array is returned.
   */
  std::vector<std::string> splitUName(const std::string& uname) const;

  /** Uses the sql database to construct the UName for a device. */
  std::string uuidToUName(const std::string& uuid) const;

private:
  std::unique_ptr<ObjectDepotImpl> impl_;
};


std::ostream& operator<<(std::ostream& os, const ObjectDepot::ObjectType& obj);

}  // namespace ctlsvc
