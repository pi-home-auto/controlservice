//
//   Copyright 2020-2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <iostream>
#include <sockstr/Stream.h>

#include "controlservice.h"
#include "handler.h"
#include "ipcmessage.h"
#include "logger.h"
#include "peer.h"
#include "statistics.h"
using namespace ctlsvc;
using std::endl;


Handler::Handler(const std::string& name)
    : name_(name)
    , stats_() {}

Handler::~Handler() {}

bool Handler::sendToNode(const IpcMessage* msg) {
    cslog << loglevel(Info) << "Send to node: " << msg->toShortString() << endl;
    Peer* node = ControlService::instance()->findNodePeer();
    if (node == nullptr || !node->isRunning()) {
        cslog << loglevel(Error) << "Unable to find a node server" << endl;
        return false;
    }
    sockstr::Stream* strm = node->getStream();
    if (!strm || !strm->good() || !strm->is_open()) {
        cslog << loglevel(Error) << "Unable to open stream to node server" << node->getName() << endl;
        return false;
    }
    *strm << msg->toString() << endl;
    cslog << loglevel(Info) << "Message sent to node.js: " << msg->getId() << endl;
    return true;
}
