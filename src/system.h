//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "string_tools.h"
#include <vector>

/**
 * The ctlsvc::System namespace contains various convenience functions such as reading
 * a whole text file, shutting down the Controlservice, etc.
 */
namespace ctlsvc::System {

/** Return a string describing the current system error (errno global variable).
 *  @param  prefix  An optional string that is prepended to the error messages
 *  @return Error message
 */
std::string getErrorString(const std::string& prefix = std::string());

/** Open a file, read its contents and return as a string.
 *  @param fileName Name of file
 */
std::string readFileContent(const std::string& fileName);

/** Shutdown the Control Service.
 *  @param when  Currently this parameter is not used
 */
void shutdown(const std::string& when);

}  // namespace ctlsvc::System
