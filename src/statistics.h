//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <map>
#include <string>
#include <variant>

namespace ctlsvc {
// Forward references
class IpcMessage;

/**
 * Class to represent one statistical value.  This class acts as a
 * union of various value types.
 */
class StatValue {
public:
    /** Type of value that is either undefined or one of the scalar types. */
    using ValueType = std::variant<std::monostate, int, float, double>;

    StatValue();
    StatValue(int value);
    StatValue(float value);
    StatValue(double value);

    void add(StatValue delta);
    void increment();
    int getInt() const;
    float getFloat() const;
    double getDouble() const;

    std::string toString() const;
    ValueType& value();
    const ValueType& value() const;

private:
    ValueType value_;
};


/**
 *  Data holder for collecting statistics.
 *
 *  Any class can include Statistics as a member, typically a handler,
 *  but could be a device, driver, evaluator, sequencer, etc.
 *
 */
class Statistics {
public:
    /** Type used for statistics table */
    using  StatsMap = std::map<std::string, StatValue>;
public:
    Statistics(const std::string& name);
    virtual ~Statistics();

    // Getters
    /** Get the name of the statistics */
    std::string getName() const { return name_; }

    /** Get the int value of the named statistic. The stat from this
     *  instance is returned if found, otherwise the shared (global)
     *  stats are searched. If stat is not found in either then a
     *  default value is returned (usually zero).
     */
    int getInt(const std::string& statName) const;
    /** Get the float value of the named statistic. The stat from this
     *  instance is returned if found, otherwise the shared (global)
     *  stats are searched. If stat is not found in either then a
     *  default value is returned (usually zero).
     */
    float getFloat(const std::string& statName) const;
    /** Get the double value of the named statistic. The stat from this
     *  instance is returned if found, otherwise the shared (global)
     *  stats are searched. If stat is not found in either then a
     *  default value is returned (usually zero).
     */
    double getDouble(const std::string& statName) const;

    template <typename T>
    bool addTo(const std::string& statName, T delta) {
        auto it = stats_.find(statName);
        if (it != stats_.end()) {
            it->second.add(delta);
            return true;
        }
        return false;
    }
    void increment(const std::string& statName);

    /** Create a new integer stat value */
    void newStat(const std::string& statName, int start, bool shared = false);
    /** Create a new float stat value */
    void newStat(const std::string& statName, float start, bool shared = false);
    /** Create a new double stat value */
    void newStat(const std::string& statName, double start, bool shared = false);

    /**
     * Fill in an IpcMessage structure with statistics.
     * @param msg Message will be cleared and filled with stat values.
     * @param shared If true then the shared (global) statistics are returned.
     *               Otherwise, the stats for this instance are returned.
     * @return True if successful, false if an error occurred.
     */
    bool toMessage(IpcMessage& msg, bool shared = false) const;
    /** Only useful for debug/log.  Use toMessage instead. */
    std::string toString() const;
    time_t getInitTime(bool shared = false) const;
private:
    std::string name_;
    StatsMap stats_;
    time_t since_; // when stats were initialized

private:
    static StatsMap sharedStats_;
    static time_t sharedSince_; // when shared stats were initialized
};

}  // namespace ctlsvc
