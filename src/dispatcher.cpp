//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "dispatcher.h"
#include "ipcmessage.h"
#include "handler.h"
#include "logger.h"
using std::endl;


namespace ctlsvc {

Dispatcher::Dispatcher()
    : module_(ControlService::instance()->getOptions()->getModuleName())
    , handlers_(ControlService::instance()->getHandlers()) {
  cslog << loglevel(Info) << "Dispatcher created, module=" << module_ << endl;
}

Dispatcher::ROUTE
Dispatcher::execute(const IpcMessage& message, IpcReply& reply, Peer* peer) {
  ROUTE ret = DR_INVALID;
  std::string id = message.getId();
  if (id.empty()) {
    reply.setId(IpcReply::ID);
    reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
    return ret;
  } else if (id != IpcControl::ID && id != IpcEvent::ID) {
    // Do NOT reply to a reply
    if (id == IpcReply::ID) return DR_NOREPLY;
    cslog << loglevel(Error) << "Error: id is '" << id << "'. Expected 'control' or 'event'." << endl;
    reply.setId(IpcReply::ID);
    reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
    return ret;
  }

  auto module = message.getMember(IpcControl::MODULE);
  cslog << loglevel(Info) << "Dispatch control, module=" << module << endl;

  if (id == IpcControl::ID && message.isMember(IpcControl::HANDLER)) {
    id = message.getMember(IpcControl::HANDLER);
    const auto it = handlers_.find(id);
    if (it == handlers_.end()) {
      cslog << loglevel(Error) << "Error: Handler \"" << id << "\" not found" << endl;
      reply.setId(IpcReply::ID);
      reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
      return ret;
    }

    Handler* handler = it->second;
    //TODO? handler.request() needs to return more than a bool
    ret = handler->request(message, reply, peer) ? DR_NOREPLY : DR_ERROR;
    if (reply.getId().empty()) {
      reply.setId(IpcReply::ID);
    }
    cslog << loglevel(Debug) << "Run handler " << message.toShortString()
          << ", reply=" << reply.toShortString() << endl;
    // csaudit << "-reply: " << reply.getIntMember(IpcReply::STATUS)
    //         << " " << reply.getMember(IpcReply::RESULT) << endl;
  } else if (id == IpcEvent::ID) {
    //TODO call event factory and push event
  }
  return ret;
}

}  // namespace ctlsvc
