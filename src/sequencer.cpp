//
//   Copyright 2021-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "logger.h"
#include "program.h"
#include "sequencer.h"

#include <algorithm>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <cstdint>
#include <forward_list>
#include <memory>
#include <mutex>
#include <thread>
#include <vector>

using std::endl;

namespace {
ctlsvc::SequenceEntry dummy_entry;
}

namespace ctlsvc {

//////////////////    IMPL  CLASS    ///////////////////////
class SequencerImpl {
public:
    SequencerImpl()
      : bkgnd_running_(false)
      , active_(false)
      , running_(false)
      , do_repeats_(true) {}
    ~SequencerImpl();

    bool addBackgroundProgram(std::unique_ptr<Program>&& program);
    bool addProgram(std::unique_ptr<Program>&& program, LevelNum level, int repeat,
                    SequenceEntry::EntryType type);
    bool removeProgram(const std::string& name, LevelNum level);
    bool runPrograms(bool one_shot);

    /** Update the sequence from the queued up transactions. */
    void updateSequence();

private:
    std::vector<std::forward_list<SequenceEntry>> sequence_;
    std::thread run_thread_;
    std::thread background_thread_;
    std::unique_ptr<Program> bkgnd_program_;
    std::atomic_bool bkgnd_running_;
    std::atomic_bool active_;
    std::atomic_bool running_;
    std::atomic_bool do_repeats_;
    /** Mutex that protects to_add_ and to_remove_ lists */
    mutable std::mutex list_mod_mutex_;
    /** Condition variable so the run loop thread is paused when the
     *  sequence is empty.
     */
    std::condition_variable run_cv_;
    /** List of programs that will be added to the sequence after all
     *  the programs in the sequence have played.
     */
    std::vector<SequenceEntry> to_add_;
    /** List of programs that will be removed after all the programs in the
     *  sequence have played.
     *  The list contains elements with the level number and name of the
     *  program to be removed. If the level number specified is greater than kMaxLevelNum,
     *  then all levels are searched for a program with the specified name. The first one
     *  found will be removed.
     */
    std::vector<std::pair<LevelNum, std::string>> to_remove_;

    friend Sequencer;
};

SequencerImpl::~SequencerImpl() {
    if (run_thread_.joinable()) {
        running_ = false;
        run_cv_.notify_one();
        run_thread_.join();
    }
    if (background_thread_.joinable()) {
        background_thread_.join();
    }
}

bool SequencerImpl::addBackgroundProgram(std::unique_ptr<Program>&& program) {
    if (bkgnd_running_) {
        bkgnd_running_ = false;
        if (background_thread_.joinable()) {
            background_thread_.join();
        }
    }
    bkgnd_running_ = true;
    bkgnd_program_ = std::move(program);
    background_thread_ = std::thread([this](){
                                     while (bkgnd_running_) {
                                         if (bkgnd_program_) {
                                             if (bkgnd_program_->execute(bkgnd_program_->begin()) !=
                                                 bkgnd_program_->end()) {
                                                 break;  // Some step got an error
                                             }
                                         } else {
                                             break;
                                         }
                                     }
                                     bkgnd_running_ = false;
                                 });
    return true;
}

bool SequencerImpl::addProgram(std::unique_ptr<Program>&& program, LevelNum level, int repeat,
                               SequenceEntry::EntryType type) {
    if (level > Sequencer::kMaxLevelNum) {
        cslog << loglevel(Error) << "Level " << level << " is greater than max level "
              << Sequencer::kMaxLevelNum << endl;
        return false;
    }
    while (sequence_.size() <= level) {
        sequence_.push_back(std::forward_list<SequenceEntry>());
    }
    sequence_[level].emplace_front(std::move(program), level, repeat, type);
    return true;
}

bool SequencerImpl::removeProgram(const std::string& name, LevelNum level) {
    bool ret = false;
    if (level < sequence_.size()) {
        auto& seqlvl = sequence_[level];
        auto sz = std::distance(seqlvl.begin(), seqlvl.end());
        seqlvl.remove_if([&name](const SequenceEntry& se) {
                             return se.program->name() == name;
                         });
        ret = sz > std::distance(seqlvl.begin(), seqlvl.end());
    }
    if (ret) {
        while (!sequence_.empty()) {
            if (sequence_.back().empty()) {
                sequence_.pop_back();
            } else {
                break;
            }
        }
    }
    return ret;
}

bool SequencerImpl::runPrograms(bool one_shot) {
    active_ = true;
    while (running_) {
        updateSequence();
        if (sequence_.empty()) {
            active_ = false;
            if (one_shot || !do_repeats_) {
                break;
            } else {
                cslog << "Sequencer enter locked wait" << endl;
                std::unique_lock lck(list_mod_mutex_);
                run_cv_.wait(lck, [this]{ return !to_add_.empty() || !running_; });
                cslog << "Sequencer run notified" << endl;
                continue;
            }
        }
        uint32_t active_count = 0U;
        auto level = sequence_.size() - 1;
        do {
            // cslog << loglevel(Debug) << "Sequencer play level " << level << endl;
            for (auto& se : sequence_[level]) {
                if (se.type == SequenceEntry::Off) {
                    continue;
                }
                int repeat = se.repeat;
                // cslog << loglevel(Debug) << " + play program " << se.program->name()
                //       << " " << repeat << " times." << endl;
                while (repeat != 0) {
                    ++active_count;
                    if (se.program->execute(se.program->begin()) != se.program->end()) {
                        break;
                    }
                    if (!do_repeats_) {
                        break;
                    }
                    if (repeat > 0) {
                        --repeat;
                    }
                }
                if (se.type == SequenceEntry::RunOnce) {
                    se.type = SequenceEntry::Off;
                    std::lock_guard lck(list_mod_mutex_);
                    to_remove_.push_back(std::make_pair(level, se.program->name()));
                } else if (se.type == SequenceEntry::RunOnceKeep) {
                    se.type = SequenceEntry::Off;
                }
            }
        } while (level-- > 0);
        active_ = active_count > 0;
        if (one_shot || !do_repeats_) {
            break;
        }
    }
    active_ = false;
    running_ = false;
    return true;
}

void SequencerImpl::updateSequence() {
    std::lock_guard lck(list_mod_mutex_);
    for (auto& p : to_remove_) {
        if (p.first > Sequencer::kMaxLevelNum) {
            for (size_t level = 0; level < sequence_.size(); ++level) {
                if (removeProgram(p.second, level)) {
                    break;
                }
            }
        } else {
            removeProgram(p.second, p.first);
        }
    }
    to_remove_.clear();

    for (auto& se : to_add_) {
        addProgram(std::move(se.program), se.level, se.repeat, se.type);
    }
    to_add_.clear();
}

//////////////////    MAIN  CLASS    ///////////////////////
Sequencer::Sequencer()
    : impl_(std::make_unique<SequencerImpl>()) {}

Sequencer::~Sequencer() {
    stop(true);
}

bool Sequencer::addBackgroundProgram(std::unique_ptr<Program>&& program) {
    return impl_->addBackgroundProgram(std::move(program));
}

bool Sequencer::addDefaultProgram(std::unique_ptr<Program>&& program) {
    return addProgram(std::move(program), 0, 1, SequenceEntry::Loop);
}

bool Sequencer::addProgram(SequenceEntry& entry) {
    bool do_notify = false;
    {
        std::lock_guard lck(impl_->list_mod_mutex_);
        if (impl_->running_) {
            impl_->to_add_.emplace_back(std::move(entry.program), entry.level, entry.repeat, entry.type);
            if (impl_->sequence_.empty()) {
                do_notify = true;
            }
        } else {
            return impl_->addProgram(std::move(entry.program), entry.level, entry.repeat, entry.type);
        }
    }
    if (do_notify) {
        impl_->run_cv_.notify_one();
    }
    return true;
}

bool Sequencer::addProgram(std::unique_ptr<Program>&& program, LevelNum level, int repeat, SequenceEntry::EntryType type) {
    bool do_notify = false;
    {
        std::lock_guard lck(impl_->list_mod_mutex_);
        if (impl_->running_) {
            impl_->to_add_.emplace_back(std::move(program), level, repeat, type);
            if (impl_->sequence_.empty()) {
                do_notify = true;
            }
        } else {
            return impl_->addProgram(std::move(program), level, repeat, type);
        }
    }
    if (do_notify) {
        impl_->run_cv_.notify_one();
    }
    return true;
}

const SequenceEntry& Sequencer::getProgram(const std::string& name) const {
    std::lock_guard lck(impl_->list_mod_mutex_);
    for (auto& lvlprogs : impl_->sequence_) {
        for (auto& se : lvlprogs) {
            if (se.program->name() == name) {
                return se;
            }
        }
    }
    return dummy_entry;
}

bool Sequencer::isBackgroundRunning() const {
    return impl_->bkgnd_running_;
}

bool Sequencer::isPolling() const {
    return impl_->running_;
}

bool Sequencer::isRunning() const {
    return impl_->active_;
}

bool Sequencer::modifyProgram(const std::string& name, LevelNum level, int repeat, SequenceEntry::EntryType type) {
    std::lock_guard lck(impl_->list_mod_mutex_);
    if (level < impl_->sequence_.size()) {
        auto it = std::find_if(impl_->sequence_[level].begin(), impl_->sequence_[level].end(),
                               [&name](const SequenceEntry& se) {
                                   return se.program->name() == name;
                               });
        if (it != impl_->sequence_[level].end()) {
            auto& se = *it;
            se.repeat = repeat;
            se.type = type;
            return true;
        }
    }
    return false;
}

bool Sequencer::removeBackgroundProgram() {
    return false;  // TODO implement
}

bool Sequencer::removeProgram(const std::string& name) {
    std::lock_guard lck(impl_->list_mod_mutex_);
    if (impl_->running_) {
        impl_->to_remove_.push_back(std::make_pair(kMaxLevelNum + 1, name));
    } else {
        return impl_->removeProgram(name, kMaxLevelNum + 1);
    }
    return true;
}

bool Sequencer::removeProgram(const std::string& name, LevelNum level) {
    std::lock_guard lck(impl_->list_mod_mutex_);
    if (impl_->running_) {
        impl_->to_remove_.push_back(std::make_pair(level, name));
    } else {
        return impl_->removeProgram(name, level);
    }
    return true;
}

bool Sequencer::run(bool spawn_thread, bool one_shot) {
    if (impl_->running_) {
        cslog << loglevel(Warning) << "Sequencer thread already running" << endl;
        return false;
    }
    impl_->do_repeats_ = true;
    impl_->running_ = true;
    if (spawn_thread) {
        if (impl_->sequence_.empty()) {
            cslog << loglevel(Info) << "Sequencer has empty sequence." << endl;
        }
        impl_->run_thread_ = std::thread(&SequencerImpl::runPrograms, impl_.get(), one_shot);
    } else {
        return impl_->runPrograms(one_shot);
    }
    return true;
}

void Sequencer::stop(bool immediate) {
    if (immediate) {
        impl_->do_repeats_ = false;
    }
    impl_->running_ = false;
    impl_->bkgnd_running_ = false;
    impl_->run_cv_.notify_one();
}

}  // namespace ctlsvc
