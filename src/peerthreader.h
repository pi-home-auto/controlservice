//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <atomic>
#include <memory>
#include <thread>

namespace ctlsvc {
// Forward references
class Dispatcher;
class Peer;
class Statistics;

/**
 * Runs in a separate thread and responsible for handling a peer connection.
 * Commands are read from the peer's stream and dispatched using a dispatcher.
 */
class PeerThreader {
public:
    /** Construct a PeerThreader */
    PeerThreader(Peer* peer);
    /** Destroy a PeerThreader */
    ~PeerThreader();

    bool start();
    void stop();

    /** Get the peer threader statistics.
     *  @return pointer to the statistics packet.
     */
    Statistics* getStats() const { return stats_.get(); }

    /** Method that runs a message loop from a peer stream.
     *  Commands are executed through a #Dispatcher.
     */
    void handle(Peer* peer);
    bool isRunning() const;

private:
    Peer* peer_;
    /** Used to dispatch messages from peer to correct handler */
    std::unique_ptr<Dispatcher> dispatch_;
    /** Statistics for peer threader activity */
    std::unique_ptr<Statistics> stats_;
    /** If true, try to reconnect a dropped connection */
    bool reconnect_;

    std::atomic_bool is_running_;
    std::thread thread_;
};

}  // namespace ctlsvc
