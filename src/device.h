//
//   Copyright 2020-2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "properties.h"

#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

namespace ctlsvc {
// Forward references
class Statistics;

/** Types of events that can trigger a callback from the device.
 *  Many devices do not support callbacks and those that do would likely
 *  only implement a few of the below possibilities.
 */
enum class DeviceEvent {
    None,
    Close,          // Device was closed
    Error,          // Device encountered an error
    Read,           // Device was read
    Write,          // Device was written
    DataAvailable,  // Device has data that can be read
    SpaceAvailable, // Device is ready to written to
    StateChange,    // Device had some state change
    Timeout         // Device timed out
};

/**
 * This class provides a common, idiomatic interface for interacting with
 * resources. Although a Device can be anything, it is meant to represent
 * hardware such as an LED, button, file, etc.
 */
class Device {
public:
    /** Signature of function that a device trigger will call.
     *  The function is expected to return 0 on success, a positive number
     *  to indicate some condition, or a negative number to indicate "errno".
     */
    using Callback = std::function<int(DeviceEvent, Device&)>;
    using Handle = void *;
    enum Mode {
        Unknown,
        Unopened,
        ReadOpen,
        WriteOpen,
        ReadWriteOpen
    };

protected:
    Device(const std::string& name);

    Device(const Device& other) = delete;
    Device(Device&&) = delete;
    Device& operator=(const Device&) = delete;
public:
    virtual ~Device();

    virtual void deinit() {}
    virtual bool init() { return true; }

    virtual bool close() = 0;
    virtual bool configure(const std::string& propname, const PropertyVar& prop) = 0;
    virtual bool configureSet(const Properties& props);
    virtual bool open(Mode mode) = 0;
    virtual PropertyVar query(const std::string& propname) = 0;
    virtual bool querySet(Properties& props);
    /** Read an integer (or byte, word) from device. */
    virtual bool read(int& value);

    virtual int read(size_t num, void* buf) = 0;
    virtual Mode status() = 0;
    /** Write an integer to device. */
    virtual bool write(int value);
    virtual int write(size_t num, const void* buf) = 0;

    // Optional callback trigger interface
    virtual bool handles(DeviceEvent) { return false; }
    virtual bool is_listening(DeviceEvent) { return false; }
    virtual Handle listen(DeviceEvent, Callback) { return nullptr; }
    virtual bool unlisten(Handle) { return false; }

    // getters and setters
    /** Get the device mode */
    Mode getMode() const { return mode_; }
    /** Get the name of the device */
    std::string getName()  const { return name_; }

private:
    /** Device name */
    const std::string name_;

protected:
    Mode mode_;
};

}  // namespace ctlsvc
