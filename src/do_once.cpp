//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "do_once.h"

namespace ctlsvc {

DoOnce::DoOnce(Func func)
    : func_(func)
    , has_run_(false)
    , count_(0) {}

bool DoOnce::run() {
  if (has_run_) {
    ++count_;
    return false;
  }
  func_();
  has_run_ = true;
  count_ = 0;
  return true;
}

bool DoOnce::run(Func func) {
  func_ = func;
  return run();
}

bool DoOnce::has_run() const {
  return has_run_;
}

void DoOnce::reset() {
  has_run_ = false;
}

bool DoOnce::reset(Func func) {
  // TODO count_ = 0; ??
  if (has_run_) {
    has_run_ = false;
    func();
    return true;
  }
  return false;
}

size_t DoOnce::suppressed() const {
  return count_;
}

}  // namespace ctlsvc
