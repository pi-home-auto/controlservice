//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "forker.h"
#include "logger.h"
#include "options.h"
#include "system.h"

#include <memory>
#include <signal.h>
#include <stdlib.h>

using namespace ctlsvc;
using std::endl;

/**
 * @mainpage Central control service for Homeauto modules.
 * This service runs continuously on all homeauto modules.  It is responsible for
 * connecting modules together, maintaining system state (and status), audit logging,
 * alerts and more.
 *
 * This service also controls hardware devices through dynamically loaded handlers.
 * Each handler can have a request/response method as well as a loop running in
 * a separate thread.
 */

static ControlService* service = nullptr;
static bool quitAgain = false;

// Signal handler
void quit(int sig) {
    cslog << endl << "Exiting...";
    csaudit << loglevel(Warning) << "Signal " << sig << " received. Exiting..." << endl;

    // try to exit as cleanly as possible
    if (service != nullptr) {
        //TODO if service BCS call shutdown, else goodbye
        service->stop();
        if (quitAgain) {
            // OK, the second time force it
            delete service;
            service = nullptr;
        } else {
            quitAgain = true;
        }
    } else {
        exit(3);
    }
    cslog << endl;
}

int main(int argc, char* argv[]) {
    Forker forker;
    cslog << loglevel(Info) << "Homeauto Control Service initializing..." << endl;

    std::unique_ptr<Options> options = std::make_unique<Options>();
    if (!options->readConfig()) {
      cslog << loglevel(Error) << "Error reading configuration file -- exiting." << endl;
      exit(2);
    }
    options->setConfig();
    auto csType = options->parseCommandLine(argc, argv);
    if (csType == Options::CS_BASE) {
      forker.init();
    }
    switch (csType) {
      case Options::CS_BASE:
        service = BaseControlService::instance();
        break;
      case Options::CS_SHUTDOWN:
        System::shutdown(options->getWhenDown());
        exit(0);
        break;
      case Options::CS_INVALID:
        cslog << loglevel(Error) << "Must specify service type or shutdown (-b or -S) -- exiting."
              << endl;
        exit(2);
    }
    if (service != nullptr) {
      service->setForker(&forker);
    }

    auto lll = options->getAuditLogLevel();
    cslog << loglevel(Info) << "Set loglevel on csaudit stream to " << lll << endl;
    cslog.setAuditLogLevel(lll);
    lll = options->getLogLevel();
    cslog << loglevel(Info) << "Set loglevel on cslog stream to " << lll << endl;
    cslog.setDefaultLogLevel(lll);

    signal(SIGINT,  quit);
    signal(SIGQUIT, quit);

    service->init(std::move(options));
    service->run();
    cslog << loglevel(Info) << "Going to wait for threads" << endl;
    service->wait();

    delete service;
    exit(0);
}
