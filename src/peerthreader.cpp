//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <cerrno>
#include <iostream>
#include <unistd.h>
#include <sockstr/Stream.h>

#include "controlservice.h"
#include "dispatcher.h"
#include "eval/events.h"
#include "ipcmessage.h"
#include "logger.h"
#include "peer.h"
#include "peerthreader.h"
#include "protocol.h"
#include "statistics.h"
using std::endl;
using namespace sockstr;

namespace ctlsvc {

PeerThreader::PeerThreader(Peer* peer)
    : peer_(peer)
    , dispatch_(std::make_unique<Dispatcher>())
    , stats_(std::make_unique<Statistics>("Peer Statistics"))
    , reconnect_(true)
    , is_running_(false) {

    stats_->newStat("requests", 0);
    stats_->newStat("errors", 0);
}

PeerThreader::~PeerThreader() {}

bool PeerThreader::start() {
    if (is_running_) {
        cslog << loglevel(Error) << "Error: PeerThreader was already started" << endl;
        return false;
    }
    is_running_ = true;
    thread_ = std::thread(&PeerThreader::handle, this, peer_);
    cslog << loglevel(Info) << "PeerThreader started for " << peer_->getName() << endl;
    return true;
}

void PeerThreader::stop() {
    if (!is_running_) {
        cslog << loglevel(Error) << "Error: PeerThreader was already stopped" << endl;
        return;
    }
    cslog << loglevel(Info) << "PeerThreader stopping peer " << peer_->getName() << endl;
    is_running_ = false;
    if (thread_.joinable()) {
        thread_.join();
    }
}

void PeerThreader::handle(Peer* peer) {
    cslog << "Peer thread started" << endl;
    Stream& client = *peer->getStream();
    ControlService* cs = ControlService::instance();

    // auto gpio = ControlService::getHandler("gpio");
    // cslog << loglevel(Info) << "Gpio handler is " << gpio << endl;
    // IpcControl blinkStatusLed("gpio", "blink");
    // blinkStatusLed.addMember("param1", 0);   // 0 is status light
    // blinkStatusLed.addMember("param2", 20);  // delay in msecs
    // IpcReply gpioReply;

    while (isRunning() && peer->isRunning() && client.good() && client.is_open()) {
        std::string strbuf;
        errno = 0;
        unsigned int sz = client.read(strbuf, COMMAND_END);
        if (sz == 0) {
            cslog << "Nothing read from peer -- continuing" << endl;
            continue;
        }
        if (errno) {
            cslog << loglevel(Error) << "Read got errno=" << errno << endl;
            if (errno == EAGAIN) continue;
            break;
        }

        // show indication of activity
        stats_->increment("requests");
        // if (gpio != nullptr) {
        //     gpio->request(blinkStatusLed, gpioReply, nullptr);
        // }

        // dispatch command
        IpcMessage msg;
        if (msg.fromString(strbuf)) {
            cslog << loglevel(Debug) << "Got IPC=" << msg.toShortString() << endl;
            if (msg.getId() == IpcShutdown::ID) {
                break;
            } else if (msg.getId() == IpcGoodbye::ID) {
                break;
            } else if (msg.getId() == IpcStatistics::ID) {
                std::string detail = msg.getMember(IpcStatistics::DETAIL);
                IpcStatistics stats(detail);
                cs->getStats(stats);
                cslog << "Send stats: " << stats.toShortString() << endl;
                client << stats.toString() << endl;
                continue;
            }

            IpcReply reply;
            //TODO get return code from execute and obey reply
            dispatch_->execute(msg, reply, peer);
            if (reply.getId() == "NoReply") {
                cslog << loglevel(Debug) << "No reply for dispatch" << endl;
            } else {
                cslog << loglevel(Debug) << "client reply: " << reply.toShortString() << endl;
                client << reply.toString() << endl;
            }
        } else {
            cslog << loglevel(Error) << "Invalid json from client: " << strbuf << std::flush;
            IpcReply reply(IpcControl::ERROR, "Invalid json");
            client << reply.toString() << endl;
            stats_->increment("errors");
        }
    }

    std::string alertmsg("Peer thread exited for ");
    alertmsg.append(peer->getName());
    cslog << alertmsg << endl;
    auto alert = std::make_shared<eval::EventAlert>(alertmsg);
    cs->pushEvent(alert);
    //if (peer->isRunning()) peer->stop();
    //TODO if client is open then send a shutdown
    if (client.is_open()) {
        client.close();
    }
    //delete peer;
    cs->deletePeer(peer);
}

bool PeerThreader::isRunning() const {
  return is_running_;
}

}  // namespace ctlsvc
