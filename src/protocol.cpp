//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "protocol.h"

namespace ctlsvc
{

const char COMMAND_END[] = "\r\n\r\n";
const char COMMAND_ENDL[] = "\r\n\r";	// useful for << COMMAND_ENDL << endl

const char CS_VERSION[] = "0.1.3";
const char CS_ROLE_BCS[] = "bcs";

}
