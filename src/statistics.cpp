//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <ctime>
#include <sstream>

#include "ipcmessage.h"
#include "logger.h"
#include "statistics.h"
using std::endl;

namespace ctlsvc {

// static class member instanciation
Statistics::StatsMap Statistics::sharedStats_;
time_t Statistics::sharedSince_ = time(0);


StatValue::StatValue()
    : value_() {}

StatValue::StatValue(int value)
    : value_(value) {}

StatValue::StatValue(float value)
    : value_(value) {}

StatValue::StatValue(double value)
    : value_(value) {}

void StatValue::add(StatValue delta) {
    struct AddValue {
        AddValue(StatValue::ValueType& vt) : vt_(vt) {}

        void operator()(std::monostate&) {
            cslog << loglevel(Error) << "Invalid stat type for add" << endl;
        }
        void operator()(int& delta) {
            std::get<int>(vt_) += delta;
        }
        void operator()(float& delta) {
            std::get<float>(vt_) += delta;
        }
        void operator()(double& delta) {
            std::get<double>(vt_) += delta;
        }
        StatValue::ValueType& vt_;
    };
    AddValue av(value_);
    std::visit(av, delta.value());
}

void StatValue::increment() {
    struct IncValue {
        void operator()(std::monostate&) {
            cslog << loglevel(Error) << "Invalid stat type for increment" << endl;
        }
        void operator()(int& value) {
            value++;
        }
        void operator()(float& value) {
            value++;
        }
        void operator()(double& value) {
            value++;
        }
    };
    std::visit(IncValue(), value_);
}

int StatValue::getInt() const {
    return std::get<int>(value_);
}

float StatValue::getFloat() const {
    return std::get<float>(value_);
}

double StatValue::getDouble() const {
    return std::get<double>(value_);
}

std::string StatValue::toString() const {
    struct ValueToString {
        std::string operator()(const std::monostate&) {
            return "Invalid stat type";
        }
        std::string operator()(const int& value) {
            return std::to_string(value);
        }
        std::string operator()(const float& value) {
            return std::to_string(value);
        }
        std::string operator()(const double& value) {
            return std::to_string(value);
        }
    };
    return std::visit(ValueToString(), value_);
}

StatValue::ValueType& StatValue::value() {
    return value_;
}

const StatValue::ValueType& StatValue::value() const {
    return value_;
}


Statistics::Statistics(const std::string& name)
    : name_(name)
    , since_(time(0)) {}

Statistics::~Statistics() {
    stats_.clear();
}

int Statistics::getInt(const std::string& statName) const {
    auto it = stats_.find(statName);
    if (it != stats_.end()) {
        return it->second.getInt();
    }
    return sharedStats_.at(statName).getInt();
}

float Statistics::getFloat(const std::string& statName) const {
    auto it = stats_.find(statName);
    if (it != stats_.end()) {
        return it->second.getFloat();
    }
    return sharedStats_.at(statName).getFloat();
}

double Statistics::getDouble(const std::string& statName) const {
    auto it = stats_.find(statName);
    if (it != stats_.end()) {
        return it->second.getDouble();
    }
    return sharedStats_.at(statName).getDouble();
}

time_t Statistics::getInitTime(bool shared) const {
    return shared ? sharedSince_ : since_;
}

void Statistics::newStat(const std::string& statName, int start, bool shared) {
    stats_[statName] = StatValue(start);
}

void Statistics::newStat(const std::string& statName, float start, bool shared) {
    stats_[statName] = StatValue(start);
}

void Statistics::newStat(const std::string& statName, double start, bool shared) {
    stats_[statName] = StatValue(start);
}

void Statistics::increment(const std::string& statName) {
    auto it = stats_.find(statName);
    if (it == stats_.end()) {
        cslog << loglevel(Error) << "Error: Statistic \"" << statName << "\" not found" << endl;
    } else {
        StatValue& sv = it->second;
        sv.increment();
    }
}

bool Statistics::toMessage(IpcMessage& msg, bool shared) const {
    const StatsMap& sm = shared ? sharedStats_ : stats_;
    if (msg.getId().empty()) msg.setId("statistics");
    
    for (const auto& stat : sm) {
        msg.addMember(stat.first, stat.second.toString());
    }

    // include timestamp
    msg.addMember("since", static_cast<int>(shared ? sharedSince_ : since_));

    return true;
}

std::string Statistics::toString() const {
    std::ostringstream oss("\"statistics\": {", std::ios::app);
    bool firstTime = true;
    for (const auto& [name, value] : stats_) {
        if (firstTime) {
            firstTime = false;
        } else {
            oss << ",";
        }
        oss << " \"" << name << "\": " << value.toString();
    }
    oss << " }";
    return oss.str();
}

}  // namespace ctlsvc
