//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "logger.h"

#include <algorithm>
#include <condition_variable>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <fstream>
#include <functional>
#include <iostream>
#include <mutex>
#include <queue>
#include <thread>
#include <unistd.h>

using std::cout;
using std::endl;

const int ctlsvc::Trace = 0;
const int ctlsvc::Debug = 1;
const int ctlsvc::Info = 2;
const int ctlsvc::Warning = 3;
const int ctlsvc::Error = 4;
const int ctlsvc::Fatal = 5;

namespace {

static const char* const LevelLetter = "TDIWEF";
static const char* const LevelName[] = {
    "Trace", "Debug", "Info", "Warning", "Error", "Fatal"
};
// Signal EOF when streambuf is closed
constexpr unsigned int LOGSTREAM_EOF = 311;

unsigned int getCurrentTime(char* buf, size_t bufSize) {
    time_t now = time(0);
    struct tm tstruct;
    localtime_r(&now, &tstruct);
    strftime(buf, bufSize, "%Y-%m-%d.%X  ", &tstruct);
    return strlen(buf);
}

}  // namespace

ctlsvc::Logger& ctlsvc::csalert = *new Logger(ALERT);
ctlsvc::Logger& ctlsvc::csaudit = *new Logger(AUDIT, "csaudit.log", true);
ctlsvc::Logger& ctlsvc::cslog = *new Logger(LOG /*, "cslog.log"*/);

namespace ctlsvc {

class NullBuffer : public std::streambuf {
public:
    int overflow(int c) {
        return std::char_traits<char>::not_eof(c);
    }
};
static NullBuffer gNullBuffer;


class LogStreamBufImpl {
public:
    LogStreamBufImpl(CsLogType logType, const std::string& file);
    ~LogStreamBufImpl() {
        close();
    }

    void close();
    void queueBuffer(const char* buf, unsigned int sz);
    void writeBuffers();

    CsLogType  logType_;
    CsLogLevel logLevel_;
    CsLogLevel auditLogLevel_;
    CsLogLevel defaultLogLevel_;

    unsigned int preambleSize_;
    char buf_[1024];
    std::filebuf fileBuf_;

    // Elements in the queue consist of size of buffer and buffer pointer (either can be 0)
    std::queue<std::pair<int, char*>> queue_;
    std::thread thread_;
    std::mutex mutex_;
    std::condition_variable cond_;
};

LogStreamBufImpl::LogStreamBufImpl(CsLogType logType, const std::string& file)
    : logType_(logType)
    , logLevel_(Debug)
    , auditLogLevel_(Fatal + 1) // no audit by default
    , defaultLogLevel_(Debug)
    , preambleSize_(24) {
    if (logType == LOG) {
        preambleSize_ = 0;
    }

    if (logType != INVALID) {
        if (file.empty()) {
            // As daemon, trying to open tty will fail
            if (!fileBuf_.open("/dev/tty", std::ios::out)) {
                std::cerr << "Failed to open /dev/tty for logging" << endl;
                fileBuf_.open("/dev/null", std::ios::out);
            }
        } else {
            std::string logFile;
            if (file[0] != '/' || file[0] != '.') {
                bool append_logs = false;
                const char* home = getenv("HA_LOG_DIR");
                if (!home || !home[0]) {
                  home = getenv("HOME");
                  append_logs = true;
                }
                if (!home || !home[0]) {
                  home = DEFAULT_HOMEDIR;
                  append_logs = true;
                }
                logFile = std::string(home) + "/";
                if (append_logs) {
                  logFile.append("logs/");
                }
                logFile.append(file);
            } else {
                logFile = file;
            }
            if (!fileBuf_.open(logFile, std::ios::out /*| ios::app*/)) {
                std::cerr << "Could not open file " << logFile << " for logging" << endl;
                fileBuf_.open("/dev/null", std::ios::out);
            }
        }
    } else {
        if (file.empty()) {
            fileBuf_.open("/dev/tty", std::ios::out);
        } else {
            fileBuf_.open(file, std::ios::out);
        }
    }
    thread_ = std::thread(std::bind(&LogStreamBufImpl::writeBuffers, this));
}

void LogStreamBufImpl::close() {
    queueBuffer(nullptr, LOGSTREAM_EOF);
    if (thread_.joinable()) {
        thread_.join();
    }
    fileBuf_.close();
}

void LogStreamBufImpl::queueBuffer(const char* buf, unsigned int sz) {
    char* out = new char[sz];
    memcpy(out, buf, sz);
    std::lock_guard lock(mutex_);
    queue_.push({sz, out});
    cond_.notify_one();
    //cout << "queueBuffers: " << sz << endl;
}

void LogStreamBufImpl::writeBuffers() {
    while (true) {
        {
            std::unique_lock lock(mutex_);
            cond_.wait(lock, [this]{ return !queue_.empty(); });
            auto [sz, buf] = queue_.front();
            queue_.pop();
            if (buf == nullptr && sz == LOGSTREAM_EOF) {
                break;
            }
            fileBuf_.sputn(buf, sz);
            fileBuf_.pubsync();
            delete [] buf;
        }
    }
}


LogStreamBuf::LogStreamBuf(CsLogType logType, const std::string& file)
    : impl_(std::make_unique<LogStreamBufImpl>(logType, file)) {
    setbuf(impl_->buf_ + impl_->preambleSize_, sizeof(impl_->buf_) - impl_->preambleSize_);
}

int LogStreamBuf::sync() {
    // cout << std::hex << "before sync logstream, pbase=" << static_cast<void*>(pbase())
    //      << ", pptr= " << static_cast<void*>(pptr())
    //      << ", epptr=" << static_cast<void*>(epptr()) << endl;

    // Send non-debug logs to audit, too
    if (impl_->logType_ == LOG && impl_->auditLogLevel_ <= impl_->logLevel_) {
        csaudit.setLogLevel(impl_->logLevel_);
        csaudit.write(pbase(), pptr() - pbase());
        csaudit.flush();
    }

    //TODO have a callback that fills the preamble
    if (impl_->preambleSize_ > 20) {
        //TODO peek that last char is newline
        char timestamp[impl_->preambleSize_ + 1];
        unsigned int szPreamble = getCurrentTime(timestamp, impl_->preambleSize_);
        memcpy(impl_->buf_, timestamp, szPreamble);
        memset(&impl_->buf_[szPreamble], ' ', impl_->preambleSize_ - szPreamble);
        if (impl_->logLevel_ >= Trace && impl_->logLevel_ <= Fatal) {
            impl_->buf_[szPreamble - 1] = LevelLetter[impl_->logLevel_];
        }

        int savePos = pptr() - impl_->buf_;
        setp(impl_->buf_, epptr());
        pbump(savePos);
    }
    //cout << hex << "after sync logstream, pbase=" << static_cast<void*>(pbase())
    //     << ", pptr= " << static_cast<void*>(pptr())
    //     << ", epptr=" << static_cast<void*>(epptr()) << endl;

    int ret = pptr() - impl_->buf_;
    impl_->queueBuffer(impl_->buf_, ret);

    std::filebuf::sync();
    if (impl_->preambleSize_ > 0) {
        setp(impl_->buf_ + impl_->preambleSize_, epptr());
    }

    setLogLevel(impl_->defaultLogLevel_);    // Reset to default loglevel
    return ret;
}


CsLogLevel LogStreamBuf::getLogLevel() const { return impl_->logLevel_; }
void LogStreamBuf::setAuditLogLevel(CsLogLevel level) { impl_->auditLogLevel_ = level; }
void LogStreamBuf::setLogLevel(CsLogLevel level) { impl_->logLevel_ = level; }
void LogStreamBuf::setDefaultLogLevel(CsLogLevel level) { impl_->defaultLogLevel_ = level; }


Logger::Logger(CsLogType logType, const std::string& file, bool append_pid)
    : lowLogLevel_(Debug)
    , streamBuf_(logType, append_pid ? file + "." + std::to_string(getpid()) : file)
    , nullStream_(&gNullBuffer) {
    std::ostream::rdbuf(&streamBuf_);
    streamBuf_.open("/dev/null", std::ios::out);
}

Logger::~Logger() {
    close();
}

CsLogLevel Logger::getLowLogLevel() const {
    return lowLogLevel_;
}

std::ostream& Logger::getNullStream() {
    return nullStream_;
}

const std::ostream& Logger::getNullStream() const {
    return nullStream_;
}

void Logger::setAuditLogLevel(CsLogLevel level) {
    streamBuf_.setAuditLogLevel(level);
}

void Logger::setDefaultLogLevel(ctlsvc::CsLogLevel level) {
    streamBuf_.setDefaultLogLevel(level);
    lowLogLevel_ = level;
}

void Logger::setLogLevel(ctlsvc::CsLogLevel level) {
    streamBuf_.setLogLevel(level);
}

void Logger::writeAlert(const std::string& msg) {
    write("ALERT: ", 7);
    *this << msg << endl;
}

void Logger::writeAudit(const std::string& msg) {
    write("AUDIT: ", 7);
    *this << msg << endl;
}

void Logger::writeLog(const std::string& msg) {
    *this << msg << endl;
}

_Level loglevel(CsLogLevel level) {
    _Level ret = { level };
    return ret;
}

std::ostream& operator<<(Logger& logger, _Level level) {
    logger.setLogLevel(level.level);
    if (logger.getLowLogLevel() > level.level) {
        return logger.getNullStream();
    }
    return logger;
}

}  // namespace ctlsvc
