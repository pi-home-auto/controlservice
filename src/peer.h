//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <memory>
#include <string>

namespace sockstr {
// Forward references
class Stream;
}

namespace ctlsvc {
// Forward references
class PeerThreader;

using CookieType = int;

/** The type of peer. */
enum class PeerType {
  None, Control, Rest, UserApp, Tester, Transient
};

/**
 *  Data holder and start/stop controller for a peer connection.
 */
class Peer {
public:
  Peer(const std::string& name, sockstr::Stream* stream, PeerType type = PeerType::Control);
  virtual ~Peer();

  /** Spawn a thread to run this peer using PeerThreader */
  void start();
  /** Stop the peer by signalling an interrupt to the PeerThreader.
   *  An IpcGoodbye message is sent if the other side is still listening.
   */
  void stop();

  // Getters
  /** Get the peer cookie that uniquely identifies this peer connection */
  CookieType getCookie() const { return cookie_; }
  /** Set the peer cookie that uniquely identifies this peer connection */
  void setCookie(CookieType cookie) { cookie_ = cookie; }

  /** Get the name of the peer */
  std::string getName() const { return name_; }
  /** Get the (socket) stream associated with peer. */
  sockstr::Stream* getStream() const { return stream_; }
  /** Check if peer is currently connected. */
  bool isConnected() const { return isConnected_; }
  /** Check if peer is still running. */
  bool isRunning() const;
  bool getPingResponse() const { return pingResponse_; }
  void setPingResponse(bool response);

private:
  std::string name_;
  PeerType type_;
  sockstr::Stream* stream_;
  std::unique_ptr<PeerThreader> peerThreader_;
  CookieType cookie_;

  bool isConnected_;
  bool pingPending_;
  bool pingResponse_;
};

}  // namespace sockstr
