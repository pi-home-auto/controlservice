//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <functional>
#include <memory>
#include <string>
#include <variant>
#include <vector>

namespace ctlsvc {
namespace driver {

// TODO: use Structured Data Name instead of string
using DriverName = std::string;

enum class DriverStatus {
    Unknown, Success, Fail, Ignore, Interrupt, AlreadyOpen, NotFound, InvalidParameter, Closed, Unsupported
};

using BufSize = uint64_t;

class DriverBuffer {
public:
    DriverBuffer();
    DriverBuffer(int size);
    DriverBuffer(const std::byte* data, BufSize size);
    DriverBuffer(const std::vector<std::byte>& data);
    DriverBuffer(const std::string& str);
    ~DriverBuffer();

    std::byte* data();
    const std::byte* data() const;
    BufSize size() const;
    /* TODO change to templated writeValue<T>(T value) */
    void writeInt(int value);
    void writeString(const std::string& str);

private:
    std::unique_ptr<std::byte[]> buf_;
    BufSize size_;
};

using DriverHandle = void *;

enum class DriverAlert {
    Unknown, Error, PinHigh, PinLow, ReadReady, WriteReady, Timer
};

using DriverListener = std::function<int(DriverHandle, DriverAlert)>;

enum class DevMode {
    None, Read, Write, ReadWrite
};

// 
class Driver {
public:
    virtual ~Driver() = default;

    virtual void close(DriverHandle handle) = 0;
    virtual bool listen(DriverHandle handle, DriverListener listener) = 0;
    virtual DriverName name() const = 0;
    virtual DriverHandle open(const DriverName& instance_name, DevMode mode) = 0;
    virtual BufSize read(DriverHandle handle, DriverBuffer& buffer) = 0;
    virtual void unlisten(DriverHandle handle) = 0;
    virtual BufSize write(DriverHandle handle, const DriverBuffer& buffer) = 0;

    // TODO get a variant of the driver extension for this class of driver
    // templated method to get extention class

};

}  // namespace driver
}  // namespace ctlsvc
