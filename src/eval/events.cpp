//
//   Copyright 2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "events.h"
#include <array>

namespace ctlsvc {
namespace eval {

DelayedEvent& DelayedEvent::operator=(const DelayedEvent& other) {
    if (this != &other) {
        Event::operator=(other);
        delay_ = other.delay_;
        wrappedEvent_ = other.wrappedEvent_;
        cancelled_ = other.cancelled_.load();
    }
    return *this;
}


EventAlert::EventAlert(const std::string& message, int priority)
    : Event("alert")
    , message_(message)
    , priority_(priority) {}

const std::string& EventAlert::message() const {
  return message_;
}

const int EventAlert::priority() const {
  return priority_;
}


EventDistance::EventDistance(int sensor, int value)
    : Event("distance")
    , sensor_(sensor) {
    setValue(value);
}

int EventDistance::sensor() const {
    return sensor_;
}


EventInit::EventInit(int instance)
    : Event("init") {
    setValue(instance);
}


EventNotice::EventNotice(int value, const std::string& note)
    : Event("Notice " + std::to_string(value))
    , value_(value)
    , note_(note) {}

int EventNotice::value() const {
    return value_;
}

const std::string& EventNotice::note() const {
    return note_;
}


EventPinChanged::EventPinChanged(int pin, int value)
    : Event("Pin Changed " + std::to_string(pin) + ":" + std::to_string(value))
    , pin_(pin) {
    setValue(value);
}

int EventPinChanged::pin() const {
    return pin_;
}


EventFileAdded::EventFileAdded(const std::string& fileName, const std::string& contents)
    : Event("File Added")
    , fileContents_(contents)
    , fileName_(fileName) {}

const std::string& EventFileAdded::fileContents() const {
    return fileContents_;
}

const std::string& EventFileAdded::fileName() const {
    return fileName_;
}


EventFileChanged::EventFileChanged(const std::string& fileName, const std::string& contents)
    : Event("File Changed")
    , fileContents_(contents)
    , fileName_(fileName) {}

const std::string& EventFileChanged::fileContents() const {
    return fileContents_;
}

const std::string& EventFileChanged::fileName() const {
    return fileName_;
}


EventFileDeleted::EventFileDeleted(const std::string& fileName)
    : Event("File Deleted")
    , fileName_(fileName) {}

const std::string& EventFileDeleted::fileName() const {
    return fileName_;
}


EventUser::EventUser(const std::string& label)
    : Event("User")
    , label_(label) {}

const std::string& EventUser::label() const {
    return label_;
}

}  // namespace eval
}  // namespace ctlsvc
