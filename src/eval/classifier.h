//
//   Copyright 2022-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "tokenizer.h"
#include <functional>
#include <iostream>
#include <vector>


namespace ctlsvc {
class Properties;
namespace eval {
class Context;

using TokenArray = std::vector<Token>;
using TokenIndex = TokenArray::size_type;
using TokenGroup = std::pair<TokenIndex, TokenIndex>;

enum class PhraseType {
    Unknown, RuleBase, DefEvent, SetVar, Rule, Action, Event, Name, Value, Parameters, Block,
    Predicate, IfElse, IfAction, ElseAction, Cancel, Send, Condition,
    _Count
};

/** RbNode (short for Rulebase Node) is used as the elements of the parsing tree.
 *
 *  Each node has a phrase (or grammar) type and a range of indices into an array
 *  of Token.
 */
struct RbNode {
    RbNode(PhraseType pt = PhraseType::Unknown)
        : type(pt) {}
    ~RbNode() {
        for (auto child : children) {
            delete child;
        }
    }
    PhraseType type;
    TokenGroup range;
    std::vector<RbNode*> children;
};


class Classifier {
public:
    using Visitor = std::function<bool(const RbNode*, Classifier&)>;

    Classifier(TokenArray&& tokens, Context* context);
    ~Classifier() = default;

    void collectParameters(const RbNode* node, const std::vector<std::string>& names, Properties& props);
    void createTree();
    const TokenArray& getTokens() const { return tokens_; }
    std::string getTokenStr(const RbNode* node) const;
    RbNode& getTree() { return tree_; }
    const RbNode& getTree() const { return tree_; }
    void printTree(int indent = 0, const RbNode* tree = nullptr);
    std::string replaceVars(const std::string& input) const;

    /** Call the visitor with each node of the whole tree. First the node itself is visited
     *  and then each child (recursively).
     *  @param visitor Visitor that will be called.
     *  @param tree Root node of tree to visit. If nullptr then this class's internal tree is visited.
     */
    size_t visit(Visitor visitor, const RbNode* tree = nullptr);
    /** Call the visitor with each child of the specified node. Only direct children of the top
     *  node are visited.
     *  @param visitor Visitor that will be called.
     *  @param tree Root node of tree to visit. If nullptr then this class's internal tree is visited.
     */
    size_t visitChildren(Visitor visitor, const RbNode* tree = nullptr);
private:
    RbNode* getParams(size_t start, size_t end);
    RbNode* processAction(size_t start, size_t end);
    RbNode* processBlock(size_t start, size_t end);
    RbNode* processActionBlock(size_t start, size_t end);
    RbNode* processEvent(size_t start, size_t end);
    RbNode* processPredicate(size_t start, size_t end);
    bool processStatement(RbNode* node);

private:
    TokenArray tokens_;
    RbNode tree_;
    Context* context_;
};

std::ostream& operator<<(std::ostream& os, const ctlsvc::eval::RbNode& node);

}  // namespace eval
}  // namespace ctlsvc

