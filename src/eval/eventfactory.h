//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "events.h"
#include "properties.h"
#include <memory>
#include <unordered_map>
#include <vector>

namespace ctlsvc {
namespace eval {

template<typename T, typename... Tp>
std::shared_ptr<T> make_event(Tp&&... params) {
    return std::make_shared<T>(std::forward<Tp>(params)...);
}

class EventFactory {
public:
    using EventPtr = std::shared_ptr<Event>;
    using EventMap = std::unordered_map<std::string, std::weak_ptr<const Event>>;
private:
    EventFactory() = default;
public:
    ~EventFactory() = default;

    static EventFactory* instance();
    bool defineEvent(const std::string& name, const std::string& label, int value = -1);
    EventPtr makeEvent(const std::string& name, const Properties& params = Properties());
    std::vector<std::string> parameterNames(const std::string& name);

private:
    static EventFactory* instance_;
    EventMap cache_;
};

}  // namespace eval
}  // namespace ctlsvc
