//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "actionfactory.h"
#include "actions.h"
#include "properties.h"
#include <cstdlib>

namespace ctlsvc {
namespace eval {
namespace ActionFactory {

std::vector<std::string> parameterNames(const std::string& _name) {
    std::vector<std::string> names;
    std::string name = _name.substr(0, 7) == "action:" ? _name.substr(7) : _name;
    if (name == "debug") {
        names.push_back("message");
    } else if (name == "playSound") {
        names.push_back("file");
    } else if (name == "readPin") {
        names.push_back("pin");
        names.push_back("varName");
    } else if (name == "run") {
        names.push_back("command");
        names.push_back("parameters");
    } else if (name == "runProgram") {
        names.push_back("program");
        names.push_back("repeat");
    } else if (name == "servo") {
        names.push_back("servo");
        names.push_back("degrees");
        names.push_back("relative");
    } else if (name == "setPin") {
        names.push_back("pin");
        names.push_back("value");
    }
   return names;
}

Action* makeAction(const std::string& _name, const Properties& params) {
    Action* action = nullptr;
    std::string name = _name.substr(0, 7) == "action:" ? _name.substr(7) : _name;
    if (name == "debug") {
        auto msg = params.getString("message");
        action = new ActionDebug(msg);
    } else if (name == "readPin") {
        int pin;
        auto varName = params.getString("varName");
        if (params.getAsInt("pin", pin) && !varName.empty()) {
            action = new ActionReadPin(pin, varName);
        }
    } else if (name == "run") {
        auto cmd = params.getString("command");
        auto args = params.getString("parameters");
        if (!cmd.empty()) {
          action = new ActionRun(cmd, args);
        }
    } else if (name == "runProgram") {
      int repeat = 1;
      params.getAsInt("repeat", repeat);
      auto prog = params.getString("program");
      if (!prog.empty()) {
        action = new ActionRunProgram(prog, repeat);
      }
    } else if (name == "servo") {
        int servo;
        int degrees;
        if (params.getAsInt("servo", servo) && params.getAsInt("degrees", degrees)) {
            auto relative = params.getString("relative");
            bool is_relative = false;
            if (relative == "true") {
                is_relative = true;
            }
            auto servoact = new ActionServo(servo, degrees, is_relative);
            if (servoact->is_valid()) {
                action = servoact;
            } else {
                delete servoact;
            }
        }
    } else if (name == "setPin") {
        int pin;
        int value;
        if (params.getAsInt("pin", pin) && params.getAsInt("value", value)) {
            action = new ActionSetPin(pin, value);
        }
    } else if (name == "playSound") {
        auto file = params.getString("file");
        if (!file.empty()) {
            action = new ActionPlaySound(file);
        }
    } else if (name == "stopSound") {
        action = new ActionStopSound();
    }
    if (!action) {
        action = new ActionDebug("Cannot create action " + name);
    }
    return action;
}

}  // namespace ActionFactory
}  // namespace eval
}  // namespace ctlsvc
