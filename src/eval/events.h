//
//   Copyright 2021-2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "evaluator.h"
#include <atomic>

namespace ctlsvc {
namespace eval {

/* TODO define new events: EventDeinit  */
class DelayedEvent : public Event {
public:
    using EventPtr = std::shared_ptr<Event>;

    template<typename T, typename P>
    DelayedEvent(const std::chrono::duration<T,P>& delay, EventPtr event)
        : Event("Delayed Event")
        , delay_(std::chrono::duration_cast<decltype(delay_)>(delay))
        , wrappedEvent_(event)
        , cancelled_(false) {
        setTimestamp(std::chrono::steady_clock::now() + delay);
    }
    virtual ~DelayedEvent() = default;
    DelayedEvent& operator=(const DelayedEvent&);

    void cancel() { cancelled_ = true; }
    bool cancelled() const { return cancelled_; }
    std::chrono::microseconds getDelay() const {
        return delay_;
    }
    EventPtr getWrappedEvent() const {
        return wrappedEvent_;
    }
private:
    std::chrono::microseconds delay_;
    EventPtr wrappedEvent_;
    std::atomic_bool cancelled_;
};

/** Any subsystem (such as device, driver or handler) will send an alert event
 *  in case of an error that requires attention. An "on event:alert" rule
 *  must be defined that specifies the actual handling of the alert. Actions
 *  can be writing to a database, sending an email, pushing a notification to a phone,
 *  etc.
 */
class EventAlert : public Event {
public:
  EventAlert(const std::string& message, int priority = 1);
  virtual ~EventAlert() = default;

  const std::string& message() const;
  const int priority() const;

private:
  std::string message_;
  int priority_;
};

class EventDistance : public Event {
public:
  EventDistance(int sensor, int value);
  virtual ~EventDistance() = default;

  int sensor() const;
private:
  int sensor_;
};

class EventInit : public Event {
public:
    EventInit(int instance = 0);
    virtual ~EventInit() = default;
};

class EventNotice : public Event {
public:
    EventNotice(int value, const std::string& note);
    virtual ~EventNotice() = default;

    int value() const;
    const std::string& note() const;
private:
    int value_;
    std::string note_;
};

class EventPinChanged : public Event {
public:
    EventPinChanged(int pin, int value);
    virtual ~EventPinChanged() = default;

    int pin() const;
private:
    int pin_;
};

class EventFileAdded : public Event {
public:
    EventFileAdded(const std::string& fileName, const std::string& contents);
    virtual ~EventFileAdded() = default;

    const std::string& fileContents() const;
    const std::string& fileName() const;
private:
    std::string fileContents_;
    std::string fileName_;
};

class EventFileChanged : public Event {
public:
    EventFileChanged(const std::string& fileName, const std::string& contents);
    virtual ~EventFileChanged() = default;

    const std::string& fileContents() const;
    const std::string& fileName() const;
private:
    std::string fileContents_;
    std::string fileName_;
};

class EventFileDeleted : public Event {
public:
    EventFileDeleted(const std::string& fileName);
    virtual ~EventFileDeleted() = default;

    const std::string& fileName() const;
private:
    std::string fileName_;
};

/** Used for events defined in a rulebase. */
class EventUser : public Event {
public:
    EventUser(const std::string& label);
    virtual ~EventUser() = default;

    const std::string& label() const;
private:
    std::string label_;
};

}  // namespace eval
}  // namespace ctlsvc
