//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "evaluator.h"

#include <memory>
#include <string>
#include <vector>

namespace ctlsvc {
namespace eval {
class RulebaseImpl;

class Rulebase {
public:
    Rulebase(const std::string& rbname = std::string());
    ~Rulebase();

    bool addRule(const Rule& rule);
    const std::string& name() const;
    size_t parse(const std::string& filename, const Context& context);
    const std::vector<Rule>& rules() const;

private:
    std::unique_ptr<RulebaseImpl> impl_;
};

}  // namespace eval
}  // namespace ctlsvc
