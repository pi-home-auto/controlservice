//
//   Copyright 2022, 2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "tokenizer.h"
#include <sstream>

namespace {

constexpr char kEscapeChar = '\\';
constexpr char kVarStartChar = '$';
const std::string kWhiteSpace(" \n\r\t");
const std::string kCommentStart("/*");
const std::string kCommentEnd("*/");
const std::string kCommentToEOL("//");
const std::string kSpecialChars(".,;/*()[]{}");
const std::string kQuoteChar("'\"");
const std::string kValidNameBegin("_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
const std::string kValidNameChars(kValidNameBegin + "0123456789");

}  // namespace

namespace ctlsvc {
namespace eval {

std::ostream& operator<<(std::ostream& os, ReadType type) {
    const char* names[] = {
        "Unknown", "Normal", "WhiteSpace", "Comment", "Special", "Escaped", "Quote",
        "VariableStart", "VariableName", "UnclosedQuote", "EndOF"
    };
    static_assert((sizeof(names) / sizeof(names[0])) == static_cast<int>(ReadType::_Count));
    int ti = static_cast<int>(type);
    if (type >= ReadType::_Count) {
        ti = 0;
    }
    os << "ReadType::" << names[ti];
    return os;
}

Token::Token(ReadType type, const std::string& chars)
    : type_(type)
    , chars_(chars)
    , quote_(0) {}


bool isA(char ch, const std::string& charset) {
    return charset.find_first_of(ch) != std::string::npos;
}

// Returns the number of characters that form a valid name. For example,
// given string "Hello(today)" this routine returns 5 since "Hello" is valid
// name, but "(" is not.
int isValidName(const std::string& str) {
    if (str.empty() || !isA(str.front(), kValidNameBegin)) {
        return 0;
    }
    size_t idx = 1;
    for ( ; idx < str.size(); ++idx) {
        const char ch = str[idx];
        if (!isA(ch, kValidNameChars)) {
            break;
        }
    }
    return idx;
}

ReadType getChar(std::istream& is, char& out) {
    char ch;
    if (!is.get(ch)) {
        return ReadType::EndOF;
    }
    if (ch == kEscapeChar) {
        char c2;
        if (!is.get(c2)) {
            out = ch;
            return ReadType::Special;
        }
        if (c2 == kEscapeChar || isA(c2, kQuoteChar)) {
            out = c2;
            return ReadType::Escaped;
        }
        is.putback(c2);
        out = ch;
        return ReadType::Special;
    }
    out = ch;
    if (ch == kVarStartChar) {
        return  ReadType::VariableStart;
    } else if (isA(ch, kSpecialChars)) {
        return  ReadType::Special;
    } else if (isA(ch, kWhiteSpace)) {
        return ReadType::WhiteSpace;
    } else if (isA(ch, kQuoteChar)) {
        return ReadType::Quote;
    }
    return ReadType::Normal;
}

void skipUntil(std::istream& is, const std::string& str) {
    if (str.empty()) {
        return;
    }
    size_t idx = 0;
    char ch;
    ReadType stat;
    do {
        stat = getChar(is, ch);
        if (stat == ReadType::EndOF) {
            return;
        }
        if (ch == str[idx]) {
            if (idx + 1 == str.size()) {
                return;
            } else {
                ++idx;
            }
        } else {
            idx = 0;
        }
    } while (true);
}

/*  Read input until non-whitespace, then accumulate characters in the symbol.
 *  The first char read has a type, so accumulate until the type changes.
 *  If the first char is a quote, then accumulate until the closing quote.
 *  Handle backslash escaping:
 *  \\ = \  \' = '   \" = "
 */
std::optional<Token> streamGetToken(std::istream& is) {
    char ch;
    ReadType stat;
    do {        // skip and discard leading whitespace
        stat = getChar(is, ch);
        if (stat == ReadType::EndOF) {
            return {};
        }
    } while (stat == ReadType::WhiteSpace);

    Token token;
    if (stat == ReadType::Quote) {    // Extract a quoted string
        token.quoteChar() = ch;
        token.type() = ReadType::Quote;
        while (true) {
            stat = getChar(is, ch);
            if (stat == ReadType::EndOF) {
                token.type() = ReadType::UnclosedQuote;
                break;
            }
            if (ch == token.quoteChar() && stat != ReadType::Escaped) {
                break;
            }
            token.data().append(1, ch);
        };
    } else if (stat == ReadType::VariableStart) {
        std::string varname;
        bool first_char = true;
        do {
            stat = getChar(is, ch);
            if (stat == ReadType::Normal &&
                ((first_char & isA(ch, kValidNameBegin)) || isA(ch, kValidNameChars))) {
                varname.append(1, ch);
                first_char = false;
            } else {
                is.putback(ch);
            }
        } while (stat == ReadType::Normal);
        if (first_char) {  // No valid name after "$", so treat it as a normal dollar sign
            token.type() = ReadType::Normal;
            token.data() = "$";
        } else {
            token.type() = ReadType::VariableName;
            token.data() = varname;
        }
    } else {                          // Extract all characters of the same type
        ReadType type = stat;
        token.type() = type;
        char prevChar = ch;
        do {
            token.data().append(1, ch);
            stat = getChar(is, ch);
            if (stat == ReadType::EndOF) {
                break;
            }
            if ((prevChar == kCommentStart[0] && ch == kCommentStart[1]) ||
                (prevChar == kCommentToEOL[0] && ch == kCommentToEOL[1])) {
                stat = ReadType::Comment;
                break;
            }
            if (stat != type) {
                is.putback(ch);
            }
            prevChar = ch;
        } while (type == stat);
        if (stat == ReadType::Comment) {
            std::string toEnd = ch == kCommentStart[1] ? kCommentEnd : "\n";
            skipUntil(is, toEnd);
            token.type() = stat;
        }
    }
    return token;
}


Tokenizer::Tokenizer(std::istream& istr, bool saveComments)
    : is_(istr)
    , saveComments_(saveComments) {}

std::optional<Token> Tokenizer::getToken() {
    return streamGetToken(is_);
}

std::vector<Token> Tokenizer::getTokensUntil(const std::string& ending) {
    std::vector<Token> tokens;
    std::string str;
    char ch;
    do {
        if (!is_.get(ch)) {
            break;
        }
        if (ending.size() == 1 && ch == ending[0]) {
            break;
        }
        str.append(1, ch);
        if (!ending.empty() && str.find(ending) != std::string::npos) {
            break;
        }
    } while (is_.good());

    std::istringstream iss(str);
    do {
        auto token = streamGetToken(iss);
        if (!token) {
            break;
        }
        tokens.push_back(*token);
    } while (true);
    return tokens;
}

std::vector<Token> Tokenizer::getTokensUntil(const std::vector<std::string>& endings) {
    std::vector<Token> tokens;
    std::string str;
    char ch;
    bool end_found = false;
    do {
        if (!is_.get(ch)) {
            break;
        }
        for (const auto& ending : endings) {
            if (ending.size() == 1 && ch == ending[0]) {
                end_found = true;
                break;
            }
            str.append(1, ch);
            if (!ending.empty() && str.find(ending) != std::string::npos) {
                end_found = true;
                break;
            }
        }
        if (end_found) {
            break;
        }
   } while (is_.good());

    std::istringstream iss(str);
    do {
        auto token = streamGetToken(iss);
        if (!token) {
            break;
        }
        tokens.push_back(*token);
    } while (true);
    return tokens;
}

}  // namespace eval
}  // namespace ctlsvc
