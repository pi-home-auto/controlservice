//
//   Copyright 2022, 2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "actionfactory.h"
#include "actions.h"
#include "context.h"
#include "classifier.h"
#include "eventfactory.h"
#include "properties.h"
#include "rulebase.h"
#include "tokenizer.h"

#include <fstream>
#include <iostream>
#include <unordered_set>

using std::cout;
using std::endl;

namespace {
using ctlsvc::eval::ReadType;
using ctlsvc::eval::Tokenizer;

// Find Rulebase <name> : and return the name.
// Note that if there is no space between the name and the colon, then the name plus
// colon are returned in one token (i.e., "MyRulebase:"). Otherwise, the name and colon
// will be two separate tokens. This function handles both cases.
std::string startRulebase(Tokenizer& tkr) {
    do {
        auto token = tkr.getToken();
        if (!token) {
            break;
        }
        if (token->type() == ReadType::Normal && token->data() == "Rulebase") {
            token = tkr.getToken();
            if (!token || token->type() != ReadType::Normal) {
                break;
            }
            if (token->data().back() == ':') {
                return token->data().substr(0, token->data().size() - 1);
            }
            auto colon = tkr.getToken();
            if (colon && colon->data() == ":") {
                return token->data();
            }
        }
    } while (true);
    return "";
}

}  // namespace

namespace ctlsvc {
namespace eval {

using EventPtr = std::shared_ptr<Event>;

std::ostream& operator<<(std::ostream& os, PhraseType type) {
    const char* names[] = {
        "Unknown", "RuleBase", "DefEvent", "SetVar", "Rule", "Action", "Event", "Name", "Value", "Parameters", "Block",
        "Predicate", "IfElse", "IfAction", "ElseAction", "Cancel", "Send", "Condition"
    };
    static_assert((sizeof(names) / sizeof(names[0])) == static_cast<int>(PhraseType::_Count));
    int ti = static_cast<int>(type);
    if (type >= PhraseType::_Count) {
        ti = 0;
    }
    os << "PhraseType::" << names[ti];
    return os;
}

class RulebaseImpl {
public:
    RulebaseImpl(const std::string& name)
        : name_(name)
        , context_(nullptr)
        , debug_(false) {}
    const std::string& name() const { return name_; }
    std::vector<Rule>& rules() { return rules_; }
    Action* genAction(RbNode* node, Classifier& cfr);
    EventPtr genEvent(RbNode* node, Classifier& cfr);
    size_t parse(const std::string& filename, const Context& context);
    bool processNode(const RbNode* node, Classifier& cfr);
private:
    std::string name_;
    std::vector<Rule> rules_;
    Context context_;
    bool debug_;
};

Action* RulebaseImpl::genAction(RbNode* node, Classifier& cfr) {
    Action* action = nullptr;
    switch (node->type) {
        case PhraseType::Action: {
            if (node->children.size() < 1 || node->children.size() > 2) {
                break;
            }
            Properties props;
            auto name = cfr.getTokenStr(node->children[0]);
            if (node->children.size() == 2) {
                auto names = ActionFactory::parameterNames(name);
                cfr.collectParameters(node->children[1], names, props);
            }
            action = ActionFactory::makeAction(name, props);
            if (debug_) {
                cout << "Created " << name << endl;
            }
            break;
        }
        case PhraseType::Block: {
            std::vector<Action*> actions;
            for (const auto actnode : node->children) {
                auto act = genAction(actnode, cfr);
                if (act != nullptr) {
                    actions.push_back(act);
                }
            }
            action = new ActionMulti(actions);
            if (debug_) {
                cout << "Created ActionMulti" << endl;
            }
            break;
        }
        case PhraseType::Cancel: {
            auto name = cfr.getTokenStr(node->children[0]->children[0]);
            action = new ActionCancelEvent(name);
            if (debug_) {
                cout << "Created ActionCancelEvent" << endl;
            }
            break;
        }
        case PhraseType::IfElse: {
            auto prednode = node->children[0];
            if (prednode->type == PhraseType::Predicate) {
                auto name = cfr.getTokenStr(prednode->children[0]);
                auto pred = cfr.getTokenStr(prednode->children[1]);
                auto value = cfr.getTokenStr(prednode->children[2]);
                auto actif = genAction(node->children[1], cfr);
                Action* actelse = nullptr;
                if (node->children.size() == 3) {
                    actelse = genAction(node->children[2], cfr);
                }
                // Handle all conditions: is, not, greater, less
                if (pred == "not") {
                    action = new ActionIfElse([=](eval::Context* ctx) -> bool {
                                                  cout << "Pred " << name << " != " << ctx->getVarStr(name)
                                                       << " val:" << value << endl;
                                                  return ctx->getVarStr(name) != value;
                                              }, actif, actelse);
                } else if (pred == "greater") {
                    action = new ActionIfElse([=](eval::Context* ctx) -> bool {
                                                  cout << "Pred " << name << " > " << ctx->getVarStr(name)
                                                       << " val:" << value << endl;
                                                  return ctx->getVarStr(name) > value;
                                              }, actif, actelse);
                } else if (pred == "less") {
                    action = new ActionIfElse([=](eval::Context* ctx) -> bool {
                                                  cout << "Pred " << name << " < " << ctx->getVarStr(name)
                                                       << " val:" << value << endl;
                                                  return ctx->getVarStr(name) < value;
                                              }, actif, actelse);
                } else {  // assume (pred == "is") {
                    action = new ActionIfElse([=](eval::Context* ctx) -> bool {
                                                  cout << "Pred " << name << " == " << ctx->getVarStr(name)
                                                       << " val:" << value << endl;
                                                  return ctx->getVarStr(name) == value;
                                              }, actif, actelse);
                }
                if (debug_) {
                    cout << "Created ActionIfElse" << endl;
                }
            }
            break;
        }
        case PhraseType::Send: {
            auto ev = genEvent(node->children[0], cfr);
            if (ev) {
                action = new ActionSendEvent(ev);
                if (debug_) {
                    cout << "Created ActionSendEvent(" << ev->name() << endl;
                }
            } else {
                cout << "Unable to gen event for SendEvent" << endl;
            }
            break;
        }
        case PhraseType::SetVar: {
            if (node->children[1]->type == PhraseType::Event) {
                cout << "ActionSetVariable can only have string value." << endl;
            } else {
                auto name = cfr.getTokenStr(node->children[0]);
                auto value = cfr.getTokenStr(node->children[1]);
                action = new ActionSetVariable(name, value);
                if (debug_) {
                    cout << "Created ActionSetVariable(" << name << ", " << value << ")" << endl;
                }
            }
            break;
        }
        default:
            cout << "Cannot make Action from " << node->type << endl;
            break;
    }
    return action;
}

EventPtr RulebaseImpl::genEvent(RbNode* node, Classifier& cfr) {
    EventPtr event;
    auto nameIdx = node->children.front()->range.first;
    auto nameStr = cfr.getTokens()[nameIdx].data();
    if (cfr.getTokens()[nameIdx].type() == ReadType::VariableName) {
            auto ev = context_.getVar(nameStr);
            if (std::holds_alternative<EventPtr>(ev)) {
                event = std::get<EventPtr>(ev);
            }
    } else {
        Properties props;
        auto factory = EventFactory::instance();
        if (node->children.size() > 1) {
            auto names = factory->parameterNames(nameStr);
            cfr.collectParameters(node->children[1], names, props);
        }
        event = factory->makeEvent(nameStr, props);
    }
    return event;
}

size_t RulebaseImpl::parse(const std::string& filename, const Context& context) {
    debug_ = context.getVarStr("Debug") == "true" ? true : false;
    rules_.clear();
    std::ifstream istr(filename.c_str());
    if (!istr.good()) {
        cout << "Cannot open file \"" << filename << "\"" << endl;
        return 0;
    }
    Tokenizer tokenizer(istr);
    std::vector<Rule> rules;
    context_ = context;
    auto rbname = startRulebase(tokenizer);
    if (rbname.empty()) {
        cout << "Cannot find Rulebase keyword" << endl;
        return 0;
    }
    cout << "Begin parsing rulebase " << rbname << endl;
    name_ = rbname;
    TokenArray tokens;
    std::optional<Token> token;
    size_t num = 0;
    while ((token = tokenizer.getToken())) {
        if (token->type() == ReadType::Comment) {
            continue;
        }
        tokens.push_back(*token);
        if (debug_) {
            cout << "Token " << num++ << ": " << token->type() << "    " << token->data();
            if (token->type() == ReadType::Quote) {
                cout << "   QuoteChar=" << token->quoteChar();
            }
            cout << endl;
        }
    };
    cout << "Parsed " << tokens.size() << " tokens." << endl;

    Classifier cfr(std::move(tokens), &context_);
    cfr.createTree();
    if (debug_) {
        cfr.printTree();
    }
    cout << "\nVisit tree:" << endl;
    using namespace std::placeholders;
    auto visits = cfr.visitChildren(std::bind(&RulebaseImpl::processNode, this, _1, _2));
    cout << "Parse tree visits = " << visits << endl;

    return rules_.size();
}

bool RulebaseImpl::processNode(const RbNode* node, Classifier& cfr) {
    switch (node->type) {
        case PhraseType::DefEvent: {
            if (node->children.size() != 1 || node->children.front()->type != PhraseType::Event) {
                return true;
            }
            auto& eventNode = node->children.front();
            auto& nameIdx = eventNode->children.front()->range.first;
            auto nameStr = cfr.getTokens()[nameIdx].data();
            Properties props;
            if (eventNode->children.size() > 1) {
                cfr.collectParameters(eventNode->children[1], { "name", "value" }, props);
            }
            auto inName = props.getString("name");
            // TODO try to get "value" prop
            auto factory = EventFactory::instance();
            if (factory->defineEvent(nameStr, inName) && debug_) {
                cout << "Defined event " << nameStr << "(\"" << inName << "\")" << endl;
            }
            break;
        }
        case PhraseType::Rule: {
            if (node->children.size() < 2 || node->children.front()->type != PhraseType::Event) {
                return true;
            }
            auto eventNode = node->children.front();
            auto ev = genEvent(eventNode, cfr);
            std::shared_ptr<Action> action(genAction(node->children[1], cfr));
            Rule rule(ev, action);
            rules_.push_back(rule);
            cout << "Created rule " << rule.name() << endl;
            break;
        }
        case PhraseType::SetVar:
            if (node->children.size() != 2 || node->children.front()->type != PhraseType::Name) {
                return true;
            } else {
                auto name = cfr.getTokenStr(node->children[0]);
                if (node->children[1]->type == PhraseType::Event) {
                    auto ev = genEvent(node->children[1], cfr);
                    if (context_.setVar(name, ev) && ev != nullptr) {
                        if (debug_) {
                            cout << "SetVar " << name << " to event " << ev->name() << endl;
                        }
                    } else {
                        cout << "Cannot SetVar " << name << " to event." << endl;
                    }
                } else {
                    auto value = cfr.getTokenStr(node->children[1]);
                    if (context_.setVar(name, value)) {
                        if (debug_) {
                            cout << "SetVar " << name << " to " << value << endl;
                        }
                    } else {
                        cout << "Cannot SetVar " << name << " to " << value << endl;
                    }
                }
            }
            break;
        default:
            break;
    }
    return true;
}


Rulebase::Rulebase(const std::string& rbname)
    : impl_(std::make_unique<RulebaseImpl>(rbname)) {}

bool Rulebase::addRule(const Rule& rule) {
    impl_->rules().push_back(rule);
    return true;
}

Rulebase::~Rulebase() {}

const std::string& Rulebase::name() const { return impl_->name(); }

size_t Rulebase::parse(const std::string& filename, const Context& context) {
    return impl_->parse(filename, context);
}

const std::vector<Rule>& Rulebase::rules() const { return impl_->rules(); }

}  // namespace eval
}  // namespace ctlsvc
