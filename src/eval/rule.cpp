//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "rule.h"
#include "context.h"
#include "evaluator.h"
#include "events.h"
#include "logger.h"
#include "properties.h"
#include "sequencer.h"

#include <atomic>
#include <chrono>
#include <mutex>
#include <ios>
#include <optional>
#include <queue>
#include <thread>
#include <unordered_map>
using std::endl;

namespace ctlsvc {
namespace eval {

Rule::Rule()
    : EvalBase("<empty-rule>")
    , action_()
    , event_()
    , checker_([](const EventPtr){ return true; })
    , has_custom_checker_(false) {}

Rule::Rule(EventPtr event, ActionPtr action)
    : EvalBase(event->name() + std::to_string(event->value()) + ":" + action->name())
    , action_(action)
    , event_(event)
    , checker_([](const EventPtr){ return true; })
    , has_custom_checker_(false) {}

Rule::Rule(EventPtr event, ActionPtr action, CheckFunction checker)
    : EvalBase(event->name() + std::to_string(event->value()) + ":" + action->name())
    , action_(action)
    , event_(event)
    , checker_(checker)
    , has_custom_checker_(true) {}

Rule::Rule(const Rule& other)
    : EvalBase(other.name())
    , action_(other.action_)
    , event_(other.event_)
    , checker_(other.checker_)
    , has_custom_checker_(other.has_custom_checker_) {}

Rule::~Rule() {}

std::string Rule::description() const {
    return "Rule " + name();
}

bool Rule::hasCustomChecker() const {
    return has_custom_checker_;
}

bool Rule::matches(const EventPtr event) const {
    return *event == *event_ && checker_(event) == true;
}

const Rule::ActionPtr Rule::getAction() const {
    return action_;
}

Rule::ActionPtr Rule::getAction() {
    return action_;
}

const Rule::EventPtr Rule::getEvent() const {
    return event_;
}

Rule::EventPtr Rule::getEvent() {
    return event_;
}

void Rule::setCondition(CheckFunction checker) {
    checker_ = checker;
}

Rule& Rule::operator=(const Rule& other) {
    if (this != &other) {
        name_ = other.name_;
        action_ = other.action_;
        event_ = other.event_;
        checker_ = other.checker_;
    }
    return *this;
}

Rule& Rule::operator=(Rule&& other) {
    if (this != &other) {
        name_ = other.name_;
        action_ = other.action_;
        event_ = other.event_;
        checker_ = other.checker_;
    }
    return *this;
}

bool Rule::operator==(const Rule& other) const {
    return name() == other.name();
}

}  // namespace eval
}  // namespace ctlsvc
