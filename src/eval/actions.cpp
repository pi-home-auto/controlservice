//
//   Copyright 2021-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "eval/actions.h"
#include "eval/context.h"
#include "eval/events.h"
#include "controlservice.h"
#include "handlers/gpiohandler.h"
#include "handlers/servohandler.h"
#include "handlers/soundhandler.h"
#include "handlers/stepmotorhandler.h"
#include "logger.h"

#include <iostream>
using std::endl;

namespace ctlsvc {
namespace eval {

ActionCall::ActionCall(CallbackFunc cb, void* data)
    : Action("Call")
    , func_(cb)
    , data_(data) {}

bool ActionCall::run(Context* context) {
    return func_(context, data_);
}


ActionCancelEvent::ActionCancelEvent(const std::string& name)
    : Action("CancelEvent")
    , eventName_(name) {}

bool ActionCancelEvent::run(Context* context) {
    auto prop = context->getVar(eventName_);
    if (std::holds_alternative<std::shared_ptr<Event>>(prop)) {
        auto event = std::get<std::shared_ptr<Event>>(prop);
        eval::DelayedEvent* devent = static_cast<eval::DelayedEvent*>(event.get());
        devent->cancel();
        return true;
    }
    return false;
}


ActionDebug::ActionDebug(const std::string& message)
    : Action("Debug")
    , message_(message) {}

bool ActionDebug::run(Context* context) {
  cslog << loglevel(Info) << "DBG: " << message_ << endl;
  return true;
}


ActionIfElse::ActionIfElse(Predicate pred, Action* action_if, Action* action_else)
    : Action("IfElse")
    , pred_(pred)
    , action_if_(action_if)
    , action_else_(action_else) {}

bool ActionIfElse::run(Context* context) {
    cslog << loglevel(Info) << "ActionIfElse " << action_if_ << endl;
    if (action_if_ == nullptr) {
        return false;
    }
    if (pred_(context)) {
        cslog << loglevel(Info) << "pred true: ActionIfElse" << endl;
        return action_if_->run(context);
    } else if (action_else_ != nullptr) {
        cslog << loglevel(Info) << "pred false: ActionIfElse" << endl;
        return action_else_->run(context);
    }
    cslog << loglevel(Info) << "ActionIfElse fallthru" << endl;
    return false;
}


ActionMulti::ActionMulti(const std::vector<Action*>& actions)
    : Action("Multi")
    , actions_(actions) {}

ActionMulti::~ActionMulti() {
    for (auto act : actions_) {
        delete act;
    }
}

bool ActionMulti::run(Context* context) {
    bool ret = true;
    for (auto action : actions_) {
        ret = action->run(context);
    }
    return ret;
}


ActionPlaySound::ActionPlaySound(const std::string& file)
    : Action("PlaySound")
    , file_(file) {}

bool ActionPlaySound::run(Context* context) {
    if (file_.empty()) {
        return false;
    }
    auto soundHandler = dynamic_cast<SoundHandler*>(ControlService::getHandler("sound"));
    return soundHandler->play(file_);
}

ActionReadPin::ActionReadPin(int pin, const std::string& varName)
    : Action("ReadPin")
    , pin_(pin)
    , varName_(varName) {
    auto gpiohandler = dynamic_cast<GpioHandler*>(ControlService::getHandler("gpio"));
    auto& outDevices = gpiohandler->getOutputDevices();
    if (pin > 0 && pin <= static_cast<int>(outDevices.size())) {
        device_ = &outDevices[pin - 1];
    } else {
        device_ = nullptr;
    }
}

bool ActionReadPin::run(Context* context) {
    if (device_ == nullptr || context == nullptr) {
        return false;
    }
    auto stat = device_->status();
    if (stat != Device::ReadOpen && stat != Device::ReadWriteOpen) {
        return false;
    }
    int value;
    if (!device_->read(value)) {
        return false;
    }
    return context->setVar(varName_, value);
}


ActionRun::ActionRun(const std::string& cmd, const std::string& args)
    : Action("Run")
    , cmd_(cmd)
    , args_(args) {}

bool ActionRun::run(Context* context) {
  if (cmd_.empty()) {
    cslog << loglevel(Error) << "ActionRun -- no command specified" << endl;
    return false;
  }
  auto fullcmd = cmd_;
  if (!args_.empty()) {
    fullcmd.append(" ").append(args_);
  }
  cslog << loglevel(Info) << "ActionRun::run: " << fullcmd << endl;
  auto forker = ControlService::instance()->forker();
  auto handle = forker->spawn(fullcmd);
  if (handle == ForkEntry::INVALID_HANDLE) {
    cslog << loglevel(Error) << "ActionRun -- invalid handle" << endl;
    return false;
  }
#if 0
  sleep(1);
  std::string sout;
  std::string serr;
  if (forker->readFrom(handle, sout, serr)) {
    cslog << loglevel(Info) << "ActionRun::run output:" << endl;
    cslog << loglevel(Info) << "cout=" << sout << endl;
    cslog << loglevel(Info) << "cerr=" << serr << endl;
  }
#endif
  return true;
}


ActionRunProgram::ActionRunProgram(const std::string& program, uint32_t repeat)
    : Action("RunProgram")
    , program_(program)
    , repeat_(repeat) {}

bool ActionRunProgram::run(Context* context) {
  cslog << loglevel(Info) << "ActionRunProgram::run " << program_ << std::endl;
  auto gpiohandler = dynamic_cast<GpioHandler*>(ControlService::getHandler("gpio"));
  return gpiohandler->runProgram(program_, repeat_);;
}


ActionSendEvent::ActionSendEvent(std::shared_ptr<Event> event)
    : Action("SendEvent")
    , event_(event) {}

bool ActionSendEvent::run(Context* context) {
    if (context == nullptr) {
        return false;
    }
    return context->evaluator()->push(event_);
}


ActionSetPin::ActionSetPin(int pin, int value)
    : Action("SetPin")
    , pin_(pin)
    , value_(value)
    , device_(nullptr) {
    auto gpiohandler = dynamic_cast<GpioHandler*>(ControlService::getHandler("gpio"));
    auto dev = gpiohandler->getInDevice(pin);
    if (dev) {
        device_ = dev.value();
    } else {
        auto odev = gpiohandler->getOutDevice(pin);
        if (odev) {
            device_ = odev.value();
        }
    }
}

bool ActionSetPin::run(Context* context) {
    if (device_ == nullptr) {
        return false;
    }
    auto stat = device_->status();
    if (stat != Device::WriteOpen && stat != Device::ReadWriteOpen) {
        return false;
    }
    return device_->write(1, &value_);
}

////////////////////

ActionSetVariable::ActionSetVariable(const std::string& name, const std::string& value)
    : Action("SetVariable")
    , name_(name)
    , value_(value) {}

bool ActionSetVariable::run(Context* context) {
    if (context == nullptr) {
        return false;
    }
    return context->setVar(name_, value_);
}

////////////////////

ActionServo::ActionServo(int servo_num, int degrees, bool is_relative)
    : Action("Servo" + std::to_string(degrees))
    , servo_num_(servo_num)
    , degrees_(degrees)
    , is_relative_(is_relative) {
    servo_ = dynamic_cast<ServoHandler*>(ControlService::getHandler("servo"));
}

bool ActionServo::run(Context* context) {
    cslog << loglevel(Info) << "ActionServo::run " << degrees_ << std::endl;
    if (is_relative_) {
        return servo_->move(degrees_);
    }
    return servo_->moveTo(degrees_);
}

bool ActionServo::is_valid() const {
    return servo_;
}

ActionStepMotor::ActionStepMotor(int steps, bool is_relative)
    : Action("StepMotor" + std::to_string(steps))
    , steps_(steps)
    , is_relative_(is_relative) {
    stepper_ = dynamic_cast<StepMotorHandler*>(ControlService::getHandler("stepmotor"));
}

bool ActionStepMotor::run(Context* context) {
    cslog << loglevel(Info) << "ActionStepMotor::run " << steps_ << std::endl;
    if (is_relative_) {
        return stepper_->move(steps_, 5);
    }
    return stepper_->moveTo(steps_, 5);
#if 0
    constexpr int kMotorStepMs = 3;
    // Stepping motor on devices 7-10  (1000, 1100, 0100, 0110, 0010, 0011, 0001, 1001)
    std::array<std::pair<int,int>, 8> cwise = { {
        {1,1}, {0,0}, {2,1}, {1,0}, {3,1}, {2,0}, {0,1}, {3,0}
    } };
    std::array<std::pair<int,int>, 8> anticwise = { {
        {2,1}, {3,0}, {1,1}, {2,0}, {0,1}, {1,0}, {3,1}, {0,0}
    } };
    int devnr = steps_ > 0 ? 0 : 3;
    int val = 1;  // The starting value does not go into the loop
    devices_[devnr]->write(1, &val);
    auto& seq = steps_ > 0 ? cwise : anticwise;
    for (int ii = 0; ii < abs(steps_); ++ii) {
        for (auto [pin, value] : seq) {
            devices_[pin]->write(1, &value);
            std::this_thread::sleep_for(std::chrono::milliseconds(kMotorStepMs));
        }
    }
    val = 0;
    devices_[devnr]->write(1, &val);
    return true;
#endif
}


ActionStopSound::ActionStopSound()
    : Action("StopSound") {}

bool ActionStopSound::run(Context* context) {
    auto soundHandler = dynamic_cast<SoundHandler*>(ControlService::getHandler("sound"));
    return soundHandler->stop();
}

}  // namespace eval
}  // namespace ctlsvc
