//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "event.h"

#include <functional>
#include <memory>
#include <string>

namespace ctlsvc {
namespace eval {
// Forward reference
class Action;

class Rule : public EvalBase {
public:
    using ActionPtr = std::shared_ptr<Action>;
    using EventPtr = std::shared_ptr<Event>;
    using CheckFunction = std::function<bool(const EventPtr)>;

    Rule();
    Rule(EventPtr event, ActionPtr action);
    Rule(EventPtr event, ActionPtr action, CheckFunction checker);
    Rule(const Rule& other);
    ~Rule();

    std::string description() const;
    bool hasCustomChecker() const;
    bool matches(const EventPtr event) const;
    const ActionPtr getAction() const;
    ActionPtr getAction();
    const EventPtr getEvent() const;
    EventPtr getEvent();
    void setCondition(CheckFunction checker);

    Rule& operator=(const Rule& other);
    Rule& operator=(Rule&& other);
    bool operator==(const Rule& other) const;
private:
    ActionPtr action_;
    EventPtr event_;
    CheckFunction checker_;
    bool has_custom_checker_;
};

}  // namespace eval
}  // namespace ctlsvc
