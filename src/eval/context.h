//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "properties.h"

#include <memory>
#include <vector>

namespace ctlsvc {
namespace eval {

// Forward references
class ContextImpl;
class Evaluator;
class Event;
class Rule;

/**
 * The Context holds a set of objects that the evaluator, rules and actions
 * use. Thus this is a common, shared between components while evaluator is
 * running.
 */
class Context {
public:
    Context(Evaluator* eval);
    Context(const Context& other);
    Context& operator=(const Context& other);
    ~Context();

    Evaluator* evaluator();
    std::shared_ptr<Event> event();

    PropertyVar getVar(const std::string& name) const;
    /** Just a shortcut to evaluator()->getVar() */
    std::string getVarStr(const std::string& name) const;
    /** Just a shortcut to evaluator()->removeVar() */
    bool removeVar(const std::string& name);
    Rule* rule();
    void setEvent(std::shared_ptr<Event> event);
    void setRule(Rule* rule);
    bool setVar(const std::string& name, const PropertyVar& value);
    /** Just a shortcut to evaluator()->setVar() */
    bool setVarStr(const std::string& name, const std::string& value);

private:
    std::unique_ptr<ContextImpl> impl_;
};

} // namespace eval
} // namespace ctlsvc
