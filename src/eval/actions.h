//
//   Copyright 2021-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "device.h"
#include "evaluator.h"

#include <array>
#include <functional>

namespace ctlsvc {
// Forward reference
class ServoHandler;
class StepMotorHandler;

namespace eval {
class Context;
class Event;

class ActionCall : public Action {
public:
    using CallbackFunc = std::function<bool(Context*, void*)>;

    ActionCall(CallbackFunc cb, void* data);
    ~ActionCall() = default;
    virtual bool run(Context* context) override;
private:
    CallbackFunc func_;
    void* data_;
};

class ActionCancelEvent : public Action {
public:
    ActionCancelEvent(const std::string& name);
    ~ActionCancelEvent() = default;
    virtual bool run(Context* context) override;
private:
    std::string eventName_;
};

class ActionDebug : public Action {
public:
    ActionDebug(const std::string& message);
    virtual ~ActionDebug() = default;
    virtual bool run(Context* context) override;
    // Getters
    const std::string& message() const { return message_; }
private:
    std::string message_;
};

class ActionIfElse : public Action {
public:
    using Predicate = std::function<bool(Context*)>;

    ActionIfElse(Predicate pred, Action* action_if, Action* action_else = nullptr);
    virtual ~ActionIfElse() = default;
    virtual bool run(Context* context) override;
private:
    Predicate pred_;
    Action* action_if_;
    Action* action_else_;
};

class ActionMulti : public Action {
public:
    ActionMulti(const std::vector<Action*>& actions);
    virtual ~ActionMulti();
    virtual bool run(Context* context) override;
private:
    std::vector<Action*> actions_;
};

class ActionPlaySound : public Action {
public:
    ActionPlaySound(const std::string& file);
    virtual ~ActionPlaySound() = default;
    virtual bool run(Context* context) override;
private:
    std::string file_;
};

class ActionReadPin : public Action {
public:
    ActionReadPin(int pin, const std::string& varName);
    virtual ~ActionReadPin() = default;
    virtual bool run(Context* context) override;
    // Getters
    int pin() const { return pin_; }
    const std::string& varName() const { return varName_; }
private:
    int pin_;
    std::string varName_;
    Device* device_;
};

class ActionRun : public Action {
public:
  ActionRun(const std::string& cmd, const std::string& args);
  virtual ~ActionRun() = default;
  virtual bool run(Context* context) override;
private:
  const std::string cmd_;
  const std::string args_;
};

class ActionRunProgram : public Action {
public:
  ActionRunProgram(const std::string& program, uint32_t repeat = 1);
  virtual ~ActionRunProgram() = default;
  virtual bool run(Context* context) override;
private:
  const std::string program_;
  uint32_t repeat_;
};

class ActionSendEvent : public Action {
public:
    ActionSendEvent(std::shared_ptr<Event> event);
    virtual ~ActionSendEvent() = default;
    virtual bool run(Context* context) override;
private:
    std::shared_ptr<Event> event_;
};

class ActionSetPin : public Action {
public:
    ActionSetPin(int pin, int value);
    virtual ~ActionSetPin() = default;
    virtual bool run(Context* context) override;
    // Getters
    int pin() const { return pin_; }
    int value() const { return value_; }
private:
    int pin_;
    int value_;
    Device* device_;
};

class ActionSetVariable : public Action {
public:
    ActionSetVariable(const std::string& name, const std::string& value);
    virtual ~ActionSetVariable() = default;
    virtual bool run(Context* context) override;
    // Getters, Setters
    const std::string& name() const { return name_; }
    const std::string& value() const { return value_; }
    void value(const std::string& value) { value_ = value; }
private:
    std::string name_;
    std::string value_;
};

class ActionServo : public Action {
public:
    /** Moves the number of degrees clockwise if positive, or anticlockwise
     *  if degrees is negative.
     *  @param degrees Number of degrees to move
     *  @param is_relative If true, then the motor will move the specified number
     *        of degrees. If false, then the motor is moved to specified degree position.
     */
    ActionServo(int servo_num, int degrees, bool is_relative = false);
    virtual ~ActionServo() = default;
    virtual bool run(Context* context) override;

    bool is_valid() const;
    // Getters
    int degrees() const { return degrees_; }
private:
    int servo_num_;
    int degrees_;
    bool is_relative_;
    ServoHandler* servo_;
};

class ActionStepMotor : public Action {
public:
    /** Moves the number of steps clockwise if positive, or anticlockwise
     *  if steps is negative.
     *  @param steps Number of steps to move
     *  @param is_relative If true, then the motor will move the specified number
     *        of steps. If false, then the motor is moved to specified step position.
     */
    ActionStepMotor(int steps, bool is_relative = false);
    virtual ~ActionStepMotor() = default;
    virtual bool run(Context* context) override;
    // Getters
    int steps() const { return steps_; }
private:
    int steps_;
    bool is_relative_;
    StepMotorHandler* stepper_;
};
    

class ActionStopSound : public Action {
public:
    ActionStopSound();
    virtual ~ActionStopSound() = default;
    virtual bool run(Context* context) override;
};

}  // namespace eval
}  // namespace ctlsvc
