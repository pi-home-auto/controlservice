//
//   Copyright 2022, 2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "classifier.h"
#include "context.h"
#include "properties.h"

using std::cout;
using std::endl;

namespace {

using ctlsvc::eval::PhraseType;
using ctlsvc::eval::RbNode;
using ctlsvc::eval::ReadType;
using ctlsvc::eval::Token;
using ctlsvc::eval::TokenArray;

PhraseType isStatement(const std::string& str) {
    if (str == "define") {
        return PhraseType::DefEvent;
    } else if (str == "on") {
        return PhraseType::Rule;
    } else if (str == "set") {
        return PhraseType::SetVar;
    }
    // TODO unset command
    return PhraseType::Unknown;
}

RbNode* getStatement(const TokenArray& tokens, size_t idx) {
    auto stype = isStatement(tokens[idx].data());
    if (stype == PhraseType::Unknown) {
        return nullptr;
    }
    auto node = new RbNode;
    node->type = stype;
    node->range.first = idx;
    size_t nesting = 0;
    size_t next = idx + 1;
    for ( ; next < tokens.size(); ++next) {
        const Token& token = tokens[next];
        if (token.type() == ReadType::Special) {
            if (token.data() == "{") {
                ++nesting;
            } else if (token.data() == "}") {
                --nesting;
            }
        } else if (nesting == 0) {
            stype = isStatement(token.data());
            if (stype != PhraseType::Unknown) {
                break;
            }
        }
    }
    node->range.second = next;
    return node;
}

}  // namespace

namespace ctlsvc {
namespace eval {

std::ostream& operator<<(std::ostream& os, const ctlsvc::eval::RbNode& node) {
    const char* names[] = {
        "Unknown", "RuleBase", "DefEvent", "SetVar", "Rule", "Action", "Event",
        "Name", "Value", "Parameters", "Block", "Predicate", "IfElse", "IfAction",
        "ElseAction", "Cancel", "Send", "Condition"
    };
    int ti = static_cast<int>(node.type);
    if (node.type > ctlsvc::eval::PhraseType::Condition) {
        ti = 0;
    }
    os << names[ti] << " range=(" << node.range.first << ", " << node.range.second << ")";
    return os;
}

Classifier::Classifier(TokenArray&& tokens, Context* context)
    : tokens_(tokens)
    , context_(context) {}

// TODO check that all parameters are collected, allow for optional params.
void Classifier::collectParameters(const RbNode* node, const std::vector<std::string>& names, Properties& props) {
    size_t ni = 0;
    for (size_t idx = node->range.first; idx < node->range.second && ni < names.size(); ++idx) {
        auto& token = tokens_[idx];
        if (token.type() == ReadType::VariableName) {
            auto val = context_->getVar(token.data());
            if (std::holds_alternative<std::string>(val)) {
                auto valstr = std::get<std::string>(val);
                cout << "Replace var " << token.data() << " with " << valstr << endl;
                token.type() = ReadType::Quote;
                token.data() = valstr;
            } else if (std::holds_alternative<int>(val)) {
                auto valint = std::get<int>(val);
                cout << "Replace var " << token.data() << " with " << valint << endl;
                props.set(names[ni++], val);
                continue;
            } else if (std::holds_alternative<std::shared_ptr<Event>>(val)) {
                props.set(names[ni++], val);
                continue;
            } else {
                cout << "Context variable can only be string, int or Event*" << endl;
                token.type() = ReadType::Quote;
                token.data() = "(unknown)";
            }
        }
        if (token.type() == ReadType::Normal) {
            std::string numeric("0123456789");
            char ch1 = token.data().front();
            bool is_num = ch1 == '+' || ch1 == '-' || numeric.find_first_of(ch1) != std::string::npos;
            if (is_num) {
                for (auto ch : token.data().substr(1)) {
                    if (numeric.find_first_of(ch) == std::string::npos) {
                        is_num = false;
                        break;
                    }
                }
            }
            if (is_num) {
                int val = atoi(token.data().c_str());
                props.set(names[ni++], val);
            } else {
                props.set(names[ni++], token.data());
            }
        } else if (token.type() == ReadType::Quote) {
            if (token.quoteChar() == '"') {
                auto str = replaceVars(token.data());
                if (token.data() != str) {
                    cout << "=== Replaced \"" << token.data() << "\" with \"" << str << "\"" << endl;
                }
                props.set(names[ni++], str);
            } else {
                props.set(names[ni++], token.data());
            }
        }
    }
}

// TODO This replaces variables at compile time. We should really do this at runtime
//      in order to get the active value of the variable.
std::string Classifier::replaceVars(const std::string& input) const {
    size_t prev = 0;
    size_t pos = 0;
    std::string out;
    do {
        pos = input.find('$', prev);
        if (pos == std::string::npos) {
            out.append(input.substr(prev));
        } else {
            //if pos > 0 && pos-1 == \\ then insert "$"
            out.append(input.substr(prev, pos - prev));
            auto varEnd = input.find_first_of(" ,.;:-!@#$%^&*()=+[]{}<>?~`/\\", pos+1);
            auto varName = varEnd == std::string::npos ? input.substr(pos + 1) : input.substr(pos + 1, varEnd - pos - 1);
            auto val = context_->getVarStr(varName);
            out.append(val);
            prev = varEnd;
        }
    } while (pos != std::string::npos && prev != std::string::npos);
    return out;
}

void Classifier::createTree() {
    tree_.type = PhraseType::RuleBase;
    tree_.range.first = 0;
    tree_.range.second = tokens_.size();

    size_t idx = 0;
    while (idx < tokens_.size()) {
        auto node = getStatement(tokens_, idx);
        if (node == nullptr || node->type == PhraseType::Unknown) {
            if (node != nullptr) {
                delete node;
            }
            break;    // some error occurred
        }
        if (!processStatement(node)) {
            cout << "Got error processing node: " << *node << endl;
        }
        tree_.children.push_back(node);
        idx = node->range.second;
    };
}

RbNode* Classifier::getParams(size_t start, size_t end) {
    if (start + 2 > end) {
        return nullptr;
    }
    RbNode* params = nullptr;
    if (tokens_[start].data() == "(") {
        size_t idx = start + 1;
        for ( ; idx < end; ++idx) {
            if (tokens_[idx].data() == ")") {
                break;
            }
            if (tokens_[idx].data() == ",") {
                continue;
            }
        }
        params = new RbNode;
        params->type = PhraseType::Parameters;
        params->range.first = start + 1;
        params->range.second = idx + 1;
    }
    return params;
}

/* Actions:
 * do action:<action>(<params>)
 * set <var> = <value>
 * cancel <event>
 * if <condition> <action> [ else <action> ]
 * send <event>
 * { <action> [ <action> ... ] }
 */
RbNode* Classifier::processAction(size_t start, size_t end) {
    const std::string& actstr = tokens_[start].data();
    RbNode* act = nullptr;
    if (actstr == "do") {
        if (tokens_[start + 1].data().substr(0, 7) != "action:") {
            return nullptr;
        }
        act = new RbNode(PhraseType::Action);
        act->range.first = start;
        act->range.second = end;
        auto actname = new RbNode(PhraseType::Name);
        actname->range.first = start + 1;
        actname->range.second = start + 2;
        act->children.push_back(actname);
        auto params = getParams(start + 2, end);
        if (params != nullptr) {
            act->children.push_back(params);
            act->range.second = params->range.second;
        }
    } else if (actstr == "set") {
        act = new RbNode(PhraseType::SetVar);
        act->range.first = start;
        act->range.second = end;
        auto child = new RbNode(PhraseType::Name);
        child->range.first = start + 1;
        child->range.second = start + 2;
        act->children.push_back(child);
        if (tokens_[start + 3].data().substr(0, 6) == "event:") {
            auto evt = processEvent(start + 3, end);
            if (evt != nullptr) {
                act->children.push_back(evt);
                act->range.second = evt->range.second;
            }
        } else {
            auto val = new RbNode(PhraseType::Value);
            val->range.first = start + 3;
            val->range.second = start + 4;
            if (val != nullptr) {
                act->children.push_back(val);
                act->range.second = val->range.second;
            }
        }
    } else if (actstr == "cancel") {
        act = new RbNode(PhraseType::Cancel);
        act->range.first = start;
        act->range.second = end;
        auto evt = processEvent(start + 1, end);
        if (evt != nullptr) {
            act->children.push_back(evt);
            act->range.second = evt->range.second;
        }
    } else if (actstr == "send") {
        act = new RbNode(PhraseType::Send);
        act->range.first = start;
        act->range.second = end;
        auto evt = processEvent(start + 1, end);
        if (evt != nullptr) {
            act->children.push_back(evt);
            act->range.second = evt->range.second;
        }
    } else if (actstr == "if") {
        auto pred = processPredicate(start + 1, end);
        if (pred == nullptr) {
            return nullptr;
        }
        act = new RbNode(PhraseType::IfElse);
        act->range.first = start;
        act->range.second = end;
        act->children.push_back(pred);
        start = pred->range.second;
        auto actif = processActionBlock(start, end);
        if (actif == nullptr) {
            return nullptr;
        }
        act->children.push_back(actif);
        start = actif->range.second;
        act->range.second = start;
        // Check for "else"
        if (tokens_[start].data() == "else") {
            auto actelse = processActionBlock(start + 1, end);
            if (actelse != nullptr) {
                act->children.push_back(actelse);
                act->range.second = actelse->range.second;
            }
        }
    }
    return act;
}

RbNode* Classifier::processBlock(size_t start, size_t end) {
  size_t nesting = 1;
  size_t next = start + 1;
  for ( ; next < end; ++next) {
    const Token& token = tokens_[next];
    if (token.type() == ReadType::Special) {
      if (token.data() == "{") {
        ++nesting;
        cout << "+blk nest " << nesting << " @ " << next << endl;
      } else if (token.data() == "}") {
        --nesting;
        cout << "-blk nest " << nesting << " @ " << next << endl;
        if (nesting == 0) {
          cout << "+-+-processBlock(" << start << ", " << next + 1 << ")" << endl;
          auto blk = new RbNode(PhraseType::Block);
          blk->range.first = start;
          blk->range.second = next + 1;
          return blk;
        }
      }
    }
  }
  return nullptr;
}

RbNode* Classifier::processActionBlock(size_t start, size_t end) {
  RbNode* actblk = nullptr;
  cout << "processActionBlock start, end=" << start << ", " << end << endl;
  if (tokens_[start].data() == "{") {
    actblk = processBlock(start, end);
    if (actblk == nullptr) {
      cout << "Error processing block" << endl;
      return actblk;
    }
    start += 1;   // Strip enclosing brackets
    end = actblk->range.second - 1;
    while (true) {
      cout << " =processAction(" << start << ", " << end << ")" << endl;
      auto action = processAction(start, end);
      if (action == nullptr) {
        break;
      }
      actblk->children.push_back(action);
      start = action->range.second;
    };
  } else {
    actblk = processAction(start, end);
    if (actblk == nullptr) {
      cout << "Error processing action" << endl;
    }
  }
  return actblk;
}

RbNode* Classifier::processEvent(size_t start, size_t end) {
    auto child = new RbNode(PhraseType::Event);
    child->range.first = start;
    child->range.second = start + 1;
    auto evname = new RbNode;
    evname->type = PhraseType::Name;
    evname->range.first = start;
    evname->range.second = start + 1;
    child->children.push_back(evname);
    auto params = getParams(start + 1, end);
    if (params != nullptr) {
        child->children.push_back(params);
        child->range.second = params->range.second;
    }
    return child;
}

/*
  <predicate> := <subject> <condition> <value>
  <condition> := is [ equal | not | greater | less ]
*/
RbNode* Classifier::processPredicate(size_t start, size_t end) {
    auto num = end - start;
    if (num < 3) {
        cout << "Predicate needs 3 or 4 tokens, got " << num << endl;
        return nullptr;
    }
    if (tokens_[start + 1].data() != "is") {
        return nullptr;
    }
    const std::string& condstr = tokens_[start + 2].data();
    auto pred = new RbNode(PhraseType::Predicate);
    pred->range.first = start;
    pred->range.second = start + 3;
    auto vari = new RbNode(PhraseType::Name);
    vari->range.first = start;
    vari->range.second = start + 1;
    pred->children.push_back(vari);
    auto cond = new RbNode(PhraseType::Condition);
    auto val = new RbNode(PhraseType::Value);
    cond->range.first = start + 1;
    val->range.first = start + 2;
    if (condstr == "equal" || condstr == "not" || condstr == "greater" || condstr == "less") {
        pred->range.second++;
        cond->range.first++;
        val->range.first++;
    }
    cond->range.second = cond->range.first + 1;
    val->range.second = val->range.first + 1;
    pred->children.push_back(cond);
    pred->children.push_back(val);
    return pred;
}

bool Classifier::processStatement(RbNode* node) {
    if (node == nullptr) {
        return false;
    }
    auto start = node->range.first;
    auto end = node->range.second;
    if (node->type == PhraseType::DefEvent) {
        auto child = processEvent(start + 1, end);
        node->children.push_back(child);
        return true;
    } else if (node->type == PhraseType::SetVar) {
        auto child = new RbNode;
        child->type = PhraseType::Name;
        child->range.first = start + 1;
        child->range.second = start + 2;
        node->children.push_back(child);
        if (tokens_[start + 3].data().substr(0, 6) == "event:") {
            auto evt = processEvent(start + 3, end);
            node->children.push_back(evt);
        } else {
            auto val = new RbNode(PhraseType::Value);
            val->range.first = start + 3;
            val->range.second = start + 4;
            node->children.push_back(val);
        }
        return true;
    } else if (node->type == PhraseType::Rule) {
        if (tokens_[start + 1].data().substr(0, 6) != "event:") {
            cout << "Error parsing \"on event ...\"" << endl;
            return false;
        }
        auto evt = processEvent(start + 1, end);
        node->children.push_back(evt);
        start = evt->range.second;
        auto action = processActionBlock(start, end);
        if (action == nullptr) {
            return false;
        }
        node->children.push_back(action);
        return true;
    }
    return false;
}

std::string Classifier::getTokenStr(const RbNode* node) const {
    return tokens_[node->range.first].data();
}

void Classifier::printTree(int indent, const RbNode* tree) {
    if (tree == nullptr) {
        tree = &tree_;
    }
    if (indent == 0) {
        cout << "Phrase Tree: start=" << tree->range.first << ", end=" << tree->range.second << endl;
    }
    for (const auto statement : tree->children) {
        cout << std::string(indent, ' ') << "+-- " << *statement << endl;
        if (!statement->children.empty()) {
            printTree(indent + 4, statement);
        }
    }
}

size_t Classifier::visit(Visitor visitor, const RbNode* tree) {
    if (tree == nullptr) {
        tree = &tree_;
    }
    cout << "visit: " << *tree << endl;
    if (!visitor(tree, *this)) {
        return 0;
    }
    size_t ret = 1;
    for (const auto phrase : tree->children) {
        if (!phrase->children.empty()) {
            ret += visit(visitor, phrase);
        }
    }
    return ret;
}

size_t Classifier::visitChildren(Visitor visitor, const RbNode* tree) {
    if (tree == nullptr) {
        tree = &tree_;
    }
    size_t ret = 0;
    for (const auto phrase : tree->children) {
        cout << "visit child: " << *phrase << endl;
        ++ret;
        if (!visitor(phrase, *this)) {
            break;
        }
    }
    return ret;
}

}  // namespace eval
}  // namespace ctlsvc
