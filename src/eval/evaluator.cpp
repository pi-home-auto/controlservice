//
//   Copyright 2021-2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "actions.h"
#include "context.h"
#include "evaluator.h"
#include "events.h"
#include "logger.h"
#include "properties.h"

#include <atomic>
#include <chrono>
#include <condition_variable>
#include <mutex>
#include <ios>
#include <optional>
#include <queue>
#include <thread>
#include <unordered_map>
using std::endl;

namespace ctlsvc {
namespace eval {

Action::Action(const std::string& name)
    : EvalBase(name) {}

bool Action::run(Context*) {
  return true;
}

Action& Action::operator=(const Action& other) {
  if (this != &other) {
    EvalBase::operator=(other);
  }
  return *this;
}


class EvaluatorImpl {
public:
  using EventPtr = Evaluator::EventPtr;

  EvaluatorImpl(Evaluator* eval)
      : running_(false)
      , delayedEvent_(std::make_shared<DelayedEvent>(std::chrono::microseconds(0), nullptr))
      , context_(eval) {
    setupRules();
  }
  ~EvaluatorImpl() {
    running_ = false;
    cv_.notify_one();
    if (thread_.joinable()) {
      thread_.join();
    }
  }

  bool addRule(const Rule& rule) {
    if (rule.name().empty()) {
      return false;
    }
    auto it = rules_.find(rule.name());
    if (it == rules_.end()) {
      rules_[rule.name()] = rule;
      return true;
    }
    return false;
  }
  bool changeRule(const Rule& rule) {
    if (rule.name().empty()) {
      return false;
    }
    auto it = rules_.find(rule.name());
    if (it != rules_.end()) {
      rules_[rule.name()] = rule;
      return true;
    }
    return false;
  }
  Context& context() { return context_; }
  bool deleteRule(const std::string& ruleName) {
    if (ruleName.empty()) {
      return false;
    }
    auto it = rules_.find(ruleName);
    if (it == rules_.end()) {
      return false;
    }
    rules_.erase(it);
    return true;
  }
  std::optional<Rule> getRule(const std::string& ruleName) {
    if (ruleName.empty()) {
      return {};
    }
    auto it = rules_.find(ruleName);
    if (it == rules_.end()) {
      return {};
    }
    return it->second;
  }
  std::vector<Rule> getRules() const {
    std::vector<Rule> rules;
    for (const auto& rule : rules_) {
      rules.push_back(rule.second);
    }
    return rules;
  }
  bool processEvent(EventPtr event) {
    bool matched = false;
    for (auto re : rules_) {
      auto& rule = re.second;
      if (rule.matches(event)) {
        context_.setEvent(event);
        context_.setRule(&rule);
        bool result = rule.getAction()->run(&context_);
        cslog << loglevel(Info) << "Running rule: " << rule
              << " returned " << std::boolalpha << result << endl;
        matched = true;
      }
    }
    if (matched) {
      context_.setEvent(EventPtr());
    }
    return matched;
  }
  void pushEvent(EventPtr& event) {
    if (!event) {
      return;
    }
    if (event->timestamp().time_since_epoch().count() == 0) {
      event->setTimestamp();
    }
    {
      std::lock_guard lck(mutex_);
      eventQueue_.push(event);
    }
    cv_.notify_one();
  }
  bool run() {
    if (running_ && thread_.joinable()) {
      return false;
    }
    cslog << loglevel(Info) << "Starting evaluator" << endl;
    running_ = true;
    thread_ = std::thread([this]() {
      while (running_) {
        EventPtr event;
        {
          std::unique_lock lck(mutex_);
          cv_.wait(lck, [this]{ return !eventQueue_.empty() || !running_; });
          if (!eventQueue_.empty()) {
            event = eventQueue_.front();
            eventQueue_.pop();
          }
        }
        if (event) {
          bool ok = processEvent(event);
          cslog << loglevel(Info) << "Processed event " << event->name()
                << " value:" << event->value() << " = " << ok << endl;
        }
      }
      cslog << loglevel(Info) << "Ended evaluator" << endl;
    });
    return true;
  }
  void setupRules();
  void stop() {
    running_ = false;
  }
  Properties& vars() { return vars_; }

 private:
  std::unordered_map<std::string, Rule> rules_;
  std::atomic_bool running_;
  std::condition_variable cv_;
  std::mutex mutex_;
  std::queue<EventPtr> eventQueue_;
  std::thread thread_;
  Properties vars_;
  std::shared_ptr<DelayedEvent> delayedEvent_;
  Context context_;
};

void EvaluatorImpl::setupRules() {
    auto runAfterDelay
            = [this](Context* context, void* userdata) {
                  if (context != nullptr) {
                      auto ctx_event = context->event();
                      auto devent = dynamic_cast<DelayedEvent*>(ctx_event.get());
                      if (devent->cancelled()) {
                          return false;
                      }
                      auto thr = std::thread([this, ctx_event]() {
                                                 auto devent = dynamic_cast<DelayedEvent*>(ctx_event.get());
                                                 std::this_thread::sleep_for(devent->getDelay());
                                                 auto event = devent->getWrappedEvent();
                                                 if (!event) {
                                                     return false;
                                                 }
                                                 if (devent->cancelled()) {
                                                     return false;
                                                 }
                                                 pushEvent(event);
                                                 return true;
                                             });
                      thr.detach();
                      return true;
                  }
                  return false;
              };
    std::shared_ptr<Action> actionDelayRun(new ActionCall(runAfterDelay, &delayedEvent_));
    Rule ruleDelayedEvent(delayedEvent_, actionDelayRun,
                          [this](const std::shared_ptr<eval::Event> event) {
                              if (event) {
                                  //auto dev = dynamic_cast<const DelayedEvent*>(event);
                                  //delayedEvent_ = std::make_shared<DelayedEvent>(event);
                              }
                              return true;
                          });
    addRule(ruleDelayedEvent);
}

///////////////////    MAIN  CLASS    //////////////////

Evaluator::Evaluator()
    : impl_(std::make_unique<EvaluatorImpl>(this)) {}

Evaluator::~Evaluator() {}

bool Evaluator::addRule(Rule& rule) {
    return impl_->addRule(rule);
}

size_t Evaluator::addRules(const std::vector<Rule>& rules) {
    size_t ret = 0;
    for (auto& rule : rules) {
        if (impl_->addRule(rule)) {
            ++ret;
        }
    }
    return ret;
}

bool Evaluator::changeRule(const Rule& rule) {
    return impl_->changeRule(rule);
}

bool Evaluator::deleteRule(const std::string& ruleName) {
    return impl_->deleteRule(ruleName);
}

bool Evaluator::evaluateRule(EventPtr newEvent, const std::string& ruleName) {
    auto rule = impl_->getRule(ruleName);
    if (rule && rule->matches(newEvent)) {
        impl_->context().setEvent(newEvent);
        impl_->context().setRule(&(*rule));
        return rule->getAction()->run(&impl_->context());
    }
    return false;
}

std::vector<Rule> Evaluator::getRules() {
    return impl_->getRules();
}

std::string Evaluator::getVar(const std::string& name) const {
    struct VarToString {
        std::string operator()(std::monostate& noval) {
            return std::string();
        }
        std::string operator()(int& ival) {
            return std::to_string(ival);
        }
        std::string operator()(float& fval) {
            return std::to_string(fval);
        }
        std::string operator()(const std::string& sval) {
            return sval;
        }
        std::string operator()(bool& bval) {
            return std::string(bval ? "true" : "false");
        }
        std::string operator()(std::vector<bool>& fval) {
            return std::string("(bool array)");
        }
        std::string operator()(std::vector<int>& fval) {
            return std::string("(int array)");
        }
        std::string operator()(std::vector<float>& fval) {
            return std::string("(float array)");
        }
        std::string operator()(std::vector<std::string>& fval) {
            return std::string("(string array)");
        }
        std::string operator()(std::shared_ptr<Event> evp) {
            return std::string("(event)");
        }
    };
    auto val = impl_->vars().get(name);
    auto str = std::visit(VarToString(), val);
    return str;
}

bool Evaluator::push(EventPtr event) {
    // TODO should we keep a history?
    cslog << loglevel(Info) << "Evaluator::push event " << event->name() << endl;
    impl_->pushEvent(event);
    return true;
}

bool Evaluator::removeVar(const std::string& name) {
    return impl_->vars().remove(name);
}

bool Evaluator::setVar(const std::string& name, const std::string& value) {
    return impl_->vars().set(name, value);
}

bool Evaluator::start() {
    return impl_->run();
}

void Evaluator::stop() {
    impl_->stop();
}

Properties& Evaluator::vars() {
    return impl_->vars();
}

const Properties& Evaluator::vars() const {
    return impl_->vars();
}

}  // namespace eval
}  // namespace ctlsvc
