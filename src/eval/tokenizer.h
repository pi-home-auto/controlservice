//
//   Copyright 2022, 2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <istream>
#include <optional>
#include <vector>

namespace ctlsvc {
namespace eval {

/** List of all the types of tokens. */
enum class ReadType {
    Unknown, Normal, WhiteSpace, Comment, Special, Escaped, Quote,
    VariableStart, VariableName, UnclosedQuote, EndOF,
    _Count
};


/** Class that represents a token.
 *  Each token has a value and a type (ReadType). If the type is Quote then the
 *  quoteChar() method will return which quote was used (either ' or "") since the
 *  quote character is not included in the Quote's string value.
 */
class Token {
public:
    /** Construct a token.
     *  @param type  Type of token (see ReadType).
     *  @param chars String value of the token.
     */
    Token(ReadType type = ReadType::Unknown, const std::string& chars = std::string());
    ~Token() = default;

    /** Return the string value of the token. */
    std::string& data() { return chars_; }
    /** Return the string value of the token. */
    const std::string& data() const { return chars_; }
    /** Return the quote character. This is only set and valid when the token's type
     *  is ReadType::Quote.
     */
    char& quoteChar() { return quote_; }
    /** Return the quote character. This is only set and valid when the token's type
     *  is ReadType::Quote.
     */
    const char quoteChar() const { return quote_; }
    /** Return the type of the token. */
    ReadType& type() { return type_; }
    /** Return the type of the token. */
    ReadType type() const { return type_; }

private:
    ReadType type_;
    std::string chars_;
    char quote_;
};


/** The Tokenizer class reads an input stream and breaks up the characters into tokens.
 *  Each token has a type as defined in the ReadType enum. Note that C style comments
 *  (both slash-star and slash-slash) are simply skipped over.
 */
class Tokenizer {
public:
    /** Construct a Tokenizer
     *  @param istr  The input stream to read tokens from.
     *  @param saveComments  This is currently unused, but in the future if this is true
     *               then each comment would be returned unmodified as a token of type
     *               ReadType::Comment.
     */
    Tokenizer(std::istream& istr, bool saveComments = false);
    ~Tokenizer() = default;

    /** Read input until non-whitespace, then accumulate characters in the symbol.
     *  The first char read has a type, so accumulate until the type changes.
     *  If the first char is a quote, then accumulate until the closing quote.
     *  Handles backslash escaping as follows:
     *  \\ = \  \' = '   \" = "
     *  C-style comments are also supported and are just skipped.
     *
     *  @return Optionally, returns a Token if parsing was successful.
     */
    std::optional<Token> getToken();

    /** Read tokens until the given string is encountered. By default, the remaining
     *  tokens on the current line are returned. If an empty string is specified then
     *  all remaining tokens are read from the stream and returned.
     *  @param ending  String of characters that will end parsing when encountered.
     *  @return Array of Tokens that were parsed.
     */
    std::vector<Token> getTokensUntil(const std::string& ending = std::string("\n"));
    /** Read tokens until the given string is encountered. By default, the remaining
     *  tokens on the current line are returned. If an empty string is specified then
     *  all remaining tokens are read from the stream and returned.
     *  @param endings  Array of strings that will end parsing when any of them are encountered.
     *  @return Array of Tokens that were parsed.
     */
    std::vector<Token> getTokensUntil(const std::vector<std::string>& endings);

private:
    std::istream& is_;
    bool saveComments_;
};

/** Write textual name of the given ReadType to the given output stream.
 *  @param os   The output stream.
 *  @param type The type.
 *  @return Reference to the given output stream.
 */
std::ostream& operator<<(std::ostream& os, ReadType type);

}  // namespace eval
}  // namespace ctlsvc
