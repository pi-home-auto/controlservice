//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <chrono>
#include <string>

namespace ctlsvc {
namespace eval {

class EvalBase {
protected:
    EvalBase(const std::string& name)
        : name_(name) {}
public:
    const std::string& name() const {
        return name_;
    }
    EvalBase& operator=(const EvalBase& other) {
        name_ = other.name_;
        return *this;
    }
protected:
    std::string name_;
};


class Event : public EvalBase {
public:
    using TimePoint = std::chrono::steady_clock::time_point;
protected:
    Event(const std::string& name);
public:
    virtual ~Event() = default;

    TimePoint timestamp() { return timestamp_; }
    void setTimestamp() { timestamp_ = std::chrono::steady_clock::now(); }
    void setTimestamp(TimePoint time) { timestamp_ = time; }
    void setValue(int value) { value_ = value; }
    int value() const { return value_; }
    bool operator==(const Event& other) const;
protected:
    int value_;
    TimePoint timestamp_;
};

}  // namespace eval
}  // namespace ctlsvc
