//
//   Copyright 2022-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "eventfactory.h"
#include <cstdlib>
#include <unordered_map>

namespace {
std::unordered_map<std::string, std::shared_ptr<ctlsvc::eval::Event>> user_events_;
}

namespace ctlsvc {
namespace eval {

EventFactory* EventFactory::instance_ = nullptr;

EventFactory* EventFactory::instance() {
    if (instance_ == nullptr) {
        instance_ = new EventFactory;
    }
    return instance_;
}

bool EventFactory::defineEvent(const std::string& _name, const std::string& label, int value) {
    if (_name.empty()) {
        return false;
    }
    std::string name = _name.substr(0, 6) == "event:" ? _name.substr(6) : _name;
    EventPtr event;
    event.reset(new EventUser(label));
    event->setValue(value);
    user_events_[name] = event;
    return true;
}

std::vector<std::string> EventFactory::parameterNames(const std::string& _name) {
    std::vector<std::string> names;
    std::string name = _name.substr(0, 6) == "event:" ? _name.substr(6) : _name;
    if (name == "init") {
        names.push_back("instance");
    } else if (name == "pinChanged") {
        names.push_back("pin");
        names.push_back("value");
    } else if (name == "delayed") {
        names.push_back("event");
        names.push_back("delay");
    } else if (name == "distance") {
        names.push_back("sensor");
        names.push_back("value");
    }
   return names;
}

std::shared_ptr<Event> EventFactory::makeEvent(const std::string& _name, const Properties& params) {
    EventPtr event;
    std::string name = _name.substr(0, 6) == "event:" ? _name.substr(6) : _name;
    if (name == "init") {
        int instance = 0;  // Instance parameter is optional, defaults to 0
        params.getAsInt("instance", instance);
        event = std::make_shared<EventInit>(instance);
    } else if (name == "pinChanged") {
        int pin;
        int value;
        if (params.getAsInt("pin", pin) && params.getAsInt("value", value)) {
            event = std::make_shared<EventPinChanged>(pin, value);
        }
    } else if (name == "delayed") {
        auto delayParam = params.getString("delay");
        auto eventParam = params.get("event");
        EventPtr subevent;
        if (std::holds_alternative<EventPtr>(eventParam)) {
            subevent = std::get<EventPtr>(eventParam);
        } else if (std::holds_alternative<std::string>(eventParam)) {
            subevent = makeEvent(std::get<std::string>(eventParam));
        }
        if (subevent && !delayParam.empty()) {
            // The delay is an integer, optionally followed by "s", "ms" or "us"
            // (for seconds, milliseconds, microseconds). If no suffix, then seconds are assumed.
            int64_t delay = atoll(delayParam.c_str());
            enum { SecUnits, MsUnits, UsUnits } units = SecUnits;
            if (delayParam.size() > 1 && delayParam.back() == 's') {
                delayParam.pop_back();
                if (delayParam.back() == 'm') {
                    units = MsUnits;
                } else if (delayParam.back() == 'u') {
                    units = UsUnits;
                }
            }
            if (units == SecUnits) {
                event.reset(new DelayedEvent(std::chrono::seconds(delay), subevent));
            } else if (units == MsUnits) {
                event.reset(new DelayedEvent(std::chrono::milliseconds(delay), subevent));
            } else if (units == UsUnits) {
                event.reset(new DelayedEvent(std::chrono::microseconds(delay), subevent));
            }
        }
    } else if (name == "distance") {
        int sensor;
        int value;
        if (params.getAsInt("sensor", sensor) && params.getAsInt("value", value)) {
            event = std::make_shared<EventDistance>(sensor, value);
        }
    } else if (name == "fileAdded") {
        event = std::make_shared<EventFileAdded>(params.getString("name"), params.getString("contents"));
    } else if (name == "fileChanged") {
        event = std::make_shared<EventFileChanged>(params.getString("name"), params.getString("contents"));
    } else if (name == "fileDeleted") {
        event = std::make_shared<EventFileDeleted>(params.getString("name"));
    } else {
        auto it = user_events_.find(name);
        if (it != user_events_.end()) {
            event = it->second;
        }
    }
    return event;
}

}  // namespace eval
}  // namespace ctlsvc
