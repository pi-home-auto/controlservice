//
//   Copyright 2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "context.h"
#include "eval/evaluator.h"
#include "logger.h"

using std::endl;


namespace ctlsvc {
namespace eval {

class ContextImpl {
public:
    ContextImpl(Evaluator* eval)
        : eval_(eval)
        , event_()
        , rule_(nullptr) {}
    ~ContextImpl() = default;

private:
    Evaluator* eval_;
    std::shared_ptr<Event> event_;
    Rule* rule_;

    friend Context;
};


Context::Context(Evaluator* eval)
    : impl_(std::make_unique<ContextImpl>(eval)) {}

Context::Context(const Context& other)
    : impl_(std::make_unique<ContextImpl>(other.impl_->eval_)) {
    impl_->event_ = other.impl_->event_;
    impl_->rule_ = other.impl_->rule_;
}

Context& Context::operator=(const Context& other) {
    if (this != &other) {
        impl_->eval_ = other.impl_->eval_;
        impl_->event_ = other.impl_->event_;
        impl_->rule_ = other.impl_->rule_;
    }
    return *this;
}

Context::~Context() {}

Evaluator* Context::evaluator() {
    return impl_->eval_;
}

std::shared_ptr<Event> Context::event() {
    return impl_->event_;
}

PropertyVar Context::getVar(const std::string& name) const {
    return impl_->eval_->vars().get(name);
}

std::string Context::getVarStr(const std::string& name) const {
    return impl_->eval_->getVar(name);
}

bool Context::removeVar(const std::string& name) {
    return impl_->eval_->removeVar(name);
}

Rule* Context::rule() {
    return impl_->rule_;
}

void Context::setEvent(std::shared_ptr<Event> event) {
    impl_->event_ = event;
}

void Context::setRule(Rule* rule) {
    impl_->rule_ = rule;
}

bool Context::setVar(const std::string& name, const PropertyVar& value) {
    return impl_->eval_->vars().set(name, value);
}

bool Context::setVarStr(const std::string& name, const std::string& value) {
    return impl_->eval_->setVar(name, value);
}

}  // namespace eval
}  // namespace ctlsvc
