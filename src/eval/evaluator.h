//
//   Copyright 2021-2022 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "rule.h"

#include <chrono>
#include <functional>
#include <memory>
#include <ostream>
#include <string>
#include <vector>

namespace ctlsvc {
// Forward references
class Properties;
class Statistics;

namespace eval {
class Context;

class Action : public EvalBase {
public:
    Action(const std::string& name);
    virtual ~Action() = default;
    virtual bool run(Context* context);

    Action& operator=(const Action& other);
};

// Forward reference
class EvaluatorImpl;

/**
 * The Evaluator performs actions based on received events.
 * This provides the controlservice its programability and thus uncouples
 * actions and events. Without this class, the logic would have to be written in parts of the 
 * code and compiled in; each system having a custom source/binary.
 * The rules for running actions can be specified in json or a simple "onEvent A runAction B." syntax.

When an event arrives, each rule is checked for a match (or partial match). If no match is found then
the event is discarded. If a rule matches, then it's action is run and the event is discarded.
If the event is part of a multi-event rule, then the event is saved.
When the next event arrives that is part of a multi-event rule, then the saved events are checked if
they can complete the multi-part rule's event list. If so, then these events are removed from the saved
events and the rule's action is executed. If the rule is still only partially matched, then the event is
saved.  Note, the event is saved in the Multi-event rule itself.

When a delayed event arrives, a timer is created with the event. The evalulator has a thread that checks timers.

Questions:
When are the saved events determined to be stale and removed?
 - could be indefinite or an expiration time, or when a different event arrives that cancels it.
If a rule matches, should the remaining rules be searched for more matches?
 - maybe the rule could determine if further rules are allowed to be searched. It could be implemented
   via a separate method, or based on the match check or the result of running the action.
 - simple way is to stop searching once a match occurs. Use multi-action if more than one action should be 
   executed.

Scenarios:

RuleA(EventA, ActionA)
RuleB(EventB, ActionB)
RuleC(EventC & EventG, ActionC)
RuleD(EventD, ActionD & ActionG)
RuleE(EventE & EventB, ActionE)

When EventB arrives, RuleB matches and runs, but RuleE partially matches, so EventB is saved.

Rules can be constructed as Rule(MultiEvent(vector<Event>, AndOr, Action)
For example:
       MultiEvent
         /   \
    EventA & MultiEvent
               /   \
          EventB | EventC

  Note: The evaluator also has a set of global variables that actions can read/write.

  Rules will be written in a rulebase that is parsed and compiled into Rules. See Design.md for a description of
  the rulebase language.
 */
class Evaluator {
public:
    Evaluator();
    ~Evaluator();

    using EventPtr = std::shared_ptr<Event>;

    // TODO Allow merging actions when the rule already exists
    bool addRule(Rule& rule);
    size_t addRules(const std::vector<Rule>& rules);
    bool changeRule(const Rule& rule);
    bool deleteRule(const std::string& ruleName);
    bool evaluateRule(EventPtr newEvent, const std::string& ruleName);
    std::vector<Rule> getRules();
    std::string getVar(const std::string& name) const;
    bool push(EventPtr event);
    bool removeVar(const std::string& name);
    bool setVar(const std::string& name, const std::string& value);
    bool start();
    void stop();
    Properties& vars();
    const Properties& vars() const;

private:
    std::unique_ptr<EvaluatorImpl> impl_;
};


inline std::ostream& operator<<(std::ostream& os, const Rule& rule) {
    os << rule.description();
    return os;
}

}  // namespace eval
}  // namespace ctlsvc
