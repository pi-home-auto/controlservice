//
//   Copyright 2021-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "forker.h"
#include "string_tools.h"

#include <algorithm>
#include <cassert>
#include <cstring>
#include <cerrno>
#include <fcntl.h>
#include <functional>
#include <iostream>
#include <mutex>
#include <sstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unordered_map>
using std::cout;
using std::endl;

namespace {

std::mutex pfMapMutex;
std::unordered_map<ctlsvc::Forker::ForkHandle, ctlsvc::Forker*> pidForkerMap;

}

namespace ctlsvc {

Forker::Forker() {}

Forker::~Forker() {
    {
        std::lock_guard lck(pfMapMutex);
        for (auto fe : entries_) {
            if (fe->pid > 1) {
                _terminate(fe->pid, SIGKILL);
            }
            pidForkerMap.erase(fe->pid);
            delete fe;
        }
    }
    entries_.clear();
    command(Command::Exit);
    //::close(cmd_pipe_[0]);
    ::close(resp_pipe_[1]);
}

bool Forker::init() {
    // TODO add check so init is not called twice
    if (pipe(cmd_pipe_)) {
        cout << "Error: cannot open launcher command pipe" << endl;
        return false;
    }
    if (pipe(resp_pipe_)) {
        cout << "Error: cannot open launcher response pipe" << endl;
        close(cmd_pipe_[0]);
        close(cmd_pipe_[1]);
        return false;
    }
    int pid = fork();
    if (pid == 0) {
        // chdir("/tmp");
        ::close(cmd_pipe_[1]);
        ::close(resp_pipe_[0]);
        setupPath();
        launcher();
        ::close(cmd_pipe_[0]);
        ::close(resp_pipe_[1]);
        sleep(3);
        _exit(0);
    } else {
        ::close(cmd_pipe_[0]);
        ::close(resp_pipe_[1]);
    }
    return true;
}

void Forker::setupPath() {
  static char forkerPath[] = "PATH=/home/andy/homeauto/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin";
  putenv(forkerPath);
}

void Forker::uninit() {
  command(Command::Exit);
}

Forker::ForkHandle Forker::spawn(const std::string& cmd, bool do_io) {
  command(do_io ? Command::SpawnIO : Command::Spawn, cmd);
    
  // check response
  int pid = 0;
  std::string resp;
  auto rcode = response(resp);
  if (rcode == Response::Spawned) {
    cout << "Got response: " << resp << endl;
    pid = std::stoi(resp);
    return pid;
  }
  return kInvalidForkHandle;
}

Forker::ForkHandle Forker::spawn(const std::string& cmd, const std::vector<std::string>& args, bool do_io) {
  if (cmd.empty()) {
    return kInvalidForkHandle;
  }

  std::string fullcmd(cmd);
  if (!args.empty()) {
    fullcmd.append(" ");
    auto sz = StringTools::mergeStrings(args, fullcmd, " ");
    assert(sz > 1);
  }
  return spawn(fullcmd, do_io);
}

int Forker::getStatus(ForkHandle handle) {
    ForkEntry* child = find(handle);
    if (child == nullptr) {
        return -1;
    }
    return -1;
}

bool Forker::isRunning(ForkHandle handle) {
    ForkEntry* child = find(handle);
    if (child == nullptr) {
        return false;
    }
    return false;
}

bool Forker::readFrom(ForkHandle handle, std::string& stdout, std::string& stderr) {
    command(Command::ReadOut, std::to_string(handle));
    auto rcode1 = response(stdout);
    command(Command::ReadError, std::to_string(handle));
    auto rcode2 = response(stderr);
    return rcode1 == Response::Ok || rcode2 == Response::Ok;
}

bool Forker::writeTo(ForkHandle handle, const std::string& input, bool append_return) {
    std::string cmdout = std::to_string(handle) + " " + input;
    if (append_return) {
        cmdout.append("\n");
    }
    command(Command::Write, cmdout);
    std::string resp;
    auto rcode = response(resp);
    return rcode == Response::Ok;
}

bool Forker::terminate(ForkHandle handle, int signal) {
    command(Command::Terminate, std::to_string(handle) + " " + std::to_string(signal));
    std::string resp;
    auto rcode = response(resp);
    return rcode == Response::Ok;
}

void Forker::command(Command cmd, const std::string& parameters) {
    uint32_t len = sizeof(cmd) + parameters.size();
    auto sz = ::write(cmd_pipe_[1], &len, sizeof(len));
    sz = ::write(cmd_pipe_[1], &cmd, sizeof(cmd));
    if (sz == sizeof(cmd)) {
        sz = ::write(cmd_pipe_[1], parameters.c_str(), parameters.size());
    } else {
        resync(false);
    }
}

Response Forker::response(std::string& resp) {
    uint32_t fullsize;
    int fd = resp_pipe_[0];
    Response rcode = Response::Bad;
    auto sz = read(fd, &fullsize, sizeof(fullsize));
    if (sz != sizeof(fullsize)) {
        cout << "Launcher error reading command of size " << sz << endl;
        resync(false);
        return rcode;
    }
    if (fullsize < (uint32_t)sizeof(rcode)) {
        resync(false);
        return Response::Bad;
    }
    fullsize -= sizeof(rcode);
    sz = ::read(fd, &rcode, sizeof(rcode));
    if (sz != sizeof(rcode)) {
        resync(false);
        return Response::Bad;
    }
    char buf[fullsize + 1];
    if (fullsize > 0) {
        auto sz = read(fd, buf, sizeof(buf));
        if (sz != (int)fullsize) {
            cout << "csLauncher out of sync, need " << fullsize << " but got " << sz << endl;
            resync(false);
            return Response::Bad;
        } else if (sz >= 0) {
            buf[sz] = 0;
        }
        resp.assign(buf, sz);
    } else {
        resp.clear();
    }
    return rcode;
}

ForkEntry* Forker::find(ForkHandle handle) {
    auto it = std::find_if(entries_.begin(), entries_.end(),
                           [&handle](const ForkEntry* fe) {
                               return handle == fe->handle;
                           });
    if (it == entries_.end()) {
        return nullptr;
    }
    return *it;
}

void Forker::launcher() {
  using namespace std::placeholders;
  // sigset_t old_ss;
  // sigset_t sset;
  // sigemptyset(&sset);
  // sigaddset(&sset, SIGCHLD);
  // sigprocmask(SIG_BLOCK, &sset, &old_ss);
  // sigprocmask(SIG_SETMASK, &old_ss, NULL);  // restore signals
  struct sigaction sigchld_action = {};
  sigchld_action.sa_handler
      = [](int sig) {
        cout << "Launcher: sigchld=" << sig << endl;
        while (true) {
          int status;
          auto pid = waitpid(-1, &status, WNOHANG);
          if (pid <= 0) {
            break;
          }
          cout << "Launcher: child pid " << pid << " exited ";
          if (WIFEXITED(status)) {
            cout << "normally with exit code: " << WEXITSTATUS(status) << endl;
          } else if (WIFSIGNALED(status)) {
            cout << "with signal: " << WTERMSIG(status) << endl;
          } else {
            cout << "raw status=" << status << endl;
          }
          std::lock_guard lck(pfMapMutex);
          auto forker = pidForkerMap[pid];
          if (forker != nullptr) {
            forker->removePid(pid);
          }
        }
      };
  sigemptyset(&sigchld_action.sa_mask);
  sigchld_action.sa_flags = SA_RESTART | SA_NOCLDSTOP;
  sigaction(SIGCHLD, &sigchld_action, NULL);

  int fd = cmd_pipe_[0];
  while (true) {
    uint32_t fullsize;
    Command cmd;
    auto sz = read(fd, &fullsize, sizeof(fullsize));
    if (sz != sizeof(fullsize)) {
      cout << "Launcher error reading command of size " << sz << endl;
      if (sz == 0) {
        break;
      }
      resync(true);
      continue;
    }
    if (fullsize < (int)sizeof(cmd)) {
      resync(true);
      continue;
    }
    fullsize -= sizeof(cmd);
    sz = read(fd, &cmd, sizeof(cmd));
    if (sz != sizeof(cmd)) {
      cout << "Launcher error reading command of size " << sz << endl;
      if (sz == 0) {
        break;
      }
      resync(true);
      continue;
    }
    char buf[fullsize + 1];
    if (fullsize > 0) {
      int sz = read(fd, buf, sizeof(buf));
      if (sz != (int)fullsize) {
        cout << "Launcher out of sync, need " << fullsize << " but got " << sz << endl;
        resync(true);
        continue;
      } else if (sz >= 0) {
        buf[sz] = 0;
      }
      cout << "read buf=" << buf << endl;
    }
    bool do_io = cmd == Command::SpawnIO;
    switch (cmd) {
      case Command::Exit:
        cout << "Launcher got exit command" << endl;
        return;
      case Command::ReadError:
        cout << "Launcher got ReadError command" << endl;
        if (fullsize > 2) {
          char* pidstr = strtok(buf, " ");
          int cpid = std::stoi(pidstr);
          auto fe = find(cpid);
          Response resp = Response::Bad;
          if (fe != nullptr && fe->pipes[ForkEntry::PIPEERR] != -1) {
            char outbuf[1000];
            outbuf[0] = 0;
            int sz = read(fe->pipes[ForkEntry::PIPEERR], outbuf, sizeof(outbuf));
            if (sz >= 0) {
              resp = Response::Ok;
              outbuf[sz] = 0;
            }
            uint32_t len = sizeof(resp) + strlen(outbuf);
            write(resp_pipe_[1], &len, sizeof(len));
            write(resp_pipe_[1], &resp, sizeof(resp));
            write(resp_pipe_[1], outbuf, strlen(outbuf));
          }
        }
        break;
      case Command::ReadOut:
        cout << "Launcher got ReadOut command" << endl;
        if (fullsize > 2) {
          char* pidstr = strtok(buf, " ");
          int cpid = std::stoi(pidstr);
          auto fe = find(cpid);
          Response resp = Response::Bad;
          if (fe != nullptr && fe->pipes[ForkEntry::PIPEOUT] != -1) {
            char outbuf[1000];
            outbuf[0] = 0;
            int sz = read(fe->pipes[ForkEntry::PIPEOUT], outbuf, sizeof(outbuf));
            if (sz >= 0) {
              resp = Response::Ok;
              outbuf[sz] = 0;
            }
            uint32_t len = sizeof(resp) + strlen(outbuf);
            write(resp_pipe_[1], &len, sizeof(len));
            write(resp_pipe_[1], &resp, sizeof(resp));
            write(resp_pipe_[1], outbuf, strlen(outbuf));
          }
        }
        break;
      case Command::SpawnIO:
      case Command::Spawn: {
        cout << "Launcher got spawn command, controlIO=" << std::boolalpha << do_io << endl;
        int stdin[2] = { -1, -1 };
        int stdout[2] = { -1, -1 };
        int stderr[2] = { -1, -1 };
        if (do_io) {
          if (pipe(stdin)) {
            cout << "Launcher cannot open pipe to child stdin" << endl;
          }
          if (pipe2(stdout, O_NONBLOCK)) {
            cout << "Launcher cannot open pipe to child stdout" << endl;
          }
          if (pipe2(stderr, O_NONBLOCK)) {
            cout << "Launcher cannot open pipe to child stderr" << endl;
          }
        }
        auto pid = fork();
        if (pid == 0) {
          auto prog = strtok(buf, " ");
          std::vector<char*> argv;
          argv.push_back(prog);
          char* arg = nullptr;
          do {
            arg = strtok(NULL, " ");
            argv.push_back(arg);
          } while (arg != nullptr);
          if (do_io) {
            ::close(stdin[1]);
            dup2(stdin[0], 0);
            ::close(stdout[0]);
            dup2(stdout[1], 1);
            ::close(stderr[0]);
            dup2(stderr[1], 2);
          }
          if (execvpe(prog, argv.data(), environ)) {
            cout << "Launcher unable to exec program " << prog << " errno=" << errno << endl;
          }
          _exit(0);
        } else if (pid > 0) {
          ForkEntry* entry = new ForkEntry;
          entry->cmdline = strdup(buf);
          entry->handle = pid;
          entry->pid = pid;
          entry->pipes[0] = stdin[1];
          entry->pipes[1] = stdout[0];
          entry->pipes[2] = stderr[0];
          ::close(stdin[0]);
          ::close(stdout[1]);
          ::close(stderr[1]);
          entries_.push_back(entry);
          std::lock_guard lck(pfMapMutex);
          pidForkerMap[pid] = this;
          Response resp = Response::Spawned;
          std::string pid_resp = std::to_string(pid);
          pid_resp.append("\n");
          uint32_t len = sizeof(resp) + pid_resp.size();
          write(resp_pipe_[1], &len, sizeof(len));
          write(resp_pipe_[1], &resp, sizeof(resp));
          write(resp_pipe_[1], pid_resp.c_str(), pid_resp.size());
        } else {  // Error in fork, no child created
          ::close(stdin[0]);
          ::close(stdin[1]);
          ::close(stdout[0]);
          ::close(stdout[1]);
          ::close(stderr[0]);
          ::close(stderr[1]);
          Response resp = Response::Bad;
          std::string pid_resp = std::to_string(pid);
          pid_resp.append("\n");
          uint32_t len = sizeof(resp) + pid_resp.size();
          write(resp_pipe_[1], &len, sizeof(len));
          write(resp_pipe_[1], &resp, sizeof(resp));
          write(resp_pipe_[1], pid_resp.c_str(), pid_resp.size());
        }
        break;
      }
      case Command::Terminate: {
        cout << "Launcher got terminate command" << endl;
        Response resp = Response::Bad;
        if (fullsize > 2) {
          char* saveptr = nullptr;
          char* pidstr = strtok_r(buf, " ", &saveptr);
          int cpid = std::stoi(pidstr);
          char* sigstr = strtok_r(NULL, " ", &saveptr);
          int csig = std::stoi(sigstr);
          auto ret = _terminate(cpid, csig);
          if (ret) {
            resp = Response::Ok;
          }
        }
        uint32_t len = sizeof(resp) + 1;
        write(resp_pipe_[1], &len, sizeof(len));
        write(resp_pipe_[1], &resp, sizeof(resp));
        write(resp_pipe_[1], "\n", 1);
        break;
      }
      case Command::Write:
        cout << "Launcher got Write command" << endl;
        if (fullsize > 2) {
          char* saveptr;
          char* pidstr = strtok_r(buf, " ", &saveptr);
          int cpid = std::stoi(pidstr);
          auto fe = find(cpid);
          Response resp = Response::Bad;
          if (fe != nullptr && fe->pipes[ForkEntry::PIPEIN] != -1) {
            int sz = write(fe->pipes[ForkEntry::PIPEIN], saveptr, strlen(saveptr));
            if (sz >= 0) {
              resp = Response::Ok;
            }
            uint32_t len = sizeof(resp);
            write(resp_pipe_[1], &len, sizeof(len));
            write(resp_pipe_[1], &resp, sizeof(resp));
          }
        }
        break;
      default:
        cout << "Launcher got invalid command " << static_cast<int>(cmd) << endl;
    }
  }
  cout << "Exit forker launcher" << endl;
}

bool Forker::removePid(pid_t pid) {
    auto it = std::find_if(entries_.begin(), entries_.end(),
                           [&pid](const ForkEntry* fe) {
                               return pid == fe->pid;
                           });
    if (it == entries_.end()) {
        return false;
    }
    free((void *)(*it)->cmdline);
    entries_.erase(it);
    pidForkerMap.erase(pid);
    return true;
}

void Forker::resync(bool is_launcher) {
    uint32_t len = 0;
    if (is_launcher) {
        write(resp_pipe_[1], &len, sizeof(len));
    } else {
        write(cmd_pipe_[1], &len, sizeof(len));
    }
}

bool Forker::_terminate(int pid, int signal) {
    int ret = ::kill(pid, signal);
    return ret == 0;
}

}  // namespace ctlsvc
