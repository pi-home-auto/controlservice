//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <sockstr/Socket.h>

#include "ipcmessage.h"
#include "logger.h"
#include "peer.h"
#include "peermanager.h"
#include "peerthreader.h"
#include "protocol.h"
#include "subclient.h"

#include <cerrno>
#include <cstring>
#include <thread>
#include <unistd.h>

using namespace sockstr;
using std::endl;

namespace ctlsvc {

SubClient::SubClient()
    : bcsStream_()
    , keepRunning_(true)
    , is_connected_(false)
    , greeting_{"Control Service", CS_ROLE_SCS} {}

SubClient::~SubClient() {
  stop();
}

bool SubClient::connectToBase() {
  if (bcsStream_) {
    peerMan_->addBcsPeer(nullptr);
    bcsStream_->close();
    bcsStream_.release();
  }

  Stream* bcsStr = new Socket;
  cslog << loglevel(Info) << "Connecting to base at " << baseUrl_ << endl;
  for (int ii = 0; ii < 3; ++ii) {
    if (bcsStr->open(baseUrl_.c_str(), Socket::modeReadWrite)) {
      cslog << "Connected with base at " << baseUrl_ << endl;
      bcsStream_.reset(bcsStr);
      is_connected_ = true;
      break;
    }
    cslog << loglevel(Error) << "Cannot connect to BCS socket: "
          << errno << ": " << strerror(errno) << endl;
    bcsStr->close();
    is_connected_ = false;
    sleep(3);
  }
  if (!is_connected_ && bcsStr != nullptr) {
    delete bcsStr;
  }
  return is_connected_;
}

bool SubClient::init(const std::string& baseUrl, PeerManager* pm) {
  baseUrl_ = baseUrl;
  peerMan_ = pm;

  connectToBase();
  return true;
}

void SubClient::run(bool background /*= true*/) {
    if (background) {
        thread_ = std::thread(&SubClient::handle, this);
    } else {
        bool lstat = handle();
        cslog << "SubClient listener exited with " << std::boolalpha << lstat << endl;
    }
}

void SubClient::stop() {
    keepRunning_ = false;
    if (thread_.joinable()) {
        thread_.join();
    }
    if (bcsStream_ && bcsStream_->is_open()) {
        bcsStream_->close();
    }
}

bool SubClient::handle(void) {
  bool needGreeting = true;

  while (keepRunning_) {
    if (!(bcsStream_ && bcsStream_->good() && bcsStream_->is_open())) {
      if (bcsStream_) bcsStream_->close();
      connectToBase();
      needGreeting = true;
    }

    if (needGreeting && is_connected_) {
      cslog << "Send BCS greeting: " << greeting_.toShortString() << endl;
      // exchange identity messages with BCS
      *bcsStream_ << greeting_.toString() << endl;
      // (const char*) stream_ gives my URL for connection
      //if (!stream_.is_open()) continue; //return false;	// the BCS didn't like us

      std::string strbuf;
      IpcGreeting peerGreeting;
      cslog << "Read BCS greeting" << endl;
      bcsStream_->read(strbuf, COMMAND_END);
      cslog << "-got: " << strbuf << endl;
      if (peerGreeting.fromString(strbuf)) {
        auto peerRole = peerGreeting.getMember(IpcGreeting::ROLE);
        auto peerType = peerGreeting.getMember(IpcGreeting::TYPE);
        cslog << "SCS greeting from " << peerGreeting.getName()
              << ", role=" << peerRole
              << ", type=" << peerType << endl;

        // verify role is BCS and type is control
        if (peerRole != CS_ROLE_BCS || peerType != "control") {
          cslog << loglevel(Error) << "Connection to BCS is not of correct type.  role="
                << peerRole << ", type=" << peerType << endl;
          bcsStream_->close();
          continue; //return false;
        }
        needGreeting = false;
        Peer* peer = new Peer(CS_ROLE_BCS, bcsStream_.get());
        peer->start();
        peerMan_->addBcsPeer(peer);
      } else {
        cslog << loglevel(Error) << "Cannot parse greeting message from BCS: "
              << peerGreeting.getStatus() << endl;
      }
    } else {
      sleep(4);
    }
    //return true;
  }

  return false;
}

}  // namespace ctlsvc
