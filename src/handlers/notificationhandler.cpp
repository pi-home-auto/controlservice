//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "ipcmessage.h"
#include "logger.h"
#include "notificationhandler.h"
#include "statistics.h"

#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

using std::endl;

namespace {

int gState;

void onStateChanged(void *container, int state) {
    using namespace ctlsvc;
    cslog << "Test :Application State Changed:" << state << endl;
    gState = state;
    cslog << "No Segfaults state:"<< gState << endl;
}

void onConnectionError(void *container, const char* message) {
    using namespace ctlsvc;
    cslog << loglevel(Error) << "Test :Application onConnectionError" << endl;
}

void onMessageReceived(void* container, char* data, int len) {
   // cslog <<"Test :Application onMessageReceived:%.*s\n",len, data);
}

void onFileProgress(void* container, int id, long progress) {
    //cslog <<"Test :Application onFileProgress\n");
}

}  // namespace

namespace ctlsvc {

NotificationHandler::NotificationHandler(const std::string& name)
    : Handler(name) {
    stats_ = std::make_unique<Statistics>("Notification Statistics");
}

NotificationHandler::~NotificationHandler() {
    //TODO try to stop the thread
}

void NotificationHandler::deinit() {
    sessionStopListen(&session_);
    cleanUpSession(&session_);
}

bool NotificationHandler::init(const Options* opts) {
    remotePort_ = opts->getConfigInt("control.notificationHandler.androidPort");
    if (remotePort_ == -1) {
        remotePort_ = 21111;
        cslog << loglevel(Info) << "remotePort number set to default: " << remotePort_ << endl;
    } else {
        cslog << loglevel(Info) << "remotePort set to " << remotePort_ << " as per config file" << endl;
    }
    persistenceFile_ = opts->getConfig("control.notificationHandler.persistenceFile");
    if (!persistenceFile_.empty()) {
        cslog << loglevel(Info) << "Persistence file set to " << persistenceFile_
              << " as per config file" << endl;
    } else {
        persistenceFile_ = "/tmp/bearer";
        cslog << loglevel(Info) << "Persistence file set to default /tmp/bearer" << endl;
    }

    cslog << loglevel(Info) << "Notification Handler init" << endl;
    initSession(&session_);
    int ret = readBearerFromFile(&bearerInfo_,persistenceFile_.c_str());
    if (ret == NETERROR) {
        cslog << loglevel(Error) << "Test: The bearer info is not found" << endl;
        fillLinSockBearerInfo(&bearerInfo_, "localhost", remotePort_, 1024);
    } else {
        cslog <<"Test :The bearer info is loaded from file" << endl;
    }
    setBearerInfo(&session_,&bearerInfo_);
    listener_.onStateChanged = onStateChanged;
    listener_.onConnectionError = onConnectionError;
    listener_.onMessageReceived = onMessageReceived;
    listener_.onFileProgress = onFileProgress;
    sessionRegisterListener(&session_, &listener_, NULL);
    sessionStartListen(&session_);

    stats_->newStat("notifications", 0);
    return true;
}

bool NotificationHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
    time_t timeStamp;

    cslog << "Notification handler processing: " << message.toShortString() << endl;

    auto method = message.getMember(IpcControl::METHOD);
    cslog << "The method is:" << method << endl;

    if (method == "sendnotification") {
        auto msg = message.getMember("message");
        cslog << "The Notification message is " << msg << endl;
        sessionSend(&session_,msg.c_str(), msg.size());
        time(&timeStamp);
        stats_->increment("notifications");
        cslog << loglevel(Info) << "Num Notifications:" << stats_->getInt("notifications") << " Time Taken:"
              << difftime(timeStamp, stats_->getInitTime()) << endl;
    } else if(method  == "setTcp") {
        std::string ip = message.getMember("ipaddress");
        cslog << "The Ip Address to set is " << ip << endl;
        int port = message.getIntMember("port");
        cslog << "The port is " << port << endl;
        remotePort_ = port;
        cslog << "The port number is " << remotePort_ << endl;
        cslog << "Tear down connectiona and set it again" << endl;
        // fill in socket bearer, write bearer to file.
        sessionStopListen(&session_);
        fillLinSockBearerInfo(&bearerInfo_,ip.c_str(), remotePort_, 1024);
        writeBearerToFile(&bearerInfo_, persistenceFile_.c_str());
        setBearerInfo(&session_, &bearerInfo_);
        sessionStartListen(&session_);
    } else if (method == "setSpns") {
        cslog << "Tear down connection" << endl;
        sessionStopListen(&session_);
    }

    reply.setId(IpcReply::ID);
    reply.addMember(IpcReply::STATUS, IpcControl::OK);
    reply.addMember(IpcReply::RESULT, "Notification handler done");

    return true;
}

}  // namespace ctlsvc
