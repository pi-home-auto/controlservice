//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "eval/eventfactory.h"
#include "ipcmessage.h"
#include "logger.h"
#include "monitorhandler.h"
#include "properties.h"
#include "statistics.h"
#include "system.h"

#include <atomic>
#include <cstdlib>
#include <fstream>
#include <thread>
#include <unistd.h>
#include <sys/inotify.h>
using std::endl;

namespace {

static constexpr unsigned int kEventSize = sizeof(struct inotify_event);
static constexpr unsigned int kBufLength = 1024 * (sizeof(struct inotify_event) + 16);

}  // namespace

namespace ctlsvc {

class MonitorThread {
public:
    MonitorThread(MonitorHandler& monitor)
        : monitor_(monitor)
        , fd_(-1)
        , running_(false) {}
    ~MonitorThread() {
        stop();
    }
    void monitorLoop();
    void start();
    void stop();

private:
    std::string printEvent(struct inotify_event *event);
    std::string readFileContent(const std::string& fileName);

private:
    MonitorHandler& monitor_;
    int fd_;
    std::atomic_bool running_;
    std::thread mon_thread_;
};


void MonitorThread::monitorLoop() {
    cslog << loglevel(Info) << "Monitor background process started." << endl;
    Statistics* stats = monitor_.getStats();

    fd_ = inotify_init1(IN_NONBLOCK);
    if (fd_ < 0) {
        cslog << loglevel(Error) << "MonitorThread: Unable to init inotify" << endl;
        stats->increment("errors");
        return;
    }
    int wd = inotify_add_watch(fd_, monitor_.getDir().c_str(),
                               IN_MODIFY | IN_CREATE | IN_DELETE);

    // polling loop
    while (running_) {
        char buffer[kBufLength];
        int length = read(fd_, buffer, kBufLength);
        if (length < 0) {
            if (errno == EAGAIN) {
                std::this_thread::sleep_for(std::chrono::milliseconds(500));
                continue;
            }
            cslog << loglevel(Error) << "MonitorThread: Error reading inotify data, errno: " << errno << endl;
            stats->increment("errors");
            break;  // TODO retry for a while
        }

        bool sendEvent = false;
        int idx = 0;
        while (idx < length) {
            struct inotify_event *event = (struct inotify_event *) &buffer[idx];
            if (event->len) {
                std::string whatEvent = printEvent(event);
                // A file was created or modified
                if (event->mask & (IN_CREATE|IN_MODIFY)) {
                    std::string fname = monitor_.getDir() + "/" + event->name;
                    monitor_.lastEvent_ = whatEvent;
                    monitor_.lastFile_ = event->name;
                    monitor_.lastContent_ = System::readFileContent(fname);
                    cslog << loglevel(Debug) << "File " << whatEvent << ": " << monitor_.lastFile_ << endl;
                    sendEvent = true;
                } else if (event->mask & IN_DELETE) {
                    monitor_.lastEvent_ = whatEvent;
                    monitor_.lastFile_ = event->name;
                    monitor_.lastContent_ = "";
                    cslog << loglevel(Debug) << "File delete: " << monitor_.lastFile_ << endl;
                    sendEvent = true;
                }                    
            }
            idx += kEventSize + event->len;

            if (sendEvent) {
                std::shared_ptr<eval::Event> csevent;
                Properties props;
                props.set("contents", monitor_.lastContent_);
                props.set("name", monitor_.lastFile_);
                if (event->mask & IN_CREATE) {
                    csevent = eval::EventFactory::instance()->makeEvent("fileAdded", props);
                } else if (event->mask & IN_MODIFY) {
                    csevent = eval::EventFactory::instance()->makeEvent("fileChanged", props);
                } else if (event->mask & IN_DELETE) {
                    csevent = eval::EventFactory::instance()->makeEvent("fileDeleted", props);
                }
                if (csevent) {
                    ControlService::instance()->pushEvent(csevent);
                }
                IpcControlAlert msg(monitor_.getName(), "fileEvent", IpcControl::ALERTDEBUG,
                                    IpcControl::OK, monitor_.lastEvent_);
                msg.addMember("param1", monitor_.lastFile_);
                msg.addMember("param2", monitor_.lastContent_);
                stats->increment("events");
                monitor_.sendToNode(&msg);
            }
        }
    }

    inotify_rm_watch(fd_, wd);
    close(fd_);
}

std::string MonitorThread::printEvent(struct inotify_event *event) {
    std::string dftype(((event->mask & IN_ISDIR) ? "directory " : "file "));

    std::string happened;
    if (event->mask & IN_CREATE) {
        happened = "created";
    } else if (event->mask & IN_DELETE) {
        happened = "deleted";
    } else if (event->mask & IN_MODIFY) {
        happened = "modified";
    }

    cslog << "Monitor: " << dftype << event->name << " was " << happened << endl;
    return happened;
}

void MonitorThread::start() {
    running_ = true;
    mon_thread_ = std::thread(&MonitorThread::monitorLoop, this);
}

void MonitorThread::stop() {
    running_ = false;
    ::close(fd_);
    if (mon_thread_.joinable()) {
        mon_thread_.join();
    }
}

/////////////////////////////////////////////////////////////////

MonitorHandler::MonitorHandler(const std::string& name)
    : Handler(name)
    , thread_(std::make_unique<MonitorThread>(*this))
    , dir_("monitored") {
    stats_ = std::make_unique<Statistics>("Monitor Statistics");
}

MonitorHandler::~MonitorHandler() {
    thread_->stop();
}

void MonitorHandler::deinit() {
    thread_->stop();
}

bool MonitorHandler::init(const Options* opts) {
    auto mondir = opts->getConfig("control.monitorHandler.location");
    if (!mondir.empty()) {
        dir_ = mondir;
    }
    if (dir_[0] != '/') {
#ifdef BASE_DIR
        dir_ = std::string(BASE_DIR) + "/incoming/" + dir_;
#else
        const char* home = getenv("HOME");
        dir_ = std::string(home) + "/" + dir_;
#endif
    }
    cslog << loglevel(Info) << "Monitoring directory " << dir_ << endl;

    stats_->newStat("requests", 0);
    stats_->newStat("events", 0);
    stats_->newStat("errors", 0);

    thread_->start();
    return true;
}

bool MonitorHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
    cslog << "Monitor handler processing: " << message.toString() << std::flush;
    stats_->increment("requests");

    reply.setId(IpcReply::ID);
    reply.addMember(IpcReply::RESULT, "Monitor handler");
    reply.addMember("param1", lastFile_);
    reply.addMember("param2", lastContent_);

    return true;
}

}  // namespace ctlsvc
