//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "devices/gpioinputdevice.h"
#include "devices/gpiooutputdevice.h"
#include "handler.h"
#include "sequencer.h"

#include <chrono>
#include <memory>
#include <optional>
#include <string>
#include <vector>

namespace ctlsvc {

// Forward references
class GpioHandlerImpl;
class IpcMessage;
class Peer;

/** Handler to control and query Gpio pins.
 *  This handler uses Statistics.
 *  TODO allow pwm and color leds
 */
class GpioHandler : public Handler {
public:
  GpioHandler(const std::string& name);
  virtual ~GpioHandler();

  virtual void deinit() override;
  virtual bool init(const Options* options) override;
  virtual bool setup() override;
  virtual bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) override;

  const std::vector<device::GpioInputDevice>& getInputDevices() const;
  const std::vector<device::GpioOutputDevice>& getOutputDevices() const;
  std::vector<device::GpioOutputDevice>& getOutputDevices();

  bool addProgram(SequenceEntry& entry);
  std::unique_ptr<ctlsvc::Program> makeGpioProgram(const std::string& name, const std::string& strseq);
  bool runProgram(const std::string& progName, int repeat, int level = 1);

  std::optional<device::GpioInputDevice*> getInDevice(int pin);
  std::optional<const device::GpioInputDevice*> getInDevice(int pin) const;
  std::optional<device::GpioOutputDevice*> getOutDevice(int pin);
  std::optional<const device::GpioOutputDevice*> getOutDevice(int pin) const;

private:
  std::unique_ptr<GpioHandlerImpl> impl_;
};

}  // namespace ctlsvc
