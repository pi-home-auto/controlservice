//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "handler.h"

#include <memory>
#include <string>

namespace ctlsvc {

// Forward references
class IpcMessage;
class MonitorThread;
class Peer;

/** Handler to monitor changes to a directory.  Useful as an example handler.
 *  This handler uses Statistics. */
class MonitorHandler : public Handler {
public:
    MonitorHandler(const std::string& name);
    virtual ~MonitorHandler();

    void deinit() override;
    bool init(const Options* options) override;
    bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) override;

    const std::string& getDir() const { return dir_; }

private:
    std::unique_ptr<MonitorThread> thread_;
    /** Directory to monitor.
     *  If not absolute path, assumes relative to HA_BASE_DIR directory. */
    std::string dir_;
    std::string lastContent_;
    std::string lastEvent_;
    std::string lastFile_;

    friend class MonitorThread;
};

}  // namespace ctlsvc
