//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "garagehandler.h"
#include "ipcmessage.h"
#include "logger.h"
#include <assert.h>
#include <atomic>
#include <fcntl.h>
#include <stdint.h>
#include <string.h>
#include <thread>
#include <unistd.h>
#ifdef USE_RADIOLIB
#include "sirf_radiolib.h"
#include "sirf_tuning.h"
#else
class sirf_key;
#endif
using std::endl;

#define KEYFOB_MAX_KEYS 32

#define kf_printf(x...)                                      \
    do {                                                     \
        fprintf(stderr, x);                                  \
        fprintf(stderr, "\n");                               \
    } while (0)

namespace ctlsvc {

class GarageDoorThread {
public:
    GarageDoorThread(GarageHandler& ghandler);
    ~GarageDoorThread() = default;
    bool handle();
    bool start();
    void stop();

    int kf_server();
    int kf_sendKeyThenRecv(sirf_key * key);
private:
    GarageHandler& ghandler_;
    std::atomic_bool is_running_;
    std::thread thread_;
};

#ifdef USE_RADIOLIB
static sirf_dev dev;
static struct {
    sirf_key broadcast;
    sirf_key key[KEYFOB_MAX_KEYS];
    size_t   key_count;
    int      sendkey_req;
    sirf_key sendkey;
} keyfob_ring;

int kf_strtokey (sirf_key * key, const char * keyStr) {
    int ret = sirf_strtokey (key, keyStr);

    if (ret) {
        kf_printf("Invalid key format: %s", keyStr);
    }

    return ret;
}

void kf_shutdown() {
    if (dev.fd != 0) {
        sirf_close(&dev);
        dev.fd = 0;
    }
}

int kf_set_rx() {
    int ret = 0;
    kf_shutdown();
    ret = sirf_open(&dev, "/dev/sirf", SIRF_FLAG_DEFAULT|SIRF_FLAG_RAW_RX,&builtin_tune_310MHz_OOK_2kbps_rx);
    //sirf_reset_raw_fifo(&dev);
    return ret;
}

int kf_set_tx() {
    int ret = 0;
    kf_shutdown();
    ret = sirf_open(&dev, "/dev/sirf", SIRF_FLAG_DEFAULT|SIRF_FLAG_RAW_TX,&builtin_tune_310MHz_OOK_2kbps_tx);
    //sirf_reset_raw_fifo(&dev);
    return ret;
}
#endif

GarageDoorThread::GarageDoorThread(GarageHandler& ghandler)
    : ghandler_(ghandler) {}

int GarageDoorThread::kf_sendKeyThenRecv(sirf_key * key) {
#ifdef USE_RADIOLIB
    int j;
    int ret;

    ret = kf_set_tx();
    if ( !ret )
    {
        for (j = 0; j < 12; j ++ )
        {
            sirf_write_key(&dev, key);
        }
    }
    else
    {
        kf_printf("Writer Driver Error on init: %d", ret);
    }
    if ( !ret )
    {
        usleep(1000*250);
        ret = kf_set_rx();
    }
    else
    {
        kf_printf("Reset radio to RX failed.");
    }
    return ret;
#else
    return -1;
#endif
}

int GarageDoorThread::kf_server()
{
#ifdef USE_RADIOLIB
    //cslog << "Initializing key server." << endl;
    char     buff[SIRF_KEYSTR_LENGTH];
    int      ret;
    unsigned int i;
    sirf_key key;

    //cslog << "Server Configuration:" << endl;
    //cslog << "  Broadcast Key: "
    //      << sirf_keytostr(&keyfob_ring.broadcast, buff, SIRF_KEYSTR_LENGTH) << endl;
    //cslog << "  User Keys:" << endl;
    for ( i = 0; i < keyfob_ring.key_count; i ++ )
    {
        //cslog << "    " <<  sirf_keytostr(&keyfob_ring.key[i], buff, SIRF_KEYSTR_LENGTH) << endl;
    }

    ret = kf_set_rx();
    if ( ! ret )
    {
        cslog << "Waiting for key." << endl;
        for (;;)
        {
            ret = sirf_read_key(&dev, &key);
            if ( !ret )
            {
                kf_printf("Recv Key: %s", sirf_keytostr(&key, buff, SIRF_KEYSTR_LENGTH));
                for (i = 0; i < keyfob_ring.key_count && !ret; i ++ )
                {
                    if (key.key == keyfob_ring.key[i].key )
                    {
                        kf_printf("Accepted on index %d.", i);

                        snprintf(buff, sizeof(buff), "user%d", i);
                        buff[sizeof(buff)-1] = '\0';
                        std::string buffAsStdStr = buff;
                        IpcControlAlert msg("garage", "key", IpcControl::ALERTHIGH,
                                            IpcControl::OK, "key_accepted");
                        msg.addMember("param2", buff);
                        ghandler_.sendToNode(&msg);
                        ret = kf_sendKeyThenRecv(&keyfob_ring.broadcast);
                    }
                }
            } else if (ret != -1) {
                kf_printf("Writer Driver Error: %d", ret);
            }
            if (keyfob_ring.sendkey_req) {
                kf_printf("Sending Alt Key: %s", sirf_keytostr(&keyfob_ring.broadcast, buff, SIRF_KEYSTR_LENGTH));
                keyfob_ring.sendkey_req = 0;
                ret = kf_sendKeyThenRecv(&keyfob_ring.broadcast);
            }
        }
        kf_shutdown();
    } else if (ret == -ETIMEDOUT) {
        // ignore read timeout
    } else {
        kf_printf("Unable to initialize radio.");
    }
    return ret;
#else
    return -1;
#endif
}


bool GarageDoorThread::handle() {
#ifdef USE_RADIOLIB
    cslog << "Garage door background process started." << endl;

    // pretty easy to add new keys
    // if they don't validate..... I haven't caught it so
    // make sure to check the log.
    kf_strtokey(&keyfob_ring.broadcast,"0x000:10");
    kf_strtokey(&keyfob_ring.key[0],"0x155:10");
    kf_strtokey(&keyfob_ring.key[1],"0x0AA:10");
    keyfob_ring.key_count = 2;

    while (is_running_) {
        kf_server();
        sleep(5);
    }
    return true;
#else
    cslog << "Garage door process exiting." << endl;
    return false;
#endif
}

bool GarageDoorThread::start() {
    if (is_running_) {
        return false;
    }
    is_running_ = true;
    thread_ = std::thread(&GarageDoorThread::handle, this);
    return true;
}

void GarageDoorThread::stop() {
    is_running_ = false;
    if (thread_.joinable()) {
        thread_.join();
    }
}


GarageHandler::GarageHandler(const std::string& name)
    : Handler(name)
    , thread_(std::make_unique<GarageDoorThread>(*this))
{
}

GarageHandler::~GarageHandler() {}

bool GarageHandler::init(const Options*) {
    return thread_->start();
}

void GarageHandler::deinit() {
    thread_->stop();
}

bool GarageHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
    cslog << "Garage handler processing: " << message.toShortString() << endl;

    std::string method = message.getMember(IpcControl::METHOD);
    if (method == "readkey") {
        //TODO check param1
    } else if (method == "writekey") {
        fobId_ = message.getMember("param2");
#ifdef USE_RADIOLIB
        kf_strtokey(&keyfob_ring.sendkey, message.getMember("key").c_str());
        keyfob_ring.sendkey_req = 1;
        //TODO check param1
#else
        if (fobId_ == "open") {
            cslog << loglevel(Info) << "The garage door was opened." << endl;
        }
#endif
    } else {
        return false;
    }

    reply.setId(IpcReply::ID);
    //TODO reply.addMember(IpcReply::STATUS, IpcControl::OK);
    reply.addMember(IpcReply::RESULT, "Garage handler fob=" + fobId_);

    return true;
}

}  // namespace ctlsvc
