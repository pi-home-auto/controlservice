//
//   Copyright 2021-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "devices/gpiooutputdevice.h"
#include "devices/minipeerdevice.h"
#include "gpiohandler.h"
#include "ipcmessage.h"
#include "logger.h"
#include "object_depot.h"
#include "sequencer.h"
#include "stepmotorhandler.h"
#include "steps.h"

#include <array>
#include <chrono>
#include <string>

using std::endl;

namespace ctlsvc {

class StepMotorImpl {
public:
  static constexpr size_t kNumPins = 4;
  static constexpr int kStepDelayMs = 4;
  static constexpr int kProgramLevel = 1;
  inline static const std::string FORWARD = "Forward";
  inline static const std::string BACKWARD = "Backward";

  StepMotorImpl();
  void deinit();
  bool init(const Options* opts);

private:
  Sequencer sequencer_;
  int32_t step_;
  float degreeToStep_;
  std::array<device::GpioOutputDevice*, kNumPins> devout_;
  std::shared_ptr<Device> mpdevice_;

  friend StepMotorHandler;
};

StepMotorImpl::StepMotorImpl()
    : sequencer_()
    , step_(0)
    , degreeToStep_(1) {}

void StepMotorImpl::deinit() {
    sequencer_.stop(true);
    sequencer_.removeProgram(FORWARD, kProgramLevel);
    sequencer_.removeProgram(BACKWARD, kProgramLevel);
}

bool StepMotorImpl::init(const Options* opts) {
    auto pins = opts->getConfigIntArray("control.stepMotorHandler.pins");
    float degreeToStep = opts->getConfigFloat("control.stepMotorHandler.degreeToStep");
    int startStep = opts->getConfigInt("control.stepMotorHandler.startStep");

    auto handler = ControlService::getHandler("gpio");
    auto& outDevices = dynamic_cast<GpioHandler*>(handler)->getOutputDevices();
    // for (size_t ii = 0; ii < pins.size(); ++ii) {
    //     int pin = pins[ii];
    int ii = 0;
    for (int pin : pins) {
      if (pin < 1 || pin > static_cast<int>(outDevices.size())) {
        return false;
      }
      devout_[ii++] = &outDevices[pin - 1];
    }
    step_ = startStep;
    degreeToStep_ = degreeToStep;

    // TODO The first step should only be done the first time when looping. And should
    //      probably start and end by clearing all pins.
    // TODO use speed instead of hardcoded delay
    auto delay = std::chrono::milliseconds(kStepDelayMs);
    // Forward, clockwise pattern:  (1000, 1100, 0100, 0110, 0010, 0011, 0001, 1001)
    std::array<std::pair<int,int>, 9> cwise = { {
        {0,1}, {1,1}, {0,0}, {2,1}, {1,0}, {3,1}, {2,0}, {0,1}, {3,0}
    } };
    auto fwdProgram = std::make_unique<Program>(FORWARD);
    for (auto [devnr, value] : cwise) {
        fwdProgram->addStep(new GpioOutStep(devout_[devnr], value, delay));
    }
    sequencer_.addProgram(std::move(fwdProgram), kProgramLevel, 0, SequenceEntry::Off);

    // Backward, counterclockwise:  (1000, 1100, 0100, 0110, 0010, 0011, 0001, 1001)
    std::array<std::pair<int,int>, 9> anticwise = { {
        {3,1}, {2,1}, {3,0}, {1,1}, {2,0}, {0,1}, {1,0}, {3,1}, {0,0}
    } };
    auto bkProgram = std::make_unique<Program>(BACKWARD);
    for (auto [devnr, value] : anticwise) {
        bkProgram->addStep(new GpioOutStep(devout_[devnr], value, delay));
    }
    sequencer_.addProgram(std::move(bkProgram), kProgramLevel, 0, SequenceEntry::Off);
    sequencer_.run(true);
    return true;
}

//  MAIN CLASS
StepMotorHandler::StepMotorHandler(const std::string& name)
    : Handler(name)
    , impl_(std::make_unique<StepMotorImpl>()) {}

StepMotorHandler::~StepMotorHandler() {}

// Handler interface
void StepMotorHandler::deinit() {
    impl_->deinit();
}

bool StepMotorHandler::init(const Options* options) {
    return impl_->init(options);
}

bool StepMotorHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
    cslog << "StepMotor handler processing request: " << message.toString() << endl;
    reply.setId(IpcReply::ID);
    reply.addMember(IpcReply::RESULT, "StepMotor handler");
    // check if this is a minipeer device
    device::minipeer::StepMotor* mp_motor = nullptr;
    auto uuid = message.getMember("uuid");
    if (!uuid.empty()) {
      auto depot = ControlService::instance()->getObjectDepot();
      auto uname = depot->uuidToUName(uuid);
      // remove the peripheral from the minipeer name
      auto pos = uname.find_last_of(' ');
      std::string mp_devname;
      if (pos != std::string::npos) {
        mp_devname = uname.substr(uname.find_last_of(' ') + 1);
        mp_devname = mp_devname.substr(0, mp_devname.find_last_of('.'));
      }
      // Check if minipeer is in cache
      device::MiniPeerDevice* mpd = nullptr;
      if (impl_->mpdevice_) {
        mpd = static_cast<device::MiniPeerDevice*>(impl_->mpdevice_.get());
        if (mpd->getName() == mp_devname) {
          cslog << loglevel(Info) << "Found minipeer " << mp_devname << " in cache." << endl;
        } else {
          mpd = nullptr;
        }
      }
      if (mpd == nullptr) {
        auto mp_periname = uname.substr(uname.find_last_of('.') + 1);
        auto devobj = depot->find(mp_devname);
        cslog << "StepMotorHandler uuid=" << uuid << " mp name=" << mp_devname
              << " peripheral=" << mp_periname << ",  object: " << devobj << endl;
        if (std::holds_alternative<DeviceObject>(devobj)) {
          impl_->mpdevice_ = std::get<DeviceObject>(devobj);
          mpd = static_cast<device::MiniPeerDevice*>(impl_->mpdevice_.get());
        }
      }
      if (mpd == nullptr) {
        cslog << loglevel(Error) << "Cannot find MiniPeer device." << endl;
        return false;
      }
      if (mpd->status() != Device::Mode::ReadWriteOpen) {
        mpd->open(Device::Mode::ReadWriteOpen);
      }
      if (!mpd->peripheral(0, &mp_motor)) {
        cslog << loglevel(Error) << "Minipeer device cannot find s0 peripheral" << endl;
        return false;
      }
    } else {
      cslog << loglevel(Error) << "No uuid for stepmotor device" << endl;
      return false;
    }
    std::string method = message.getMember(IpcControl::METHOD);
    if (method == "readkey") {
      if (mp_motor) {
        impl_->step_ = mp_motor->position();
      }
      reply.addMember("moving", isMoving());
      reply.addMember("step", impl_->step_);
    } else if (method == "move") {
        // members: steps, speed
        int steps = message.getIntMember("steps");
        int speed = message.getIntMember("speed");
        cslog << loglevel(Info) << "++Params: steps=" << steps << "   speed=" << speed << endl;
        if (mp_motor) {
          int num = abs(steps);
          int moved = (steps < 0) ? mp_motor->moveDown(num) : mp_motor->moveUp(num);
          if (moved <= 0) {
            cslog << loglevel(Error) << "stepmotor failed to move, returned " << moved << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
          }
        } else {
          if (!move(steps, speed)) {
            cslog << loglevel(Error) << "stepmotor failed to move" << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
          }
        }
    } else if (method == "moveTo") {
      int steps = message.getIntMember("steps");
      int speed = message.getIntMember("speed");
      cslog << loglevel(Info) << "++Params: steps=" << steps << "   speed=" << speed << endl;
      if (mp_motor) {
        if (steps != impl_->step_) {
          auto numsteps = impl_->step_ - steps;
          int moved = (numsteps < 0) ? mp_motor->moveDown(numsteps) : mp_motor->moveUp(numsteps);
          if (moved <= 0) {
            cslog << loglevel(Error) << "stepmotor failed to move, returned " << moved << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
          }
        }
      } else {
        if (!moveTo(steps, speed)) {
            cslog << loglevel(Error) << "stepmotor failed to move" << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
        }
      }
    } else {
        cslog << loglevel(Error) << "stepmotor method not found: " << method << endl;
        reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
        return false;
    }
    reply.addMember(IpcReply::STATUS, IpcControl::OK);
    return true;
}

// Step motor interface
void StepMotorHandler::step(StepType step) {
    impl_->step_ = step;
}

bool StepMotorHandler::isMoving() const {
    return impl_->sequencer_.isRunning();
}

StepMotorHandler::StepType StepMotorHandler::step() const {
    return impl_->step_;
}

bool StepMotorHandler::move(StepType steps, int speed) {
    if (isMoving()) {
        return false;
    }
    const auto& prog = (steps > 0) ? StepMotorImpl::FORWARD : StepMotorImpl::BACKWARD;
    impl_->sequencer_.modifyProgram(prog, 1, abs(steps), SequenceEntry::RunOnceKeep);
    impl_->step_ += steps;
    return true;
}

bool StepMotorHandler::moveTo(StepType pos, int speed) {
    cslog << loglevel(Info) << "StepMotor move moving=" << isMoving() << std::endl;
    if (isMoving() || pos == impl_->step_) {
        return false;
    }
    StepType numsteps = impl_->step_ - pos;
    return move(numsteps, speed);
}

int StepMotorHandler::degreeToStep(float deg) {
  int stp = deg * impl_->degreeToStep_ + 0.49f;
  return stp;
}

float StepMotorHandler::stepToDegree(int stp) {
  auto& d2s = impl_->degreeToStep_;
  float deg = (static_cast<float>(stp) + d2s * 0.5f) / d2s;
  return deg;
}

}  // namespace ctlsvc
