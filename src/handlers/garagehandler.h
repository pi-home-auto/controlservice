//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "handler.h"

#include <memory>
#include <string>

namespace ctlsvc {
// Forward references
class IpcMessage;
class GarageDoorThread;

/**
 * Handler to control and monitor (garage door) radios.
 * It has a separate thread for monitoring and a #request method to
 * respond to requests to/from this handler's data.
 */
class GarageHandler : public Handler {
public:
    GarageHandler(const std::string& name);
    virtual ~GarageHandler();

    /** Initialize the radios, etc. */
    bool init(const Options* options) override;
    void deinit() override;
    /** Execute a user request.
     *
     *  This API must not block.
     *  @param message  contains the IPC request
     *  @param reply  Reply message returned from this request
     *  @param peer  Peer that is making the request
     *  @return  True if successful, otherwise false. */
    bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) override;

private:
    /** Poll the radio status and alert on important state changes.
     *  This thread polls radio api in a loop */
    std::unique_ptr<GarageDoorThread> thread_;
    std::string fobId_;
};

}  // namespace ctlsvc
