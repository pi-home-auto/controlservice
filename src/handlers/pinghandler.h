//
//   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "handler.h"

#include <memory>
#include <string>

namespace ctlsvc {
// Forward references
class IpcMessage;
class PingerThread;
class Peer;

/** Handler to respond to pings and (optionally) send ping requests */
class PingHandler : public Handler {
public:
    PingHandler(const std::string& name);
    virtual ~PingHandler();

    bool init(const Options* options) override;
    void deinit() override;
    bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) override;

private:
    std::unique_ptr<PingerThread> pinger_;
};

}
