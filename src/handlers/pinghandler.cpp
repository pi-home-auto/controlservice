//
//   Copyright 2020-2025 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include <atomic>
#include <unistd.h>
#include <thread>
#include <vector>
#include <sockstr/Stream.h>

#include "controlservice.h"
#include "do_once.h"
#include "eval/events.h"
#include "ipcmessage.h"
#include "logger.h"
#include "peer.h"
#include "pinghandler.h"
#include "protocol.h"
#include "statistics.h"
using std::endl;

namespace ctlsvc {

class PingerThread {
public:
  PingerThread(PingHandler& handler)
      : handler_(handler) {}
  ~PingerThread() = default;
  void handle(int interval);
  bool start(int interval);
  void stop();

private:
  void pingPeer(Peer* peer);

  PingHandler& handler_;
  std::atomic_bool is_running_;
  std::thread thread_;
  DoOnce doer_;
};

using namespace sockstr;


void PingerThread::handle(int interval) {
  cslog << loglevel(Info) << "Pinger started, interval=" << interval << " seconds." << endl;
  ControlService* cs = ControlService::instance();

  // polling loop
  while (is_running_) {
    sleep(interval);

    cslog << loglevel(Debug) << "Ping loop through peers" << endl;
    auto& peers = cs->getPeers();
    for (auto peer : peers) {
      if (peer == nullptr || peer == cs->findNodePeer()) {
        continue;
      }
      if (peer->isRunning()) {
        pingPeer(peer);
      }
    }
  }
}

bool PingerThread::start(int interval) {
  if (is_running_) {
    return false;
  }
  is_running_ = true;
  thread_ = std::thread(&PingerThread::handle, this, interval);
  return true;
}

void PingerThread::stop() {
  is_running_ = false;
  if (thread_.joinable()) {
    thread_.join();
  }
}

void PingerThread::pingPeer(Peer* peer) {
  IpcControl pingRequest("ping", "pingRequest");
  pingRequest.addMember("param1", "0");

  /* Haven't heard from peer in 2 intervals, send an alert. */
  if (!peer->getPingResponse()) {
    // TODO Each peer will have its own DoOnce class. Currently this will only handle 1 peer
    auto doAlert = [peer]() {
      std::string msg("Unable to reach peer: ");
      msg.append(peer->getName());
      cslog << loglevel(Warning) << msg << endl;
      auto alert = std::make_shared<eval::EventAlert>(msg);
      ControlService* cs = ControlService::instance();
      cs->pushEvent(alert);
    };
    doer_.run(doAlert);
  } else {
    doer_.reset([peer]() {
      std::string msg("Peer: ");
      msg.append(peer->getName());
      msg.append(" is reachable");
      cslog << loglevel(Info) << msg << endl;
      // TODO maybe also send a "good" Alert
    });
  }

  Stream* client = peer->getStream();
  if (client && client->good() && client->is_open()) {
    auto cookie = peer->getCookie();
    cslog << loglevel(Trace) << "Pinging " << peer->getName() 
          << ", cookie=" << cookie << std::boolalpha
          << " pingStatus=" << peer->getPingResponse() << "..." << endl;
    Statistics* stats = handler_.getStats();
    stats->increment("pingsSent");

    pingRequest.addMember("cookie", cookie);
    cslog << loglevel(Trace) << pingRequest.toShortString() << std::flush;
    *client << pingRequest.toString() << endl;
    peer->setPingResponse(false);
    if (!client->good()) {
      cslog <<loglevel(Error) << "-ping client for peer " << peer->getName() << " not good" << endl;
      stats->increment("errors");
    }
  } else {
    // Watch out, the peerthreader may have deleted peer by now
    peer->setPingResponse(false);
  }
}

/////////////////////////////////////////////////////////////////

PingHandler::PingHandler(const std::string& name)
    : Handler(name)
    , pinger_(std::make_unique<PingerThread>(*this)) {
    stats_ = std::make_unique<Statistics>("Ping Statistics");
}

PingHandler::~PingHandler() {}

bool PingHandler::init(const Options* opts) {
    cslog << loglevel(Info) << "Initializing Ping Handler" << endl;

    stats_->newStat("requests", 0);
    stats_->newStat("pingsSent", 0);
    stats_->newStat("pingsReceived", 0);
    stats_->newStat("errors", 0);

    bool runPinger = opts->getConfigInt("control.pingHandler.runPinger") > 0;
    if (runPinger) {
      // TODO connect to "modules"
      int pingInterval = opts->getConfigInt("control.pingHandler.interval");
      pinger_->start(pingInterval);
    } else {
      cslog << loglevel(Warning)
            << "Controlservice will not initiate pings." << endl;
    }
    return true;
}

void PingHandler::deinit() {
  pinger_->stop();
}

bool PingHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
  cslog << loglevel(Debug) << "Ping handler processing: " << message.toShortString() << endl;
  stats_->increment("requests");

  //reply.addMember(IpcReply::RESULT, "Ping handler");
  std::string method = message.getMember(IpcControl::METHOD);

  if (method == "pingRequest") {
    std::string who = message.getMember("param1");
    cslog << loglevel(Debug) << "got ping request from " << who << endl;
    auto cookie = message.getIntMember("cookie");
    reply.setId(IpcControl::ID);
    reply.addMember(IpcControl::HANDLER, "ping");
    reply.addMember(IpcControl::METHOD, "pingResponse");
    reply.addMember("cookie", cookie);
  } else if (method == "pingResponse") {
    // find peer with cookie and set their ping response
    auto cookie = message.getIntMember("cookie");
    cslog << loglevel(Debug) << "Got pingResponse from cookie " << cookie << endl;
    if (peer) peer->setPingResponse(true);
    reply.setId("NoReply");
    reply.addMember(IpcReply::RESULT, "Ping responded");
    stats_->increment("pingsReceived");
  } else {
    cslog << loglevel(Error) << "Invalid ping method: " << method << endl;
    reply.setId(IpcReply::ID);
    reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
    stats_->increment("errors");
    return false;
  }

  reply.addMember(IpcReply::STATUS, IpcControl::OK);
  return true;
}

}  // namespace ctlsvc
