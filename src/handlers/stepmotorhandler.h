//
//   Copyright 2021-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "handler.h"

#include <memory>

namespace ctlsvc {

// Forward references
class IpcMessage;
class Peer;
class StepMotorImpl;

/* TO DO
 * Only 1 stepping motor is supported. Need to be able to specify which pins to use
 * or if it is on a minipeer.
 */
/** Handler to control a stepping motor that is connected to 4 gpio lines.
 *  This handler uses Statistics.
 */
class StepMotorHandler : public Handler {
public:
  using AngleType = float;
  using StepType = int32_t;
public:
  StepMotorHandler(const std::string& name);
  virtual ~StepMotorHandler();

  // Handler interface
  virtual void deinit() override;
  virtual bool init(const Options* options) override;
  virtual bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) override;

  // Step motor interface
  void step(StepType step);
  bool isMoving() const;
  StepType step() const;
  bool move(StepType steps, int speed);
  bool moveTo(StepType pos, int speed);

  // convenience conversions
  int degreeToStep(float deg);
  float stepToDegree(int stp);

private:
    std::unique_ptr<StepMotorImpl> impl_;
};

}  // namespace ctlsvc
