//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "devices/minipeerdevice.h"
#include "eval/events.h"
#include "gpiohandler.h"
#include "ipcmessage.h"
#include "logger.h"
#include "object_depot.h"
#include "options.h"
#include "peer.h"
#include "statistics.h"
#include "steps.h"
#include "string_tools.h"
#include "system.h"

#include <algorithm>
#include <cstdlib>
#include <fcntl.h>
#include <random>
#include <set>
#include <sstream>
#include <unistd.h>
#include <unordered_map>

using std::endl;

namespace ctlsvc {

class GpioHandlerImpl {
public:
  using GpioPins = std::vector<int>;
  using ProgramDb = std::unordered_map<std::string, std::string>;

  GpioHandlerImpl()
      : statusPin_(-1)
      , status_device_{} {}

private:
  Sequencer sequencer_;

  /** Database of program sequences that can be added to the sequencer */
  ProgramDb programDb_;

  /** Gpio pin for status LED */
  int statusPin_;

  std::unordered_map<int, int> pinToInputSensor_;
  std::unordered_map<int, int> pinToOutputSensor_;
  std::vector<device::GpioInputDevice> input_devices_;
  std::vector<device::GpioOutputDevice> led_devices_;
  std::set<int> motion_sensors_;
  std::unique_ptr<device::GpioOutputDevice> status_device_;

  /** GPIO pin numbers for motion detectors (optional) */
  GpioPins motionDetectors_;
  std::vector<std::chrono::steady_clock::time_point> lastMotion_;
  /** Last used minipeer */
  std::shared_ptr<Device> mpdevice_;

  friend GpioHandler;
};


GpioHandler::GpioHandler(const std::string& name)
    : Handler(name)
    , impl_(std::make_unique<GpioHandlerImpl>()) {
  stats_ = std::make_unique<Statistics>("Gpio Statistics");
}

GpioHandler::~GpioHandler() {
  // try to stop the threads
  impl_->sequencer_.stop(true);
}

// TODO close devices
void GpioHandler::deinit() {}

bool GpioHandler::init(const Options* opts) {
    cslog << loglevel(Info) << "Initializing Gpio Handler" << endl;
    auto inpins = opts->getConfigIntArray("control.gpioHandler.inpins");
    auto outpins = opts->getConfigIntArray("control.gpioHandler.outpins");
    auto pwmpins = opts->getConfigIntArray("control.gpioHandler.pwm");
    auto motion_detectors = opts->getConfigIntArray("control.gpioHandler.motionDetector");
    impl_->statusPin_ = opts->getConfigInt("control.gpioHandler.status");
    if (impl_->statusPin_ > 0) {
        outpins.push_back(impl_->statusPin_);
    }

    std::ostringstream ospins;
    ospins << "Gpio input pins = ";
    // Open the pins for input
    for (size_t idx = 0; idx < inpins.size(); ++idx) {
      int pin = inpins[idx];
      ospins << pin;
      impl_->input_devices_.push_back(device::GpioInputDevice(pin, idx + 1));
      impl_->pinToInputSensor_[pin] = idx + 1;
      if (std::find(motion_detectors.begin(), motion_detectors.end(), pin)
          != motion_detectors.end()) {
        impl_->motion_sensors_.insert(pin);
        ospins << "M ";
      } else {
        ospins << " ";
      }
    }

    for (auto& indev : impl_->input_devices_) {
        if (!indev.init()) {
            cslog << loglevel(Error) << "Cannot init input device "
                  << indev.getName() << endl;
            continue;
        }
        if (!indev.open(Device::ReadOpen)) {
            cslog << loglevel(Error) << "Cannot open input device "
                  << indev.getName() << endl;
            continue;
        }
    }
    cslog << loglevel(Info) << ospins.str() << endl;
    ospins.str("");

    ospins << "Gpio output pins =";
    for (size_t idx = 0; idx < outpins.size(); ++idx) {
        int pin = outpins[idx];
        if (pin == impl_->statusPin_) {
          ospins << " " << pin << "*";
          impl_->status_device_ = std::make_unique<device::GpioOutputDevice>(pin, 0);
        } else {
          impl_->led_devices_.emplace_back(pin, idx + 1);
          ospins << " " << pin;
          impl_->pinToOutputSensor_[pin] = idx + 1;
        }
    }
    cslog << loglevel(Info) << ospins.str() << endl;

    // Open Led Devices
    if (impl_->status_device_) {
      impl_->status_device_->init();
      impl_->status_device_->open(Device::ReadWriteOpen);
      impl_->status_device_->configure(device::GpioOutputDevice::USE_PWM, true);
    }
    for (int idx = 0; idx < static_cast<int>(impl_->led_devices_.size()); ++idx) {
        auto& dev = impl_->led_devices_[idx];
        if (!dev.init()) {
            cslog << loglevel(Error) << "Cannot init output device " << dev.getName() << endl;
            continue;
        }
        if (!dev.open(Device::ReadWriteOpen)) {
            cslog << loglevel(Error) << "Cannot open output device " << dev.getName() << endl;
            continue;
        }
        int pin = outpins[idx];
        if (std::find(pwmpins.begin(), pwmpins.end(), pin) != pwmpins.end()) {
            dev.configure(device::GpioOutputDevice::USE_PWM, true);
            cslog << loglevel(Info) << "Pin " << pin << " configured as PWM" << endl;
        }
    }

    Properties progs;
    if (opts->getProperties("control.gpioHandler.programs", progs)) {
        auto numprogs = progs.visit([this](const std::string& name, PropertyVar& var) {
                                        auto val = std::get<std::string>(var);
                                        cslog << "++ Program " << name << ": \"" << val << "\"" << endl;
                                        impl_->programDb_[name] = val;
                                        return true;
                                    });
        cslog << loglevel(Info) << "Loaded " << numprogs << " programs into GpioHandler" << endl;
    }
    stats_->newStat("requests", 0);
    stats_->newStat("events", 0);
    stats_->newStat("errors", 0);
    return true;
}

bool GpioHandler::setup() {
    cslog << loglevel(Info) << "Setting up Gpio handler" << endl;
    for (auto& indev : impl_->input_devices_) {
        cslog << loglevel(Info) << "Configure input listener on device " << indev.getName() << endl;
        auto handle = indev.listen(DeviceEvent::StateChange,
                     [this](DeviceEvent event, Device& dev) {
                         auto& gpiodev = static_cast<device::GpioInputDevice&>(dev);
                         if (impl_->motion_sensors_.count(gpiodev.pin())) {
                             int val = gpiodev.value();
                             if (val == 0) {
                                 cslog << loglevel(Info) << "Motion detector reset." << endl;
                             } else {
                                 cslog << loglevel(Info) << "MOTION DETECTED on " << dev.getName() << " !!" << endl;
                                 // auto prog = makeGpioProgram("Flash All", programDb_["Flash All"]);
                                 // sequencer_.addProgram(std::move(prog), 1, 4, SequenceEntry::RunOnce);
                             }
                         }
                         return 0;
                     });
        cslog << "Input device " << indev.pin() << " listener "
              << (handle == nullptr ? "Failed" : "Succeeded") << endl;
    }

    // Load default and background programs if specified
    ControlService* cs = ControlService::instance();
    const Options* opts = cs->getOptions();
    auto pname = opts->getConfig("control.gpioHandler.defaultProgram");
    if (!pname.empty()) {
        auto it = impl_->programDb_.find(pname);
        if (it != impl_->programDb_.end()) {
            auto defaultProgram = makeGpioProgram(pname, it->second);
            impl_->sequencer_.addDefaultProgram(std::move(defaultProgram));
        }
    }
    pname = opts->getConfig("control.gpioHandler.backgroundProgram");
    if (!pname.empty()) {
        auto it = impl_->programDb_.find(pname);
        if (it != impl_->programDb_.end()) {
            auto bkgndProgram = makeGpioProgram(pname, it->second);
            impl_->sequencer_.addDefaultProgram(std::move(bkgndProgram));
        }
    }
    if (!impl_->sequencer_.run(true)) {
        cslog << loglevel(Error) << "Sequencer failed to start" << endl;
        return false;
    }
    return true;
}

std::optional<device::GpioInputDevice*> GpioHandler::getInDevice(int pin) {
  auto it = impl_->pinToInputSensor_.find(pin);
  if (it != impl_->pinToInputSensor_.end()) {
    auto idx = it->second;
    return &impl_->input_devices_[idx - 1];
  }
  return {};
}

std::optional<const device::GpioInputDevice*> GpioHandler::getInDevice(int pin) const {
  return const_cast<GpioHandler*>(this)->getInDevice(pin);
}

std::optional<device::GpioOutputDevice*> GpioHandler::getOutDevice(int pin) {
  auto it = impl_->pinToOutputSensor_.find(pin);
  if (it != impl_->pinToOutputSensor_.end()) {
    auto idx = it->second;
    return &impl_->led_devices_[idx - 1];
  }
  return {};
}

std::optional<const device::GpioOutputDevice*> GpioHandler::getOutDevice(int pin) const {
  return const_cast<GpioHandler*>(this)->getOutDevice(pin);
}

const std::vector<device::GpioInputDevice>& GpioHandler::getInputDevices() const {
  return impl_->input_devices_;
}
const std::vector<device::GpioOutputDevice>& GpioHandler::getOutputDevices() const {
  return impl_->led_devices_;
}
std::vector<device::GpioOutputDevice>& GpioHandler::getOutputDevices() {
  return impl_->led_devices_;
}

bool GpioHandler::addProgram(SequenceEntry& entry) {
  return impl_->sequencer_.addProgram(entry);
}

std::unique_ptr<ctlsvc::Program>
GpioHandler::makeGpioProgram(const std::string& name, const std::string& strseq) {
  if (name.empty() || strseq.empty()) {
    return nullptr;
  }
  auto prog = std::make_unique<ctlsvc::Program>(name);
  StringArray steps;
  auto num_steps = StringTools::splitString(strseq, steps);
  cslog << loglevel(Info) << "Create program " << name << " with " << num_steps << " steps." << endl;
  for (const auto& step : steps) {
    StringArray parms;
    auto num_parms = StringTools::splitString(step, parms, ',');
    if (num_parms != 3) {
      cslog << loglevel(Error) << "Program steps must have 3 params, got " << num_parms << endl;
      continue;
    }
    auto led = std::stoul(parms[0]);
    int state = std::stoi(parms[1]);
    int delayMs = std::stoi(parms[2]);
    if (led > impl_->led_devices_.size()) {
      cslog << loglevel(Error) << "Invalid LED number: " << led << endl;
      continue;
    }
    auto led_dev = led == 0 ? impl_->status_device_.get() : &impl_->led_devices_[led - 1];
    prog->addStep(new GpioOutStep(led_dev, state, std::chrono::milliseconds(delayMs)));
  }
  return prog;
}

bool GpioHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
  cslog << "Gpio handler processing request: " << message.toString() << std::flush;
  stats_->increment("requests");

  reply.setId(IpcReply::ID);
  reply.addMember(IpcReply::RESULT, "Gpio handler");
  std::string method = message.getMember(IpcControl::METHOD);
  int value = -1;
  int pin = message.getIntMember("pin");
  if (pin == 0) {
    pin = impl_->statusPin_;
  }
  device::minipeer::Input* mp_in = nullptr;
  device::minipeer::Output* mp_out = nullptr;
  auto uuid = message.getMember("uuid");
  if (!uuid.empty()) {
    auto depot = ControlService::instance()->getObjectDepot();
    auto uname = depot->uuidToUName(uuid);
    // remove the peripheral from the minipeer name
    auto pos = uname.find_last_of(' ');
    std::string mp_devname;
    if (pos != std::string::npos) {
      mp_devname = uname.substr(uname.find_last_of(' ') + 1);
      mp_devname = mp_devname.substr(0, mp_devname.find_last_of('.'));
    }
    auto mp_periname = uname.substr(uname.find_last_of('.') + 1);
    // Check if minipeer is in cache
    device::MiniPeerDevice* mpd = nullptr;
    if (!mp_devname.empty()) {
      if (impl_->mpdevice_) {
        mpd = static_cast<device::MiniPeerDevice*>(impl_->mpdevice_.get());
        if (mpd->getName() == mp_devname) {
          cslog << loglevel(Info) << "Found minipeer " << mp_devname << " in cache." << endl;
        } else {
          mpd = nullptr;
        }
      }
      if (mpd == nullptr) {
        auto devobj = depot->find(mp_devname);
        cslog << "GpioHandler uuid=" << uuid << " mp name=" << mp_devname
              << " peripheral=" << mp_periname << ",  object: " << devobj << endl;
        if (std::holds_alternative<DeviceObject>(devobj)) {
          impl_->mpdevice_ = std::get<DeviceObject>(devobj);
          mpd = static_cast<device::MiniPeerDevice*>(impl_->mpdevice_.get());
        }
      }
      if (mpd == nullptr) {
        std::string msg("Cannot find MiniPeer device.");
        cslog << loglevel(Error) << msg << endl;
        reply.setReply(IpcControl::ERROR, msg);
        return false;
      }
      if (mpd->status() != Device::Mode::ReadWriteOpen) {
        mpd->open(Device::Mode::ReadWriteOpen);
      }
      bool is_gpioin = mp_periname.front() == 'i';
      int perinum = std::stoi(mp_periname.substr(1));
      bool got_peri = is_gpioin ? mpd->peripheral(perinum, &mp_in) : mpd->peripheral(perinum, &mp_out);
      if (!got_peri) {
        std::string msg("Minipeer device cannot find " + mp_periname + " peripheral");
        cslog << loglevel(Error) << msg << endl;
        reply.setReply(IpcControl::ERROR, msg);
        return false;
      } else {
        cslog << loglevel(Info) << "Using minipeer " << mp_devname << ", peripheral " << mp_periname << endl;
      }
    }
  } else {
    cslog << loglevel(Error) << "No uuid for gpio device" << endl;
    reply.setReply(IpcControl::ERROR, "No uuid for gpio device");
    return false;
  }
  // TODO Use Sequencer
  // TODO use LED numbers instead of gpio pin numbers
  if (method == "readkey") {
    if (mp_in) {
      value = mp_in->get();
    } else if (mp_out) {
      value = mp_out->get();
    } else {
      if (pin == impl_->statusPin_ && impl_->status_device_) {
        impl_->status_device_->read(value);
      } else {
        auto in_dev = getInDevice(pin);
        if (in_dev) {
          (*in_dev)->read(value);
        } else {
          auto out_dev = getOutDevice(pin);
          if (out_dev) {
            (*out_dev)->read(value);
          }
        }
      }
    }
    cslog << "gpio read pin " << pin << " is " << value << endl;
    reply.addMember("pin", pin);
    reply.addMember("param1", pin);  // Deprecated, will be removed
    reply.addMember("param2", value);
  } else if (method == "writekey") {
    value = message.getIntMember("param2");
    if (mp_out) {  // TODO need to determine if set(value) or pwm(value) should be called
      mp_out->pwm(value);
    } else {
      auto led_dev = getOutDevice(pin);
      if (led_dev) {
        (*led_dev)->write(value);
        cslog << "gpio set pin " << pin << " to " << value << endl;
      }
    }
    reply.addMember("pin", pin);
    reply.addMember("param1", pin);  // Deprecated, will be removed
    reply.addMember("param2", value);
  } else if (method == "blink") {
    int delay = message.getIntMember("param2");
    if (delay < 40) {
      delay = 40;
    }
    auto dev = pin == impl_->statusPin_ ? std::optional(impl_->status_device_.get()) : getOutDevice(pin);
    if (dev) {
      (*dev)->read(value);
      (*dev)->write(value == 0 ? 255 : 0);
      cslog << "gpio blink pin " << pin << " state " << value << endl;
      usleep(delay * 1000);
      (*dev)->write(value);
    }
  } else if (method == "addProgram") {
    // params: name, "prog.seq.", autorun once
    auto progName = message.getMember("name");
    auto progSeq = message.getMember("program");
    auto autorun = message.getMember("autorun");
    if (progName.empty() || progSeq.empty()) {
      reply.setReply(IpcControl::ERROR, "Invalid parameters to addProgram");
      return false;
    }
    impl_->programDb_[progName] = progSeq;
    if (!autorun.empty()) {
      auto prog = makeGpioProgram(progName, progSeq);
      impl_->sequencer_.addProgram(std::move(prog), 1, 1, SequenceEntry::RunOnce);
    }
  } else if (method == "deleteProgram") {
    auto progName = message.getMember("name");
    if (progName.empty()) {
      reply.setReply(IpcControl::ERROR, "Invalid parameters to deleteProgram");
      return false;
    }
    auto it = impl_->programDb_.find(progName);
    if (it == impl_->programDb_.end()) {
      reply.setReply(IpcControl::ERROR, "Program not found in deleteProgram");
      return false;
    }
    impl_->programDb_.erase(it);
    // TODO also stop the program if running
  } else if (method == "listPrograms") {
    IpcMessage progs("programs");
    for (const auto& [name, strseq] : impl_->programDb_) {
      progs.addMember(name, strseq);
    }
    reply.addMember(progs);
  } else if (method == "runProgram") {
    // params: name, repeat, level
    auto progName = message.getMember("name");
    auto level = message.getIntMember("level");
    auto repeat = message.getIntMember("repeat");
    if (!runProgram(progName, repeat, level)) {
      cslog << loglevel(Error) << "Error running gpio program " << progName << endl;
      reply.setReply(IpcControl::ERROR, "Error running gpio program");
      return false;
    }
  } else if (method == "stopProgram") {
    auto progName = message.getMember("name");
    auto level = message.getIntMember("level");
    impl_->sequencer_.removeProgram(progName, level);
  } else {
    std::string err = "gpio method not found: " + method;
    cslog << err << endl;
    reply.setReply(IpcControl::ERROR, err);
    return false;
  }

  reply.addMember(IpcReply::STATUS, IpcControl::OK);
  return true;
}

bool GpioHandler::runProgram(const std::string& progName, int repeat, int level) {
  if (impl_->programDb_.find(progName) == impl_->programDb_.end()) {
    cslog << loglevel(Error) << "Program not found: " << progName << endl;
    //reply.setReply(IpcControl::ERROR, "Program not found");
    return false;
  } else {
    if (level < 1) {
      level = 1;
    }
    auto prog = makeGpioProgram(progName, impl_->programDb_[progName]);
    impl_->sequencer_.addProgram(std::move(prog), level, repeat, SequenceEntry::RunOnce);
  }
  return true;
}

}  // namespace ctlsvc
