//
//   Copyright 2022-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "handler.h"

#include <memory>

namespace ctlsvc {

// Forward references
class IpcMessage;
class Peer;
class ServoHandlerImpl;

// TODO allow class to handle multiple servos
/** Handler to control a servo motor that is connected to a gpio line with PWM.
 *  This handler uses Statistics.
 */
class ServoHandler : public Handler {
public:
    using AngleType = float;
    using PulseType = int32_t;
public:
    ServoHandler(const std::string& name);
    virtual ~ServoHandler();

    // Handler interface
    virtual void deinit() override;
    virtual bool init(const Options* options) override;
    virtual bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) override;

    // Servo motor interface
    AngleType degrees() const;
    void degrees(AngleType pos);
    PulseType pulseWidth() const;
    void pulseWidth(PulseType pw);
    bool move(AngleType degrees);
    bool moveTo(AngleType pos);

private:
    std::unique_ptr<ServoHandlerImpl> impl_;
};

}  // namespace ctlsvc
