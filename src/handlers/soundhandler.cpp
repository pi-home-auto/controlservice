//
//   Copyright 2022-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "forker.h"
#include "ipcmessage.h"
#include "logger.h"
#include "soundhandler.h"

using std::endl;

namespace ctlsvc {

SoundHandler::SoundHandler(const std::string& name)
    : Handler(name)
    , player_("mpg321") {}

void SoundHandler::deinit() {
    stop();
}

bool SoundHandler::init(const Options* opts) {
    auto dir = opts->getConfig("control.soundHandler.mediaDir");
    if (!dir.empty()) {
        if (dir.back() != '/') {
            dir.append("/");
        }
        media_dir_ = dir;
    }
    auto player = opts->getConfig("control.soundHandler.player");
    if (!player.empty()) {
      player_ = player;
    }
    return true;
}

bool SoundHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
    cslog << "Sound handler processing request: " << message.toString() << endl;
    reply.setId(IpcReply::ID);  // TODO should this be the request ID?
    reply.addMember(IpcReply::RESULT, "Sound handler");
    std::string method = message.getMember(IpcControl::METHOD);
    if (method == "play") {
        auto filename = message.getMember("filename");
        cslog << loglevel(Info) << "SoundHandler play got file: " << filename << endl;
        if (filename.empty()) {
            cslog << loglevel(Error) << "SoundHandler play -- no file specified" << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
        }
        if (!play(filename)) {
            cslog << loglevel(Error) << "SoundHandler play -- error spawning audio player" << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
        }
    } else if (method == "stop") {
        if (!stop()) {
            cslog << loglevel(Error) << "SoundHandler stop -- error spawning killall" << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
        }
    } else {
        cslog << loglevel(Error) << "sound handler method not found: " << method << endl;
        reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
        return false;
    }
    reply.addMember(IpcReply::STATUS, IpcControl::OK);
    return true;
}

bool SoundHandler::play(const std::string& filename) {
    std::string dir;
    if (filename.front() != '/' && !media_dir_.empty()) {
      dir = media_dir_;
    }
    auto dot = filename.find_last_of('.');
    std::string player;
    std::string filetype = "(Unknown)";
    if (dot == std::string::npos) {
      player = "aplay";
    } else {
      filetype = filename.substr(dot+1);
    }
    if (filetype == "mp3") {
      player = player_;
    } else {
      player = "aplay";
    }
    auto forker = ControlService::instance()->forker();
    auto handle = forker->spawn(player + " " + dir + filename);
    if (handle != ForkEntry::INVALID_HANDLE) {
      file_playing_ = dir + filename;
      return true;
    }
    return false;
}

bool SoundHandler::stop() {
    auto forker = ControlService::instance()->forker();
    auto handle = forker->spawn("killall " + player_.substr(0, player_.find(' ')));
    file_playing_.erase();
    return handle != ForkEntry::INVALID_HANDLE;
}

}  // namespace ctlsvc
