//
//   Copyright 2020, 2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <Session.h>
#include <Bearer.h>
#include "handler.h"
#include <string>


namespace ctlsvc {
// Forward references
class IpcMessage;

/**
 * Handler to send notifications to the phone. It uses the "network"
 * library to connect to another instance of the "network" library and send
 * notification data which is returned to it from node.
 */

class NotificationHandler : public Handler {
public:
    NotificationHandler(const std::string& name);
    virtual ~NotificationHandler();

    /** Shutdown the Session and close out the network library. */
    void deinit() override;
    /** Initialize the network library and establish a Session connection. */
    bool init(const Options* options) override;
    /** Execute a user request.
     *  This API must not block. */
    bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) override;

private:
    /** Variables from the "network" library
     *  Session is used as a connection abstraction,
     *  bearerInfo holds the ip address and port numeber
     *  Listener is the callback coming from the "network"
     *  library on network events.
    */
    Session session_;
    BearerInfo bearerInfo_;
    SessionListener listener_;
    std::string persistenceFile_;
    int remotePort_;
    static const int NETERROR = -1;
};

}
