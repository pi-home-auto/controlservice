//
//   Copyright 2022-2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "devices/gpiooutputdevice.h"
#include "gpiohandler.h"
#include "ipcmessage.h"
#include "logger.h"
#include "sequencer.h"
#include "servohandler.h"
#include "steps.h"

#include <utility>

namespace ctlsvc {

using device::GpioOutputDevice;
using Mode = Device::Mode;
using std::endl;


class ServoHandlerImpl {
public:
    using AngleType = ServoHandler::AngleType;
    using PulseType = ServoHandler::PulseType;

    ServoHandlerImpl()
        : sequencer_()
        , degrees_(0)
        , pulse_width_(0)
        , degree_min_(0)
        , degree_max_(180)
        , pulse_min_(750)
        , pulse_max_(2250)
        , dev_(nullptr)
        , sensor_(-1) {
        slope_ = ((float)pulse_max_ - pulse_min_) / ((float) degree_max_ - degree_min_);
    }
    ~ServoHandlerImpl() = default;

    bool init(const Options* opts);
    bool setDegrees(AngleType degrees);
    bool setPulseWidth(PulseType pw);

private:
    Sequencer sequencer_;
    AngleType degrees_;
    PulseType pulse_width_;
    AngleType degree_min_;
    AngleType degree_max_;
    PulseType pulse_min_;
    PulseType pulse_max_;
    float slope_;
    device::GpioOutputDevice* dev_;
    int sensor_;

    friend ServoHandler;
};

bool ServoHandlerImpl::init(const Options* opts) {
    auto sensor = opts->getConfigInt("control.servoHandler.sensor");
    // TODO use angleRange, pulseRangeUsec and periodUsec from configuration
    auto handler = ControlService::getHandler("gpio");
    auto& outDevices = dynamic_cast<GpioHandler*>(handler)->getOutputDevices();
    if (sensor < 1 || sensor > static_cast<int>(outDevices.size())) {
        return false;
    }
    dev_ = &outDevices[sensor - 1];
    if (dev_->status() == Mode::Unopened) {
        if (!dev_->open(Mode::ReadWriteOpen)) {
            cslog << loglevel(Error) << "Unable to open device for gpio " << sensor_ << endl;
            return false;
        }
    }
    if (!dev_->configure(GpioOutputDevice::USE_SERVO, true)) {
        cslog << loglevel(Error) << "Cannot configure servo mode on gpio " << sensor_ << endl;
        return false;
    }

    sequencer_.run(true);
    sensor_ = sensor;
    return true;
}

bool ServoHandlerImpl::setDegrees(AngleType degrees) {
    if (sensor_ == -1) {
        return false;
    }
    if (degrees < degree_min_ || degrees > degree_max_) {
        return false;
    }
    pulse_width_ = (pulse_min_ - degree_min_) + slope_ * degrees;
    degrees_ = degrees;
    return true;
}

bool ServoHandlerImpl::setPulseWidth(PulseType pw) {
    if (sensor_ == -1) {
        return false;
    }
    if (pw < pulse_min_ || pw > pulse_max_) {
        return false;
    }
    degrees_ = (float)(degree_min_ - pulse_min_ + pw) / slope_;
    pulse_width_ = pw;
    return true;
}

//////////   MAIN  HANDLER  CLASS   ///////////

ServoHandler::ServoHandler(const std::string& name)
    : Handler(name)
    , impl_(std::make_unique<ServoHandlerImpl>()) {}

ServoHandler::~ServoHandler() {}

void ServoHandler::deinit() {
    impl_->sequencer_.stop(true);
    if (impl_->dev_ != nullptr) {
        // TODO write 0 to device to turn off servo
        impl_->dev_->configure(GpioOutputDevice::USE_SERVO, false);
    }
}

bool ServoHandler::init(const Options* opts) {
    return impl_->init(opts);
}

bool ServoHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
    cslog << "Servo handler processing request: " << message.toString() << endl;
    reply.setId(IpcReply::ID);  // TODO should this be the request ID?
    reply.addMember(IpcReply::RESULT, "Servo handler");
    std::string method = message.getMember(IpcControl::METHOD);
    if (method == "readkey") {
        reply.addMember("degrees", impl_->degrees_);
        reply.addMember("pulseWidth", impl_->pulse_width_);
        reply.addMember("param1", 0);
        reply.addMember("param2", impl_->degrees_);
    } else if (method == "move") {
        // members: degrees, speed
        float degrees = message.getFloatMember("degrees");
        cslog << loglevel(Info) << "++Params: degrees=" << degrees << endl;
        if (!move(degrees)) {
            cslog << loglevel(Error) << "servo failed to move" << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
        }
    } else if (method == "moveTo") {
        float degrees = message.getFloatMember("degrees");
        cslog << loglevel(Info) << "++Params: degrees=" << degrees << endl;
        if (!moveTo(degrees)) {
            cslog << loglevel(Error) << "servo failed to move" << endl;
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            return false;
        }
    } else {
        cslog << loglevel(Error) << "servo method not found: " << method << endl;
        reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
        return false;
    }
    reply.addMember(IpcReply::STATUS, IpcControl::OK);
    return true;
}

ServoHandler::AngleType ServoHandler::degrees() const {
    return impl_->degrees_;
}

void ServoHandler::degrees(AngleType pos) {
    moveTo(pos);
}

ServoHandler::PulseType ServoHandler::pulseWidth() const {
    return impl_->pulse_width_;
}

void ServoHandler::pulseWidth(PulseType pw) {
    impl_->setPulseWidth(pw);  // This sets degrees_ too
    moveTo(impl_->degrees_);
}

bool ServoHandler::move(AngleType degrees) {
    AngleType pos = impl_->degrees_ + degrees;
    return moveTo(pos);
}

bool ServoHandler::moveTo(AngleType pos) {
    constexpr int32_t kPulseStep = 18;
    constexpr auto kDelayMs = std::chrono::milliseconds(80);

    auto prevPulse = impl_->pulse_width_;
    if (impl_->setDegrees(pos)) {
        auto pulse = impl_->pulse_width_;
        if (abs(pulse - prevPulse) <= kPulseStep * 2) {
            return impl_->dev_->write(sizeof(PulseType), &impl_->pulse_width_) > 0;
        } else {
            auto slowMover = std::make_unique<Program>("Slow mover");
            if (pulse > prevPulse) {
                for ( ; prevPulse <= pulse; prevPulse += kPulseStep) {
                    slowMover->addStep(new GpioOutStep(impl_->dev_, prevPulse, kDelayMs));
                }
            } else {
                for ( ; prevPulse >= pulse; prevPulse -= kPulseStep) {
                    slowMover->addStep(new GpioOutStep(impl_->dev_, prevPulse, kDelayMs));
                }
            }
            if (prevPulse != pulse) {
                slowMover->addStep(new GpioOutStep(impl_->dev_, pulse, std::chrono::seconds(0)));
            }
            impl_->sequencer_.addProgram(std::move(slowMover), 1, 1, SequenceEntry::RunOnce);
        }
    }
    return false;
}

}  // namespace ctlsvc
