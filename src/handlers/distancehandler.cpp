//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "distancehandler.h"
#include "devices/distdevice.h"
#include "ipcmessage.h"
#include "controlservice.h"
#include "logger.h"
#include "object_depot.h"

#include <string>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

using ctlsvc::device::DistDevice;
using std::endl;

namespace ctlsvc {

class DistanceHandlerImpl {
public:
  DistanceHandlerImpl(int sensor);
  ~DistanceHandlerImpl();

  void deinit();
  bool init();
  int getPollDelayMs() const { return pollDelayMs_; }
  void setNumberOfSensors(int numSensors);
  void setSensorFileName(const std::string& sensorFileName);
  void setPollDelayMs(int pollDelayMs);

  int dataAvailable(DeviceEvent event, Device& device);
  Device* distDevice() { return distDev_.get(); }
  Device::Handle& handle() { return handle_; }

private:
  int numSensors_;
  int pollDelayMs_;
  std::string sensorFileName_;
  std::shared_ptr<Device> distDev_;
  Device::Handle handle_;
};

DistanceHandlerImpl::DistanceHandlerImpl(int sensor)
    : numSensors_(0)
    , pollDelayMs_(1000)
    , handle_(nullptr) {
}

DistanceHandlerImpl::~DistanceHandlerImpl() {
    if (handle_ != nullptr) {
        distDev_->unlisten(handle_);
    }
}

void DistanceHandlerImpl::deinit() {
  distDev_->close();
  distDev_->deinit();
  distDev_.reset();
}

bool DistanceHandlerImpl::init() {
  ControlService* cs = ControlService::instance();
  auto depot = cs->getObjectDepot();
  auto dd = depot->find("device", "dist.0");
  if (std::holds_alternative<ctlsvc::DeviceObject>(dd)) {
    distDev_ = std::get<ctlsvc::DeviceObject>(dd);
  }
  if (!distDev_) {
    cslog << loglevel(Error) << "Unable to open dist.0 device" << endl;
    return false;
  }
  distDev_->init();
  distDev_->open(Device::ReadWriteOpen);
  return true;
}

void DistanceHandlerImpl::setNumberOfSensors(int numSensors) {
    numSensors_ = numSensors;
}

void DistanceHandlerImpl::setSensorFileName(const std::string& sensorFileName) {
    sensorFileName_ = sensorFileName;
}

void DistanceHandlerImpl::setPollDelayMs(int pollDelayMs) {
    pollDelayMs_ = pollDelayMs;
}

int DistanceHandlerImpl::dataAvailable(DeviceEvent event, Device& device) {
  auto& distDev = static_cast<device::DistDevice&>(device);
  auto val = distDev.lastValue();
  cslog << loglevel(Info) << "Distance Handler dataAvailable: " << val << endl;
  return 0;
}


DistanceHandler::DistanceHandler(const std::string& name)
    : Handler(name)
    , impl_(std::make_unique<DistanceHandlerImpl>(0)) {}

DistanceHandler::~DistanceHandler() {
  stopStreaming();
}

void DistanceHandler::deinit() {
  impl_->deinit();
}

bool DistanceHandler::init(const Options* opts) {
  int numSensors = opts->getConfigInt("control.distanceHandler.numSensors");
  cslog << loglevel(Info) << "Initializing distance handler, number of sensors is " << numSensors << endl;
  impl_->setNumberOfSensors(numSensors);

  auto poll_ms = opts->getConfigInt("control.distanceHandler.pollDelayMs");
  impl_->setPollDelayMs(poll_ms);
  cslog << loglevel(Info) << "pollInterval for the distance handler is "
        << impl_->getPollDelayMs() << "ms" << endl;

  std::string sensorFileName = opts->getConfig("control.distanceHandler.sensorFile");
  impl_->setSensorFileName(sensorFileName);
  return impl_->init();
}

bool DistanceHandler::request(const IpcMessage& message, IpcReply& reply, Peer* peer) {
    std::string method = message.getMember(IpcControl::METHOD);
    cslog << "DistanceHandler method:" << method << endl;

    auto dev = impl_->distDevice();
    reply.setId(IpcReply::ID);
    if (method == "readkey") {
        if (dev->status() == Device::ReadWriteOpen) {
            char dbuf[40];
            auto sz = dev->read(sizeof(dbuf), dbuf);
            if (sz > 0) {
                reply.addMember("distance", dbuf);
            } else {
                reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
                reply.addMember(IpcReply::RESULT, "No distance read for method " + method);
                return true;
            }
        }
    } else if (method == "writekey") {
        auto units = message.getMember("param1");
        if (units.empty() || dev->write(units.size(), units.c_str()) < 1) {
            reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
            reply.addMember(IpcReply::RESULT, "Distance handler no data written for method " + method);
            return true;
        }
    } else if (method == "startstreaming") {
        int poll_ms = impl_->getPollDelayMs();
        if (message.isMember("poll_delay_ms")) {
            poll_ms = message.getIntMember("poll_delay_ms");
        }
        dev->configure(device::DistDevice::POLL_DELAY_MS, poll_ms);
        if (startStreaming()) {
            cslog << loglevel(Info) << "Distance handler streaming at " << poll_ms << "ms" << endl;
        } else {
            cslog << loglevel(Error) << "Distance handler cannot start streaming" << endl;
        }
    } else if (method == "stopstreaming") {
        stopStreaming();
    } else {
        reply.addMember(IpcReply::STATUS, IpcControl::ERROR);
        reply.addMember(IpcReply::RESULT, "Distance handler unknown method " + method);
        return true;
    }

    reply.addMember(IpcReply::STATUS, IpcControl::OK);
    reply.addMember(IpcReply::RESULT, "Distance handler done");
    return true;
}

bool DistanceHandler::startStreaming() {
    auto dev = impl_->distDevice();
    impl_->handle() = dev->listen(DeviceEvent::DataAvailable,
                                 [this](DeviceEvent event, Device& device) {
                                     return impl_->dataAvailable(event, device);
                                 });
    return impl_->handle() != nullptr;
}

void DistanceHandler::stopStreaming() {
    auto dev = impl_->distDevice();
    dev->unlisten(impl_->handle());
}

}  // namespace ctlsvc
