//
//   Copyright 2022, 2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "handler.h"

#include <string>

namespace ctlsvc {

// Forward references
class IpcMessage;
class Peer;
class SoundHandlerImpl;

// TODO allow class to handle multiple sound devices
/** Handler to control a "sound device", mainly playing sound files and stopping.
 *  This handler uses Statistics.
 */
class SoundHandler : public Handler {
public:
    SoundHandler(const std::string& name);
    virtual ~SoundHandler() = default;

    // Handler interface
    virtual void deinit() override;
    virtual bool init(const Options* opts) override;
    virtual bool request(const IpcMessage& message, IpcReply& reply, Peer* peer) override;

    // Sound player interface
    bool play(const std::string& filename);
    bool stop();
    // TODO volume control: int getVolume(), setVolume(int vol)

private:
    std::string media_dir_;
    std::string file_playing_;
    std::string player_;
};

}  // namespace ctlsvc
