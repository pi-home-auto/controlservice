//
//   Copyright 2020-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include "logger.h"
#include "properties.h"

#include <memory>
#include <vector>

// Default values
// TODO use a defaults structure to collect defaults for an environment (test, prod, demo)
#define BROADCAST_PORT 1800
#define CONTROL_PORT 1800
#define REST_PORT 1801

namespace ctlsvc {
// Forward reference
class OptionsImpl;

/**
 * Class to parse and retain parameters and configuration options used by the
 * control service program.
 * These options are initialized from various sources and merged within this class.
 * When the same option is specified in multiple sources, then the precedence is as
 * follows:
 *  -# command line arguments
 *  -# host-specific configuration file (${HOMEAUTO}/<i>hostname</i>-config.json)
 *  -# common, shared configuration file (${HOMEAUTO}/commonconfig.json)
 *  -# SQL database of static data (${HOMEAUTO}/homeautoplan.sql)
 */
class Options {
public:
    enum CSTYPE {
        CS_INVALID = -1,
        CS_BASE,
        CS_SHUTDOWN
    };

    Options();
    virtual ~Options();

    // Getters
    CsLogLevel getAuditLogLevel() const;
    const std::vector<std::string>& getBootupHandlers() const;
    int getControlPort() const;
    const std::string& getDbFilename() const;
    CsLogLevel getLogLevel() const;
    const std::string& getModuleName() const;
    int getRestPort() const;
    enum CSTYPE getType() const;
    int getUdpPort() const;
    std::string getWhenDown() const;

    /** Get a configuration option value.
     *  Options may be specified as dot values, for example "control.gpioHandler.outpins".
     *  TODO this could be templatized like "T getConfig<T>(option) const"
     */
    std::string getConfig(const std::string& option) const;
    const std::string& getConfigDir() const;
    float getConfigFloat(const std::string& option) const;
    int getConfigInt(const std::string& option) const;
    std::vector<std::string> getConfigArray(const std::string& option) const;
    std::vector<int> getConfigIntArray(const std::string& option) const;
    bool getProperties(const std::string& option, Properties& props) const;

    CSTYPE parseCommandLine(int argc, char* argv[]);
    bool readConfig();

    /** Set or reset internal state based on current configuration. */
    void setConfig();

private:
    // Disable copy constructor and assignment operator
    Options(const Options&) = delete;
    Options& operator=(const Options&) = delete;

private:
    std::unique_ptr<OptionsImpl> impl_;
};

} // namespace ctlsvc
