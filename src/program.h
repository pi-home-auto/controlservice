//
//   Copyright 2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#pragma once

#include <string>
#include <vector>

namespace ctlsvc {

class Program;

/** The interface for one Step of a Program. */
class Step {
public:
    virtual ~Step() = default;
    virtual bool run() = 0;
    virtual bool set(const std::string& params) = 0;
    virtual std::string results() const = 0;
};

/** Iterator class to step through a program */
class ProgramStepper {
public:
    ProgramStepper() = default;
    ~ProgramStepper() = default;

    ProgramStepper(const ProgramStepper& other);
    ProgramStepper operator=(const ProgramStepper& other);
    bool operator==(const ProgramStepper& other) const;
    bool operator!=(const ProgramStepper& other) const;
    ProgramStepper& operator++();
    ProgramStepper operator++(int);
    Step* operator*();
private:
    std::vector<Step *>::iterator val_;

    friend Program;
};

/** A program is a sequence of steps. The steps can be executed sequentially and the
 *  execution can optionally be single-stepped.
 *
 *  Note that the program takes ownership for the Step pointers and will delete them
 *  when the Program is destructed.
 */
class Program {
public:
    using iterator = ProgramStepper;

    Program(const std::string& name);
    Program(Program&& other) = delete;
    Program& operator=(const Program& other) = delete;
    ~Program();

    const std::string& name() const;
    size_t size() const;

    bool addStep(Step* step);
    bool addSteps(const std::vector<Step *>& steps);
    void clear();
    /**
     *  Run the program starting at the specified step.
     *  @param it  Iterator for the steps of this program
     *  @param single_step  The execution will stop after a single step
     *                 has executed.
     *  @return Iterator to the next step (if single step mode) or the end of sequence.
     */
    iterator execute(iterator it, bool single_step = false);

    iterator begin();
    iterator end();

private:
    std::string name_;
    std::vector<Step *> steps_;
};

}  // namespace ctlsvc
