#include <string.h>
#include <CircBuf.h>

#include <log.h>


void printBuf(ReadBuffer *rB)
{
    logCircBuf("CircBuffer :Printing buffer:%.*s\n", rB->len, rB->buffer);
}

void initBuffer(ReadBuffer* readBuffer)
{
    //pthread_mutex_init(&(readBuffer->bufLock), NULL);
    readBuffer->len = CIRC_BUFFER_SIZE;
    bzero(readBuffer->buffer,CIRC_BUFFER_SIZE);
    readBuffer->readPos = 0;
    readBuffer->writePos = 0;
}
void cleanUpBuffer(ReadBuffer* readBuffer)
{
    //pthread_mutex_destroy(&(readBuffer->bufLock));
    //free(readBuffer->buffer);
}

// write to circular buffer
int writeBuf(ReadBuffer*rB,char* src, int offset, int len)
{
    int bytesToWrite, bytesAvailable, bytesWrite1Pass, bytesWritten;
    //pthread_mutex_lock(&(rB->bufLock));
    bytesAvailable = bytesAvailableForWrite(rB);
    bytesToWrite = bytesAvailable < len ? bytesAvailable:len;

    if(bytesToWrite <= (rB->len - rB->writePos))
    {
        if(src != NULL)
            memcpy(rB->buffer + rB->writePos,src+offset,bytesToWrite);
        rB->writePos  = (rB->writePos + bytesToWrite)% rB->len;
        bytesWritten = bytesToWrite;
    }
    else
    {
        bytesWrite1Pass = rB->len - rB->writePos;
        if(bytesWrite1Pass && src != NULL)
            memcpy(rB->buffer + rB->writePos, src+offset, bytesWrite1Pass);
        bytesToWrite -= bytesWrite1Pass;
        if(src != NULL)
            memcpy(rB->buffer,src+offset+bytesWrite1Pass,  bytesToWrite);
        rB->writePos = (bytesToWrite)%rB->len;
        bytesWritten = bytesToWrite + bytesWrite1Pass;

    }
    //pthread_mutex_unlock(&(rB->bufLock));
    return bytesWritten;
}

// read from circular buffer
int readBuf(ReadBuffer* rB,char* dest, int offset, int len)
{

    int bytesToRead, bytesAvailable, bytesRead1Pass, bytesRead;
    //pthread_mutex_lock(&(rB->bufLock));
    bytesAvailable = bytesAvailableForRead(rB);
    bytesToRead = bytesAvailable < len ? bytesAvailable:len;

    if(rB->readPos + bytesToRead <= (rB->len - 1))
    {
        if(dest != NULL)
            memcpy(dest + offset,rB->buffer + rB->readPos,bytesToRead);
        rB->readPos  = (rB->readPos + bytesToRead)% rB->len;
        bytesRead = bytesToRead;
    }
    else
    {
        bytesRead1Pass = rB->len - rB->readPos;
        if(bytesRead1Pass && dest != NULL)
            memcpy(dest+offset, rB->buffer + rB->readPos, bytesRead1Pass);
        bytesToRead -= bytesRead1Pass;
        if(dest != NULL)
            memcpy(dest+offset+bytesRead1Pass, rB->buffer, bytesToRead);
        rB->readPos = (bytesToRead)%rB->len;
        bytesRead = bytesToRead + bytesRead1Pass;

    }
    //pthread_mutex_unlock(&(rB->bufLock));
    return bytesRead;
}

int peek(ReadBuffer* rB,char* dest, int offset, int len)
{

    int bytesToRead, bytesAvailable, bytesRead1Pass, bytesRead;
    //pthread_mutex_lock(&(rB->bufLock));
    bytesAvailable = bytesAvailableForRead(rB);
    bytesToRead = bytesAvailable < len ? bytesAvailable:len;

    if(rB->readPos + bytesToRead <= (rB->len - 1))
    {
        if(dest != NULL)
            memcpy(dest + offset,rB->buffer + rB->readPos,bytesToRead);
        bytesRead = bytesToRead;
    }
    else
    {
        bytesRead1Pass = rB->len - rB->readPos;
        if(bytesRead1Pass && dest != NULL)
            memcpy(dest+offset, rB->buffer + rB->readPos, bytesRead1Pass);
        bytesToRead -= bytesRead1Pass;
        if(dest != NULL)
            memcpy(dest+offset+bytesRead1Pass, rB->buffer, bytesToRead);
        bytesRead = bytesToRead + bytesRead1Pass;

    }
    //pthread_mutex_unlock(&(rB->bufLock));
    return bytesRead;
}
int bytesAvailableForWrite(ReadBuffer* rB)
{
    if(rB->writePos >= rB->readPos)
        return rB->len - 1 - rB->writePos + rB->readPos;
    else
        return rB->readPos - 1 - rB->writePos;
}

int bytesAvailableForRead(ReadBuffer *pReadBuffer)
{
    if(pReadBuffer->writePos >= pReadBuffer->readPos)
        return pReadBuffer->writePos - pReadBuffer->readPos;
    else
        return pReadBuffer->len - pReadBuffer->readPos + pReadBuffer->writePos;
}

int isFull(ReadBuffer* rB)
{
    if((rB->writePos+1)%rB->len == rB->readPos)
        return 1;
    else return 0;
}

int isEmpty(ReadBuffer* rB)
{
    if(rB->readPos == rB->writePos)
        return 1;
    else return 0;
}

void reset(ReadBuffer* rB)
{
    rB->readPos = 0;
    rB->writePos = 0;
}


