#include <Bearer.h>
#include <string.h>
#include <netdb.h>
#include <stdio.h>
#include <log.h>
//TODO write and read from file  for this, not implemented yet
void fillLinSockBearerInfo(BearerInfo* bearerInfo, const char* ipaddress, int portNumber,int msgMaxLen)
{
    struct hostent* server;

    bearerInfo->msgMaxLen = msgMaxLen;
    bearerInfo->eBearerType = LINUX_SOCKET;

    server = gethostbyname(ipaddress);
    if (server == NULL)
    {
        perror("ERROR, no such host");
        return;
    }

    bzero((char *) &(bearerInfo->linSockBearer.remoteAddress), sizeof(struct sockaddr_in));

    bearerInfo->linSockBearer.remoteAddress.sin_family = AF_INET;

    bcopy((char *)server->h_addr,(char *)&(bearerInfo->linSockBearer.remoteAddress.sin_addr.s_addr),
         server->h_length);

    bearerInfo->linSockBearer.remoteAddress.sin_port = htons(portNumber);
}

int readBearerFromFile(BearerInfo *bearerInfo, const char * filename)
{
    FILE *fp;
    fp = fopen(filename,"rb");
    if (fp == NULL)
        return ERROR;
    fread(bearerInfo, sizeof(BearerInfo), 1, fp);
    fclose(fp);
    printf("Read : Port: %hu, Maxlen %d, ip %lu\n",(unsigned short)bearerInfo->linSockBearer.remoteAddress.sin_port,
        bearerInfo->msgMaxLen, (unsigned long)bearerInfo->linSockBearer.remoteAddress.sin_addr.s_addr);

    return SUCCESS;
}

void writeBearerToFile(BearerInfo *bearerInfo, const char* filename)
{
    FILE *fp;
    printf("Writing : Port: %hu, Maxlen %d, ip %lu\n",(unsigned short)bearerInfo->linSockBearer.remoteAddress.sin_port,
     bearerInfo->msgMaxLen, (unsigned long)bearerInfo->linSockBearer.remoteAddress.sin_addr.s_addr);

    fp = fopen(filename,"wb");
    if(fp == NULL)
    {
        printf("File could not be opened\n");
        return;
    }
    else
        printf("Written bearerinfo to file\n");
    fwrite(bearerInfo,sizeof(BearerInfo),1,fp);
    fclose(fp);
}
