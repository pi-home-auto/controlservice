/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <Socket.h>
#include <malloc.h>
#include <string.h>
#include <unistd.h>
#include <netdb.h>
#include <errno.h>
#include <log.h>


int initSocket(Socket* pSocket)
{
    memset(pSocket, 0, sizeof(Socket));
    pSocket->eState = STATE_NONE;
    pSocket->container = NULL;
    pSocket->threadRunning = FALSE;
    pSocket->listener = NULL;
    pthread_mutex_init(&(pSocket->stateLock),NULL);
    pSocket->maxRetries = 5;

    return 0;
}

void setSocketRetries(Socket* pSocket,int maxRetries)
{
    if (maxRetries > 0)
        pSocket->maxRetries = maxRetries;
}

void wait (Socket* pSocket)
{
    pthread_join(pSocket->listeningThread,NULL);
}
int cleanUpSocket(Socket* pSocket)
{
    pthread_mutex_destroy(&(pSocket->stateLock));
    return 0;
}

void callConnectionError(Socket* pSocket, const char * msg)
{
    if (pSocket->listener != NULL && pSocket->listener->onConnectionError != NULL)
        pSocket->listener->onConnectionError(pSocket->container,msg);
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-but-set-variable"

void setState(Socket*pSocket, int state)
{
    int prevState = 0;
    pthread_mutex_lock(&(pSocket->stateLock));
    prevState = pSocket->eState;
    pSocket->eState = (SockState)state;
    pthread_mutex_unlock(&(pSocket->stateLock));
    logSocket("Socket: State Transition %d -> %d\n", pSocket->eState, state);
    if (pSocket->listener != NULL && pSocket->listener->onStateChanged != NULL)
        pSocket->listener->onStateChanged(pSocket->container, state);
}

#pragma GCC diagnostic pop

int getState(Socket *pSocket)
{
    int state;
    pthread_mutex_lock(&(pSocket->stateLock));
    state = pSocket->eState;
    pthread_mutex_unlock(&(pSocket->stateLock));
    return state;
}

int createSocket(Socket *pSocket)
{
    int bindfd = 0;
    struct sockaddr_in serverAddress;

    bindfd = socket(AF_INET, SOCK_STREAM, 0);
    if (bindfd < 0)
    {
        perror("ERROR opening socket");
        close(bindfd);
        return ERROR;
    }

    bzero((char *) &(serverAddress), sizeof(serverAddress));

    serverAddress.sin_family = AF_INET;
    serverAddress.sin_addr.s_addr = INADDR_ANY;
    serverAddress.sin_port = pSocket->bearerInfo.linSockBearer.remoteAddress.sin_port;

    if (bind(bindfd, (struct sockaddr *) &(serverAddress),
              sizeof(serverAddress)) < 0)
    {
        perror("ERROR binding socket");
        close(bindfd);
        return ERROR;
    }

    listen(bindfd,5);

    pSocket->bindFd = bindfd;

    return SUCCESS;
}

void* listeningFunc (void* data)
{
    char buffer[SOCK_BUFFER_SIZE];
    struct sockaddr_in clientAddress;
    socklen_t clientLength = sizeof(clientAddress);
    Socket *pSocket = (Socket*)data;
    int bytesRead;
    int sockfd=0;
    int state = 0;
    unsigned short portNumber;

    while(1)
    {
        state = getState(pSocket);
        logSocket("Socket: listening thread loop, state:%d\n", state);
        if (state == STATE_NONE)
        {
            logSocket("Socket :Listening thread stopping\n");
            return NULL;
        }

        if (state == STATE_LISTENING || state == STATE_CONNECTING)
        {
            bzero(&clientAddress,sizeof(struct sockaddr_in));
            logSocket("Socket: Blocking on Accept state is %d\n", getState(pSocket));
            sockfd = accept(pSocket->bindFd,
                            (struct sockaddr *) &(clientAddress),
                            &(clientLength));
            if (sockfd < 0)
            {
                perror("ERROR on Accept");
                close(pSocket->bindFd);
                if (errno == EBADF)
                {
                    logSocket("Socket: BADF shutdwodn problem\n");
                    if (createSocket(pSocket)==ERROR)
                        return NULL;
                    logSocket("Socket: Created socket again\n");
                }
                continue;
            }
            logSocket("Socket: Setting state from listener thread \n");
            state = getState(pSocket);
            if (state == STATE_LISTENING || state == STATE_CONNECTING)
            {
                setState(pSocket,STATE_CONNECTED);
                pSocket->socketFd = sockfd;
                portNumber = pSocket->bearerInfo.linSockBearer.remoteAddress.sin_port;
                memcpy(&(pSocket->bearerInfo.linSockBearer.remoteAddress),&clientAddress,sizeof(clientAddress));
                pSocket->bearerInfo.linSockBearer.remoteAddress.sin_port = portNumber;
            }
            else if (state == STATE_NONE)
            {
                logSocket("Socket: State is NONE return\n");
                return NULL;
            }
        }

        if (getState(pSocket) == STATE_CONNECTED)
        {
            bzero(buffer,sizeof(buffer));

            while(1)
            {
                logSocket("Socket: Blocking on recv state is %d\n", getState(pSocket));
                bytesRead = recv(pSocket->socketFd,buffer,sizeof(buffer),0);
                if (bytesRead <= 0)
                {
                    //perror("ERROR reading from socket bytes read");
                    callConnectionError(pSocket,"ERROR reading from socket");
                    if (getState(pSocket) == STATE_CONNECTED)
                    {
                        setState(pSocket,STATE_NONE); //TODO done for compliance with android wifi and bluetooth
                        //The accept call once bind is done will never fail.
                        setState(pSocket,STATE_LISTENING);
                    }
                    break;
                }
                logSocket("Socket: Read %d bytes\n",bytesRead);
                //logSocket("Buffer: %.*s\n",bytesRead,buffer);
                if (pSocket->listener != NULL && pSocket->listener->onDataReceived != NULL)
                    pSocket->listener->onDataReceived(pSocket->container,buffer,bytesRead);
                //TODO remove this
                //fwrite((void*)buffer,1,bytesRead,sockIn);
                //fflush(sockIn);
                bzero(buffer,bytesRead);
            }
            bzero(buffer,sizeof(buffer));
            if (getState(pSocket)==STATE_NONE)
                close(pSocket->bindFd);
            close(pSocket->socketFd);
        }

        else
        {
            logSocket("Socket: Illegal State\n");
            pSocket->threadRunning = FALSE;
            return NULL;
        }
    }
}


void socketSetBearerInfo(Socket *pSocket, BearerInfo* bearerInfo)
{
    memcpy(&(pSocket->bearerInfo),bearerInfo,sizeof(BearerInfo));
}

//TODO not thread safe can lock the state variable and get the state of the socket
int startListen(Socket* pSocket)
{

    if (getState(pSocket) == STATE_LISTENING || getState(pSocket) == STATE_CONNECTED || getState(pSocket) == STATE_CONNECTING)
        return SUCCESS;
    else if (getState(pSocket) == STATE_NONE)
    {
        if (pSocket->bearerInfo.linSockBearer.remoteAddress.sin_port == 0)
        {
            logSocket("Socket: Bearer info not set\n");
            return ERROR;
        }

        if (createSocket(pSocket) == ERROR)
            return ERROR;

        if (pthread_create(&(pSocket->listeningThread),NULL,&listeningFunc,(void*)pSocket) != 0)
        {
            perror("Error creating listening thread");
            return ERROR;
        }

        setState(pSocket,STATE_LISTENING);
        pSocket->threadRunning = TRUE;
        logSocket("Socket: Started Listening\n");
        return SUCCESS;
    }
    // should not reach here
    logSocket("Socket: Illegal state of socket\n");
    return ERROR;
}

//TODO all error cases when stop listen can be called
void stopListen(Socket* pSocket)
{
    logSocket("Socket: StopListen called\n");

    if (getState(pSocket) == STATE_LISTENING || getState(pSocket) == STATE_CONNECTING)
    {
        setState(pSocket,STATE_NONE);
        shutdown(pSocket->bindFd, SHUT_RDWR);
        //close(pSocket->bindFd);
    }

    if (getState(pSocket) == STATE_CONNECTED)
    {
        setState(pSocket,STATE_NONE);
        shutdown(pSocket->bindFd, SHUT_RDWR);
        close(pSocket->bindFd);
        shutdown(pSocket->socketFd, SHUT_RDWR);
        close(pSocket->socketFd);
        //callConnectionError(pSocket,"Stop Listen Called");
    }

    if (pSocket->threadRunning)
        pthread_join(pSocket->listeningThread,NULL);
}


void registerListener(Socket* pSocket, SocketListener* listener,void* container)
{
    pSocket->listener = listener;
    pSocket->container = container;

}

void unregisterListener(Socket* pSocket)
{
    pSocket->listener = NULL;
}

int connectToHost(Socket* pSocket)
{
    int socketfd;

    int retries = pSocket->maxRetries;

    socketfd = socket(AF_INET, SOCK_STREAM, 0);
    if (socketfd < 0)
    {
        perror("ERROR opening socket");
        return SEND_FAILURE;
    }

    while(retries > 0) {
        if (connect(socketfd, (struct sockaddr *) &((pSocket->bearerInfo).linSockBearer.remoteAddress),
                    sizeof(struct sockaddr_in)) < 0)
        {
            perror("ERROR connecting");
            retries--;
            sleep(SLEEP_TIME);
            if (getState(pSocket) == STATE_CONNECTED)
            {
                logSocket("Socket: Socket set to connected while connecting\n");
                return SEND_SUCCESS;
            }
        } else {
            pSocket->socketFd = socketfd;
            return SEND_SUCCESS;
        }
    }

    logSocket("Socket: Reached Max Retries\n");
    close(socketfd);
    return SEND_FAILURE;

}
int sendData(Socket* pSocket, char* data, int len)
{
    int numBytesSent;

    logSocket("Socket: SendData called, state:%d\n",getState(pSocket));

    if (getState(pSocket) == STATE_LISTENING)
    {
        if (pSocket->bearerInfo.linSockBearer.remoteAddress.sin_port == 0 )
        {   logSocket("Socket: Host or portnum not set\n"); return SEND_FAILURE; }

        setState(pSocket,STATE_CONNECTING);

        if (connectToHost(pSocket) == SEND_FAILURE)
        {
            logSocket("Socket: Could not connect to host\n");
            if (getState(pSocket) == STATE_CONNECTING)
                setState(pSocket,STATE_LISTENING);
            else
                logSocket("Socket: Could not connect and state set to %d",getState(pSocket));
            return SEND_FAILURE;
        } else {
            // if socket is already connected then no need to signal the listener thread to do anything
            if (getState(pSocket) == STATE_CONNECTING || getState(pSocket) == STATE_LISTENING)
            {
                logSocket("Socket: Setting state in SendData\n");
                setState(pSocket,STATE_CONNECTED);
                shutdown(pSocket->bindFd, SHUT_RDWR);
                close(pSocket->bindFd);
            } else {
                logSocket("Socket: Send data: The state is already set to %d when connectToHost returns\n",getState(pSocket));
            }
        }
    }

    if (getState(pSocket) == STATE_CONNECTED)
    {
        // TODO handle multiple writes needed
        numBytesSent = send(pSocket->socketFd, data, len, MSG_NOSIGNAL);
        if (numBytesSent == len)
        {
            //fwrite((void*)data,1,len,sockOut);
            //fflush(sockOut);
            return SEND_SUCCESS;
        } else {
            logSocket("Socket :Write Error msg:%d bytes, sent:%d bytes\n",len,numBytesSent);
            setState(pSocket,STATE_CONNECTING);
            shutdown(pSocket->socketFd, SHUT_RDWR);
            if (connectToHost(pSocket) == SEND_FAILURE) // retry 5 times
            {
                setState(pSocket,STATE_LISTENING);
                callConnectionError(pSocket,"Write Error");
                return SEND_FAILURE;
            }
            logSocket("Socket: Send Data: Setting state to connected after write failed and reconnected\n");
            setState(pSocket,STATE_CONNECTED);
            //TODO on send fail set to listening and shudown etc. and make code more elegant
            if (send(pSocket->socketFd,data,len,MSG_NOSIGNAL) > 0)
                return SEND_SUCCESS;
            else
            {
                logSocket("Socket: This should not fail\n");
                // unless the connection is disrupted between the connect and send call
                return SEND_FAILURE;
            }
        }
    } else {
        logSocket("Socket: Illegal state while sending Data, call startListen before sending data\n");
        return SEND_FAILURE;
    }
}
