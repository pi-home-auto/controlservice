#include <stdio.h>
#include <Transport.h>
#include <malloc.h>
#include <string.h>
#include <log.h>
#include <endian.h>

void printBufInfo(ReadBuffer* readBuf)
{
    logCircBuf("CircBuf :E? :%d, F? :%d, Readable %d bytes, writable %d bytes, W->%d, R->%d\n\n\n",
            isEmpty(readBuf), isFull(readBuf), bytesAvailableForRead(readBuf), bytesAvailableForWrite(readBuf),
            readBuf->writePos,readBuf->readPos);
}
void socketOnStateChanged(void *container,int state)
{
    logTransport("Transport :Socket State Changed:%d\n",state);
    if(container == NULL)
        return;
    Transport *transport = (Transport*)container;
    if(transport->listener !=NULL && transport->listener->onStateChanged !=NULL)
        transport->listener->onStateChanged(transport->container,state);
}

void socketOnConnectionError(void *container, const char* message)
{
    logTransport("Transport :Socket onConnectionError%s\n",message);
    if(container == NULL)
        return;
    Transport *transport = (Transport *)container;
    if(transport->listener !=NULL && transport->listener->onConnectionError!=NULL)
        transport->listener->onConnectionError(transport->container,message);

    reset(&(transport->readBuffer));
}

void transSetBearerInfo(Transport * transport, BearerInfo* bearerInfo)
{
    socketSetBearerInfo(&(transport->socket),bearerInfo);
}

void processReceivedMesssage(Transport* transport)
{
    static MsgState state = MSG_BEFORE_START;
    static int msgBufOffset = 0;
    int delimiter;
    static int msgLenLeft = 0;
    static int msgLength = 0;
    int bytesAvailable = 0;


    while(isEmpty(&(transport->readBuffer)) == 0)
    {
        switch(state)
        {
            case MSG_BEFORE_START:
                logTransport("Transport :In MSG_BEFORE_START\n");
                printBufInfo(&(transport->readBuffer));
                if(bytesAvailableForRead(&(transport->readBuffer)) < DELIMITER_SIZE)
                {
                    printBufInfo(&(transport->readBuffer));
                    logTransport("Transport :Cannot read 4 bytes for start\n");
                    return;
                }
                //readBuf(&(transport->readBuffer),(char*)&delimiter,0,DELIMITER_SIZE);
                peek(&(transport->readBuffer),(char*)&delimiter,0,DELIMITER_SIZE);
                delimiter = be32toh(delimiter);
                if(delimiter == START_BYTES)
                {
                    readBuf(&(transport->readBuffer),(char*)&delimiter,0,DELIMITER_SIZE);
                    printBufInfo(&(transport->readBuffer));
                    logTransport("Transport :Start Bytes received\n");
                    state = MSG_BEFORE_LEN;
                }
                else
                {
                    logTransport("Transport :Start bytes not received, bytes:%u\n",delimiter);
                    readBuf(&(transport->readBuffer),NULL,0,1);
                    printBufInfo(&(transport->readBuffer));
                    //TODO remove this
                    //exit(0);
                }
                break;

            case MSG_BEFORE_LEN:
                logTransport("Transport :In MSG_BEFORE_LEN\n");
                if(bytesAvailableForRead(&(transport->readBuffer)) < DELIMITER_SIZE)
                {
                    logTransport("Transport :Cannot read 4 bytes for length\n");
                    return;
                }

                readBuf(&(transport->readBuffer),(char*)&msgLength,0,DELIMITER_SIZE);
                msgLength = be32toh(msgLength);
                msgLength -= OVERHEAD;
                logTransport("Transport :msgLength received: %d\n",msgLength);
                msgLenLeft = msgLength;
                transport->msgBuf = (char*)malloc(sizeof(char)*msgLength);
                state = MSG_DATA;
                break;

            case MSG_DATA:
                logTransport("Transport :In MSG_DATA\n");
                bytesAvailable = bytesAvailableForRead(&(transport->readBuffer));
                if(bytesAvailable < msgLenLeft)
                {
                    printBufInfo(&(transport->readBuffer));
                    readBuf(&(transport->readBuffer),(char*)(transport->msgBuf),msgBufOffset,bytesAvailable);
                    printBufInfo(&(transport->readBuffer));
                    msgBufOffset += bytesAvailable;
                    msgLenLeft -= bytesAvailable;
                    logTransport("Transport :Read part of the msg in the buffer\n") ;
                    return;
                }

                readBuf(&(transport->readBuffer),(char*)(transport->msgBuf),msgBufOffset,msgLenLeft);
                msgLenLeft = 0;
                msgBufOffset = 0;
                state = MSG_BEFORE_END;
                logTransport("Transport :Read the full message in the buffer\n");
                printBufInfo(&(transport->readBuffer));
                break;

            case MSG_BEFORE_END:
                logTransport("Transport :In MSG_BEFORE_END\n");
                if(bytesAvailableForRead(&(transport->readBuffer)) < DELIMITER_SIZE)
                {
                    logTransport("Transport :Cannot read 4 bytes for end\n");
                    return;
                }
                printBufInfo(&(transport->readBuffer));
                //TODO do the same thing for start
                //readBuf(&(transport->readBuffer),(char*)&delimiter,delimiterOffset,DELIMITER_SIZE);
                peek(&(transport->readBuffer),(char*)&delimiter,0,DELIMITER_SIZE);
                delimiter = be32toh(delimiter);
                if(delimiter == END_BYTES)
                {
                    readBuf(&(transport->readBuffer),(char*)&delimiter,0,DELIMITER_SIZE);
                    printBufInfo(&(transport->readBuffer));
                    state = MSG_BEFORE_START;
                    if(transport->listener != NULL && transport->listener->onMessageReceived != NULL)
                        transport->listener->onMessageReceived(transport->container,transport->msgBuf,msgLength);
                    msgLength = 0;
                    logTransport("Transport :End Bytes received\n");

                }
                else
                {
                    logTransport("Transport :End bytes not received %d\n", delimiter);
                    // read 1 byte and start peeking from the next byte now
                    readBuf(&(transport->readBuffer),NULL,0,1);
                    printBufInfo(&(transport->readBuffer));
                    //TODO remove this
                    //exit(0);
                }
                free(transport->msgBuf);
                break;
        }
    }
}

void socketOnDataReceived(void *container, char* data, int len)
{
    Transport* transport = (Transport*)(container);
    int bytesAvailable = 0;
    int bytesWritten = 0;
    int bytesToWrite = 0;
    logTransport("Transport :onDataReceived:%d bytes\n",len);
    //logTransport("Buffer %.*s\n",len,data);

    if(container == NULL)
        return;

    while(bytesWritten < len)
    {
        bytesAvailable = bytesAvailableForWrite(&(transport->readBuffer));
        if(bytesAvailable == 0)
        {
            logTransport("Transport :Not enough space in the buffer message discarded\n");
            return;
        }
        bytesToWrite = bytesAvailable > (len - bytesWritten) ? (len-bytesWritten):bytesAvailable;
        bytesWritten += writeBuf(&(transport->readBuffer),data,bytesWritten,bytesToWrite);
        logTransport("Transport :Written %d/%d bytes\n",bytesWritten,len);
        printBufInfo(&(transport->readBuffer));
        processReceivedMesssage(transport);

    }
}

void socketOnFileProgress(void* container, int id, long progress)
{
    logTransport("Transport :onFileProgress");
}

void initTransport(Transport *transport)
{
    initSocket(&(transport->socket));
    transport->socketListener.onStateChanged = socketOnStateChanged;
    transport->socketListener.onConnectionError = socketOnConnectionError;
    transport->socketListener.onDataReceived = socketOnDataReceived;
    transport->socketListener.onFileProgress = socketOnFileProgress;
    registerListener(&(transport->socket),&(transport->socketListener),(void*)transport);
    transport->listener = NULL;
    initBuffer(&(transport->readBuffer));
}

void cleanUpTransport(Transport *transport)
{
    cleanUpSocket(&(transport->socket));
    cleanUpBuffer(&(transport->readBuffer));
}

int transSend(Transport *transport, char* data, int len)
{
    int bytesToSend = 0;
    int bytesSent = 0;
    int header;

    if (len > MAX_SIZE_4MB)
    {
        logTransport("Transport :packet length for send exceeds maximum size\n");
        return SEND_FAILURE;
    }

    header = START_BYTES;
    header = htobe32(header);
    if(sendData(&(transport->socket),(char*)&header,DELIMITER_SIZE) == SEND_FAILURE)
        return SEND_FAILURE;

    header = len + OVERHEAD;
    header = htobe32(header);
    if(sendData(&(transport->socket),(char*)&header,sizeof(int)) == SEND_FAILURE)
        return SEND_FAILURE;


    while(bytesSent < len)
    {
        if(len - bytesSent > (transport->socket).bearerInfo.msgMaxLen)
            bytesToSend = (transport->socket).bearerInfo.msgMaxLen;
        else
            bytesToSend = len - bytesSent;
        if(sendData(&(transport->socket),data + bytesSent, bytesToSend) == SEND_FAILURE)
            return SEND_FAILURE;
        bytesSent += bytesToSend;

    }

    header = END_BYTES;
    header = htobe32(header);
    if(sendData(&(transport->socket),(char*)&header,DELIMITER_SIZE) == SEND_FAILURE)
        return SEND_FAILURE;

    return SEND_SUCCESS;
}

void transRegisterListener(Transport* transport, TransportListener *listener, void* container)
{
    if(listener != NULL)
        transport->listener = listener;
    transport->container = container;
}

void transUnregisterListener(Transport* transport)
{
    transport->listener = NULL;
}

void transStopListen(Transport* transport)
{
    stopListen(&(transport->socket));
}

int transStartListen(Transport *transport)
{
    return startListen(&(transport->socket));
}
