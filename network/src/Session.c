#include <stdio.h>
#include <cJSON.h>
#include <malloc.h>
#include <Session.h>
#include <string.h>
#include <log.h>

void transOnStateChanged(void *container,int state)
{
    logSession("Session :Transport State Changed:%d\n", state);
    if (container == NULL)
        return;
    Session *session = (Session*)container;
    if(session->listener != NULL && session->listener->onStateChanged != NULL)
        session->listener->onStateChanged(session->container,state);
}

void transOnConnectionError(void *container, const char* message)
{
    logSession("Session :onConnectionError%s\n", message);
    if (container == NULL)
        return;
    Session *session = (Session *)container;
    if(session->listener !=NULL && session->listener->onConnectionError!=NULL)
        session->listener->onConnectionError(session->container,message);

}

void transOnFileProgress(void* container, int id, long progress)
{
    logSession("Session :onFileProgress");
}


void transOnMessageReceived(void *container, char* data, int len)
{
    Session* session = (Session*)(container);
    char *message;
    cJSON *protoMsg,*stringMessage, *msg;

    protoMsg=cJSON_Parse(data);

    logSession("Session :transOnDataReceived\n");

    stringMessage = cJSON_GetObjectItem(protoMsg,"stringMessage");
    if(stringMessage == NULL)
        logSession("Session :StringMessage not found in Json object\n");
    msg = cJSON_GetObjectItem(stringMessage,"stringMessage");
    if (msg == NULL)
        logSession("Session :StringMessage not found in Json object\n");

    message = cJSON_Print(msg);
    logSession("Session :The Message is %s\n",message);

    if(session->listener !=NULL && session->listener->onConnectionError!=NULL)
        session->listener->onMessageReceived(session->container,message,strlen(message));

    cJSON_Delete(protoMsg);
    free(message);
}
void initSession(Session * session)
{
    initTransport(&(session->transport));
    session->listener = NULL;
    session->container = NULL;
    session->transportListener.onStateChanged = transOnStateChanged;
    session->transportListener.onConnectionError = transOnConnectionError;
    session->transportListener.onMessageReceived = transOnMessageReceived;
    session->transportListener.onFileProgress = transOnFileProgress;
    transRegisterListener(&(session->transport), &(session->transportListener), (void*)session);
}
void cleanUpSession(Session *session)
{
    cleanUpTransport(&(session->transport));
}

void setBearerInfo(Session *session, BearerInfo* bearerInfo)
{
    transSetBearerInfo(&(session->transport),bearerInfo);
}

//TODO send other kinds of protocol message
int sessionSend(Session *session,const char * message,int len)
{
    char *out;
    cJSON *protoMsg,*fmt;

    protoMsg=cJSON_CreateObject();
    cJSON_AddStringToObject(protoMsg,"message", "GENERIC_STRING_MESSAGE");
    cJSON_AddItemToObject(protoMsg, "stringMessage", fmt=cJSON_CreateObject());
    cJSON_AddStringToObject(fmt,"stringMessage",message);
    cJSON_AddStringToObject(protoMsg,"messageType", "COMMAND");
    cJSON_AddStringToObject(protoMsg,"priority", "DEFAULT_PRIORITY");
    cJSON_AddNumberToObject(protoMsg,"messageId", 6);
    cJSON_AddFalseToObject (protoMsg,"isError");

    out = cJSON_Print(protoMsg);
    logSession("Session :Sending message of length %u\n", (unsigned int)strlen(out));


    if(transSend(&(session->transport),out,strlen(out))==SEND_FAILURE)
    {
        logSession("Session :Send error\n");
        return SEND_FAILURE;
    }

    logSession("Session :Successfully sent data\n");
    cJSON_Delete(protoMsg);
    free(out);
    return SEND_SUCCESS;
}

void sessionRegisterListener(Session* session, SessionListener *listener, void* container)
{
    if(listener != NULL)
        session->listener = listener;
    if(container != NULL)
        session->container = container;
}

void sessionUnregisterListener(Session* session)
{
    session->listener = NULL;
}

void sessionStopListen(Session* session)
{
    transStopListen(&(session->transport));
}

int sessionStartListen(Session *session)
{
    return transStartListen(&(session->transport));
}
