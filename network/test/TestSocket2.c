#include <stdio.h>
#include <unistd.h>
#include <Socket.h>
#include <string.h>
void onStateChanged(int state)
{
    printf("State Changed:%d\n",state);

}
void onConnectionError(const char* message)
{
    printf("onConnectionError%s\n",message);
}
void onDataReceived(char* data, int len)
{
    int i;
    printf("onDataReceived:");
    for(i=0;i<len;i++)
        printf("%c",data[i]);
    printf("\n");
}
void onFileProgress(int id, long progress)
{
    printf("onFileProgress");
}

void test0();
void test1();
void test2();
void test3();
int main(int argc, char *argv[])
{
    int testNum;
    if (argc < 2)
    {
        printf("Usage ./client <testnum>\n");
        return 0;
    }
    testNum = atoi(argv[1]);
    switch(testNum)
    {
        case 0:
            test0();
            break;
        case 1:
            test1();
            break;
        case 2:
            test2();
            break;
        case 3:
            test3();
            break;
        default:
            break;
    }


    return 0;
}




void test0()
{

    Socket socket;
    initSocket(&socket);
    SocketListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onDataReceived = onDataReceived;
    listener.onFileProgress = onFileProgress;
    registerListener(&socket,&listener);
    startListen(&socket);
    wait(&socket);
}


void test1()
{
    char* data = "Sample Data to send\n";
    Socket socket;
    initSocket(&socket);
    startListen(&socket);
    while(1) {
        if(sendData(&socket,data,strlen(data)) == SEND_SUCCESS)
            printf("Data Sent Successfully\n");
        else
            printf("Could not send data\n");
        sleep(5);
    }
}

void test2()
{
    char* string = "Test data this is\n";
    Socket socket;
    initSocket(&socket);
    startListen(&socket);
    sleep(10);
    printf("Sending data now\n");
    if(sendData(&socket,string,strlen(string)) != 0)
        printf("Error in send data\n");
    else
        printf("SendData successful\n");
    stopListen(&socket);
}
// with test 2 of client
void test3()
{
    char* string = "Test data this is1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111\n";
    Socket socket;
    initSocket(&socket);
    startListen(&socket);
    sleep(5);
    printf("Sending data now\n");
    if(sendData(&socket,string,strlen(string)) != 0)
        printf("Error in send data\n");
    else
        printf("SendData successful\n");
    wait(&socket);
}
