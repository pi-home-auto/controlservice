#include <stdio.h>
#include <log.h>
#include <Session.h>
#include <string.h>
#include <malloc.h>
#include <Bearer.h>
#include <stdlib.h>
#include <unistd.h>


#define NUM_TESTS 10
#define ANDROID_NET_PORT 21111

void onStateChanged(void *container, int state)
{
    logTest("Test :Application State Changed:%d\n",state);
}

void onConnectionError(void *container, const char* message)
{
    logTest("Test :Application onConnectionError%s\n",message);
}

void onMessageReceived(void *container, char* data, int len)
{
    logTest("Test :Application onMessageReceived:%.*s\n",len,data);
}

void onFileProgress(void *container, int id, long progress)
{
    logTest("Test :Application onFileProgress\n");
}

void test0(int argc, char *argv[]);
void test1(int argc, char *argv[]);
void test2(int argc, char *argv[]);
void test3(int argc, char *argv[]);
void test4(int argc, char *argv[]);
void test5(int argc, char *argv[]);
void test6(int argc, char *argv[]);
void test7(int argc, char *argv[]);
void test8(int argc, char *argv[]);
void test9(int argc, char *argv[]);


void (*testFuncs[NUM_TESTS])(int argc,char**argv) = {test0,test1,test2,test3,test4,test5,test6,test7,test8,test9};

int main(int argc, char *argv[])
{
    int testNum;
    if (argc < 2)
    {
        logTest("Test :Usage ./testSession <testnum>\n");
        return 0;
    }
    testNum = atoi(argv[1]);
    if(testNum < 0 || testNum >= NUM_TESTS)
    {
        logTest("Test :Invalid test function\n");
        return 0;
    }
    (*testFuncs[testNum])(argc,argv);

    return 0;

}

void test0(int argc, char *argv[])
{
    Session session;
    BearerInfo bearerInfo;
    initSession(&session);
    fillLinSockBearerInfo(&bearerInfo,(char*)"192.168.1.13",ANDROID_NET_PORT,1024);
    setBearerInfo(&session,&bearerInfo);
    sessionStartListen(&session);
    char* msg = (char*)"SESSION_DATA_SAMPLE";

    if(sessionSend(&session,msg,strlen(msg))==SEND_SUCCESS)
        logTest("Test :Successfully sent data\n");
    else
        logTest("Test :Send error\n");

    sleep(2);
    sessionStopListen(&session);
    cleanUpSession(&session);
}

void test1(int argc, char *argv[])
{
    Session session;
    BearerInfo bearerInfo;
    initSession(&session);
    fillLinSockBearerInfo(&bearerInfo,(char*)"localhost",ANDROID_NET_PORT,1024);
    setBearerInfo(&session,&bearerInfo);
    SessionListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived;
    listener.onFileProgress = onFileProgress;
    sessionRegisterListener(&session,&listener,NULL);
    sessionStartListen(&session);
    while(1)
        sleep(10);
}

void test2(int argc, char *argv[])
{
    Session session;
    BearerInfo bearerInfo;
    initSession(&session);
    fillLinSockBearerInfo(&bearerInfo,(char*)"192.168.1.13",ANDROID_NET_PORT,1024);
    setBearerInfo(&session,&bearerInfo);
    sessionStartListen(&session);
    SessionListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived;
    listener.onFileProgress = onFileProgress;
    sessionRegisterListener(&session,&listener,NULL);

    while(1)
    {
        logTest("Test :Sending Data now\n");
        if(sessionSend(&session,(char*)"SESSION_DATA_SAMPLE",21)==SEND_SUCCESS)
            logTest("Test :Successfully sent data\n");
        else
            logTest("Test :Send error\n");
        sleep(10);
    }
    cleanUpSession(&session);

}

void test3(int argc, char *argv[])
{
    Session session;
    BearerInfo bearerInfo;
    initSession(&session);
    fillLinSockBearerInfo(&bearerInfo,(char*)"192.168.1.13",ANDROID_NET_PORT,1024);
    setBearerInfo(&session,&bearerInfo);
    sessionStartListen(&session);
    char* message = (char*)"{\"message\":\"GENERIC_STRING_MESSAGE\",\"stringMessage\":{\"stringMessage\":\"Hello World\"},\"messageType\":\"COMMAND\",\"priority\":\"DEFAULT_PRIORITY\",\"messageId\":6,\"isError\":false}";
    if(sessionSend(&session,message,168)==SEND_SUCCESS)
        logTest("Test :Successfully sent data\n");
    else
        logTest("Test :Send error\n");

    while(1)
        sleep(10);
    cleanUpSession(&session);

}

void test4(int argc, char *argv[])
{
    Session session;
    BearerInfo bearerInfo;
    initSession(&session);
    fillLinSockBearerInfo(&bearerInfo,(char*)"192.168.1.13",ANDROID_NET_PORT,1024);
    setBearerInfo(&session,&bearerInfo);
    sessionStartListen(&session);
    SessionListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived;
    listener.onFileProgress = onFileProgress;
    sessionRegisterListener(&session,&listener,NULL);
    long sumBytesRead = 0;
    char *buffer;
    if(argc < 3)
    {
        logTest("Test :PLease give file name\n");
        return;
    }
    FILE *fp;
    long bytesRead;
    int fileSize;
    if((fp = fopen(argv[2],"r")) == NULL)
    {
        logTest("Test :Could not open File\n");
        return;
    }
    fseek(fp,0,SEEK_END);
    fileSize = ftell(fp);
    fseek(fp,0,SEEK_SET);
    logTest("Test :The file size is %d",fileSize);

    buffer = (char*)malloc(sizeof(char)*fileSize);

    bytesRead = fread((void*)buffer,1,fileSize,fp);
    logTest("Test :The number of bytes read are %d\n",bytesRead);

    if(sessionSend(&session,buffer,bytesRead)==SEND_SUCCESS)
        logTest("Test :Successfully sent %d bytes of data\n",bytesRead);
    else
    {
        logTest("Test :Send error\n");
    }

    bytesRead = fread((void*)buffer,1,100,fp);
    logTest("Test :%d bytes left in the file\n");
    sleep(5);
    fclose(fp);
}


void onFileMessageReceived(void *container, char* data, int len)
{
    static FILE *fp = NULL;
    int written = 0;
    if(fp == NULL)
    {
        fp = fopen("output.pdf","w");
    }
    written = fwrite((void*)data,1,len,fp);
    logTest("Test :Application onMessageReceived %d bytes, written %d bytes\n",len,written);
    fflush(fp);

}
void test5(int argc, char *argv[])
{
    Session session;
    BearerInfo bearerInfo;
    initSession(&session);
    fillLinSockBearerInfo(&bearerInfo,(char*)"192.168.1.13",ANDROID_NET_PORT,1024);
    setBearerInfo(&session,&bearerInfo);
    sessionStartListen(&session);
    SessionListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onFileMessageReceived;
    listener.onFileProgress = onFileProgress;
    sessionRegisterListener(&session,&listener,NULL);
    while(1)
    {
        sleep(10);
    }

}

void test6(int argc, char *argv[])
{
    Session session;
    BearerInfo bearerInfo;
    initSession(&session);
    fillLinSockBearerInfo(&bearerInfo,(char*)"192.168.1.13",ANDROID_NET_PORT,1024);
    setBearerInfo(&session,&bearerInfo);
    sessionStartListen(&session);
    SessionListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived;
    listener.onFileProgress = onFileProgress;
    sessionRegisterListener(&session,&listener,NULL);
    long sumBytesRead = 0;
    int bufsize = 2049;
    char buffer[2049];
    if(argc < 3)
    {
        logTest("Test :PLease give file name\n");
        return;
    }
    FILE *fp;
    int bytesRead;
    if((fp = fopen(argv[2],"r")) == NULL)
    {
        logTest("Test :Could not open File\n");
        return;
    }

    bytesRead = fread((void*)buffer,1,bufsize,fp);
    sumBytesRead += bytesRead;
    while(bytesRead > 0)
    {
        if(sessionSend(&session,buffer,bytesRead)==SEND_SUCCESS)
            logTest("Test :Successfully sent %d bytes of data\n",bytesRead);
        else
        {
            logTest("Test :Send error\n");
            break;
        }
        bytesRead = fread((void*)buffer,1,bufsize,fp);
        sumBytesRead += bytesRead;
        logTest("Test :Total bytes read is %ld\n",sumBytesRead);
    }
    sleep(5);
    fclose(fp);
}

void onMessageReceived1(void *container, char* data, int len)
{
    int ret;
    logTest("Test :Application onMessageReceived:%.*s\n",len,data);
    Session *session = (Session*)container;
    logTest("Test :calling session Send now\n");
    ret = sessionSend(session,"Reply",5);
    if(ret == SEND_SUCCESS)
        logTest("Test :Successfully sent\n");
    else
        logTest("Test :Send Error\n");

    writeBearerToFile(&((session->transport).socket.bearerInfo));
}


void test7(int argc, char *argv[])
{
    Session session;
    int ret;
    BearerInfo bearerInfo;
    initSession(&session);
    ret = readBearerFromFile(&bearerInfo);
    if(ret == ERROR)
    {
        logTest("Test :The bearer info is not found\n");
        fillLinSockBearerInfo(&bearerInfo,(char*)"localhost",ANDROID_NET_PORT,1024);
    }
    else
        logTest("Test :The bearer info is loaded from file\n");

    setBearerInfo(&session,&bearerInfo);
    SessionListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived1;
    listener.onFileProgress = onFileProgress;
    sessionRegisterListener(&session,&listener,&session);
    sessionStartListen(&session);
    while(1)
        sleep(10);
}

void test8(int argc, char *argv[])
{
    int ret;

    BearerInfo bearerInfo1,bearerInfo2;
    fillLinSockBearerInfo(&bearerInfo1,(char*)"192.168.1.13",ANDROID_NET_PORT,1024);
    writeBearerToFile(&(bearerInfo1));

    ret = readBearerFromFile(&bearerInfo2);
    if(ret == ERROR)
    {
        logTest("Test :The bearer info is not found\n");
    }
    else
    {
        logTest("Test :The bearer info is loaded from file\n");

    }

    Session session;
    initSession(&session);
    setBearerInfo(&session,&bearerInfo2);
    SessionListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived1;
    listener.onFileProgress = onFileProgress;
    sessionRegisterListener(&session,&listener,&session);
    sessionStartListen(&session);
    sessionSend(&session,"TEST_BEARER",11);
}


void test9(int argc, char *argv[])
{
    int ret;

    BearerInfo bearerInfo2;

    ret = readBearerFromFile(&bearerInfo2);
    if(ret == ERROR)
    {
        logTest("Test :The bearer info is not found\n");
        return;
    }
    else
    {
        logTest("Test :The bearer info is loaded from file\n");
    }

    Session session;
    initSession(&session);
    setBearerInfo(&session,&bearerInfo2);
    SessionListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived1;
    listener.onFileProgress = onFileProgress;
    sessionRegisterListener(&session,&listener,&session);
    sessionStartListen(&session);
    sessionSend(&session,"TEST_BEARER",11);
}
