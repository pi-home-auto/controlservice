#include <stdio.h>
#include <unistd.h>
#include <Socket.h>
#include <string.h>
#include <stdlib.h>
#include <log.h>

void onStateChanged(void *container, int state)
{
    logTest("Test :State Changed:%d\n",state);

}
void onConnectionError(void *container, const char* message)
{
    logTest("Test :onConnectionError%s\n",message);
}
void onDataReceived(void *container, char* data, int len)
{
    int i;
    logTest("Test :onDataReceived:");
    for(i=0;i<len;i++)
        logTest("Test :%c",data[i]);
    logTest("Test :\n");
}
void onFileProgress(void *container, int id, long progress)
{
    logTest("Test :onFileProgress");
}



void test0();
void test1();
void test2();
void test3();
int main(int argc, char *argv[])
{
    int testNum;
    if (argc < 2)
    {
        logTest("Test :Usage ./client <testnum>\n");
        return 0;
    }
    testNum = atoi(argv[1]);
    switch(testNum)
    {
        case 0:
            test0();
            break;
        case 1:
            test1();
            break;
        case 2:
            test2();
            break;
        case 3:
            test3();
            break;
        default:
            break;
    }


    return 0;
}
// test with test0 and test1 on other side.
void test0()
{
    char* data = "Sample Data to send\n";
    Socket socket;
    initSocket(&socket);

    SocketListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onDataReceived = onDataReceived;
    listener.onFileProgress = onFileProgress;
    registerListener(&socket,&listener,NULL);

    startListen(&socket);
    while(1) {
        if(sendData(&socket,data,strlen(data)) == SEND_SUCCESS)
            logTest("Test :Data Sent Successfully\n");
        else
            logTest("Test :Could not send data\n");
        sleep(5);
    }
}

// test with test0 on other side
void test1()
{
    Socket socket;
    initSocket(&socket);
    SocketListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onDataReceived = onDataReceived;
    listener.onFileProgress = onFileProgress;
    registerListener(&socket,&listener,NULL);


    startListen(&socket);
    while(1) {
        sleep(5);
    }
}

//TODO this segfaults sometimes, have to fix this.
void test2()
{
    char* data = "Sample Data to send\n";
    Socket socket;
    initSocket(&socket);
    startListen(&socket);
    if(sendData(&socket,data,strlen(data)) == SEND_SUCCESS)
    {
        logTest("Test :Data Sent Successfully\n");
        startListen(&socket);
    }
    else
    {
        logTest("Test :Could not send data\n");
        return;
    }

    sleep(10);
    logTest("Test :\n\n\nstopListen\n\n\n");
    stopListen(&socket);
    if(sendData(&socket,data,strlen(data)) == SEND_SUCCESS)
        logTest("Test :Data Sent Successfully\n");
    logTest("Test :\n\n\nstartListen\n\n\n");
    startListen(&socket);
    if(sendData(&socket,data,strlen(data)) == SEND_SUCCESS)
        logTest("Test :Data Sent Successfully\n");

}

// test with test0/test1/test3 on other side
void test3()
{
    char* string = "Test data this is1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111\n";
    Socket socket;
    initSocket(&socket);
    startListen(&socket);
    sleep(10);
    logTest("Test :Sending data now\n");
    if(sendData(&socket,string,strlen(string)) != 0)
        logTest("Test :Error in send data\n");
    else
        logTest("Test :SendData successful\n");
}



