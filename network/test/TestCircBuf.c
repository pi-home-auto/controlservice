#include <Transport.h>
#include <stdio.h>
#include <log.h>
#include <string.h>
void printBufInfo(ReadBuffer* readBuf,int size)
{
    logCircBuf("CircBuf :IsEmpty %d\n",isEmpty(readBuf));
    logCircBuf("CircBuf :IsFull %d\n",isFull(readBuf));
    logCircBuf("CircBuf :BytesAvailableForRead %d\n",bytesAvailableForRead(readBuf));
    logCircBuf("CircBuf :BytesAvailableForWrite %d\n",bytesAvailableForWrite(readBuf));
    logCircBuf("CircBuf :W->%d, R->%d\n",readBuf->writePos, readBuf->readPos);
    printBuf(readBuf);
}

int main()
{
    ReadBuffer readBuffer;
    initBuffer(&readBuffer);

    char* string = "12341234abcdefghijklmnop1234";
    int len = strlen(string);
    int bytesAvailable = 0;
    int bytesWritten = 0;
    int bytesToWrite = 0;
    int readOffset = 0;
    char outStr[100];
    printf("String length is %d\n", len);
    while(bytesWritten < len)
    {
        bytesAvailable = bytesAvailableForWrite(&(readBuffer));
        if(bytesAvailable == 0)
        {
            netlog(TRANSPORT,"Not enough space in the buffer message discarded\n");
            return;
        }
        bytesToWrite = bytesAvailable > (len - bytesWritten) ? (len-bytesWritten):bytesAvailable;
        netlog(TEST,"bytesAvailable %d, bytesToWrite %d, bytesWritten %d, chartoWrite %c\n", bytesAvailable, bytesToWrite, bytesWritten,string[bytesWritten]);
        bytesWritten += writeBuf(&(readBuffer),string,bytesWritten,bytesToWrite);
        netlog(TEST,"Written %d/%d bytes\n",bytesWritten,len);
        printBufInfo(&(readBuffer),0);
        if(bytesAvailableForRead(&readBuffer) >= 4)
        {
            readOffset += readBuf(&readBuffer,outStr,readOffset,4);
            netlog(TEST,"read 4 bytes offset%d\n", readOffset);
            printBufInfo(&(readBuffer),0);
            printf("%.*s\n",readOffset,outStr);
        }
        printf("\n");
    }
    printf("Out put string is: %.*s|end\n",len,outStr);
    return 0;
}

/*
int main()
{
    ReadBuffer readBuf;
    initBuffer(&readBuf);
    char testBuf[25];
    int numBytes,offset;

    log(TEST,"Initialized buf\n");
    printBufInfo(&readBuf,0);

    numBytes = write(&readBuf,"abcde",0,5);
    log(TEST,"Written %d/5 bytes\n", numBytes);
    printBufInfo(&readBuf,5);

    numBytes = read(&readBuf, testBuf, 0, 3);
    //testBuf[numBytes] = '\0';
    offset = numBytes;
    log(TEST,"Read %d/3 bytes new buf is:%s,offset is:%d\n",numBytes,testBuf,offset); // abc
    printBuf((char*)testBuf,25);
    printBufInfo(&readBuf,5);

    while(isFull(&readBuf) != 1)
        numBytes = write(&readBuf,"0123",0,4);
    log(TEST,"Written till buffer is full bytes\n", numBytes);
    printBufInfo(&readBuf,1024);

    numBytes = read(&readBuf, testBuf, offset, 3);
    offset += numBytes;
    log(TEST,"Read %d/3 bytes\n",numBytes); // abcd012
    printBuf((char*)testBuf,25);
    printBufInfo(&readBuf,1024);

    while(isFull(&readBuf) != 1)
        numBytes = write(&readBuf,"0123",0,4);
    log(TEST,"Written till buffer is full bytes\n", numBytes);
    printBufInfo(&readBuf,1024);

    numBytes = read(&readBuf, testBuf, offset, 3);
    log(TEST,"Read %d/3 bytes\n",numBytes); // abcd012
    printBuf((char*)testBuf,25);
    printBufInfo(&readBuf,1024);
    cleanUpBuffer(&readBuf);




    return 0;
}
*/
