#include <stdio.h>
#include <Transport.h>
#include <log.h>
#define NUM_TESTS 7

void onStateChanged(void *container, int state)
{
    logTest("Test :Session State Changed:%d\n",state);

}
void onConnectionError(void *container, const char* message)
{
    logTest("Test :Session onConnectionError%s\n",message);
}
void onMessageReceived(void *container, char* data, int len)
{
    logTest("Test :Session onMessageReceived:%.*s\n",len,data);
}
void onFileProgress(void *container, int id, long progress)
{
    logTest("Test :Session onFileProgress\n");
}



void test0(int argc, char *argv[]);
void test1(int argc, char *argv[]);
void test2(int argc, char *argv[]);
void test3(int argc, char *argv[]);
void test4(int argc, char *argv[]);
void test5(int argc, char *argv[]);
void test6(int argc, char *argv[]);

void (*testFuncs[NUM_TESTS])() = {test0,test1,test2,test3,test4,test5,test6};

int main(int argc, char *argv[])
{
    int testNum;
    if (argc < 2)
    {
        logTest("Test :Usage ./testTrans <testnum>\n");
        return -1;
    }
    testNum = atoi(argv[1]);
    if(testNum < 0 || testNum >= NUM_TESTS)
    {
        logTest("Test :Invalid test function\n");
        return -1;
    }
    (*testFuncs[testNum])(argc,argv);

    return 0;

}

void test0(int argc, char *argv[])
{
    Transport transport;
    initTransport(&transport);
    transStartListen(&transport);

    if(transSend(&transport,"TRANSPORT_DATA_SAMPLE",21)==SEND_SUCCESS)
        logTest("Test :Successfully sent data\n");
    else
        logTest("Test :Send error\n");
    transStopListen(&transport);
    cleanUpTransport(&transport);
}

void test1(int argc, char *argv[])
{
    Transport transport;
    initTransport(&transport);
    transStartListen(&transport);
    TransportListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived;
    listener.onFileProgress = onFileProgress;
    transRegisterListener(&transport,&listener,NULL);
    while(1)
        sleep(10);
}

void test2(int argc, char *argv[])
{
    Transport transport;
    initTransport(&transport);
    transStartListen(&transport);
    TransportListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived;
    listener.onFileProgress = onFileProgress;
    transRegisterListener(&transport,&listener,NULL);

    while(1)
    {
        logTest("Test :Sending Data now\n");
        if(transSend(&transport,"TRANSPORT_DATA_SAMPLE",21)==SEND_SUCCESS)
            logTest("Test :Successfully sent data\n");
        else
            logTest("Test :Send error\n");
        sleep(10);
    }
    cleanUpTransport(&transport);

}

void test3(int argc, char *argv[])
{
    Transport transport;
    initTransport(&transport);
    transStartListen(&transport);
    char* message = "{\"message\":\"GENERIC_STRING_MESSAGE\",\"stringMessage\":{\"stringMessage\":\"Hello World\"},\"messageType\":\"COMMAND\",\"priority\":\"DEFAULT_PRIORITY\",\"messageId\":6,\"isError\":false}";
    if(transSend(&transport,message,168)==SEND_SUCCESS)
        logTest("Test :Successfully sent data\n");
    else
        logTest("Test :Send error\n");

    while(1)
        sleep(10);
    cleanUpTransport(&transport);

}

void test4(int argc, char *argv[])
{
    Transport transport;
    initTransport(&transport);
    transStartListen(&transport);
    TransportListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived;
    listener.onFileProgress = onFileProgress;
    transRegisterListener(&transport,&listener,NULL);
    long sumBytesRead = 0;
    char buffer[100];
    if(argc < 3)
    {
        logTest("Test :PLease give file name\n");
        return;
    }
    FILE *fp;
    int bytesRead;
    if((fp = fopen(argv[2],"r")) == NULL)
    {
        logTest("Test :Could not open File\n");
        return;
    }

    bytesRead = fread((void*)buffer,1,100,fp);
    sumBytesRead += bytesRead;
    while(bytesRead > 0)
    {
        if(transSend(&transport,buffer,bytesRead)==SEND_SUCCESS)
            logTest("Test :Successfully sent %d bytes of data\n",bytesRead);
        else
        {
            logTest("Test :Send error\n");
            break;
        }
        bytesRead = fread((void*)buffer,1,100,fp);
        sumBytesRead += bytesRead;
        logTest("Test :Total bytes read is %ld\n",sumBytesRead);
    }
    sleep(5);
    fclose(fp);
}


void onFileMessageReceived(void *container, char* data, int len)
{
    static FILE *fp = NULL;
    int written = 0;
    if(fp == NULL)
    {
        fp = fopen("output.pdf","w");
    }
    written = fwrite((void*)data,1,len,fp);
    logTest("Test :Session onMessageReceived %d bytes, written %d bytes\n",len,written);
    fflush(fp);

}
void test5(int argc, char *argv[])
{
    Transport transport;
    initTransport(&transport);
    transStartListen(&transport);
    TransportListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onFileMessageReceived;
    listener.onFileProgress = onFileProgress;
    transRegisterListener(&transport,&listener,NULL);
    while(1)
    {
        sleep(10);
    }

}

void test6(int argc, char *argv[])
{
    Transport transport;
    initTransport(&transport);
    transStartListen(&transport);
    TransportListener listener;
    listener.onStateChanged = onStateChanged;
    listener.onConnectionError = onConnectionError;
    listener.onMessageReceived = onMessageReceived;
    listener.onFileProgress = onFileProgress;
    transRegisterListener(&transport,&listener,NULL);
    long sumBytesRead = 0;
    int bufsize = 2049;
    char buffer[2049];
    if(argc < 3)
    {
        logTest("Test :PLease give file name\n");
        return;
    }
    FILE *fp;
    int bytesRead;
    if((fp = fopen(argv[2],"r")) == NULL)
    {
        logTest("Test :Could not open File\n");
        return;
    }

    bytesRead = fread((void*)buffer,1,bufsize,fp);
    sumBytesRead += bytesRead;
    while(bytesRead > 0)
    {
        if(transSend(&transport,buffer,bytesRead)==SEND_SUCCESS)
            logTest("Test :Successfully sent %d bytes of data\n",bytesRead);
        else
        {
            logTest("Test :Send error\n");
            break;
        }
        bytesRead = fread((void*)buffer,1,bufsize,fp);
        sumBytesRead += bytesRead;
        logTest("Test :Total bytes read is %ld\n",sumBytesRead);
    }
    sleep(5);
    fclose(fp);
}


