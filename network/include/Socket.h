#ifndef _MY_SOCKET_H_
#define _MY_SOCKET_H_

#include <sys/socket.h>
#include <netinet/in.h>
#include <pthread.h>
#include <Bearer.h>
#ifdef __cplusplus
extern "C" {
#endif


typedef enum SockState {STATE_NONE = 0, STATE_LISTENING = 1, STATE_CONNECTING = 2, STATE_CONNECTED = 3} SockState;

#define SEND_SUCCESS 0
#define SEND_FAILURE -1
#define SLEEP_TIME 1
#define SOCK_BUFFER_SIZE 256
#define TRUE 1
#define FALSE 0


typedef struct SocketListener {
    void (*onStateChanged)(void* container, int state);
    void (*onConnectionError)(void* container, const char* message);
    void (*onDataReceived)(void* container, char* data, int len);
    void (*onFileProgress)(void* container, int id, long progress);
    void (*onTransferCompleted)(void* container, int transactionId, char* fileName, int errorCode);
} SocketListener;


// this is the common struct for all bearers.
typedef struct Socket {
    SockState eState;
    pthread_t listeningThread;
    int threadRunning;
    SocketListener* listener;
    BearerInfo bearerInfo;
    int socketFd;
    int bindFd;
    pthread_mutex_t stateLock;
    int maxRetries;
    void* container;
} Socket;


int initSocket(Socket* pSocket);
int cleanUpSocket(Socket* pSocket);
int startListen(Socket* pSocket);
void stopListen(Socket* pSocket);
void registerListener(Socket* pSocket, SocketListener* listener,void* container);
void unregisterListener(Socket*pSocket);
void socketSetBearerInfo(Socket *pSocket, BearerInfo* bearerInfo);
int sendData(Socket* pSocket, char* data, int len);

#ifdef __cplusplus
}
#endif
#endif
