#ifndef _TRANSPORT_H_
#define _TRANSPORT_H_

#include <pthread.h>
#include <CircBuf.h>
#include <Socket.h>

#define TRANSPORT_BUF_SIZE 1024
#define DELIMITER_SIZE 4
#define START_BYTES 0x01010101
#define END_BYTES 0x02020202
#define MAX_SIZE_4MB 5194304
#define OVERHEAD 2*DELIMITER_SIZE + sizeof(int)

#ifdef __cplusplus
extern "C" {
#endif


typedef enum MsgState {MSG_BEFORE_START=0,MSG_BEFORE_LEN=1,MSG_DATA=2,MSG_BEFORE_END=3} MsgState;

typedef struct TransportListener {
    void (*onStateChanged)(void* container, int state);
    void (*onConnectionError)(void* container, const char* message);
    void (*onMessageReceived)(void* container, char* data, int len);
    void (*onFileProgress)(void* container, int id, long progress);
    void (*onTransferCompleted)(void* container, int transactionId, char* fileName, int errorCode);
} TransportListener;


typedef struct Transport {
    Socket socket;
    ReadBuffer readBuffer;
    SocketListener socketListener;
    TransportListener* listener;
    BearerInfo* bearerInfo;
    char *msgBuf;
    void *container;
} Transport;

void initTransport(Transport *transport);
void cleanUpTransport(Transport *transport);
void transSetBearerInfo(Transport * transort, BearerInfo* bearerInfo);
int transSend(Transport *transport, char* data, int len);
int transSendFile(Transport *transport, char* localPath);
void transStopListen(Transport *transport);
int transStartListen(Transport *transport);
void transRegisterListener(Transport *transport,TransportListener *listener,void *container);
void transUnregisterListener(Transport *transport);

#ifdef __cplusplus
}
#endif
#endif
