#ifndef _CIRCBUF_H_
#define _CIRCBUF_H_

#define CIRC_BUFFER_SIZE 256
#ifdef __cplusplus
extern "C" {
#endif


typedef struct ReadBuffer {

    char buffer[CIRC_BUFFER_SIZE];
    int readPos;
    int writePos;
    int len;

} ReadBuffer;

void initBuffer(ReadBuffer* readBuffer);
void cleanUpBuffer(ReadBuffer*);
int writeBuf(ReadBuffer*,char* buffer, int offset, int len);
int readBuf(ReadBuffer*,char* buffer, int offset, int len);
int peek(ReadBuffer*,char* buffer, int offset, int len);
int bytesAvailableForRead(ReadBuffer*);
int bytesAvailableForWrite(ReadBuffer*);
int isFull(ReadBuffer*);
int isEmpty(ReadBuffer*);
void reset(ReadBuffer*);
void printBuf(ReadBuffer*);

#ifdef __cplusplus
}
#endif
#endif
