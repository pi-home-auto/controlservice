#ifndef _SESSION_H_
#define _SESSION_H_

#include <Transport.h>
#ifdef __cplusplus
extern "C" {
#endif


typedef struct SessionListener {
    void (*onStateChanged)(void* container, int state);
    void (*onConnectionError)(void* container, const char* message);
    void (*onMessageReceived)(void* container, char* data, int len);
    void (*onFileProgress)(void* container, int id, long progress);
    void (*onTransferCompleted)(void* container, int transactionId, char* fileName, int errorCode);

} SessionListener;

typedef struct Session {

    Transport transport;
    TransportListener transportListener;
    SessionListener *listener;
    void *container;
    BearerInfo *bearerInfo;

} Session;

void initSession(Session * session);
void cleanUpSession(Session *session);
void setBearerInfo(Session *session, BearerInfo* bearerInfo);
int sessionSend(Session *session,const char * message,int len);
void sessionRegisterListener(Session* session, SessionListener *listener, void* container);
void sessionUnregisterListener(Session* session);
void sessionStopListen(Session*session);
int sessionStartListen(Session *session);

#ifdef __cplusplus
}
#endif
#endif
