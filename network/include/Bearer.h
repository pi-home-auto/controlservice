#ifndef _BEARER_H_
#define _BEARER_H_

#include <netdb.h>

#ifdef __cplusplus
extern "C" {
#endif



typedef enum BearerType {NONE = 0, LINUX_SOCKET = 1, SUBGHZ = 2} BearerType;


typedef struct LinSockBearer {
    struct sockaddr_in remoteAddress;
} LinSockBearer;

typedef struct SirfBearer {
    int radioparams;
} SirfBearer;

typedef struct BearerInfo {
    BearerType eBearerType;
    LinSockBearer linSockBearer;
    SirfBearer sirfBearer;
    int msgMaxLen;
} BearerInfo;

void fillLinSockBearerInfo(BearerInfo* BearerInfo, const char* ipaddress, int portNumber,int msgMaxLen);
int readBearerFromFile(BearerInfo *bearerInfo, const char *);
void writeBearerToFile(BearerInfo *bearerInfo, const char *);
//TODO void getBearerInfo(BearerInfo *BearerInfo);

#ifdef __cplusplus
}
#endif

#endif
