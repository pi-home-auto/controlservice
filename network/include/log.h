#ifndef _LOG_H_
#define _LOG_H_

#ifdef __cplusplus
extern "C" {
#endif

//#define LOG_SOCKET 1
#define LOG_TRANSPORT 1
//#define LOG_CIRCBUF 1
//#define LOG_TEST 1
//#define LOG_BUFFER 1
#define LOG_SESSION 1

#ifdef LOG_SOCKET
#define logSocket netlog
#else
#define logSocket nolog
#endif

#ifdef LOG_TRANSPORT
#define logTransport netlog
#else
#define logTransport //
#endif

#ifdef LOG_SESSION
#define logSession netlog
#else
#define logSession //
#endif

#ifdef LOG_CIRCBUF
#define logCircBuf netlog
#else
#define logCircBuf nolog
#endif

#ifdef LOG_TEST
#define logTest netlog
#else
#define logTest //
#endif

#define SUCCESS 0
#define ERROR -1

#define nolog(x...)  // x

#define netlog(x...)  fprintf(stderr, x)
//int netlog(const char *format, ...);

#ifdef __cplusplus
}
#endif
#endif
