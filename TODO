## TODO List
- Reorg the handler/device/driver paradym to include a transport layer. A Handler has the
  high level functions (set GPIO, play sound, move motor). A Device can implement the logic
  of the request and uses a Transport to communicate the request to the device.
  Transport subclasses are TCP, REST, minipeer, i2c, raw (passthru?), usb, serial, spi, virtual, etc.
  Transports can be chained (example: set gpio on different module's minipeer uses transports REST,
  minipeer and i2c. By REST I mean the internal json protocol of the controlservice.
  Note:  A single request could go through several transports, but at the end there is a "raw"
  transport that then needs to interface with the actual hardware. A virtual transport can
  be useful for testing, simulations and (future) playback of recorded events.
- service does not always fully shut down on controlservice -S 0
- Use i2cZip in I2cDriver
- Allow multi-character input to minipeer-arduino's 2 interrupt routines that
  handle i2c messages.
- Dispatcher should accept Event messages
- Universal device/object namespace (see Architecture.md for details)
- Remove ControlService::HandlerMap and use the ObjectDepot instead
- Stop logging REST requests so verbosely
- Ping always uses cookie 0 or 1.
- If base is not available then sub will not shutdown using controlservice -S 0.
- switch from base/sub controlservice to peer-to-peer.
- Peer Type and Peer Role need a clear definition. It looks like peer roles can be "base"
  and "sub" (at least in the sql db), but there's code checking other values such as "shutdown."
  Created a PeerType enum that will be used now (see peer.h).
- some json config can be eliminated by adding a few fields to the database
- do routing of REST requests at controlservice level instead of relying on node.js.
- Logger is still not thread-safe
- Running unit tests overwrites the audit log file. Logger opens the log file during construction
  and csaudit (and others) are statically constructed at program startup. Thus, you can unset
  HA_LOG_DIR before running unit tests as a workaround.

- Pinger should also send event when peer becomes reachable again.
- More events should be used everywhere
- Add a lot more stats, such as Evaluator (events processed vs total), module stats (when connected,
  number of requests in/out, etc.
- Get rid of network directory. NotificationHandler uses it, but it could be rewritten to use sockstr
  or just remove NotifcatioHandler too.
- Add a thread-safe Updater class
- Add ActionHandler that passes json to the Dispatcher (as if a handler was called from a peer).
  Will have to create a LocalPeer that represents the current module (self).
- Should Actions have methods cancel, stop, pause?

- Classifier needs error diagnostics when the user enters an incorrect rulebase. Currently, it
  just ignores the error but leaves the block empty within the tree.
- Should be able to modify some of a rule's variables during runtime. The variables can be kept in that
  rule's context. Also need to make an API and REST interface that allows reading and changing the
  variable.
How does Evaluator handle EventMulti?
+ The rule has to be saved when a part of it is true.
- Not needed since the same functionality can be implemented using multiple rules,
  each triggering on different events. The rules can set variables, check if the
  other rule has set its variable and send a final (triggering) event when conditions
  are met.

- Improve Forker: open pipes to stdin/out/err for each child. Have a thread to poll the output
  pipes and store in the ForkerEntry for that pid. So need to map from pid -> ForkerEntry -> fd's.
  Also, when select() reads data on an fd, need to map fd -> ForkerEntry.

- Migrate to "driver/device/handler" metaphor. The driver takes care of low level details,
  the device presents a common posix-like interface and the handler processes remote
  requests (json) and other high level logic.
  The Driver is stateless and has a minimal common interface. Each method takes a Handle
  that holds all state for an instance of hardware.  The gpio subsystem is already migrated.
  Classes of drivers: bus, stream, storage, state/switch
  Examples:
  - StepMotorDevice uses 4 GpioDrivers
  - DistanceDevice::configure calls DistSensorDriver::setUnits to configure units
    DistanceDevice::read then calls DistSensorDriver::read to get then data and convert
    it as appropriate to reader.
  - ServoDevice use GpioDriver to setup pwm

- MonitorHandler to allow multiple locations to be monitored.
- Handle requests to the gpio handler on a separate thread, so the caller is
  not blocked. Retain the LEDs setting and return to that when no program,
  blink or other request is active.

- Allow messages to have multiple structs: encrypt, auth, forward, 
  switch, stats, traffic, control, etc.

- TransientPeer (subclass of Peer).  A peer that only connects periodically.  Useful for
  low power devices such as sensor modules.
  - when connected, controlservice issues a connection-cookie.  When a listener gets a
    connection it checks the connection-cookie and passes stream to existing Peer (instead
    of creating a new Peer.
  - need to keep polling intervals, i.e., send alert if it hasn't connected in a while
  - need a queue of messages destined for Peer.  Feed to Peer when it connects.
  - isConnected, lastConnectionTime, lastConnectedDuration

- Alert, notification mechanism: when handler detects event, what to do?

- On exit, send SIGINT to peer threads.  PeerThreader needs to check for error==EINTR
  and exit.

- Add cslogaudit pseudo-stream to write to both cslog and csaudit  
   or-  
  Have cslog call csaudit, depending on a logAuditLevel variable

- Write more handlers
- Implement traffic control that queues requests
  - initially Green means go, Red means queue
  - yellow can be speed limit 
- System::getError() helper
- Forker needs a number of improvements:
  + argv[1] = "(forker)"
  + set up PATH, other env variables (maybe from json config)
  + do sanity checks, including file existence before calling exec(). Check return codes, etc. and try to
    diagnose why an exec() failure occurred.
- Fix the occasional ForkerInput test failure. I think the right approach is to have forker catch the child's
  exit/terminate signals and capture the return values from there. Currently, a thread is waiting for the child to
  exit and trying to grab its output before it completely disappears (race condition).  Below is the output from the gtest:

	  [ RUN      ] CtlsvcTesting.ForkerInputTest
	  read buf=tests/simplequery 1 2 3
	  Launcher got spawn command, controlIO=true
	  Got response: 1838

	  read buf=1838
	  Launcher got ReadOut command
	  read buf=1838
	  Launcher got ReadError command
	  read buf=1838 ForkerInputTest

	  Launcher got Write command
	  read buf=1838
	  Launcher got ReadOut command
	  read buf=1838
	  Launcher got ReadError command
	  t_ctlsvclib.cpp:156: Failure
	  Expected equality of these values:
	  "Hello, ForkerInputTest!  My name is simplequery.\n"
	  sout
	  Which is: ""

	  Launcher got exit command
	  [  FAILED  ] CtlsvcTesting.ForkerInputTest (2009 ms)
