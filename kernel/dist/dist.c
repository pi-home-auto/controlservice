/*
 * dist - Driver for ultrasonic distance sensor
 *
 * This implements specific behavior of the Parallax 28015 PING))) Sensor
 * Most distance sensors work in a similar manner: a pulse is sent to the
 * sensor, which causes the speaker to emit a series of ultrasonic "chirps".
 * Then the sensor starts sending a pulse and stops the pulse when an echo
 * that matches the "chirps" is received.
 * Thus the distance is calculated using the pulse length:
 *   dist = pulse_usecs * k  (where k is some constant)
 *
 * Specifics for the PING:
 * - uses one GPIO pin for both receiving the initial pulse and sending the echo pulse
 * - dist_inches = pulse_usecs * 6752
 * - dist_cm = pulse_usecs * 17150
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>

#include <linux/delay.h>
#include <linux/device.h>
#include <linux/fs.h>
#include <linux/gpio.h>
#include <linux/interrupt.h>
#include <linux/ktime.h>
#include <linux/math64.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/uaccess.h>

#define DEVICE_NAME "dist"
#define CLASS_NAME "dist"

static unsigned int pinOut = 15;  // TODO allow user to configure, support multiple sensors
static unsigned int pinIn = 15;
static unsigned int irqno;
static irq_handler_t pininput_handler(unsigned int irq, void *dev_id,
                                      struct pt_regs *regs);

static int majorNumber;
static struct class* distClass = NULL;
static struct device* distDevice = NULL;

static ktime_t riseTime;
static ktime_t fallTime;

static wait_queue_head_t wait_for_pulse;
static int pulse_received = 2;

// Device "file" operations
static int device_open(struct inode *inodep, struct file *filep);
static int device_release(struct inode *inodep, struct file *filep);
static ssize_t device_write(struct file *filep, const char *buffer,
                            size_t length, loff_t * offset);
static ssize_t device_read(struct file *filep,  char *buffer,
                           size_t length, loff_t * offset);

static struct file_operations fops = {
    .owner   = THIS_MODULE,
    .open    = device_open,
    .release = device_release,
    .read    = device_read,
    .write   = device_write
};

static char *mes;
static size_t meslen = 0;

static DEFINE_MUTEX(dist_mutex);

enum Units {
    Usecond, Cm, Mm, Meter, Inch, Foot
};
static enum Units units = Usecond;
static uint32_t unitK = 6752;
static uint32_t unitPow = 6;

static int __init dist_init(void)
{
  int irqreqresult = 0;
  printk(KERN_INFO "DIST: init\n");

  if (!gpio_is_valid(pinOut)) {
      printk(KERN_INFO "DIST: invalid pinOut\n");
      return -ENODEV;
  }

  irqno = gpio_to_irq(pinIn);
  printk(KERN_INFO "DIST: irq no: %d\n", irqno);
  irqreqresult = request_irq(irqno, (irq_handler_t)pininput_handler,
                             IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING, "dist_pin_handler", NULL);
  // dist_pin_handler will appear as owner identity in /proc/interrupts
  printk(KERN_INFO "DIST: irq result: %d\n", irqreqresult);

  gpio_request(pinOut, "sysfs");
  gpio_direction_output(pinOut, 0); // set dir out and set it to 0
  gpio_export(pinOut, false); // export to appear as /sys/class/gpio/gpio#
  // false to disable changing the direction

  if (!gpio_is_valid(pinIn)) {
      printk(KERN_INFO "DIST: invalid pinIn\n");
      return -ENODEV;
  }
  if (pinIn != pinOut) {
      gpio_request(pinIn, "sysfs");
      gpio_direction_input(pinIn); // set dir input
      gpio_export(pinIn,false); // export to appear as /sys/class/gpio/gpio#
  }
  mutex_init(&dist_mutex);
  init_waitqueue_head(&wait_for_pulse);

  // memory allocation
  mes = kmalloc(sizeof(char)*128, GFP_KERNEL);

  // register char device
  majorNumber = register_chrdev(0, DEVICE_NAME, &fops);
  if (majorNumber < 0) {
    printk(KERN_ALERT "DIST: registering chrdev failed\n");
    return majorNumber;
  }

  // create device class
  distClass = class_create(THIS_MODULE, CLASS_NAME);
  if (IS_ERR(distClass)) {
    unregister_chrdev(majorNumber, DEVICE_NAME);
    printk(KERN_ALERT "DIST: creating device class failed\n");
    return PTR_ERR(distClass);
  }

  // create device
  distDevice = device_create(distClass, NULL, MKDEV(majorNumber,0), NULL, DEVICE_NAME);
  if (IS_ERR(distDevice)) {
    class_destroy(distClass);
    unregister_chrdev(majorNumber, DEVICE_NAME);
    printk(KERN_ALERT "DIST: creating device failed\n");
    return PTR_ERR(distDevice);
  }
  printk(KERN_INFO "DIST: device created\n");

  return 0;
}

static void __exit dist_exit(void)
{
  gpio_set_value(pinOut,0);
  gpio_unexport(pinOut);
  gpio_free(pinOut);

  if (pinIn != pinOut) {
      gpio_unexport(pinIn);
      gpio_free(pinIn);
  }
  free_irq(irqno, NULL);

  mutex_destroy(&dist_mutex);
  device_destroy(distClass,MKDEV(majorNumber,0));
  class_unregister(distClass);
  class_destroy(distClass);
  unregister_chrdev(majorNumber,DEVICE_NAME);
  // free memory
  kfree(mes);

  printk(KERN_INFO "DIST: exited\n");
}

module_init(dist_init);
module_exit(dist_exit);

static irq_handler_t pininput_handler(unsigned int irq, void *dev_id, struct pt_regs *regs)
{
    int val = gpio_get_value(pinIn);
    printk(KERN_INFO "DIST: pin input: %d\n",val);
    if (pulse_received > 1) {
        return (irq_handler_t)IRQ_NONE;
    }
    if (val && pulse_received == 0) {
        riseTime = ktime_get_ns();
        pulse_received = 1;
    } else if (!val && pulse_received == 1) {
        fallTime = ktime_get_ns();
        pulse_received = 2;
        // wake suspended read
        wake_up_interruptible(&wait_for_pulse);
    }
    return (irq_handler_t)IRQ_HANDLED;
}

static int device_open(struct inode *inodep, struct file *filep)
{
    if(!mutex_trylock(&dist_mutex)) {
        printk(KERN_ALERT "DIST: device is busy\n");
        return -EBUSY;
    }
    printk(KERN_INFO "DIST: device opened\n");
    return 0;
}

static int device_release(struct inode *inodep, struct file *filep)
{
    mutex_unlock(&dist_mutex);
    printk(KERN_INFO "DIST: device closed\n");
    return 0;
}

static ssize_t device_write(struct file *filep, const char *buffer,
                            size_t length, loff_t * offset)
{
    size_t bytesToCopy = length >= 255 ? 255: length;
    size_t bytesNotCopied = 0;
    bytesNotCopied = copy_from_user(mes, buffer, bytesToCopy);
    meslen = bytesToCopy - bytesNotCopied;
    printk(KERN_INFO "DIST: received %zu bytes\n",meslen);
    if(bytesNotCopied){
      printk(KERN_INFO "DIST: Failed to receive %zu characters",bytesNotCopied);
      return -EFAULT;
    }
    // us, cm, mm, m, in, ft === Usecond, Cm, Mm, M, Inch, Foot
    // 1, 0.017150, 0.17150, 0.0001750, 0.006752, 0.081024
    units = Usecond;
    unitK = 1;
    if (!strncmp(mes, "cm", 2)) {
        units = Cm;
        unitK = 1715;  // 0.01715;
        unitPow = 5;
    } else if (!strncmp(mes, "mm", 2)) {
        units = Mm;
        unitK = 1715;  // 0.1715
        unitPow = 4;
    } else if (!strncmp(mes, "m", 1)) {
        units = Meter;
        unitK = 1715;  // 0.0001715;
        unitPow = 7;
    } else if (!strncmp(mes, "in", 2)) {
        units = Inch;
        unitK = 6752;  // 0.006752;
        unitPow = 6;
    } else if (!strncmp(mes, "ft", 2)) {
        units = Foot;
        unitK = 81024;  // 0.081024;
        unitPow = 6;
    }
    return bytesToCopy;
}

static ssize_t device_read(struct file *filep,  char *buffer,
                           size_t length, loff_t * offset)
{
    size_t mes_size;
    size_t bytesNotCopied = 0;
    long timeout;
    uint64_t delta = 0U;
    if ((ktime_get_ns() - fallTime) < 200000000) {
        return 0;  // too soon to pulse the sensor again
    }
    pulse_received = 0;
    gpio_direction_output(pinOut, 1); // set dir out and set it 1 (high)
    udelay(4);
    gpio_set_value(pinOut, 0);
    gpio_direction_input(pinIn);
    timeout = wait_event_interruptible_timeout(wait_for_pulse, pulse_received, HZ / 2);
    if (timeout == 0) {
        printk(KERN_INFO "DIST: timed out waiting for interrupt\n");
        return -ETIMEDOUT;
    } else if (timeout < 0) {
        return timeout;
    }
    delta = div_u64((fallTime - riseTime), 1000);
    if (units == Usecond) {
        mes_size = sprintf(mes, "%llu\n", delta);
    } else {
        uint32_t remain;
        uint64_t res = unitK * delta;
        uint32_t unitDiv = int_pow(10, unitPow);
        uint64_t ires = div_u64_rem(res, unitDiv, &remain);
        char fmt[22];
        sprintf(fmt, "%%llu,%%0%uu\n", unitPow);
        mes_size = sprintf(mes, fmt, ires, remain);
        //printk(KERN_INFO "DIST: result=%llu ires=%llu rem=%u\n", res, ires, remain);
    }
    bytesNotCopied = copy_to_user(buffer, mes, mes_size);
    if(bytesNotCopied) {
        printk(KERN_INFO "DIST: error in sending %d bytes\n",bytesNotCopied);
        return -EFAULT;
    }
    printk(KERN_INFO "DIST: ToF = %llu\n", delta);
    return mes_size;
}

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Andy Warner");
MODULE_DESCRIPTION("Linux driver for distance sensor");
MODULE_VERSION("1.0");
