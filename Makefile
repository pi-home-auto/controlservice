#
#   Copyright 2020-2023 Pi Home Automation https://gitlab/pi-home-auto
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   Project: controlservice
#   Author:  Andy Warner
#
#   Top level makefile for control service
#
# This is the one definition of the top level directory. Everything else
# is within this directory or subdirectory such as bin, data, etc, nodejs, doc, ...
include common.mk

# ARMPREFIX=arm-linux-gnueabihf
ARMPREFIX=$(CCPREFIX)
ARMAR=$(ARMPREFIX)ar
ARMCC=$(ARMPREFIX)g++

# RADIOLIB = -L$(RPI_IMAGE_LIB) -lradiolib
# RADIO_CCFLAGS = -I$(RPI_IMAGE_INC) -DUSE_RADIOLIB -D_BSD_SOURCE

SUBDIRS = src tests

PROGRAMS =

all: setup network $(PROGRAMS) subdirs

install:
	make -C src install

## TODO rework for new directory structure
image: arm
	mkdir -p $(RPI_IMAGE)/home/pi/bin $(RPI_IMAGE)/home/pi/logs
	cp -p src/controlservice blink_start ctrlstart $(RPI_IMAGE)/home/pi/bin/
	mkdir -p $(RPI_IMAGE)/etc/homeauto
	cp -p commonconfig.json *-config.json $(RPI_IMAGE)/etc/homeauto/
	mkdir -p $(RPI_IMAGE)/etc/init.d
	cp -p etcinit/controlservice $(RPI_IMAGE)/etc/init.d/controlservice
	cp -p etcinit/homeautostart $(RPI_IMAGE)/etc/init.d/
	@echo "Homeauto controlservice installed in" $(RPI_IMAGE)

.PHONY: doc
doc:
	make -C doc

subdirs:
	+for dir in $(SUBDIRS); do \
		make -C $$dir; \
	done


arm:
	for dir in $(SUBDIRS); do \
		make -C $$dir CC=$(ARMCC) AR=$(ARMAR) RANLIB=$(RPI_RANLIB) \
                     EXTRA_CCFLAGS="$(RADIO_CCFLAGS)" EXTRA_LDLIBS="$(RADIOLIB)" ; \
	done

.PHONY: clean
clean:
	for dir in $(SUBDIRS); do \
		make -C $$dir clean; \
	done
	rm -f lib/libnetwork.a .setup-complete
	if [ -d network ]; then \
		make -C network clean; \
	fi

.PHONY: setup
setup: .setup-complete
.setup-complete:
	mkdir -p lib include
	touch .setup-complete

network: setup
	make -C network && \
	cp -p network/src/libnetwork.a lib/

.PHONY: depends
depends:
	for dir in $(SUBDIRS); do \
		make -C $$dir depends; \
	done
