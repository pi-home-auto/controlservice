#!/bin/bash
#

function Usage {
    cat <<EOF
Usage: testdist.sh [ rwst ] <parameters>
  r            Read distance 
  w <units>    Write units of measure (m, mm, cm, in, ft)
  s            Start streaming
  t            Stop streaming

EOF
    exit 2
}

[ $# -gt 0 ] || Usage

if [ $1 = r ]; then
    PIN=0
    CMD1="{\"control\":{\"handler\":\"distance\",\"method\":\"readkey\",\"param1\": $PIN }}"
elif [ $1 = w ]; then
    [ $# -eq 2 ] || Usage
    UNITS=$2
    CMD1="{\"control\":{\"handler\":\"distance\",\"method\":\"writekey\",\"sensor\": 0, \"param1\": \"$UNITS\" }}"
elif [ $1 = s ]; then
    CMD1="{\"control\":{\"handler\":\"distance\",\"method\":\"startstreaming\",\"sensor\": 0 }}"
elif [ $1 = t ]; then
    CMD1="{\"control\":{\"handler\":\"distance\",\"method\":\"stopstreaming\",\"sensor\": 0 }}"
else
    Usage
fi

socat - TCP:localhost:1800 <<EOF
{ "greeting": {
    "version": "0.1.3",
    "name": "tester",
    "role": "node",
    "mainGroup": "Train",
    "type": "rest" }
}


$CMD1


EOF
echo Done
