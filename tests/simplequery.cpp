
#include <iostream>
#include <unistd.h>
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

int main(int argc, char* argv[]) {
    cout << "Program arguments = " << argc << endl;
    cout << "Input your name: " << std::flush;
    std::string name;
    cin >> name;
    cout << "Hello, " << name << "!  My name is simplequery." << endl;
    cerr << "There is no error" << endl;
    sleep(2);
    return 0;
}
