//
//   Copyright 2021-2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "do_once.h"
#include "forker.h"
#include "logger.h"
#include "program.h"
#include "properties.h"
#include "sequencer.h"
#include "statistics.h"
#include "string_tools.h"
#include <gtest/gtest.h>
#include <iostream>
#include <sstream>
#include <thread>
#include <vector>

using ctlsvc::cslog;
using std::endl;

std::string runpath;

namespace ctlsvc {

class NumStep : public Step {
public:
    NumStep(int num, std::ostream& os)
        : num_(num)
        , output_(os) {}
    int getNum() const {
        return num_;
    }
    bool run() override {
        //cslog << loglevel(Info) << "Num: " << num_ << "\n";
        output_ << "Num: " << num_ << endl;
        return true;
    }
    bool set(const std::string& params) override {
        num_ = std::stoi(params);
        return true;
    }
    std::string results() const override {
        return std::to_string(num_);
    }
private:
    int num_;
    std::ostream& output_;
};

class AbcStep : public Step {
public:
    AbcStep(char ch, std::ostream& os)
        : char_(ch)
        , output_(os) {}
    char getChar() const {
        return char_;
    }
    bool run() override {
        output_ << "Char: " << char_ << std::endl;
        return true;
    }
    bool set(const std::string& params) override {
        char_ = params.front();
        return true;
    }
    std::string results() const override {
        return std::to_string(char_);
    }
private:
    char char_;
    std::ostream& output_;
};

}  // namespace ctlsvc

namespace {

class CtlsvcTesting : public ::testing::Test {
public:
    static constexpr const char* kFullSequence = "Num: 1\nNum: 2\nNum: 3\nNum: 1\nNum: 2\nNum: 3\n"
            "Char: A\nChar: B\nChar: C\n";
    static constexpr const char* kAbcProgName = "char program";
    static constexpr const char* kNumProgName = "num program";

protected:
    void SetUp() override {}
    void TearDown() override {}
    void clear() {
        oss.str("");
    }
    ctlsvc::Step* genStep(int num) {
        return new ctlsvc::NumStep(num, oss);
    }
    void genSequence() {
        using ctlsvc::AbcStep;
        using ctlsvc::NumStep;
        EXPECT_FALSE(seq.isRunning());
        auto numprog = std::make_unique<ctlsvc::Program>(std::string(kNumProgName));
        EXPECT_EQ(kNumProgName, numprog->name());
        EXPECT_TRUE(numprog->addStep(genStep(1)));
        EXPECT_TRUE(numprog->addStep(genStep(2)));
        EXPECT_TRUE(numprog->addStep(genStep(3)));
        auto abcprog = std::make_unique<ctlsvc::Program>(std::string(kAbcProgName));
        EXPECT_TRUE(abcprog->addStep(new AbcStep('A', oss)));
        EXPECT_TRUE(abcprog->addStep(new AbcStep('B', oss)));
        EXPECT_TRUE(abcprog->addStep(new AbcStep('C', oss)));

        EXPECT_TRUE(seq.addProgram(std::move(abcprog), 0, 1));
        EXPECT_TRUE(seq.addProgram(std::move(numprog), 1, 2));
    }

    std::ostringstream oss;
    ctlsvc::Sequencer seq;
};

TEST_F(CtlsvcTesting, ForkerSleepTest) {
    ctlsvc::Forker forker;
    EXPECT_TRUE(forker.init());
    auto handle = forker.spawn("sleep 100");
    EXPECT_TRUE(forker.terminate(handle));
    forker.uninit();
}

TEST_F(CtlsvcTesting, ForkerInputTest) {
    ctlsvc::Forker forker;
    std::string prog = runpath + "simplequery";
    EXPECT_TRUE(forker.init());
    auto handle = forker.spawn(prog, { "1", "2", "3" }, true);
    EXPECT_EQ(0, sleep(1));
    std::string sout;
    std::string serr;
    EXPECT_TRUE(forker.readFrom(handle, sout, serr));
    ctlsvc::StringArray lines;
    ASSERT_EQ(2, ctlsvc::StringTools::splitString(sout, lines, '\n'));
    EXPECT_EQ("Program arguments = 4", lines[0]);
    EXPECT_EQ("Input your name: ", lines[1]);
    EXPECT_TRUE(serr.empty());
    sleep(1);
    EXPECT_TRUE(forker.writeTo(handle, "ForkerInputTest", true));
    EXPECT_TRUE(forker.readFrom(handle, sout, serr));
    EXPECT_EQ("Hello, ForkerInputTest!  My name is simplequery.\n", sout);
    EXPECT_EQ("There is no error\n", serr);

    forker.uninit();
}

TEST_F(CtlsvcTesting, ProgramTest) {
    using ctlsvc::NumStep;
    constexpr const char* kProgName = "test program";
    ctlsvc::Program prog(kProgName);
    EXPECT_EQ(kProgName, prog.name());
    EXPECT_TRUE(prog.addStep(genStep(1)));
    std::vector<ctlsvc::Step *> ns;
    ns.push_back(genStep(2));
    ns.push_back(genStep(3));
    EXPECT_TRUE(prog.addSteps(ns));
    ctlsvc::Program::iterator it = prog.begin();
    EXPECT_EQ(prog.begin(), it);
    EXPECT_NE(prog.end(), it);
    EXPECT_EQ(3, prog.size());
    // Run the program
    it = prog.execute(prog.begin());
    EXPECT_EQ(prog.end(), it);
    EXPECT_EQ("Num: 1\nNum: 2\nNum: 3\n", oss.str());
    clear();
    // Single step program
    it = prog.begin();
    it = prog.execute(it, true);
    EXPECT_NE(prog.begin(), it);
    EXPECT_NE(prog.end(), it);
    EXPECT_EQ("Num: 1\n", oss.str());
    clear();
    it = prog.execute(it, true);
    EXPECT_NE(prog.begin(), it);
    EXPECT_NE(prog.end(), it);
    EXPECT_EQ("Num: 2\n", oss.str());

    prog.clear();
    EXPECT_EQ(0, prog.size());
}

TEST_F(CtlsvcTesting, SequencerTest) {
    genSequence();
    const auto& num_se = seq.getProgram(kNumProgName);
    EXPECT_EQ(1, num_se.level);
    EXPECT_EQ(2, num_se.repeat);
    EXPECT_EQ(kNumProgName, num_se.program->name());

    EXPECT_TRUE(seq.run(false, true));
    EXPECT_EQ(kFullSequence, oss.str());
}

TEST_F(CtlsvcTesting, SequencerEmptyThreadedTest) {
    EXPECT_TRUE(seq.run(true, true));
    std::this_thread::sleep_for(std::chrono::milliseconds(40));
    seq.stop();
    while (seq.isRunning()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }
    EXPECT_TRUE(oss.str().empty());
    {
        ctlsvc::Sequencer sq2;
        EXPECT_TRUE(sq2.run(true, false));
        std::this_thread::sleep_for(std::chrono::milliseconds(40));
        sq2.stop();
        while (sq2.isRunning()) {
            std::this_thread::sleep_for(std::chrono::milliseconds(40));
        }
        EXPECT_TRUE(oss.str().empty());
    }
}

TEST_F(CtlsvcTesting, SequencerOnceThreadedTest) {
    genSequence();
    EXPECT_TRUE(seq.run(true, true));
    std::this_thread::sleep_for(std::chrono::milliseconds(40));
    seq.stop();
    while (seq.isRunning()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(40));
    }
    EXPECT_EQ(kFullSequence, oss.str());
}

TEST_F(CtlsvcTesting, SequencerLoopThreadedTest) {
    genSequence();

    cslog.setDefaultLogLevel(ctlsvc::Info);  // Silence the noisy sequencer debugging
    EXPECT_TRUE(seq.run(true, false));
    std::this_thread::sleep_for(std::chrono::milliseconds(5));
    seq.stop(true);
    while (seq.isRunning()) {
        std::this_thread::sleep_for(std::chrono::milliseconds(20));
    }
    cslog.setDefaultLogLevel(ctlsvc::Debug);
    std::cout << "Buffer size: " << oss.str().size() << std::endl;
}

TEST_F(CtlsvcTesting, LoggerTest) {
    using ctlsvc::loglevel;

    cslog.getNullStream() << "Output to null stream -- SHOULD NOT SEE THIS OUTPUT" << endl;
    cslog << loglevel(ctlsvc::Debug) << "Test logging - this is first message." << endl;
    cslog.setDefaultLogLevel(ctlsvc::Info);
    cslog << loglevel(ctlsvc::Debug) << "This message should NOT appear." << endl;
    cslog << loglevel(ctlsvc::Info) << "Test logging - this is second message." << endl;
    cslog.setDefaultLogLevel(ctlsvc::Debug);
    cslog << loglevel(ctlsvc::Debug) << "Test logging - this is third (and final) message." << endl;
}

TEST_F(CtlsvcTesting, PropertiesTest) {
    ctlsvc::Properties props;
    auto value = props.get("key");
    EXPECT_EQ(0, value.index());
    EXPECT_TRUE(std::holds_alternative<std::monostate>(value));

    EXPECT_TRUE(props.set("key", std::string("Hello")));
    EXPECT_TRUE(props.set("num", 1234));
    value = props.get("key");
    EXPECT_EQ(4, value.index());
    EXPECT_TRUE(std::holds_alternative<std::string>(value));
    EXPECT_EQ("Hello", std::get<std::string>(value));
    value = props.get("num");
    EXPECT_EQ(2, value.index());
    EXPECT_TRUE(std::holds_alternative<int>(value));
    EXPECT_EQ(1234, std::get<int>(value));

    auto names = props.propertyNames();
    EXPECT_EQ(2, names.size());
    std::sort(names.begin(), names.end());
    EXPECT_EQ("key", names[0]);
    EXPECT_EQ("num", names[1]);
}

TEST_F(CtlsvcTesting, StatisticsTest) {
    ctlsvc::Statistics stats("Test stats");
    EXPECT_EQ("Test stats", stats.getName());
    stats.newStat("count", 0);
    EXPECT_EQ(0, stats.getInt("count"));
    stats.increment("count");
    stats.increment("count");
    EXPECT_EQ(2, stats.getInt("count"));
    stats.addTo("count", 8);
    EXPECT_EQ(10, stats.getInt("count"));

    stats.newStat("num", 0.f);
    EXPECT_EQ(0.f, stats.getFloat("num"));
    stats.increment("num");
    stats.increment("num");
    stats.increment("num");
    EXPECT_EQ(3.0f, stats.getFloat("num"));
    stats.addTo("num", 0.141567f);
    EXPECT_EQ(3.141567f, stats.getFloat("num"));
}

TEST_F(CtlsvcTesting, DoOnceTest) {
  using ctlsvc::loglevel;

  int how_many = 0;
  auto do_something = [&how_many](){
    ++how_many;
    cslog << loglevel(ctlsvc::Info)
          << " This Message Was Logged " << how_many << " times in a row." << endl;
  };
  ctlsvc::DoOnce doer(do_something);
  doer.run();
  EXPECT_EQ(1, how_many);
  EXPECT_EQ(0, doer.suppressed());
  doer.run();
  EXPECT_EQ(1, how_many);
  EXPECT_EQ(1, doer.suppressed());
  doer.run();
  doer.run();
  doer.run();
  EXPECT_EQ(1, how_many);
  EXPECT_EQ(4, doer.suppressed());
  how_many = 0;
  doer.run();
  EXPECT_EQ(0, how_many);
  EXPECT_EQ(5, doer.suppressed());
  doer.reset();
  doer.run();
  EXPECT_EQ(1, how_many);
  EXPECT_EQ(0, doer.suppressed());
  how_many = 0;
  EXPECT_TRUE(doer.reset(do_something));
  EXPECT_EQ(1, how_many);
  EXPECT_FALSE(doer.reset(do_something));
  EXPECT_EQ(1, how_many);
  doer.run();
  EXPECT_EQ(2, how_many);
  doer.run();
  EXPECT_EQ(2, how_many);
}

}  // namespace

int main(int argc, char* argv[]) {
    runpath = argv[0];
    auto pos = runpath.rfind('/');
    if (pos == std::string::npos) {
        std::cout << "No path given" << std::endl;
        runpath.clear();
    } else {
        runpath = runpath.substr(0, pos + 1);
        std::cout << "Program started at path: " << runpath << std::endl;
    }
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
