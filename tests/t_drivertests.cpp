//
//   Copyright 2022, 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "driver.h"
#include "drivers/i2cdriver.h"
#include "logger.h"
#include "minipeer.h"
#include <gtest/gtest.h>
#include <iostream>

using ctlsvc::driver::DriverBuffer;
using ctlsvc::cslog;
using std::cout;
using std::endl;

extern std::string runpath; // This is set from main()

namespace {

class DriverTesting : public ::testing::Test {
protected:
    void SetUp() override {}
    void TearDown() override {}

};

TEST_F(DriverTesting, DriverBuffer) {
    const std::string kSampleBuf("Hello, Buffer!\n");

    DriverBuffer emptyBuf;
    EXPECT_EQ(0, emptyBuf.size());
    EXPECT_EQ(nullptr, emptyBuf.data());

    std::byte byteBuf[] = { std::byte(5), std::byte(11) };
    DriverBuffer bbuf(byteBuf, 2);
    EXPECT_EQ(2, bbuf.size());
    EXPECT_EQ(byteBuf[0], bbuf.data()[0]);
    EXPECT_EQ(byteBuf[1], bbuf.data()[1]);

    std::vector<std::byte> byteVec;
    for (int i = 1; i <= 100; ++i) {
        byteVec.push_back(std::byte(i));
    }
    DriverBuffer vbuf(std::move(byteVec));
    EXPECT_EQ(100, vbuf.size());

    DriverBuffer buf(kSampleBuf);
    EXPECT_EQ(kSampleBuf.size(), buf.size());
    EXPECT_NE(nullptr, buf.data());
    EXPECT_EQ(static_cast<int>('B'), std::to_integer<int>(buf.data()[7]));

    DriverBuffer ibuf(sizeof(int));
    ibuf.writeInt(8675309);
    EXPECT_EQ(sizeof(int), ibuf.size());
    EXPECT_NE(nullptr, ibuf.data());
    // Compare first byte of buffer
    EXPECT_EQ(0xed, std::to_integer<int>(*ibuf.data()));
    // Compare to whole buffer, sizeof int
    EXPECT_EQ(8675309, *(int *)ibuf.data());
}

TEST_F(DriverTesting, I2cDriverTest) {
  using namespace ctlsvc::driver;
  using namespace ctlsvc;
  class TestControlService : public ControlService {
  public:
    TestControlService(const std::string& dbname) {
      database_.setDbFile(dbname);
      database_.init();
    }
    ~TestControlService() {
      database_.uninit();
    }
    static ControlService* instance() {
      if (instance_ == nullptr) {
        instance_ = new TestControlService(runpath + "test.db");
      }
      return instance_;
    }
  };
  auto tcs = TestControlService::instance();
  EXPECT_NE(nullptr, tcs);

  I2cDriver i2c;
  auto h = i2c.open("i2c.1.9", DevMode::ReadWrite);
  ASSERT_NE(nullptr, h);
  auto count = i2c.write(h, std::vector({ std::byte(mpsReadPos), std::byte(0) }));
  EXPECT_EQ(2, count);
  std::vector<std::byte> buf(2);
  DriverBuffer drb(std::move(buf));
  count = i2c.read(h, drb);
  EXPECT_EQ(2, count);
  EXPECT_EQ(2, buf.size());

  i2c.close(h);
  tcs->stop();
  delete tcs;
}

}  // namespace
