//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "database.h"
#include "logger.h"
#include "object_depot.h"
#include <gtest/gtest.h>
#include <iostream>

using ctlsvc::cslog;
using ctlsvc::Database;
using std::cout;
using std::endl;

extern std::string runpath; // This is set from main()

namespace {

class DatabaseTesting : public ::testing::Test {
protected:
  //TODO move testdb, open and close here
  void SetUp() override {
    testdb.setDbFile(runpath + "test.db");
    EXPECT_TRUE(testdb.init());
  }

  void TearDown() override {
    testdb.uninit();
  }

  Database testdb;
};

TEST_F(DatabaseTesting, ReadDbModule) {
  ctlsvc::DbModule module;
  EXPECT_TRUE(testdb.getModule("tunnel", module));
  EXPECT_EQ("tunnel", module.name());
  EXPECT_EQ("sub", module.role());
  EXPECT_EQ("http://tunnel:8888", module.url());
  EXPECT_EQ("(unknown)", module.state());
  EXPECT_EQ("localhost", module.controlUri());
  EXPECT_EQ(1800, module.controlPort());
}

TEST_F(DatabaseTesting, ReadDbDevice) {
  using namespace ctlsvc;
  ctlsvc::DbDevice device;
  EXPECT_TRUE(testdb.getDevice("T-LED-STATUS-1", device));
  EXPECT_EQ("T-LED-STATUS-1", device.uuid());
  EXPECT_EQ("Status LED", device.name());
  EXPECT_EQ(DbDataHolder::NullValueString, device.description());
  EXPECT_EQ("Sub module", device.location());
  EXPECT_EQ("Train", device.grouping());
  EXPECT_EQ("switch", device.type());
  EXPECT_EQ("enabled", device.state());
  EXPECT_EQ("18", device.addresses());
  EXPECT_EQ(DbDataHolder::NullValueString, device.parameters());
  EXPECT_EQ(DbDataHolder::NullValueString, device.gps());
  EXPECT_EQ("tunnel", device.module_name());
  EXPECT_EQ(DbDataHolder::NullValueString, device.purpose());
  EXPECT_EQ(0, device.seq_order());
  EXPECT_EQ("gpio", device.ctlsvc_handler());
}

TEST_F(DatabaseTesting, ReadDbModuleList) {
  auto modules = testdb.modules();
  EXPECT_EQ(2, modules.size());
}

TEST_F(DatabaseTesting, ReadAllDbDevices) {
  using namespace ctlsvc;
  Database::DbDeviceList devices;
  auto num = testdb.devices("", devices);
  EXPECT_EQ(29, num);
  EXPECT_EQ(29, devices.size());
}

TEST_F(DatabaseTesting, ReadPwmDbDevices) {
  using namespace ctlsvc;
  Database::DbDeviceList devices;
  auto num = testdb.devices("pwm", devices);
  EXPECT_EQ(2, num);
  EXPECT_EQ(2, devices.size());
}

TEST_F(DatabaseTesting, ReadModuleDbDevices) {
  using namespace ctlsvc;
  Database::DbDeviceList devices;
  auto num = testdb.moduleDevices("tunnel", "", devices);
  EXPECT_EQ(19, num);
  EXPECT_EQ(19, devices.size());

  devices.clear();
  num = testdb.moduleDevices("tunnel", "camera", devices);
  EXPECT_EQ(1, num);
  EXPECT_EQ(1, devices.size());

  const auto& cam = devices.front();
  EXPECT_EQ("T-CAM-123-1", cam.uuid());
  EXPECT_EQ("Tunnel Camera", cam.name());
  EXPECT_EQ(DbDataHolder::NullValueString, cam.description());
  EXPECT_EQ("Train Room", cam.location());
  EXPECT_EQ("Train", cam.grouping());
  EXPECT_EQ("camera", cam.type());
  EXPECT_EQ("enabled", cam.state());
  EXPECT_EQ("0", cam.addresses());
  EXPECT_EQ("http://@HOST@:8088/stream_simple.html,rtsp://@HOST@:8000/pi.sdp", cam.parameters());
  EXPECT_EQ("37.3883852,-121.9367605,16z", cam.gps());
//  EXPECT_TRUE(cam.appliance_uuid().empty());
  EXPECT_EQ("tunnel", cam.module_name());
  EXPECT_EQ(DbDataHolder::NullValueString, cam.purpose());
  EXPECT_EQ(-1, cam.seq_order());
  /*
T-CAM-123-1|Tunnel Camera||Train Room|Train|camera|enabled|0|http://@HOST@:8088/stream_simple.html,rtsp://@HOST@:8000/pi.sdp|37.3883852,-121.9367605,16z||tunnel||
sqlite> .schema device
CREATE TABLE device (uuid TEXT, name TEXT, description TEXT, location TEXT,
	grouping TEXT, type TEXT, state TEXT, addresses TEXT, parameters TEXT,
	gps TEXT, appliance_uuid TEXT, module_name TEXT, purpose TEXT, seq_order INTEGER,
  */
}

}  // namespace
