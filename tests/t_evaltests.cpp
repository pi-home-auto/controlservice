//
//   Copyright 2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "eval/actions.h"
#include "eval/context.h"
#include "eval/eventfactory.h"
#include "eval/events.h"
#include "eval/evaluator.h"
#include "logger.h"
#include <gtest/gtest.h>
#include <iostream>
#include <thread>

using namespace ctlsvc::eval;
using ctlsvc::cslog;
using ctlsvc::Properties;
using std::cout;
using std::endl;

namespace {

class EvalTesting : public ::testing::Test {
protected:
    void SetUp() override {}
    void TearDown() override {}

};

TEST_F(EvalTesting, EvalVarTest) {
    Evaluator eval;
    const std::string TestString("Just testing.");
    const std::string TestString2("another value");
    EXPECT_TRUE(eval.setVar("FirstVariable", TestString));
    EXPECT_TRUE(eval.setVar("myStr", TestString2));
    EXPECT_EQ(eval.getVar("FirstVariable"), TestString);
    EXPECT_EQ(eval.getVar("myStr"), TestString2);
    EXPECT_EQ(eval.getVar("novar"), "");

    EXPECT_FALSE(eval.removeVar("whatVar"));
    EXPECT_TRUE(eval.removeVar("myStr"));
    EXPECT_EQ(eval.getVar("myStr"), "");
}

TEST_F(EvalTesting, SimpleEvalTest) {
    auto a = std::make_shared<Action>("action");
    EXPECT_TRUE(EventFactory::instance()->defineEvent("event", "label"));
    auto e = EventFactory::instance()->makeEvent("event");
    auto evOther = EventFactory::instance()->makeEvent("event");
    Evaluator eval;
    Rule rule(e, a, [](const Rule::EventPtr){ return true; });
    Context context(&eval);
    EXPECT_TRUE(a->run(&context));
    EXPECT_EQ(*e, *evOther);  // events with the same name are treated as equal
    EXPECT_TRUE(rule.matches(e));
    EXPECT_TRUE(rule.getAction()->run(&context));
    EXPECT_TRUE(eval.addRule(rule));
    EXPECT_TRUE(eval.evaluateRule(e, rule.name()));
    EXPECT_TRUE(eval.push(e));
    cout << "Rule name: " << rule.description() << endl;
    rule.setCondition([](const Rule::EventPtr){ return true; });

}

TEST_F(EvalTesting, DelayedEventEvalTest) {
    Evaluator eval;
    bool was_called = false;
    std::shared_ptr<Action> ac(new ActionCall([&was_called](Context*, void*) {
                                                  cout << "Action ac called" << endl;
                                                  was_called = true;
                                                  return true;
                                              }, nullptr));
    EXPECT_TRUE(EventFactory::instance()->defineEvent("event", "label"));
    auto e = EventFactory::instance()->makeEvent("event");
    EXPECT_TRUE(e);
    Properties params;
    params.set("delay", std::string("1"));
    params.set("event", e);
    auto de = EventFactory::instance()->makeEvent("delayed", params);
    EXPECT_TRUE(de);
    Rule rule(e, ac);
    eval.addRule(rule);
    eval.start();
    EXPECT_TRUE(eval.push(de));
    std::this_thread::sleep_for(std::chrono::seconds(3));
    eval.stop();
    EXPECT_TRUE(was_called);
}

}  // namespace
