#!/bin/bash
#

function Usage {
    cat <<EOF
Usage: testhandler.sh [ agGmrs ] <parameters>
  a <progname> <seq>    Add a GPIO program
  g <gpio_pin>          Read the value of specified gpio pin
  G <gpio_pin> <value>  Write value to specified gpio pin
  m <degrees>           Move the servo to the specified degree position
  r <program>           Run a GPIO program
  s                     Show statistics

EOF
    exit 2
}

#set -xv
[ $# -ge 1 ] || Usage

if [ $1 = g ]; then
    PIN=$2
    CMD1="{\"control\":{\"handler\":\"gpio\",\"method\":\"readkey\",\"param1\": $PIN }}"
elif [ $1 = G ]; then
    PIN=$2
    VAL=$3
    CMD1="{\"control\":{\"handler\":\"gpio\",\"method\":\"writekey\",\"param1\": $PIN,\"param2\": $VAL }}"
elif [ $1 = a ]; then
    CMD1="{\"control\":{\"handler\":\"gpio\",\"method\":\"addProgram\",\"param1\": 0,\"name\":\"$2\",\"program\": \"$3\" }}"
    CMD2="{\"control\":{\"handler\":\"gpio\",\"method\":\"listPrograms\",\"param1\": 0 }}"
elif [ $1 = r ]; then
    CMD1="{\"control\":{\"handler\":\"gpio\",\"method\":\"runProgram\",\"param1\": 0,\"name\":\"$2\",\"repeat\": 3 }}"
elif [ $1 = m ]; then
    MOVETO=$2
    CMD1="{\"control\":{\"handler\":\"servo\",\"method\":\"moveTo\",\"servo\": 0, \"degrees\": $MOVE }}"
    CMD2='{"control":{"handler":"servo","method":"readkey","servo": 0 }}'
elif [ $1 = s ]; then
    CMD1="{\"statistics\":{\"detail\":\"detail\"}}"
else
    Usage
fi

TMPFILE=/tmp/cshandlert.$$
cat >$TMPFILE <<EOF
{ "greeting": {
    "version": "0.1.3",
    "name": "tester",
    "role": "node",
    "mainGroup": "Train",
    "type": "rest" }
}


$CMD1


EOF
[ x"$CMD2" = x ] || cat >>$TMPFILE <<EOF
$CMD2


EOF

socat -t1 - TCP:localhost:1800 <$TMPFILE 2>&1 | sed '/^\r$/d'
rm $TMPFILE
echo Done
