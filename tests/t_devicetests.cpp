//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "devices/minipeerdevice.h"
#include "logger.h"
#include "minipeer.h"
#include <gtest/gtest.h>
#include <iostream>

using ctlsvc::cslog;
using std::cout;
using std::endl;

extern std::string runpath; // This is set from main()

namespace {

class DeviceTesting : public ::testing::Test {
protected:
    void SetUp() override {}
    void TearDown() override {}

};

TEST_F(DeviceTesting, MiniPeerDevice) {
  using namespace ctlsvc;
  using namespace ctlsvc::device;
  class TestControlService : public ControlService {
  public:
    TestControlService(const std::string& dbname) {
      database_.setDbFile(dbname);
      database_.init();
    }
    virtual ~TestControlService() {
      database_.uninit();
    }
    static ControlService* instance() {
      if (instance_ == nullptr) {
        instance_ = new TestControlService(runpath + "test.db");
      }
      return instance_;
    }
  };
  auto tcs = TestControlService::instance();
  EXPECT_NE(nullptr, tcs);

  MiniPeerDevice mpd("logops", ARDI_UNO_BUS, ARDI_UNO_ADDR);
  EXPECT_TRUE(mpd.init());
  EXPECT_TRUE(mpd.open(Device::Mode::ReadWriteOpen));
  auto prop = mpd.query(MiniPeerDevice::INPUTS);
  EXPECT_TRUE(std::holds_alternative<int>(prop));
  int ins = std::get<int>(prop);
  EXPECT_TRUE(ins == 3 || ins == 4);
  prop = mpd.query(MiniPeerDevice::OUTPUTS);
  EXPECT_TRUE(std::holds_alternative<int>(prop));
  int outs = std::get<int>(prop);
  EXPECT_TRUE(outs == 3 || outs == 4);
  prop = mpd.query(MiniPeerDevice::STEPPING_MOTORS);
  EXPECT_TRUE(std::holds_alternative<int>(prop));
  EXPECT_EQ(1, std::get<int>(prop));

  minipeer::StepMotor* sm = nullptr;
  EXPECT_TRUE(mpd.peripheral(0, &sm));
  ASSERT_NE(nullptr, sm);
  auto pos0 = sm->position();
  EXPECT_LE(0, pos0);
  EXPECT_EQ(100, sm->moveDown(100));
  sleep(2);
  auto pos1 = sm->position();
  EXPECT_EQ(pos0 + 100, pos1);
  EXPECT_EQ(100, sm->moveUp(100));
  sleep(2);
  pos1 = sm->position();
  EXPECT_EQ(pos0, pos1);
  mpd.deinit();
  tcs->stop();
  delete tcs;
}

}  // namespace
