//
//   Copyright 2023 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "eval/classifier.h"
#include "eval/context.h"
#include "eval/evaluator.h"
#include "eval/tokenizer.h"
#include "logger.h"
#include <gtest/gtest.h>
#include <iostream>
#include <sstream>
#include <thread>

using namespace ctlsvc::eval;
using ctlsvc::cslog;
using ctlsvc::Info;
using ctlsvc::loglevel;
using std::cout;
using std::endl;

namespace {

class RulebaseTesting : public ::testing::Test {
protected:
    void SetUp() override {}
    void TearDown() override {}
public:
    bool processNode(const RbNode* node, Classifier& cfr) {
        cslog << loglevel(Info) << " - node: " << *node << endl;
        return true;
    }
};

TEST_F(RulebaseTesting, SimpleTokenizerTest) {
    std::istringstream iss("First Second Third");
    std::vector<std::string> expected { "First", "Second", "Third" };
    Tokenizer tkr(iss);
    auto tokens = tkr.getTokensUntil("");
    EXPECT_EQ(tokens.size(), expected.size());
    cslog << "Tokens parsed:" << endl;
    int idx = 0;
    for (const auto& token : tokens) {
        EXPECT_EQ(token.data(), expected[idx]);
        EXPECT_EQ(token.type(), ReadType::Normal);
        ++idx;
    }
}

// The test string is as follows (note the first 2 lines have " ." appended to prevent compiler warning:
//
// Normal Add Backslash \ or \\ .
// Mid\\token  \\BeginToken  EndToken\\ .
// Quote-with-quote "Have a \"Nice\" Day!" somemore
// Standalone \" quote tokend
// End...

TEST_F(RulebaseTesting, EscapedTokenizerTest) {
    std::istringstream iss(
"Normal Add Backslash \\ or \\\\\n"
"Mid\\\\token  \\\\BeginToken  EndToken\\\\\n"
"Quote-with-quote \"Have a \\\"Nice\\\" Day!\" somemore\n"
"Standalone \\\" quote tokend\n"
"End...\n");
    // cslog << iss.str() << endl;
    std::vector<std::string> expected { "Normal", "Add", "Backslash", "\\", "or", "\\",
                                        "Mid", "\\", "token", "\\", "BeginToken", "EndToken", "\\",
                                        "Quote-with-quote", "Have a \"Nice\" Day!", "somemore",
                                        "Standalone", "\"", "quote", "tokend", "End", "..." };
    std::vector<ReadType> type_expected {
        ReadType::Normal, ReadType::Normal, ReadType::Normal, ReadType::Special, ReadType::Normal, ReadType::Escaped, 
        ReadType::Normal, ReadType::Special, ReadType::Normal, ReadType::Escaped, ReadType::Normal, ReadType::Normal, ReadType::Special,
        ReadType::Normal, ReadType::Quote, ReadType::Normal,
        ReadType::Normal, ReadType::Escaped, ReadType::Normal, ReadType::Normal, ReadType::Normal, ReadType::Special };

    Tokenizer tkr(iss);
    auto tokens = tkr.getTokensUntil("");
    EXPECT_EQ(tokens.size(), expected.size());
    cslog << "Tokens parsed " << tokens.size() << "." << endl;
    int idx = 0;
    for (const auto& token : tokens) {
        // cslog << " - " << token.data() << " type=" << token.type() << endl;
        EXPECT_EQ(token.data(), expected[idx]);
        EXPECT_EQ(token.type(), type_expected[idx]);
        ++idx;
    }
}

TEST_F(RulebaseTesting, SimpleClassifierTest) {
    std::istringstream iss("set myvar = \"My value.\" ");
    Tokenizer tkr(iss);
    auto tokens = tkr.getTokensUntil("");
    EXPECT_EQ(tokens.size(), 4);
    Evaluator eval;
    Context ctx(&eval);
    Classifier cfr(std::move(tokens), &ctx);
    cfr.createTree();
    cfr.printTree();
    //using namespace std::placeholders;
    //auto visits = cfr.visitChildren(std::bind(&RulebaseTesting::processNode, this, _1, _2));
    auto visits = cfr.visitChildren([](const RbNode* node, Classifier& cfr) {
                                        EXPECT_EQ(node->type, PhraseType::SetVar);
                                        EXPECT_EQ(node->range.first, 0);
                                        EXPECT_EQ(node->range.second, 4);
                                        EXPECT_EQ(node->children.size(), 2);
                                        return true;
                                    });
    EXPECT_EQ(visits, 1);
}

}  // namespace
