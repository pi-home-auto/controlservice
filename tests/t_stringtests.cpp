//
//   Copyright 2021 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "logger.h"
#include "string_tools.h"
#include <gtest/gtest.h>
#include <iostream>

using ctlsvc::StringTools;
using ctlsvc::cslog;
using std::cout;
using std::endl;

namespace {

class StringTesting : public ::testing::Test {
protected:
    void SetUp() override {}
    void TearDown() override {}

};

TEST_F(StringTesting, SimpleStrTest) {
    ctlsvc::StringArray words = { "Hello", "people", "Why", "bother" };
    std::string result;
    auto sz = StringTools::mergeStrings(words, result, "-ZZZ-");
    cout << "Result string: " << result << endl;
    EXPECT_EQ(20 + 3 * 5, sz);
}

}  // namespace
