//
//   Copyright 2024 Pi Home Automation https://gitlab/pi-home-auto
//
//   Licensed under the Apache License, Version 2.0 (the "License");
//   you may not use this file except in compliance with the License.
//   You may obtain a copy of the License at
//
//       http://www.apache.org/licenses/LICENSE-2.0
//
//   Unless required by applicable law or agreed to in writing, software
//   distributed under the License is distributed on an "AS IS" BASIS,
//   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//   See the License for the specific language governing permissions and
//   limitations under the License.
//
//   Project: controlservice
//   Author:  Andy Warner

#include "controlservice.h"
#include "logger.h"
#include "object_depot.h"
#include "string_tools.h"
#include <gtest/gtest.h>
#include <iostream>

using ctlsvc::cslog;
using ctlsvc::DeviceObject;
using ctlsvc::ObjectDepot;
using std::cout;
using std::endl;

extern std::string runpath; // This is set from main()

namespace {

class ObjectDepotTesting : public ::testing::Test {
protected:
  void SetUp() override {}
  void TearDown() override {}
};

TEST_F(ObjectDepotTesting, SplitGoodUNames) {
  ObjectDepot depot;
  auto un = depot.splitUName("rpi:device:gpio.1");
  EXPECT_EQ(3, un.size());
  EXPECT_EQ("rpi", un[0]);
  EXPECT_EQ("device", un[1]);
  EXPECT_EQ("gpio.1", un[2]);
  un = depot.splitUName(":device:gpio.1");
  EXPECT_EQ(3, un.size());
  EXPECT_TRUE(un[0].empty());
  EXPECT_EQ("device", un[1]);
  EXPECT_EQ("gpio.1", un[2]);
  un = depot.splitUName("rpi::gpio.1");
  EXPECT_EQ(3, un.size());
  EXPECT_EQ("rpi", un[0]);
  EXPECT_EQ("device", un[1]);
  EXPECT_EQ("gpio.1", un[2]);
  un = depot.splitUName("device:gpio.1");
  EXPECT_EQ(3, un.size());
  EXPECT_TRUE(un[0].empty());
  EXPECT_EQ("device", un[1]);
  EXPECT_EQ("gpio.1", un[2]);
  un = depot.splitUName("::gpio.1");
  EXPECT_EQ(3, un.size());
  EXPECT_TRUE(un[0].empty());
  EXPECT_EQ("device", un[1]);
  EXPECT_EQ("gpio.1", un[2]);
  un = depot.splitUName("gpio.1");
  EXPECT_EQ(3, un.size());
  EXPECT_TRUE(un[0].empty());
  EXPECT_EQ("device", un[1]);
  EXPECT_EQ("gpio.1", un[2]);
  un = depot.splitUName("rpi:handler:ping");
  EXPECT_EQ(3, un.size());
  EXPECT_EQ("rpi", un[0]);
  EXPECT_EQ("handler", un[1]);
  EXPECT_EQ("ping", un[2]);
}

TEST_F(ObjectDepotTesting, SplitUNamesDefaultModule) {
  ObjectDepot depot("mymodule");
  auto un = depot.splitUName("mymodule:device:sound.0");
  EXPECT_EQ(3, un.size());
  EXPECT_TRUE(un[0].empty());
  EXPECT_EQ("device", un[1]);
  EXPECT_EQ("sound.0", un[2]);
  un = depot.splitUName("rpi:device:sound.0");
  EXPECT_EQ(3, un.size());
  EXPECT_EQ("rpi", un[0]);
  EXPECT_EQ("device", un[1]);
  EXPECT_EQ("sound.0", un[2]);
}

TEST_F(ObjectDepotTesting, FindUNamesDefaultModule) {
  ObjectDepot depot;
  auto obj = depot.find("dist.0");
  EXPECT_TRUE(std::holds_alternative<DeviceObject>(obj));
  auto mpObj = depot.find("minipeer.1.9");
  EXPECT_TRUE(std::holds_alternative<DeviceObject>(obj));

  cslog << "Object Depot Contents:" << endl;
  depot.listContents([](const std::string& name, const ObjectDepot::ObjectType& obj) {
    cslog << " - " << name << " { " << obj << " }" << endl;
  });
}

/* Note this test is disabled since side effects of previous tests
 * cause the sqlite3 SELECT to give a "Misuse" error.
 * This test does run successfully if run without previous tests, such as:
 *    ./t_ctlsvclib --gtest_filter='ObjectDepot*'
 */
TEST_F(ObjectDepotTesting, UuidToUName) {
  using namespace ctlsvc;
  class TestControlService : public ControlService {
  public:
    TestControlService(const std::string& dbname) {
      database_.setDbFile(dbname);
      database_.init();
    }
    ~TestControlService() {
      database_.uninit();
    }
    static ControlService* instance() {
      if (instance_ == nullptr) {
        instance_ = new TestControlService(runpath + "test.db");
      }
      return instance_;
    }
  };
  auto tcs = TestControlService::instance();
  EXPECT_NE(nullptr, tcs);

  ObjectDepot objs;
  auto name = objs.uuidToUName("S-LIGHT-STREET-B");
  cout << "UName is: " << name << endl;
  StringArray parts;
  auto sz = StringTools::splitString(name, parts, ':');
  EXPECT_EQ(3, sz);
  EXPECT_EQ("switchback", parts[0]);
  EXPECT_EQ("device", parts[1]);
  EXPECT_EQ("gpio.7", parts[2]);

  tcs->stop();
  delete tcs;
}

}  // namespace
