### controlservice
## Control service for Raspberry Pi homeauto

This directory contains source for the IOT module control service.

The control service connects with other systems on the network that are also
running a control service. Each control service runs standalone; there is no
"base" or "sub" control service anymore. However, if a request for an operation
on a device that is on a different system (module), then this request is forwarded to
the other system's controlservice.

It has the following dependencies:
* C++20 or later (earlier C++ standards may work, but only C++ 20 has been tested).
* [sockstr](https://github.com/lawarner/sockstr) - C++ socket/stream library
* pigpio - Gpio interface library for Raspberry Pi.
* json parser from jsoncpp.sourceforge.net (already included in this repo)
* sqlite3
* [googletest](https://github.com/google/googletest) (optional) Use for unit tests
* socat (optional) if you want to use the handlertest.sh script
* openssl (optional) - if using https or other encryption

# How to Build:
The source base is small enough to easily compile the controlservice directly on the Raspberry Pi.
However, cross compiling is supported.

To compile on the Raspberry Pi, first review the common.mk file and edit it if needed for your setup.
Then enter the following:

    make
    make install

The install step copies the controlservice to the homeauto bin directory and set its cap so that
running as root is not necessary. Whatever user that the service runs under needs to be a member
of the kmem group and video group. You will also need to install a udev rule that allows read/write
for the kmem group, such as the following:

    KERNEL=="mem", GROUP="kmem", MODE="0660"

Note that the DistanceHandler interfaces with a special Linux kernel driver that is included in this
source repo under the kernel/dist folder. Build and load this as you would any other kernel module.
For example, to install the kernel headers for the currently running kernel version, do:

    sudo apt install raspberrypi-kernel-headers

To cross compile for Raspberry Pi, first make sure that env.sh has been run in current
bash session.  Then type:

    make arm
    make image

If you want to install controlservice as a system daemon that runs automatically at boot time, then type:

    sudo cp etcinit/controlservice /etc/init.d/
    sudo chmod +x /etc/init.d/controlservice
    sudo update-rc.d controlservice defaults

To test if the service was installed, type:

    sudo service controlservice start
    ps -ef | grep controlservice

# Files used for controlservice
## (this assumes HA_BASE_DIR is set to /home/pi/homeauto):
* /etc/init.d/controlservice
* /home/pi/homeauto/bin/controlservice
* /home/pi/homeauto/etc/commonconfig.json
* /home/pi/homeauto/etc/$(hostname)-config.json
* /home/pi/homeauto/logs/  (for audit and run logs)
* /home/pi/homeauto/monitored/  (for monitorhandler)

> Note the json config files need to be hand edited. In the future, it will be nice to have a config file generator. This could be part of the [Rest and Web Frontend project](https://gitlab.com/pi-home-auto/rest-web-front). The project has a SQL database that describes the configuration. However, that project needs an admin web front for database maintenance.

> The controlservice and its files can be installed anywhere by changing the HA_BASE_DIR variable in the common.mk file.

## Design
See [Design.md](https://gitlab.com/pi-home-auto/controlservice/-/blob/master/Design.md) for details on the design of this service. You can also generate Doxygen documention by typing:
    make doc

The documention will be in the doc/html directory. Open doc/html/index.html in your browser to view it.
