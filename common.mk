#
# Common definitions for all Makefiles
export HA_BASE_DIR=/home/${USER}/homeauto

CC = g++
# Add -pg for gprof profiling
DEBUG_FLAG = -g -O0
#DEBUG_FLAG = -O3
INCDIR = $(TOP)/include
COMMON_FLAGS = $(DEBUG_FLAG) -std=c++20 -Wall -DTARGET_LINUX=1 -I. -I$(TOP)/src \
    -I$(TOP) -I$(INCDIR) $(JSONCPPINC) $(EXTRA_CCFLAGS) -I$(TOP)/network/include
CCFLAGS = $(COMMON_FLAGS)
DEPCPPFLAGS = $(COMMON_FLAGS)
LDFLAGS = -pthread -lm $(EXTRA_LDFLAGS)
LDLIBS = $(EXTRA_LDLIBS) -L/usr/local/lib -lpigpio
