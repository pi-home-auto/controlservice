# Controlservice Design Notes
## Operations
As an introduction, this section describes the controlservice's operations in action.
Other sections delve into the details of the interprocess communication protocol, as
well as describing the object-oriented design.

## Architectural Roadmap

The controlservice is the central component in building an automation solution. In fact, you could deploy
"headless" solutions (no user interface) using controlservice alone. However, most solutions will have a
web front-end that is typically a node.js application driven by a sqlite database.
The [REST and Web Frontend](https://gitlab.com/pi-home-auto/rest-web-front) provides such a web UI on top
of the controlservice.

## Submodule designs

### Driver, Device and Handler interfaces

### Peer communication
#### Peers
##### Roles (BCS, SCS, Node)
#### IPC messages

### Alerts

### Sequencer

### Evaluator
When certain things occur such as a GPIO line being set high or a timer expiring, then an event is sent. It is also possible to programatically
define and send "user events." All events are sent to the Evaluator, which then checks its rulebases to see if any
rule is triggered by the event. If the rule matches the event, then the rule's action is run.

#### Events
#### Actions
#### Rules
#### Rulebase Language
The rulebase language allows dynamically loading a set of rules from a file. The syntax of the rulebase language is as follows:

> Note:  C-style (slash-asterik ... asterik-slash and // ...) comments are allowed anywhere.

> Variable substituion:  $<name> is substituted with the value of the variable (or an empty value if variable is not set).

    Rulebase <name>:
      <statement>
      . . .

<i>-where-</i>

      <statement> := <defevent> | <setvar> | <rule>
      <defevent> := define <event>
      <setvar> := set <name> = <value>
      <rule> := on <event> <action>

      <action> := <setvar> | <ifelse> | <namedAction> | <multiAction> | <cancel>
      <event> := event:<name> | event:<name>("<internalName>" [ , <param1> [ , ... ] ])
      <name> : = <alpha> | "_" [ <alpha> | "_" | <number> [ ... ] ]
      <alpha> := [a-zA-Z]
      <value> := <numeric> | <quoted>

      <cancel> := cancel <event>
      <ifelse> := if <predicate> <action>
      <predicate> := <subject> <condition> <value>
      <condition> := is [ equal | not | greater | less ]
      <namedAction> := do action:<name> | do action:<name>(<param1> [ , ... ])
      <multiAction> := { <action> [ <action> ... ] }

# Internal API:
Do direct connect to control-service (persistent connection), then issue:
Top-level message: greeting, goodbye, shutdown, control, controlalert, status, event

    "greeting": {
        "name":    "Room module 1",
        "version": "0.2.0",
        "role":    "sensor",
        "type":    "control",
        "mainGroup": "Main"    // optional, specifies main grouping of devices under modules control
    }

General Reply Syntax:

    "status": {
        "code":    code,	// HTTP style error codes (i.e., 200=OK)
        "message": message,
        "state":   state	// where state is: [ "fatal" | "error" | "retry" | "warning" | "OK" ]
    }

Broadcast to find control-service:

    // message (UPD)
    BCSQuery: {
        "senderId": "whatever",
    }

    // reply (UPD)
    BCSAddress: {
        "uri":  "192.168.0.7",
        "port": 1800
    }

# Commands:

    // Use "control" for control service message, "handlerName" for commands dispatched to handler.

    "control": {
        "handler": <handlername>,
        "method":  <methodname>,
        . . .
    }

Event:

    "event": {
        "Id": "Motion",
        "message": "Motion detected in bedroom.",
        "triggerId": "REM-34123-1"    // UUID of trigger
    }


# Program structure:
main
- parse commandline options, spawn as BCS or SCS (this is a good place for future sandboxing)

main BCS
- init
  - open audits and logs
  - start thread manager
  - read config file
  - load handlers
  - start listener threads
  - create state tables

- listener thread (TCP):
  - listen/accept
  - exchange identity payloads (send first)
  - register connection in proper list

- listener thread (UDP):
  - blocking read on port
  - on QueryBCS write an identity (direct)

main SCS
- a subcontrolservice sets up a listen socket (for node.js, et al) and connects directly to BCS

