# Pi Home Automation Architecture

## Overview

The Pi Home Automation system is centered around the controlservice.
The REST/web front-end in a Node.js application that reads sqlite configuration. The controlservice is a C++ service that controls devices.

Block diagram of automation architecture:

<img src="https://gitlab.com/pi-home-auto/controlservice/-/blob/master/doc/PiHomeAuto.png"
     alt="Architecture diagram">

The controlservice runs as a "base control service" or bcs for short. Note that additional Raspberry Pi systems running the Home Automation solution can connect to the bcs by running as a "sub control service" (scs).

The REST/web front-end makes a permanent socket connection with the controlservice, by default on TCP port 1800. The front-end and controlservice exchange json messages and replies via this socket.

## Revision

### Federation of multiple modules

Changing the federation from base-sub to peer-to-peer. If a rule or a handler refers to a device from another module, then a connection is maintained between
the 2 modules. Otherwise, if a request comes from json REST that refers to another module, then an on-demand connection is made
with the module (if not already connected) and the request is forwarded to that module.

Note that it's possible for a single request to refer to devices on multiple modules. In this case, the request must be split and
portions of it forwarded to other modules. Then the responses to all the portions must be reassembled into a common response.

The controlservice now reads the sql config database. The database should be extended with info specific to running the
controlservice (handlers, start at boot?, rules, etc.).

#### Connection Manager (Hub?)

Currently the Listener accepts incoming connections and creates a peer. Each peer spawns a thread that sits in a blocking
read on the stream (socket). When the peer exits, it removes itself within the PeerThreader class (at the end of PeerThreader::handle).
The only peer connection that is initiated is from the (deprecated) SubClient.  Therefore, there needs to be a ConnectionManager that will handle both peers that request to connect to our current module, as well as making (and maintaining) a connection to those modules that are needed but
are not already connected. (Maybe create a config (string array) parameter of module names that should be connected
when ControlService starts up.

ConnectionManager must also have reconnection logic in case of a dropped connection. Since the total number of modules within
an automation solution is likely to be small (less than 20), once a connection is established then it is attempted to be
maintained (as opposed to tracking when a module was used and removing those that haven't been used in a while.

Making a connection is an expensive operation, but looking up a module by name must be quick.

#### Use Cases

List of use cases for inter-modular communication:

* Ping handler: goes through modules and sends a ping message on an interval. Also, notes the ping replies that are received.
* The Dispatcher gets a request for a different module (for example, to set/query the GPIO state of a device that is on a different module). The request is forwarded to the other module, the reply is received and sent as the reply to the original request.
* A rule is run due to an event that causes an Action to occur and the Action specifies a device on a different module. Of course, a single action could refer to multiple devices that are on multiple modules.
* An Event is received from a different module and is pushed to the Evaluator.
* An Event is sent to a different module. This can occur from a Handler, Device or rule's Action.
* A thread will be blocking on a Socket::listen call, waiting for incoming connections. Once the connection is made then a Peer is created and stored in the list of connected modules.
* A connected module shuts down. In this case, the connection is closed and removed from the list of connected modules.
* A connected module's connection drops unexpectedly. A reconnection is attempted in a loop, possibly with a maximum number of retries.
* Current module receives a shutdown request. It then broadcasts its shutdown to all connected modules so that they may remove our module from their connected modules list.

Class combines Listener, Peer, PeerThreader and (some of) Dispatcher.
PeerManager will be removed since both listener and peers will be within same class.

class ConnectionHub (Messenger, ModuleConnector, ModuleNet, etc.):
Methods:
  Connection* getConnection(string module)
  Connection* connect(string module)
  bool disconnection(string module)
  Module* getModule(Connection *)

  size_t for_each(std::function<bool(Connection*)> f)

Storage:
  unordered_map<string, Connection> connections_;


Questions:

* How are objects such as devices referred to? The sql db can resolve the module name based on UUID for devices. But what about other objects?
* Does each connected module need its own thread that sits in a blocking read on the Socket connection?

### Universal namespace

The universal device/object namespace is mainly used for devices, but can refer to other objects
such as handlers, modules or even pseudo-objects (i.e., a Program, a Sequencer, etc.).

The template for a fully qualified name is:  <moduleName>:<objectType>:<objectName>
  For example, tunnel::device:gpio.1  -or-  rpi:handler:gpio

  device:minipeer.logops.o1

>  Note: If the module name and/or the object type are omitted, then the defaults are used.
         The default module name is the current module's name and the default object
         type is "device." All of the following refer to the same universal name (assuming
         the current module's name is rpi):

        - rpi:device.gpio.1  (fully qualified)
        - rpi::gpio.1
        - device:gpio.1
        - gpio.1
        - gpio.oseq.1 (This gpio is first in the sequence of output gpio's)

The internal design to resolve a textual name into an object reference will first check if the device is on the
current module. If not, then a request to resolve the name is forwarded to the owning module. Thus, the
name resolution logic only considers the current module's devices (or other objects).

The name resolution will then use the objectType portion of the name to look for an optional NameResolver<ObjectType>.
If found, then this name resolver will be delegated to resolve the name. Otherwise if the object type does not have
a custom resolver, then an internal database (json, C++ map, sql table, tree?) is queried.

> I don't know the class definition yet, but it will have a method UNtoObject(in UN, out obj)
> A UN (Universal Name) is a string, and obj is variant of the types of possible objects, such as
> Device or Handler.

Currently, device objects are created and owned by several different Handlers. Luckily there are only
3 device types at the moment: Dist, GpioInput and GpioOutput.  Need to move ownership to the ObjectDepot
as shared pointers.  (Need to watch how device shared pointer is passed around and used. Is a "release"
method needed?)

### I2C Devices

The I2cDriver opens a dummy entry in the GpioDriver so that pigpio will be initialized/deinitialized
correctly.  The I2cDriver::open method takes a name containing both the I2C bus number and the I2C device
address and it returns a unique handle. For example, "i2c.1.9" refers to i2c device 9 that is on bus 1.
Note that the bus numbers available varies depending on the Raspberry Pi model. For the Raspberry Pi 4
the busses 1 and 11 are available.

New controlservice Devices is created to implement the actual protocol of sending/receiving bytes
over i2c. Initially, a MiniPeerDevice will implement a custom protocol that controls various hardware
that is on an Arduino.  It will support a stepping motor, a switch (GPIO input), an LED (on/off or pwm).
Another i2c Device will implement the Tilt/Pan module.

The Pi uses GPIO's 8 and 9 for i2c connections where the Pi acts as the i2c master. A second i2c
connection is available using GPIO pins 2 and 3 where the Pi acts as a i2c slave. The controlservice
currently only supports the first i2c where the pi is master.

#### Mini-peer

A minipeer is an external device that is connected to the controlservice via i2c (instead of TCP/IP).
It is typically used to control peripherals that are not directly connected to the
Raspberry Pi running an automation module.

Using minipeers is useful to reduce the number of wires used to develop a solution and helps distribute
power usage to prevent the Pi's GPIOs from using too much current (the total current
consumed by all GPIOs should total less than 150mA).

Each minipeer can be configured with a different set of peripherals of the types: stepping motor, output (for relays, etc.),
PWM (for LEDs, etc.) or input (for switches).

>    Note: The first minipeer is an Arduino Uno which has 14 GPIO pins (6 can be PWM) and 6 analog pins.

Version 1 of the minipeer only supports a single stepping motor. Version 2 can be flexibly configured with multiple
input, output and stepping motor devices. The total number of devices depends on the number of available GPIO pins
on the microcontroller.  I am using an Arduino UNO as development platform.

The Arduino UNO has:

- 14 digital I/O pins (6 can be pwm)
- 6 analog input pins

D0         D5~        D10~
D1         D6~        D11~
D2         D7         D12
D3~        D8         D13
D4         D9~

Minipeer allocation of GPIO's is as follows:
1. Allocate output pins to pwm first (D3 D5 D6 D9 D10 D11), then (D0-D2, D4, D7-D8, D12-D13)
2. Allocate input pins in order from D0 to D13 (as long as the pin was not allocated as output)
3. Remaining pins are allocated to stepping motors (each motor takes 4 GPIO's)

